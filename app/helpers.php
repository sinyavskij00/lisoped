<?php

use App\Http\Route\CategoryPath;
use App\Entities\Category;

if (!function_exists('category_path')) {
    function category_path($id)
    {
        return app()->make(CategoryPath::class)->withId($id);
    }
}

if (!function_exists('get_category_tree')) {
    function get_category_tree()
    {
        return Cache::remember('categoryTree' . Config::get('current_language_id'), 60, function () {
            return Category::withDepth()
                ->defaultOrder()
                ->with(['localDescription'])
                ->get()
                ->toTree();
        });
    }
}
