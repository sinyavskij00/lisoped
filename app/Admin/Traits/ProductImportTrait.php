<?php

namespace App\Admin\Traits;

use App\Entities\Attribute;
use App\Entities\CategoryDescription;
use App\Entities\ColorDescription;
use App\Entities\Manufacturer;
use App\Entities\ManufacturerDescription;
use App\Entities\ProductDescription;
use App\Entities\ProductToCategory;
use App\Entities\Size;
use App\Entities\SizeDescription;
use App\Entities\SizeTable;
use App\Entities\SizeTableToProduct;
use App\Entities\SkuSpecial;
use App\Entities\StockKeepingUnit;
use App\Entities\Vendor;
use App\Entities\Year;
use Intervention\Image\Facades\Image;

trait ProductImportTrait
{
    protected static $processed;
    protected static $currentLanguageId;
    protected static $attributes;
    protected static $categories;
    protected static $sizes;
    protected static $colors;
    protected static $years;
    protected static $manufacturers;
    protected static $vendors;
    protected static $existCategories;
    protected static $existSizeTable;
    protected static $sizeTables;
    protected static $products;
    protected static $skus;
    protected static $skusSpecials;

    protected static $insertBatch;
    protected static $updateSkus = [];
    protected static $uniqueArray = [];
    protected static $resetByVendor = [];

    public function __call($method, $parameters)
    {
        $methodName = 'addAttribute';

        if (in_array($method, $this->productProperties)) {
            $propertyMethod = trim(strtolower(str_replace('_', '', 'add' . $method)));
            $methodName = method_exists($this, $propertyMethod) ? $propertyMethod : $methodName;
        }

        call_user_func_array([self::class, $methodName], $parameters);
    }

    protected function setProperties()
    {
        // Set language id
        self::$currentLanguageId = config('current_language_id');

        // Set from import config
        $config = config('admin.import_products');
        self::$insertBatch = $config['insert_batch'];
        self::$processed = $config['processed'];

        // Set data for check on exist entities
        self::$attributes = Attribute::getAttributesByName();
        self::$categories = CategoryDescription::getCategoriesByName();
        self::$sizes = SizeDescription::getSizesByName();
        self::$colors = ColorDescription::getColorsByName();
        self::$years = Year::get()->pluck('id', 'value');
        self::$manufacturers = ManufacturerDescription::getManufacturersByName();
        self::$vendors = Vendor::getVendorsByName();
        self::$existCategories = ProductToCategory::getCategoriesByProductId();
        self::$existSizeTable = SizeTableToProduct::getSizeTablesByProductId();
        self::$sizeTables = SizeTable::getSizeTablesById();
        self::$products = ProductDescription::getProductsByName();
        self::$skus = StockKeepingUnit::getSkuByVendorSku();
        self::$skusSpecials = SkuSpecial::getSkuSpecials();
    }

    protected function getUniqueSlug($slug = false)
    {
        $hash = uniqid();

        return $slug ? (str_slug($slug) . '-' . $hash) : $hash;
    }

    protected function priceByPercent($stock, $price)
    {
        return strpos($stock, '%') == false ? $stock : $price - ($price / 100 * (int) $stock);
    }

    private function addImages($params)
    {
        $images = [];
        $imagesSrc = explode("\n", $params[2]);
        if (!empty($imagesSrc)) {
            foreach ($imagesSrc as $src) {
                if (filter_var($src, FILTER_VALIDATE_URL) === false) {
                    if (\Storage::disk('public')->exists($src)) {
                        $images[] = $src;
                    }
                } else {
                    $filename = basename($src);
                    $tmp = 'shop_images/import/';
                    if (!\Storage::disk('public')->exists($tmp . $filename)) {
                        $filePath = \Storage::disk('public')->path($tmp . $filename);
                        Image::make($src)->save($filePath);
                    }
                    $images[] = $tmp . $filename;
                }
            }
        }
        $params[0]['images'] = $images;
    }

    private function addCategory($params)
    {
        $catName = mb_strtolower($params[2]);

        if (!isset(self::$categories[$catName])) {
            return; //we do not create new category if manager set wrong category name
        }

        $params[0]['category_id'] = self::$categories[$catName];
    }

    private function addSubCategory($params)
    {
        $catNames = explode('|#|', $params[2]);

        $categories = [];
        foreach ($catNames as $name) {
            $name = mb_strtolower($name);

            if (isset(self::$categories[$name])) {
                $categories[] = ['id' => self::$categories[$name]];
            }
        }

        if (empty($categories)) {
            return; //we do not create new category if manager set wrong category name
        }

        $params[0]['sub_categories'] = $categories;
    }

    private function addSizeTableIds($params)
    {
        $ids = explode(',', $params[2]);

        $tableIds = [];
        if (!empty($ids)) {
            foreach ($ids as $id) {
                if (in_array($id, self::$sizeTables)) {
                    $tableIds[] = $id;
                }
            }
        }

        if (empty($tableIds)) {
            return;
        }

        $params[0]['size_table'] = $tableIds;
    }

    private function addStocks($params)
    {
        $params[0]['stocks'] = explode('|#|', $params[2]);
    }

    private function addSizeComment($params)
    {
        $sizeId = 0;
        if (isset($params[0]['size'])) {
            $sizeValue = mb_strtolower($params[0]['size'] . $params[2]);

            if (!isset(self::$sizes[$sizeValue])) {
                self::$sizes[$sizeValue] = Size::create(['value' => $params[0]['size']])->id;

                self::$insertBatch['size_descriptions']['data'][] = [
                    $params[2],
                    self::$sizes[$sizeValue],
                    self::$currentLanguageId
                ];
            }

            $sizeId = self::$sizes[$sizeValue];
        }

        $params[0]['size_id'] = $sizeId;
    }

    private function addSize($params)
    {
        $params[0]['size'] = $params[2];
    }

    private function addColor($params)
    {
        $colorName = mb_strtolower($params[2]);

        $params[0]['color_id'] = isset(self::$colors[$colorName]) ? self::$colors[$colorName] : 0;
    }

    private function addYear($params)
    {
        $yearName = (int) $params[2];

        if (!isset(self::$years[$yearName])) {
            self::$years[$yearName] = Year::create(['value' => $yearName])->id;
        }

        $params[0]['year_id'] = self::$years[$yearName];
    }

    private function addManufacturer($params)
    {
        $manufacturerName = mb_strtolower($params[2]);

        if (!isset(self::$manufacturers[$manufacturerName])) {
            $manufacturer = Manufacturer::create([
                'slug' => $this->getUniqueSlug($manufacturerName),
                'status' => 1
            ]);

            $manufacturerNameOriginal = $params[2];
            self::$insertBatch['manufacturer_descriptions']['data'][] = [
                $manufacturer->id,
                self::$currentLanguageId,
                $manufacturerNameOriginal,
                $manufacturerNameOriginal,
                $manufacturerNameOriginal,
                $manufacturerNameOriginal,
                $manufacturerNameOriginal,
                $manufacturerNameOriginal,
            ];

            self::$manufacturers[$manufacturerName] = $manufacturer->id;
        }

        $params[0]['manufacturer_id'] = self::$manufacturers[$manufacturerName];
    }

    private function addVendor($params)
    {
        $vendorName = mb_strtolower($params[2]);

        if (!isset(self::$vendors[$vendorName])) {
            self::$vendors[$vendorName] = Vendor::create(['name' => $params[2]])->id;
        }

        $params[0]['vendor_id'] = self::$vendors[$vendorName];
    }

    private function addName($params)
    {
        $params[0]['name'] = $params[2];
    }

    private function addQuantity($params)
    {
        $params[0]['quantity'] = $params[2];
    }

    private function addPrice($params)
    {
        $params[0]['price'] = $params[2];
    }

    private function addDescription($params)
    {
        $params[0]['description'] = $params[2];
    }

    private function addVendorSku($params)
    {
        $params[0]['vendor_sku'] = (string)$params[2];
    }

    private function addParentSku($params)
    {
        $params[0]['parent_sku'] = (string)$params[2];
    }

    private function addAttribute($params)
    {
        if (!isset($params[0]['attributes'])) {
            $params[0]['attributes'] = [];
        }

        $params[0]['attributes'][$params[1]] = $params[2];
    }

    private function addPreorder($params)
    {
        $params[0]['is_preorder'] = $params[2];
    }

    private function addNew($params)
    {
        $params[0]['is_new'] = $params[2];
    }

    private function addHit($params)
    {
        $params[0]['is_hit'] = $params[2];
    }

    private function addOurChoice($params)
    {
        $params[0]['is_our_choice'] = $params[2];
    }
}
