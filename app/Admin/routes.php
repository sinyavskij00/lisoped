<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resources([
        'years'                  => YearController::class,
        'languages'              => LanguageController::class,
        'sizes'                  => SizeController::class,
        'colors'                 => ColorController::class,
        'skus'                   => StockKeepingUnitController::class,
        'banners'                => BannerController::class,
        'manufacturers'          => ManufacturerController::class,
        'attribute-groups'       => AttributeGroupController::class,
        'attributes'             => AttributeController::class,
        'filters'                => FilterController::class,
        'filter-values'          => FilterValueController::class,
        'products'               => ProductController::class,
        'product-reviews'        => ProductReviewController::class,
        'product-advantages'     => ProductAdvantageController::class,
        'categories'             => CategoryController::class,
        'newses'                 => NewsController::class,
        'news-reviews'           => NewsReviewController::class,
        'specials'               => SpecialController::class,
        'customers'              => UserController::class,
        'customer-groups'        => CustomerGroupController::class,
        'orders'                 => OrderController::class,
        'order-histories'        => OrderHistoryController::class,
        'order-skus'             => OrderSkuController::class,
        'order-profitabilities'  => OrderProfitabilityController::class,
        'order-profitability-types'  => OrderProfitabilityTypesController::class,
        'order-statuses'         => OrderStatusController::class,
        'order-sku-statuses'     => OrderSkuStatusController::class,
        'preorders'              => PreorderController::class,
        'landing-pages'          => LandingPageController::class,
        'vendors'                => VendorController::class,
        'shippings'              => ShippingController::class,
        'payments'               => PaymentController::class,
        'questions'              => QuestionController::class,
        'question-groups'        => QuestionGroupController::class,
        'testimonials'           => TestimonialController::class,
        'billing-carts'          => BillingCartController::class,
        'billing-accounts'       => BillingAccountController::class,
        'size-tables'            => SizeTableController::class,
        'mass-special'           => MassSpecialController::class,
        'header-banners'         => HeaderBannerController::class,
        'sms-templates'          => SmsTemplateController::class,
    ]);

    $router->get('api/user', 'UserController@getUser')->name('admin.api.getUser');

    $router->get('product-import', 'ProductImport@index')->name('product.import');
    $router->post('product-import', 'ProductImport@update');


    $router->get('unique-download', 'UniqueProducts@getDownload')->name('product.download');
//    $router->post('product-import', 'ProductImport@update');



    $router->get('product/popups', 'ProductPopupController@index')->name('product.popups');
    $router->post('product/popups', 'ProductPopupController@update');

    $router->get('pages/about-us', 'AboutUsController@index')->name('pages.aboutUs');
    $router->post('pages/about-us', 'AboutUsController@update');

    $router->get('pages/credit', 'CreditController@index')->name('pages.credit');
    $router->post('pages/credit', 'CreditController@update');

    $router->get('pages/delivery', 'DeliveryController@index')->name('pages.delivery');
    $router->post('pages/delivery', 'DeliveryController@update');

    $router->get('mass-discount', 'MassDiscountController@index')->name('module.massDiscount');
    $router->post('mass-discount', 'MassDiscountController@update');
    $router->post('mass-discount-delete', 'MassDiscountController@delete');

    $router->get('mass-price', 'MassPriceController@index')->name('module.massPrice');
    $router->post('mass-price', 'MassPriceController@update');

    $router->get('mass-status', 'MassProductStatus@index')->name('module.massProductStatus');
    $router->post('mass-status', 'MassProductStatus@update');

    $router->get('mass-related', 'GenerateRelatedProducts@index')->name('module.massRelated');
    $router->post('mass-related', 'GenerateRelatedProducts@update');

    $router->get('liqpay-check', 'LiqpayCheckController@index')->name('module.liqpay');
    $router->post('liqpay-check', 'LiqpayCheckController@update');

    $router->get('invoice-module', 'InvoiceController@index')->name('module.invoice');
    $router->post('invoice-module', 'InvoiceController@getInvoice');

    $router->get('profit-module', 'ProfitModuleController@index')->name('module.profit');
    $router->post('profit-module', 'ProfitModuleController@generate');

    $router->get('carousel-module', 'CarouselModuleController@index')->name('module.profit');
    $router->post('carousel-module', 'CarouselModuleController@update');

    $router->get('home-text-module', 'HomeTextModuleController@index')->name('module.homeText');
    $router->post('home-text-module', 'HomeTextModuleController@update');

    $router->get('order-report', 'OrderReportController@index')->name('module.report');
    $router->post('order-report', 'OrderReportController@getReport');

    $router->get('email-export', 'EmailExportController@index')->name('module.customerExport');
    $router->post('email-export', 'EmailExportController@export');

    $router->get('new-post', 'NewPostController@index')->name('module.newPost');
    $router->get('new-post/update-cities', 'NewPostController@updateCities')
        ->name('module.newPost.updateCities');
    $router->get('new-post/update-warehouses', 'NewPostController@updateWarehouses')
        ->name('module.newPost.updateWarehouses');

    $router->get('site-settings', 'SettingsController@index')->name('siteSettings');
    $router->post('site-settings', 'SettingsController@update');
    $router->get('site-settings/clear-cache', 'SettingsController@clearCache')
        ->name('siteSettings.clearCache');
    $router->get('site-settings/order-export', 'SettingsController@orderExport')
        ->name('siteSettings.orderExport');
    $router->get('site-settings/generate-sitemap', 'SettingsController@generateSitemap')
        ->name('siteSettings.generateSitemap');
    $router->get('site-settings/db-backup', 'SettingsController@makeDbDump')
        ->name('siteSettings.db.dump');
    $router->get('site-settings/db-rollback', 'SettingsController@rollbackDb')
        ->name('siteSettings.db.rollback');
    $router->get('site-settings/db-delete', 'SettingsController@deleteDump')
        ->name('siteSettings.db.delete');
});
