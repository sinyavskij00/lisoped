<?php

namespace App\Admin\Redefined;

use Encore\Admin\Widgets\Form;

class CustomWidgetForm extends Form
{
    protected $view;

    public function __construct($view = 'admin::widgets.form')
    {
        parent::__construct();

        $this->view = $view;
    }

    public function render()
    {
        return view($this->view, $this->getVariables())->render();
    }
}
