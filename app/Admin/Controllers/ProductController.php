<?php

namespace App\Admin\Controllers;

use App\Entities\Attribute;
use App\Entities\Category;
use App\Entities\Filter;
use App\Entities\Language;
use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Entities\ProductDescription;
use App\Entities\SizeTable;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Tab;

class ProductController extends Controller
{
    use HasResourceActions;

    public $genderOptions = [
        1 => 'Мужчина',
        2 => 'Женщина',
        3 => 'Ребенок',
    ];

    public $traceTypeOptions = [
        1 => 'Асфальт',
        2 => 'Асфальт и ровный грунт',
        3 => 'Ухабистый грунт, спуски',
        4 => 'Где угодно',
    ];

    protected $manufacturers;

    protected $categories;

    public function __construct()
    {
        $this->manufacturers = Manufacturer::with('localDescription')->get();
        $this->categories = Category::withDepth()->defaultOrder()->with(['localDescription'])->get();
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Товары')
            ->description('Все товары')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр товара')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('Редактирование товара')
            ->body($this->form()->edit($id))
            ->row(function (Row $row) use ($id) {
                $controller = app(StockKeepingUnitController::class);
                $skuGrid = $controller->grid();

                $skuGrid->setName('skus')
                    ->setTitle('Артикулы товара')
                    ->setRelation(Product::find($id)->skus())
                    ->resource('/admin/skus');

                $row->column(12, $skuGrid);
            });
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание товара')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product);

        //admin bug. If pass relation as string nested relation doesn't work
        $grid->model()->with([
            'mainSku.images'
        ]);

        $grid->id('Id')->setAttributes(['width' => '60px'])->sortable();
        $grid->slug('Seo-урл')->sortable();
        $grid->column('description.name', 'Название')->sortable();
        $grid->column('image', 'Изображение')->display(function () {
            return $this->image;
        })->image(null, 100)
            ->setAttributes(['width' => '130px'])->sortable();

        $grid->is_preorder('Предзаказ')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Да' : 'Нет';
        })->setAttributes(['width' => '110px'])->sortable();
        $grid->is_new('Новый')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Да' : 'Нет';
        })->setAttributes(['width' => '90px'])->sortable();
        $grid->is_hit('Хит')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Да' : 'Нет';
        })->setAttributes(['width' => '60px'])->sortable();
        $grid->is_our_choice('Наш выбор')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Да' : 'Нет';
        })->setAttributes(['width' => '120px'])->sortable();

        $categories = $this->categories;
        $grid->column('category_id', 'Категория')->display(function ($categoryId) use ($categories) {
            $category = $categories->where('id', '=', $categoryId)->first();
            if (isset($category) && isset($category->localDescription)) {
                return $category->localDescription->name;
            }
            return '-';
        })->sortable();

        $manufacturers = $this->manufacturers;
        $grid->column('manufacturer_id', 'Производитель')->display(function ($manufacturerId) use ($manufacturers) {
            $manufacturer = $manufacturers->firstWhere('id', '=', $manufacturerId);
            if (isset($manufacturer)) {
                return $manufacturer->localDescription->name;
            }
        })->setAttributes(['width' => '160px'])->sortable();
        $grid->status('Статус')->setAttributes(['width' => '100px'])->display(function ($default) {
            return isset($default) && $default === 1 ? 'Активный' : 'Неактивный';
        })->sortable();

        $grid->filter(function (Grid\Filter $filter) use ($categories, $manufacturers) {
            $filter->disableIdFilter();

            $filter->equal('category_id', 'Категория')->select($categories->pluck('localDescription.name', 'id'));

            $filter->equal('manufacturer_id', 'Производитель')
                ->select($manufacturers->pluck('localDescription.name', 'id'));

            $filter->equal('status', 'Статус')->select([
                1 => 'Включен',
                0 => 'Отключен',
            ]);

            $filter->where(function ($query) {
                $query->whereHas('localDescription', function ($query) {
                    $query->where('name', 'like', '%' . $this->input . '%');
                });
            }, 'Название');

            $filter->where(function ($query) {
                $query
                    ->whereHas('skus', function ($query) {
                        $query->where('vendor_sku', 'like', "%{$this->input}%");
                    });
            }, 'Артикул поставщика');

            $filter->where(function ($query) {
                $query
                    ->whereHas('skus', function ($query) {
                        $query->where('sku', 'like', "%{$this->input}%");
                    });
            }, 'Артикул на сайте');

        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));

        $show->id('Id');
        $show->slug('SEO-урл');
        $show->model('Модель');
        $show->image('Изображение')->as(function () {
            return $this->image;
        })->image();
        $show->is_preorder('Предзаказ')->as(function ($isPreorder) {
            return isset($isPreorder) && $isPreorder === 1 ? 'Да' : 'Нет';
        });
        $show->is_new('Новый')->as(function ($isNew) {
            return isset($isNew) && $isNew === 1 ? 'Да' : 'Нет';
        });
        $show->is_hit('Хит')->as(function ($isHit) {
            return isset($isHit) && $isHit === 1 ? 'Да' : 'Нет';
        });
        $show->is_our_choice('Наш выбор')->as(function ($isOurChoise) {
            return isset($isOurChoise) && $isOurChoise === 1 ? 'Да' : 'Нет';
        });

        $categories = $this->categories;
        $show->category_id('Категория')->as(function ($categoryId) use ($categories) {
            if (isset($categoryId)) {
                $category = $categories->firstWhere('id', '=', $categoryId);
                if (isset($category) && isset($category->localDescription)) {
                    return $category->localDescription->name;
                }
            }
            return '---';
        });
        $manufacturers = $this->manufacturers;
        $show->manufacturer_id('Производитель')->as(function ($manufacturerId) use ($manufacturers) {
            if (isset($manufacturerId)) {
                $manufacturer = $manufacturers->firstWhere('id', '=', $manufacturerId);
                if (isset($manufacturer) && isset($manufacturer->localDescription)) {
                    return $manufacturer->localDescription->name;
                }
            }
            return '---';
        });
        $show->status('Статус')->as(function ($status) {
            return isset($status) && $status === 1 ? 'Активный' : 'Неактивный';
        });
        $show->divider();
        $show->min_weight('Минимальный вес');
        $show->max_weight('Максимальный вес');
        $show->min_height('Минимальный рост');
        $show->max_height('Максимальный рост');

        $genderOptions = $this->genderOptions;
        $show->gender('Пол')->as(function ($gender) use ($genderOptions) {
            if (isset($gender) && isset($genderOptions[$gender])) {
                return $genderOptions[$gender];
            }
            return '---';
        });

        $traceTypeOptions = $this->traceTypeOptions;
        $show->gender('Где кататься')->as(function ($traceType) use ($traceTypeOptions) {
            if (isset($traceType) && isset($traceTypeOptions[$traceType])) {
                return $traceTypeOptions[$traceType];
            }
            return '---';
        });

        $show->divider();

        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        $tab = new Tab();
        $tab->add('Основное', $show->render());

        $descriptionContent = '';
        $descriptions = ProductDescription::where('product_id', '=', $id)->get();
        foreach ($descriptions as $description) {
            $show = new Show(ProductDescription::find($description->id));

            $show->name('Название');
            $show->description('Описание');
            $show->meta_title('Мета заголовок');
            $show->meta_description('Мета описание');
            $show->meta_keywords('Мета ключевые слова');

            $show->panel()->tools(function ($tools) {
                $tools->disableEdit();
                $tools->disableList();
                $tools->disableDelete();
            });

            $descriptionContent .= $show->render();
        }

        $tab->add('Описания', $descriptionContent);

        return $tab;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Product);

        $languages = Language::all()->pluck('title', 'id');

        $form->tab('Основное', function (Form $form) {
            $states = [
                'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
                'on' => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
            ];

            $form->text('slug', 'SEO-урл')->rules('required');
            $filtersValOptions = $this->getFilterOptions();
            $form->multipleSelect('filtervalues', 'Фильтры')->options($filtersValOptions);
            $form->listbox('sizetables', 'Размеры')->options(
                SizeTable::with('localDescription')->get()->pluck('localDescription.title', 'id')
            )->help('Таблицы размеров');

            $form->switch('is_preorder', 'Предзаказ')->states($states);
            $form->switch('is_new', 'Новый')->states($states);
            $form->switch('is_hit', 'Хит')->states($states);
            $form->switch('is_our_choice', 'Наш выбор')->states($states);

            $categoryOptions = $this->getCategoryOptions();
            $form->select('category_id', 'Главная категория')->options($categoryOptions)->help('Для формирования URL');

            $form->multipleSelect('showcategories', 'Показывать в категориях')->options($categoryOptions);

            $form->select('manufacturer_id', 'Производитель')
                ->options($this->manufacturers->pluck('localDescription.name', 'id'));
            $form->switch('status', 'Статус')->states([
                'off' => ['value' => 0, 'text' => 'Неактивный', 'color' => 'danger'],
                'on' => ['value' => 1, 'text' => 'Активный', 'color' => 'success'],
            ])->default(0);

            $form->number('weight', 'Вес товара')
                ->min(0)
                ->default(35)
                ->help('Вес для новой почты');
        })->tab('Метки для модуля "Подобрать велосипед"', function (Form $form) {
            $form->number('min_weight', 'Минимальный вес');
            $form->number('max_weight', 'Максимальный вес');
            $form->number('min_height', 'Минимальный рост');
            $form->number('max_height', 'Максимальный рост');
            $form->select('gender', 'Пол')->options($this->genderOptions);
            $form->select('trace_type', 'Где кататься')->options($this->traceTypeOptions);
        })->tab('Описание', function (Form $form) use ($languages) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) use ($languages) {
                $form->select('language_id', 'Язык')
                    ->options($languages);
                $form->text('name', 'Название');
                $form->ckeditor('description', 'Описание');
                $form->text('meta_title', 'Мета заголовок');
                $form->textarea('meta_description', 'Мета описание');
                $form->textarea('meta_keywords', 'Мета ключивые слова');
            });
        })->tab('Похожие товары', function (Form $form) {
            $products = Product::with(['localDescription'])->get();
            $form->multipleSelect('related', 'Похожие товары')
                ->options($products->pluck('localDescription.name', 'id'));
        })->tab('Атрибуты', function (Form $form) use ($languages) {
            $attrOptions = $this->getAttributeOptions();
            $form->hasMany(
                'productattributes',
                'Атрибуты',
                function (Form\NestedForm $form) use ($languages, $attrOptions) {
                    $form->select('language_id', 'Язык')->options($languages);
                    $form->select('attribute_id', 'Атрибут')->options($attrOptions);
                    $form->text('text', 'Значение');
                    $form->number('sort', 'Сортировка')->help('Первые три будут выводится на превью');
                }
            );
        })->tab('Видео', function (Form $form) {
            $form->hasMany('videos', 'Видео', function (Form\NestedForm $form) {
                $form->text('video', 'Видео');
            });
        });

        return $form;
    }

    private function getFilterOptions()
    {
        $filters = Filter::with(['localDescription', 'values.localDescription'])->get();
        $filtersValOptions = [];
        foreach ($filters as $filter) {
            foreach ($filter->values as $filterValue) {
                $filtersValOptions[$filterValue->id] =
                    (isset($filter->localDescription) ? $filter->localDescription->name : '')
                    . ' > '
                    . (isset($filterValue->localDescription) ? $filterValue->localDescription->name : '');
            }
        }
        return $filtersValOptions;
    }

    private function getCategoryOptions()
    {
        $categoryOptions = [];
        $traverse = function ($categories, $prefix = '', $lastPref = 0) use (&$traverse, &$categoryOptions) {
            foreach ($categories as $category) {
                $categoryName = $category->localDescription->name;

                $categoryOptions[$category->id] = $prefix . $categoryName;

                $prefix .= $categoryName . ' > ';
                $traverse($category->children, $prefix);

                if ($category->children->count() > 0) {
                    $prefix = '';
                }
            }
        };

        $traverse(get_category_tree());

        return $categoryOptions;
    }

    private function getAttributeOptions()
    {
        $attributes = Attribute::with([
            'localDescription',
            'attributeGroup.localDescription'
        ])->get();
        $attrOptions = [];
        foreach ($attributes as $attribute) {
            $tmp = isset($attribute->attributeGroup) && isset($attribute->attributeGroup->localDescription)
                ? $attribute->attributeGroup->localDescription->name . ' > '
                : '';
            $tmp .= isset($attribute->localDescription) ? $attribute->localDescription->name : '';
            $attrOptions[$attribute->id] = $tmp;
        }
        return $attrOptions;
    }
}
