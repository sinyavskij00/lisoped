<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\Question;
use App\Entities\QuestionGroup;
use App\Entities\QuestionGroupDescription;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class QuestionController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Вопросы')
            ->description('Все вопросы')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр вопроса')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('Редактирование вопроса')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание вопроса')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Question);

        $grid->id('Id')->sortable();
        $grid->column('question_group_id', 'Группа вопросов')->display(function ($questionGroupId) {
            if (isset($questionGroupId)) {
                $questionGroup = QuestionGroup::find($questionGroupId);
                if (isset($questionGroup) && isset($questionGroup->localDescription)) {
                    return $questionGroup->localDescription->name;
                }
            }
            return '-';
        })->sortable();
        $grid->column('localDescription.question_text', 'Текст вопроса')->sortable();
        $grid->created_at('Создан')->sortable();
        $grid->updated_at('Обновлен')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();

            $questionGroups = QuestionGroup::with(['localDescription'])->get();
            $filter->equal('question_group_id', 'Групппа вопросов')
                ->select($questionGroups->pluck('localDescription.name', 'id'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Question::findOrFail($id));

        $show->id('Id');
        $show->question_group_id('Группа вопросов')->as(function ($questionGroupId) {
            if(isset($questionGroupId)){
                $languageId = config('current_language_id');
                $description = QuestionGroupDescription::where('language_id', '=', $languageId)
                    ->where('question_group_id', '=', $questionGroupId)->first();
                if(isset($description)){
                    return $description->name;
                }
            }
            return '---';
        });
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Question);
        $form->tab('Основное', function ($form) {
            $questionGroups = QuestionGroup::with('localDescription')->get();
            $form->select('question_group_id', 'Группа вопросов')
                ->options($questionGroups->pluck('localDescription.name', 'id'));
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
                $form->textarea('question_text', 'Текст вопроса');
                $form->ckeditor('answer', 'Ответ');
            });
        });


        return $form;
    }
}
