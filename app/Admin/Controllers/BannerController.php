<?php

namespace App\Admin\Controllers;

use App\Entities\Banner;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Entities\Language;

class BannerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Баннеры на главной')
            ->description('Баннеры на главной')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать баннер')
            ->description('Редактировать баннер')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать банннер')
            ->description('Создать банннер')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Banner);

        $grid->id('Id')->sortable();
        $grid->column('background_image', 'Изображение заднего фона')->image(null, 50)->sortable();
        $grid->column('main_image', 'Главное изображение')->image(null, 50)->sortable();
        $grid->column('status', 'Статус')->display(function ($status) {
            return (isset($status) && $status === 1) ? 'Включено' : 'Отключено';
        })->sortable();
        $grid->created_at('Создан');
        $grid->updated_at('Обновлен');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Banner::findOrFail($id));

        $show->id('Id');
        $show->background_image('Изображение заднего фона');
        $show->main_image('Главное изображение');
        $show->status('Статус')->as(function ($status) {
            return (isset($status) && $status === 1) ? 'Включено' : 'Отключено';
        });
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Banner);

        $form->tab('Основное', function ($form) {
            $form->elfinder('background_image', 'Изображение заднего фона');
            $form->elfinder('main_image', 'Главное изображение');
            $form->switch('status', 'Статус')->states([
                'on'  => ['value' => 1, 'text' => 'Включено', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Выключено', 'color' => 'danger'],
            ]);
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
                $form->text('text', 'Текст');
                $form->text('link', 'Ссылка');
                $form->text('link_title', 'Заголовок ссылки');
                $form->text('link_subtitle', 'Подзаголовок ссылки');
            });
        });


        return $form;
    }
}
