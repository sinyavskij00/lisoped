<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\OrderStatus;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class OrderStatusController extends Controller
{
    use HasResourceActions;

    protected $orderStatuses;

    public function __construct()
    {
        $this->orderStatuses = OrderStatus::with('localDescription')->get();
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Статусы заказов')
            ->description('Все статусы')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Описание')
            ->description('Описание статуса')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('Редактирование статуса')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание заказа')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new OrderStatus);

        $grid->id('Id')->sortable();

        $orderStatuses = $this->orderStatuses;
        $grid->column('id', 'Статус')->display(function ($id) use ($orderStatuses) {
            if (isset($id)) {
                $orderStatus = $orderStatuses->firstWhere('id', '=', $id);
                if (isset($orderStatus) && isset($orderStatus->localDescription)) {
                    return $orderStatus->localDescription->name;
                }
            }
        })->sortable();

        $grid->column('color', 'Цвет')->display(function ($color) {
            if (isset($color)) {
                return '<div style="background-color: ' . $color . '; width: 100px; height: 20px;"></div>';
            }
            return '';
        })->sortable();

        $grid->column('is_new', 'Для новых')->display(function ($isNew) {
            return isset($isNew) && $isNew === 1 ? 'Да' : 'Нет';
        })->sortable();

        $grid->column('is_finished', 'Статус закрытого')->display(function ($isFinished) {
            return isset($isFinished) && $isFinished === 1 ? 'Да' : 'Нет';
        })->sortable();

        $grid->column('export_1c', 'Статус для экспорта')->display(function ($export1C) {
            return isset($export1C) && $export1C === 1 ? 'Да' : 'Нет';
        })->sortable();

        $grid->column('created_at', 'Создано')->sortable();
        $grid->column('updated_at', 'Обновлено')->sortable();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(OrderStatus::findOrFail($id));

        $show->id('Id');
        $show->color('Цвет');
        $show->is_new('Для новых')->as(function ($isNew) {
            return isset($isNew) && $isNew === 1 ? 'Да' : 'Нет';
        });
        $show->is_finished('Статус закрытого')->as(function ($isFinished) {
            return isset($isFinished) && $isFinished === 1 ? 'Да' : 'Нет';
        });
        $show->export_1c('Статус для экспорта в 1С')->as(function ($export1C) {
            return isset($export1C) && $export1C === 1 ? 'Да' : 'Нет';
        });
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new OrderStatus);

        $form->color('color', 'Цвет выделения заказа в разделе "Заказы"')->default('#000000');
        $form->switch('is_new', 'Группа поумолчанию')->states([
            'on' => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
        ])->help('По умолчанию для новых');

        $form->switch('is_finished', 'Статус закрытого')->states([
            'on' => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
        ])->help('Данный статус считается закрывающим сделку');

        $form->select('export_1c', 'Статус для экспорта в 1С')->options([
            0 => 'Не выбран',
            1 => 'Наложенный платеж',
            2 => 'Закрыт заказ'
        ])
            ->help('В зависимости от статуса создается XML файл для экспорта заказа в 1С')
            ->default(0);
//        $form->switch('export_1c', 'Статус для экспорта в 1С')->states([
//            'on' => ['value' => 1, 'text' => 'Да', 'color' => 'danger'],
//            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
//        ])->help('При переключении на данный статус создается XML файл для экспорта заказа в 1С');

        $form->hasMany('descriptions', 'Описания', function ($form) {
            $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
            $form->text('name', 'Название');
        });

        $form->saved(function (Form $form) {
            $orderStatus = $form->model();
            if ($orderStatus->is_new === 1) {
                OrderStatus::where('id', '!=', $orderStatus->id)->update([
                    'is_new' => 0
                ]);
            }

            if ($orderStatus->is_finished === 1) {
                OrderStatus::where('id', '!=', $orderStatus->id)->update([
                    'is_finished' => 0
                ]);
            }

//            if ($orderStatus->export_1c == 1) {
//                OrderStatus::where('id', '!=', $orderStatus->id)->update([
//                    'export_1c' => 0
//                ]);
//            }
        });

        return $form;
    }
}
