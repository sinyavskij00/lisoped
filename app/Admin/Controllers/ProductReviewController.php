<?php

namespace App\Admin\Controllers;

use App\Entities\Product;
use App\Entities\ProductDescription;
use App\Entities\ProductReview;
use App\Http\Controllers\Controller;
use App\Mail\Information\ReviewAccepted;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Form\Row;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Mail;

class ProductReviewController extends Controller
{
    use HasResourceActions;

    protected $products;

    public function __construct()
    {
        $this->products = Product::with('localDescription')->get();
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Отзывыв о товарах')
            ->description('Отзывыв о товарах')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр отзыва')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать отзыв о товаре')
            ->description('Редактировать отзыв о товаре')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать отзыв о товаре')
            ->description('Создать отзыв о товаре')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProductReview);

        $grid->model()->with([
            'product.mainSku'
        ]);

        $grid->id('Id')->sortable();
        $products = $this->products;

        $grid->column('product_id', 'Товар')->display(function ($productId) use ($products) {
            if (isset($productId)) {
                $product = $products->firstWhere('id', '=', $productId);
                if (isset($product) && isset($product->localDescription)) {
                    return '<a target="_blank" href="'
                        . route('product_path', $product->slug, true) . '">'
                        . $product->localDescription->name . '</a>';
                }
            }
        })->sortable();
        $grid->column('product', 'Артикул')->display(function () {
            if (isset($this->product->mainSku)) {
                return $this->product->mainSku->sku;
            }
            return '---';
        })->sortable();
        $grid->column('status', 'Статус')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Одобрен' : 'Неодобрен';
        })->sortable();
        $grid->rating('Оценка')->sortable();
        $grid->name('Имя')->sortable();
        $grid->email('Email')->sortable();
        $grid->column('text', 'Текст')->display(function ($text) {
            if (isset($text)) {
                return str_limit($text, 100);
            }
            return '';
        })->sortable();
        $grid->created_at('Создан')->sortable();
        $grid->updated_at('Обновлен')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();

            $products = Product::with(['localDescription'])->get();
            $filter->equal('product_id', 'Товар')
                ->select($products->pluck('localDescription.name', 'id'));

            $filter->equal('status', 'Статус')->select([
                0 => 'Не одобрен',
                1 => 'Одобрен',
            ]);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProductReview::findOrFail($id));

        $show->id('Id');
        $products = $this->products;
        $show->product_id('Товар')->as(function ($productId) use ($products) {
            if (isset($productId)) {
                $product = $products->firstWhere('id', '=', $productId);
                if (isset($product) && isset($product->localDescription)) {
                    return $product->localDescription->name;
                }
            }
            return '---';
        });
        $show->status('Статус')->as(function ($default) {
            return isset($default) && $default === 1 ? 'Одобрен' : 'Неодобрен';
        });
        $show->rating('Оценка');
        $show->name('Имя');
        $show->email('E-mail');
        $show->text('Текст');
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ProductReview);

        $form->tab('Основное', function ($form) {
            $products = $this->products;
            $form->select('product_id', 'Товар')
                ->options($products->pluck('localDescription.name', 'id'));
            $form->number('rating', 'Оценка')->min(0)->max(5)->default(5);
            $form->text('name', 'Имя');
            $form->email('email', 'Email');
            $form->textarea('text', 'Текст');
            $form->switch('status', 'Статус')->states([
                'on' => ['value' => 1, 'text' => 'Одобрен', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Неодобрен', 'color' => 'danger'],
            ]);
        })->tab('Изображения', function ($form) {
            $form->hasMany('images', 'Изображения', function (Form\NestedForm $form) {
                $form->elfinder('image', 'Изображение');
            });
        });

        $form->saved(function (Form $form) {
            $productReview = $form->model();

            if (isset($productReview->status) && $productReview->status === 1 && !empty($productReview->email)) {
                $product = Product::where('id', '=', $productReview->product_id)->first();
                $link = isset($product) ? route('product_path', $product->slug) : null;
                $mail = new ReviewAccepted($productReview->text, $link, $productReview->name);
                $mail->subject(__('mails.information.review_accepted.subject'));
                Mail::to($productReview->email)->send($mail);
            }
        });

        return $form;
    }
}
