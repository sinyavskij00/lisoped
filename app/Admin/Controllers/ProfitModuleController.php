<?php

namespace App\Admin\Controllers;

use App\Entities\Category;
use App\Entities\CustomerGroup;
use App\Entities\Manufacturer;
use App\Entities\Order;
use App\Entities\Product;
use App\Entities\SkuDiscount;
use App\Entities\StockKeepingUnit;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;


class ProfitModuleController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->header('Модуль массового управления скидками')
            ->description('Модуль массового управления скидками')
            ->row(function (Row $row) {

                $form = new Form();

                $form->datetime('date_start', 'Дата начала');
                $form->datetime('date_end', 'Дата конца');

                $form->disablePjax();

                $form->action('profit-module');

                $row->column(12, $form);
            });
    }

    public function generate(Request $request)
    {
        $this->validate($request, [
            'date_start' => 'required',
            'date_end' => 'required',
        ]);

        $dateStart = $request->input('date_start');
        $dateEnd = $request->input('date_end');

        $orders = Order::where('created_at', '>=', $dateStart)->where('created_at', '<=', $dateEnd)->get();
    }
}
