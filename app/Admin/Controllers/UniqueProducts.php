<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Storage;

class UniqueProducts extends Controller
{

    public function getDownload(Request $request)
    {
        $fileName = $request->input('name');
        $file= storage_path(). "/app/$fileName.txt";
        $headers = array(
            'Content-type' => 'text/plain',
        );
        return response()->download($file, "$fileName.txt", $headers);
    }

    public function createFiles($unique)
    {

        if (isset($unique)) {
            $cnt = 2;
            foreach ($unique as $key => $file) {
                if (Storage::disk('local')->exists($key . '.txt'))
                    Storage::disk('local')->delete($key . '.txt');
                $textInFile = '';
                foreach ($file as $text) {
                    $textInFile .= 'Номер строки: ' . $cnt . '; Артукул: ' . $text['vendor_sku'] . '; Имя: ' . $text['name'] . "\r\n";
                    $cnt++;
                }
                Storage::disk('local')->put($key . '.txt', $textInFile);
            }
        }
    }
}