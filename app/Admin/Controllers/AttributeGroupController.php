<?php

namespace App\Admin\Controllers;

use App\Entities\AttributeGroup;
use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AttributeGroupController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Группы атрибутов')
            ->description(' ')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Группы атрибутов')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование группы атрибутов')
            ->description('Редактирование группы атрибутов')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание группы аттрибутов')
            ->description('Создание группы аттрибутов')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new AttributeGroup);

        $grid->id('Id')->sortable();
        $grid->sort('Сортировка')->sortable();
        $grid->column('localDescription.name', 'Название')->sortable();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(AttributeGroup::findOrFail($id));

        $show->id('Id');
        $show->sort('Сортировка');
        $show->image('Image');
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new AttributeGroup);

        $form->tab('Основная информация', function ($form) {
            $form->number('sort', 'Сортировка');
            $form->elfinder('image', 'Изображение');
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
                $form->text('name', 'Название');
            });
        });

        return $form;
    }
}
