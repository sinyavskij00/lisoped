<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\HasResourceActions;
use App\Entities\Category;
use App\Entities\CustomerGroup;
use App\Entities\Manufacturer;
use App\Entities\MassSpecials;
use App\Entities\Product;
use App\Entities\SkuSpecial;
use App\Entities\StockKeepingUnit;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Encore\Admin\Grid;

class MassSpecialController extends Controller
{
    use HasResourceActions;

    public function index(Content $content)
    {
        return $content
            ->header('Модуль массового управления акциями')
            ->description(' ')
            ->body($this->grid());
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        $form = new Form();

        $manufacturers = Manufacturer::with('localDescription')->get();
        $form->listbox('manufacturers', 'Производители')
            ->options($manufacturers->pluck('localDescription.name', 'id'));

        $categories = Category::with('localDescription')->get();
        $form->listbox('categories', 'Категории')
            ->options($categories->pluck('localDescription.name', 'id'));

        $form->select('customer_group_id', 'группа покупателей')
            ->options(CustomerGroup::all()->pluck('localDescription.name', 'id'));
        $form->number('priority', 'Приоритет')->default(0);
        $form->number('percent', 'Процент акции')->min(0)->max(100);
        $form->datetime('date_start', 'Дата начала');
        $form->datetime('date_end', 'Дата конца');

        return $content
            ->header('Модуль массового управления акциями')
            ->description('Создание')
            ->body($form);
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $specials = MassSpecials::where('id', $id)->first();

        $form = new Form();
        $form->method('POST');
        $form->action(asset('admin/mass-special/'));
        $form->disablePjax();

        $form->hidden('id')->default($id);
        $form->listbox('manufacturers', 'Производители')
            ->options(
                Manufacturer::with('localDescription')->get()->pluck('localDescription.name', 'id')
            )
            ->value(json_decode($specials->manufacturers));

        $form->listbox('categories', 'Категории')
            ->options(
                Category::with('localDescription')->get()->pluck('localDescription.name', 'id')
            )
            ->value(json_decode($specials->categories));

        $form->select('customer_group_id', 'группа покупателей')
            ->options(CustomerGroup::all()->pluck('localDescription.name', 'id'))
            ->default($specials->customer_group_id);
        $form->number('priority', 'Приоритет')->default($specials->priority);
        $form->number('percent', 'Процент акции')
            ->min(0)->max(100)
            ->default($specials->percent);
        $form->datetime('date_start', 'Дата начала')->default($specials->date_start);
        $form->datetime('date_end', 'Дата конца')->default($specials->date_end);

        return $content
            ->header('Модуль массового управления акциями')
            ->description('Редактирование')
            ->row($form);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'customer_group_id' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'percent' => 'required',
        ]);

        $id = $request->input('id');

        $percent = intval($request->input('percent'));

        if ($percent > 0) {
            $categoriesIds = $request->input('categories', []);
            $manufacturersIds = $request->input('manufacturers', []);

            $productsIds = Product::whereIn('category_id', $categoriesIds)
                ->orWhereIn('manufacturer_id', $manufacturersIds)->get(['id'])->pluck(['id']);
            $skus = StockKeepingUnit::whereIn('product_id', $productsIds);

            $priority = intval($request->input('priority', 0));
            $dateStart = $request->input('date_start');
            $dateEnd = $request->input('date_end');
            $customerGroupId = $request->input('customer_group_id');

            $storeData = [
                'manufacturers' => json_encode($manufacturersIds),
                'categories' => json_encode($categoriesIds),
                'customer_group_id' => $customerGroupId,
                'priority' => $priority,
                'percent' => $percent,
                'date_start' => $dateStart,
                'date_end' => $dateEnd
            ];

            if ($id) {
                $title = 'Акция изменена';
                MassSpecials::where('id', $id)->update($storeData);

                SkuSpecial::where('customer_group_id', '=', $customerGroupId)
                    ->whereNotIn('unit_id', $skus->get()->pluck('id')->toArray())
                    ->delete();
            } else {
                $title = 'Акция добавленна';
                MassSpecials::insert($storeData);

                $skus->chunk(1000, function ($skus) use ($percent, $priority, $dateStart, $dateEnd, $customerGroupId) {
                    SkuSpecial::whereIn('unit_id', $skus->pluck('id')->toArray())
                        ->where('customer_group_id', $customerGroupId)->delete();

                    $data = [];
                    foreach ($skus as $sku) {
                        $data[] = [
                            'customer_group_id' => $customerGroupId,
                            'unit_id' => $sku->id,
                            'priority' => $priority,
                            'price' => intval($sku->price * (1 - ($percent / 100))),
                            'date_start' => $dateStart,
                            'date_end' => $dateEnd
                        ];
                    }
                    SkuSpecial::insert($data);
                });
            }

            $success = new MessageBag([
                'title' => $title,
//            'message' => 'Кэш очищен',
            ]);

            return back()->with(compact('success'));
        }
    }

    public function destroy($id)
    {
        // Get massSpecial
        $special = MassSpecials::where('id', $id)->first();

        $productsIds = Product::whereIn('category_id', json_decode($special->categories))
            ->orWhereIn('manufacturer_id', json_decode($special->manufacturers))
            ->get(['id'])->pluck(['id']);

        // Delete SkuSpecial
        SkuSpecial::where('customer_group_id', '=', $special->customer_group_id)
            ->whereIn(
                'unit_id',
                StockKeepingUnit::whereIn('product_id', $productsIds)->get()->pluck('id')->toArray()
            )
            ->delete();

        // Delete MassSpecial
        MassSpecials::where('id', '=', $id)->delete();

        $success = new MessageBag([
            'title' => 'Акции были удалены',
        ]);

        return back()->with(compact('success'));
    }

    protected function grid()
    {
        $grid = new Grid(new MassSpecials);

        $grid->id('Id')->sortable();
        $grid->column('customer_group_id', 'Группа покупателей')->display(function ($customerGroupId) {
            if (isset($customerGroupId)) {
                $customerGroup = CustomerGroup::find($customerGroupId);
                if (isset($customerGroup) && isset($customerGroup->localDescription)) {
                    return $customerGroup->localDescription->name;
                }
            }
            return '-';
        })->sortable();
        $grid->priority('Приоритет')->sortable();
        $grid->percent('Процент')->sortable();
        $grid->date_start('Дата начала')->sortable();
        $grid->date_end('Дата конца')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $customerGroups = CustomerGroup::with(['localDescription'])->get();
            $filter->equal('customer_group_id', 'Группа покупателей')
                ->select($customerGroups->pluck('localDescription.name', 'id'));

            $filter->like('percent', 'Процент');
            $filter->like('priority', 'Приоритет');

            $filter->where(function ($query) {
                $query->where('date_start', '>=', $this->input);
            }, 'Дата начала')->datetime();

            $filter->where(function ($query) {
                $query->where('date_end', '<=', $this->input);
            }, 'Дата конца')->datetime();

        });

        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
        });

        return $grid;
    }

    protected function form()
    {
        return $this;
    }
}
