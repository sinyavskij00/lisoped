<?php

namespace App\Admin\Controllers;

use App\Entities\Attribute;
use App\Entities\AttributeGroup;
use App\Entities\AttributesValues;
use App\Entities\Filter;
use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Show;

class FilterController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Фильтры')
            ->description('Фильтры на сайте')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр фильтра')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование фильтра')
            ->description('Редактирование фильтра')
            ->row($this->form()->edit($id))
            ->row(function (Row $row) use ($id) {
                $controller = app(FilterValueController::class);
                $filterValueGrid = $controller->grid();
                $filterValueGrid->resource('/admin/filter-values')
                    ->setRelation(Filter::find($id)->values())
                    ->setName('skus')
                    ->setTitle('Значения фильтра');

                $row->column(12, $filterValueGrid);
            });
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание фильтра')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Filter);

        $grid->id('Id')->sortable();
        $grid->column('description.name', 'Название')->sortable();
        $grid->sort('Сортировка')->sortable();
        $grid->created_at('Создан')->sortable();
        $grid->updated_at('Обновлен')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->where(function ($query) {
                $query->whereHas('localDescription', function ($query) {
                    $query->where('name', 'like', '%' . $this->input . '%');
                });
            }, 'Название');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Filter::findOrFail($id));

        $show->id('Id');
        $show->sort('Сортировка');
        $show->name('Название')->as(function () {
            if (isset($this->localDescription) && isset($this->localDescription->name)) {
                return $this->localDescription->name;
            }
            return '---';
        });
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Filter);

        $form->tab('Основная информация', function ($form) {
            $form->text('slug', 'SEO-урл')->rules('required')
                ->help('Используется при постороении переменной фильтра');
            $form->number('sort', 'Сортировка');
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')
                    ->options(Language::all()->pluck('title', 'id'));
                $form->text('name', 'Название');
            });
        });

        return $form;
    }
}
