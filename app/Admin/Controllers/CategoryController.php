<?php

namespace App\Admin\Controllers;

use App\Entities\Attribute;
use App\Entities\AttributeGroup;
use App\Entities\Category;
use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class CategoryController extends Controller
{
    use HasResourceActions;

    protected $categories;


    public function __construct()
    {
        $this->categories = Category::with('localDescription')->get();
    }
    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Категории')
            ->description('Категории')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('категории')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать')
            ->description('категорию')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать')
            ->description('категорию')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Category);

        $grid->model()->with([
            'localDescription'
        ]);

        $grid->id('Id')->sortable();
        $grid->slug('SEO-урл')->sortable();
        $grid->column('description.name', 'Название')->sortable();

        $categories = $this->categories;
        $grid->column('parent_id', 'Родительская категория')->display(function ($categoryId) use ($categories) {
            $category = $categories->firstWhere('id', '=', $categoryId);
            if (isset($category) && isset($category->localDescription)) {
                return $category->localdescription->name;
            }
            return '-';
        })->sortable();
        $grid->column('status', 'Статус')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Активна' : 'Неактивна';
        });
        $grid->created_at('Создан')->sortable();
        $grid->updated_at('Обновлен')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->equal('parent_id', 'Родительская категория')
                ->select(
                    Category::with('localDescription')->get()
                        ->pluck('localDescription.name', 'id')
                );
            $filter->where(function ($query) {
                $query->whereHas('localDescription', function ($query) {
                    $query->where('name', 'like', '%' . $this->input . '%');
                });
            }, 'Название');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Category::findOrFail($id));

        $show->id('Id');
        $show->slug('Seo-урл');
        $show->image('Изображение');
        $show->field('status', 'Статус')->as(function ($default) {
            return isset($default) && $default === 1 ? 'Активна' : 'Неактивна';
        });
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Category);

        $form->tab('Основное', function ($form) {
            $form->text('slug', 'SEO-урл');

            $form->listbox('categoryattributes', 'Атрибуты')->options(
                Attribute::with('localDescription')->get()->pluck('localDescription.name', 'id')
            )->help('Фильтры');

            $form->elfinder('image', 'Изображение');
            $form->elfinder('icon', 'Иконка')->help('Используется в пиьме');
            $form->select('parent_id', 'Родительская категория')
                ->options($this->categories->pluck('localDescription.name', 'id'));

            $form->radio('layout_type', 'Тип отображения')->options([
                0 => 'Листинг товаров',
                1 => 'Подкатегории'
            ]);
            $form->switch('status', 'Статус')->states([
                'on' => ['value' => 1, 'text' => 'Активна', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Неактивна', 'color' => 'danger'],
            ]);
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', ' ', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
                $form->text('name', 'Название');
                $form->ckeditor('description', 'Описание');
                $form->textarea('meta_h1');
                $form->textarea('meta_description');
                $form->textarea('meta_keywords');
                $form->textarea('meta_title');
            });
        });


        return $form;
    }
}
