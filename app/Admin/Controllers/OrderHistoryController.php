<?php

namespace App\Admin\Controllers;

use App\Entities\BillingAccount;
use App\Entities\BillingCart;
use App\Entities\Order;
use App\Entities\OrderHistory;
use App\Entities\OrderStatus;
use App\Entities\SmsTemplate;
use App\Http\Controllers\Controller;
use App\Services\OrderService;
use Encore\Admin\Admin;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

class OrderHistoryController extends Controller
{
    use HasResourceActions;

    protected $orderStatuses;

    protected $admins;

    public function __construct()
    {
        $this->orderStatuses = OrderStatus::with('localDescription')->get();
        $this->admins = Administrator::all();
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('История заказов')
            ->description('История всех заказов')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр истории заказа')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование истории')
            ->description('Редактирование истории')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Добавление истории')
            ->description('Добавление истории')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    public function grid()
    {
        $grid = new Grid(new OrderHistory);

        $grid->id('Id')->sortable();
        $grid->column('order_id', 'Номер заказа')->display(function ($orderId) {
            $result = '№' . $orderId;
            $result .= ' <a href="' . route('orders.show', $orderId) . '"><i class="fa fa-eye"></i></a>';
            $result .= ' <a href="' . route('orders.edit', $orderId) . '"><i class="fa fa-edit"></i>';
            return $result;
        })->sortable();

        $orderStatuses = $this->orderStatuses;
        $grid->column('order_status_id', 'Статус заказа')->display(function ($orderStatusId) use ($orderStatuses) {
            if (isset($orderStatusId)) {
                if ($orderStatusId === 0) {
                    return 'Ошибочный заказ';
                }

                $orderStatus = $orderStatuses->firstWhere('id', '=', $orderStatusId);
                if (isset($orderStatus) && isset($orderStatus->localDescription)) {
                    return $orderStatus->localDescription->name;
                }
            }
            return '--';
        })->sortable();

        $grid->column('notify', 'email')->display(function ($notify) {
            return isset($notify) && $notify === 1 ? 'Да' : 'Нет';
        })->setAttributes(['width' => ' 170px'])->sortable();
        $grid->column('notify_sms', 'sms')->display(function ($notify) {
            return isset($notify) && $notify === 1 ? 'Да' : 'Нет';
        })->setAttributes(['width' => ' 160px'])->sortable();
        $grid->column('smsTemplate.name', 'Шаблон SMS');
        $grid->column('notify_viber', 'viber')->display(function ($notify) {
            return isset($notify) && $notify === 1 ? 'Да' : 'Нет';
        })->setAttributes(['width' => ' 160px'])->sortable();
        $grid->column('send_check', 'Отослать чек')->display(function ($sendCheck) {
            return isset($sendCheck) && $sendCheck === 1 ? 'Да' : 'Нет';
        })->setAttributes(['width' => ' 160px'])->sortable();
        $grid->column('public_comment', 'Пуб. коммент.')->display(function ($text) {
            return str_limit($text, 200, '...');
        })->setAttributes(['width' => ' 215px']);
        $grid->column('private_comment', 'Прив. коммент.')->display(function ($text) {
            return str_limit($text, 200, '...');
        })->setAttributes(['width' => ' 210px']);
        $grid->column('billingCart.number', 'Номер карты')->display(function ($cartNumber) {
            return !empty($cartNumber) ? $cartNumber : '---';
        })->setAttributes(['width' => '150px']);
        $grid->column('billingAccount.bank_number', 'Р/С')->display(function ($cartNumber) {
            return !empty($cartNumber) ? $cartNumber : '---';
        })->setAttributes(['width' => '150px']);
        $admins = $this->admins;
        $grid->column('admin_id', 'Пользователь')->display(function ($adminId) use ($admins) {
            if (isset($adminId)) {
                $admin = $admins->firstWhere('id', '=', $adminId);
                if (isset($admin)) {
                    return $admin->name . ' (' . $admin->username . ')';
                }
            }
            return '---';
        })->sortable();
//        $grid->created_at('Создан')->sortable();
//        $grid->updated_at('Обновлен')->sortable();

        // Filters
        $grid->filter(function (Grid\Filter $filter) use ($admins) {
            $orders = Order::all();
            $filter->equal('order_id', 'Номер заказа')->select($orders->pluck('id', 'id'));

            $adminOptions = [];
            foreach ($admins as $admin) {
                $adminOptions[$admin->id] = $admin->name . ' (' . $admin->username . ')';
            }
            $filter->equal('admin_id', 'Пользователь')->select($adminOptions);

            $filter->where(function ($query) {
                $query->where('created_at', '>=', $this->input);
            }, 'Дата начала')->datetime();

            $filter->where(function ($query) {
                $query->where('created_at', '<=', $this->input);
            }, 'Дата конца')->datetime();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(OrderHistory::findOrFail($id));

        //$show->id('Id');
        $show->order_id('Номер заказа');

        $admins = $this->admins;
        $show->admin_id('Пользователь')->as(function ($adminId) use ($admins) {
            if (isset($adminId)) {
                $admin = $admins->firstWhere('id', '=', $adminId);
                if (isset($admin)) {
                    return $admin->name . ' (' . $admin->username . ')';
                }
            }
            return '---';
        });

        $orderStatuses = $this->orderStatuses;
        $show->order_status_id('Статус заказа')->as(function ($orderStatusId) use ($orderStatuses) {
            if (isset($orderStatusId)) {
                $orderStatus = $orderStatuses->firstWhere('id', '=', $orderStatusId);
                if (isset($orderStatus) && isset($orderStatus->localDescription)) {
                    return $orderStatus->localDescription->name;
                }
            }
            return '---';
        });
        $show->notify('Уведомить покупателя по email')->as(function ($notify) {
            if ($notify === 1) {
                return 'Да';
            } else {
                return 'Нет';
            }
        });
        $show->notify_sms('Уведомить покупателя по sms')->as(function ($notifySms) {
            if ($notifySms === 1) {
                return 'Да';
            } else {
                return 'Нет';
            }
        });
        $show->notify_vuiber('Уведомить покупателя по sms')->as(function ($notifyViber) {
            if ($notifyViber === 1) {
                return 'Да';
            } else {
                return 'Нет';
            }
        });
        $show->sms_template_id('Уведомить покупателя по sms')->as(function ($smsTemplateId) {
            if (isset($smsTemplateId)) {
                $template = SmsTemplate::find($smsTemplateId);
                if (isset($template)) {
                    return $template->title;
                }
            }
        });
        $show->send_check('Отослать чек')->as(function ($sendCheck) {
            if ($sendCheck === 1) {
                return 'Да';
            } else {
                return 'Нет';
            }
        });
        $show->public_comment('Публичный комментарий');
        $show->private_comment('Приватный комментарий');

        $show->billing_account_id('Расчетный счет')->as(function ($billingAccountId) {
            if (isset($billingAccountId)) {
                $billingAccount = BillingAccount::find($billingAccountId);
                if (isset($billingAccount)) {
                    return $billingAccount->name;
                }
            }
            return '---';
        });
        $show->billing_cart_id('Расчетная карта')->as(function ($billingCartId) {
            if (isset($billingCartId)) {
                $billingCart = BillingCart::find($billingCartId);
                if (isset($billingCart)) {
                    return $billingCart->name;
                }
            }
            return '---';
        });

        $show->created_at('Создано');
        $show->updated_at('Обновленно');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $order = Order::where('id', '=', request('order_id'))->first();

        $form = new Form(new OrderHistory);
        $form->ignore(['mail_template', 'order_total', 'order_total_prepayment']);

        $admin = Auth::guard('admin')->user();

        $form->display('admin', 'Текущий пользователь')
            ->default($admin->name . ' (' . $admin->username . ')');
        $form->hidden('admin_id', 'Администратор')->default($admin->id);

        $form->select('order_id', 'Номер заказа')
            ->options(Order::all()->pluck('id', 'id'))
            ->rules('required')
            ->value(request('order_id'));

        if ($order) {
            $form->display('order_total', 'Сумма заказа')->value($order->total);
            $form->display('order_total_prepayment', 'Сумма предоплаты')
                ->value($order->skusPrepaymentTotal->total_prepayment);
        }

        $form->select('order_status_id', 'Статус заказа')
            ->options($this->orderStatuses->pluck('localDescription.name', 'id'))
            ->rules('required');

        $states = [
            'on' => ['value' => 1, 'text' => 'Да'],
            'off' => ['value' => 0, 'text' => 'Нет'],
        ];
        $form->switch('notify', 'Уведомить покупателя по email')->states($states);
        $form->select('mail_template', 'Шаблон письма')
            ->options([
                'default' => 'По умолчанию',
                'payment_details_card' => 'Письмо с реквизитами для оплаты (банк. карта)',
                'payment_details_payment_account' => 'Письмо с реквизитами для оплаты (расчетный счет)',
                'payment_details_COD' => 'Письмо с реквизитами для оплаты (наложеный платеж)',
                'proof_payment' => 'Письм подтверждения оплаты',
                'product_shipped' => 'Товар отправлен',
            ])
            ->default('default')
            ->rules('required');
        $form->switch('notify_sms', 'Уведомить покупателя по sms')->states($states);
        $form->switch('notify_viber', 'Уведомить покупателя по viber')->states($states);

        $form->select('sms_template_id', 'Шаблон для SMS')
            ->options(SmsTemplate::all()->pluck('title', 'id'));

        $form->switch('send_check', 'Отослать чек')->states($states);
        $form->switch('send_guarantee', 'Отослать гарантию')->states($states);
        $form->textarea('public_comment', 'Публичный комментарий')
            ->help('Будет отправлен покупателю');
        $form->textarea('private_comment', 'Приватный комментарий')
            ->help('Для внутреннего пользования менеджерами');

        $billingCarts = BillingCart::all();
        $form->select('billing_cart_id', 'Платежная карта')
            ->options($billingCarts->pluck('name', 'id'));

        $billingAccounts = BillingAccount::all();
        $form->select('billing_account_id', 'Платежный счет')
            ->options($billingAccounts->pluck('name', 'id'));

        $form->saving(function ($form) {
            $statusFinished = OrderStatus::where('is_finished', '=', 1)->first();
            $targetStatus = intval($form->order_status_id);
            if (isset($statusFinished) && $targetStatus === $statusFinished->id) {
                $order = Order::find($form->order_id);
                if (isset($order)) {
                    $orderTotal = floatval($order->total);
                    $orderProfitabilityTotal = 0;
                    foreach ($order->profitabilities as $profitability) {
                        $orderProfitabilityTotal += $profitability->price;
                    }
                    if ($orderProfitabilityTotal < $orderTotal) {
                        $error = new MessageBag([
                            'title' => 'Заказ не рентабельный',
                            'message' => 'Заказ нельзя закрыть пока он не будет рентабельным',
                        ]);
                        return back()->with(compact('error'));
                    }
                }
            }
        });

        $form->saved(function ($form) {
            $orderHistory = $form->model();
            $order = $orderHistory->order;
            $orderService = app()->make(OrderService::class);

            if (isset($orderHistory)) {
                $orderService->changeOrderStatus($order, $orderHistory->order_status_id);
                $orderService->notifyCustomer($orderHistory);
                if ($orderHistory->send_check === 1) {
                    $orderService->sendCheck($order->id);
                }
                if ($orderHistory->send_guarantee === 1) {
                    $orderService->sendGuarantee($order->id);
                }
            }

            return redirect()->route('orders.show', $order->id);
        });

        return $form;
    }
}
