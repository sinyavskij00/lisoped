<?php

namespace App\Admin\Controllers;

use App\Entities\Color;
use App\Entities\ColorDescription;
use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;

class ColorController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Цвета')
            ->description('Список всех цветов')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('цвета')
            ->row($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('цвета')
            ->row($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('цвета')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Color);

        $grid->id('Id')->sortable();
        $grid->column('image', 'Изображение')->image(null)->sortable();
        $grid->column('localDescription.name', 'Название')->sortable();
        $grid->created_at('Создан')->sortable();
        $grid->updated_at('Обновлен')->sortable();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Color::findOrFail($id));

        $show->id('Id');
        $show->name('Название')->as(function () {
            if (isset($this->localDescription)) {
                return $this->localDescription->name;
            }
            return '---';
        });
        $show->image('Изображение');
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Color);

        $form->tab('Основная информация', function ($form) {
            $form->elfinder('image', 'Изображение');
            $form->text('anchor', 'Якорь');
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описание', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')
                    ->options(Language::all()->pluck('title', 'id'));
                $form->text('name', 'Название');
            });
        });

        return $form;
    }
}
