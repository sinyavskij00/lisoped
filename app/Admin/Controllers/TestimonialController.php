<?php

namespace App\Admin\Controllers;

use App\Entities\Testimonial;
use App\Http\Controllers\Controller;
use App\Mail\Information\ReviewAccepted;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Mail;

class TestimonialController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Отзывы о магазине')
            ->description('Все отзывы о магазине')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Промотр отзыва')
            ->description('Просмотр отзыва о магазине')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать отзыв')
            ->description('Редактировать отзыв о магазине')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать отзыв')
            ->description('Создать отзыв о магазине')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Testimonial);

        $grid->id('Id')->sortable();
        $grid->name('Имя')->sortable();
        $grid->email('Email')->sortable();
        $grid->column('text', 'Текст')->display(function ($text) {
            if (isset($text)) {
                return str_limit($text, 100);
            }
            return '';
        })->sortable();
        $grid->rating('Оценка')->sortable();
        $grid->column('status', 'Статус')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Одобрен' : 'Неодобрен';
        })->sortable();
        $grid->created_at('Создан')->sortable();
        $grid->updated_at('Обновлен')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->equal('status', 'Статус')->select([
                0 => 'Отключено',
                1 => 'Включено',
            ]);

            $filter->equal('rating', 'Оценка')->select([
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5,
            ]);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Testimonial::findOrFail($id));

        $show->id('Id');
        $show->name('Имя');
        $show->email('E-mail');
        $show->text('Текст');
        $show->rating('Оценка');
        $show->status('Статус')->as(function ($default) {
            return isset($default) && $default === 1 ? 'Одобрен' : 'Неодобрен';
        });
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Testimonial);
        $form->tab('Основное', function ($form) {
            $form->text('name', 'Имя');
            $form->email('email', 'Email');
            $form->textarea('text', 'Текст');
            $form->number('rating', 'Оценка')->default(5)->max(5)->min(0);
            $testimonialsOptions = [];
            $testimonialTree = Testimonial::withDepth()->defaultOrder()->get()->toTree();

            $traverse = function ($testimonials, $prefix = '') use (&$traverse, &$testimonialsOptions) {
                foreach ($testimonials as $testimonial) {
                    $testimonialsOptions[$testimonial->id] = $prefix
                        . ' '
                        . $testimonial->name
                        . ' '
                        . '( ' . $testimonial->created_at . ' )';

                    $prefix .= $testimonial->name . ' ( ' . $testimonial->created_at . ' ) > ';
                    $traverse($testimonial->children, $prefix);
                }
            };

            $traverse($testimonialTree);

            $form->select('parent_id', 'Родительский комментарий')->options($testimonialsOptions);
            $form->switch('status', 'Статус')->states([
                'on' => ['value' => 1, 'text' => 'Одобрен', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Неодобрен', 'color' => 'danger'],
            ]);
        })->tab('Изображения', function ($form) {
            $form->hasMany('images', 'Изображения', function (Form\NestedForm $form) {
                $form->elfinder('image', 'Изображение');
            });
        });

        $form->saved(function (Form $form) {
            $testimonial = $form->model();
            if (isset($testimonial->status) && $testimonial->status === 1 && !empty($testimonial->email)) {
                $link = route('testimonials');
                $mail = new ReviewAccepted($testimonial->text, $link);
                $mail->subject(__('mails.information.review_accepted.subject'));
                Mail::to($testimonial->email)->send($mail);
            }
        });

        return $form;
    }
}
