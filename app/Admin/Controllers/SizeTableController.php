<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\SizeTable;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class SizeTableController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Таблицы размеров')
            ->description('Все таблицы размеров')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр таблицы размеров')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('Редактирование таблицы размеров')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание таблицы размеров')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new SizeTable);

        $grid->id('Id')->sortable();
        $grid->column('name', 'Заголовок')->display(function () {
            return isset($this->localDescription->title) ? $this->localDescription->title : '---';
        })->sortable();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(SizeTable::findOrFail($id));

        $show->id('Id');
        $show->name('Заголовок')->as(function () {
            return isset($this->localDescription->title) ? $this->localDescription->title : '---';
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new SizeTable);

        $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
            $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
            $form->text('title', 'Заголовок');
            $form->textarea('text', 'Таблица (csv)');
        });

        return $form;
    }
}
