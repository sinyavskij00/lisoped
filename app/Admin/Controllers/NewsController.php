<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\News;
use App\Entities\NewsReview;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class NewsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Новости')
            ->description('Список всех новостей')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр новости')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать новость')
            ->description('Изменение новости')
            ->row($this->form()->edit($id))
            ->row(Admin::grid(NewsReview::class, function (Grid $grid) use ($id) {
                $grid->setName('Комментарии')
                    ->setTitle('Комментарии')
                    ->setRelation(News::find($id)->reviews())
                    ->resource('/admin/news-reviews');
                $grid->id()->sortable();
                $grid->column('name', 'Имя')->sortable();
                $grid->column('text', 'Текст')->sortable();
                $grid->column('email', 'E-mail')->sortable();
                $grid->column('status', 'Статус')->sortable();
            }));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание новости')
            ->description('Новая новость')
            ->row($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new News);

        $grid->id('Id')->sortable();
        $grid->column('localDescription.title', 'Название')->sortable();
        $grid->column('slug', 'SEO-урл')->sortable();
        $grid->column('image', 'Изображение')->image(null, 70)->sortable();
        $grid->views('Просмотры')->sortable();
        $grid->column('status', 'Статус')->display(function ($status) {
            return isset($status) && $status === 1 ? 'Опубликован' : 'Неопубликован';
        })->sortable();
        $grid->created_at('Создано');
        $grid->updated_at('Обновленно');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(News::findOrFail($id));

        $show->id('Id');
        $show->slug('SEO-урл');
        $show->image('Изображение')->image();
        $show->views('Просмтры');
        $show->status('Статус')->as(function ($status) {
            return isset($status) && $status === 1 ? 'Опубликован' : 'Неопубликован';
        });
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new News);

        $form->tab('Основное', function ($form) {
            $form->text('slug', 'SEO-урл');
            $form->elfinder('image', 'Изображение');
            $form->number('views', 'Просмотры')->min(0);
            $form->switch('status', 'Статус')->states([
                'on'  => ['value' => 1, 'text' => 'Опубликован', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Неопубликован', 'color' => 'danger'],
            ])->default(0);
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
                $form->text('title', 'Название');
                $form->ckeditor('text', 'Текст');
                $form->textarea('meta_title', 'Meta Title');
                $form->textarea('meta_description', 'Meta Description');
                $form->textarea('meta_keywords', 'Meta Keywords');
            });
        });

        return $form;
    }
}
