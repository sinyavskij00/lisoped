<?php

namespace App\Admin\Controllers;

use App\Entities\Preorder;
use App\Entities\StockKeepingUnit;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class PreorderController extends Controller
{
    use HasResourceActions;

    protected $skuOptions;
    protected $skuProductOptions;

    protected $notifyStates;

    public function __construct()
    {
        $skus = StockKeepingUnit::with('product.localDescription')->get();
        foreach ($skus as $sku) {
            $this->skuOptions[$sku->id] = $this->getProductName($sku);
            $this->skuProductOptions[$sku->id] = $sku;
        }

        $this->notifyStates = [
            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
            'on' => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
        ];
    }

    protected function getProductName($sku)
    {
        $productName = isset($sku->product)
        && isset($sku->product->localDescription)
        && isset($sku->product->localDescription->name)
            ? $sku->product->localDescription->name
            : '';

        return '( ' . $sku->sku . ' ) ' . $productName;
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Предзаказы')
            ->description('Все предзаказы')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр предзаказа')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('Редактирование предзаказа')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание предзаказа')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Preorder);

        $grid->model()->orderBy('created_at', "DESC");

        $grid->id('Id');
        $grid->name('Имя');
        $grid->email('Email');
        $grid->telephone('Телефон');
        $grid->column('notify', 'Уведомлен')->sortable()->switch($this->notifyStates);

        $that = $this;
        $skuOptions = $this->skuProductOptions;
        $grid->column('sku_id', 'Товар')->display(function ($skuId) use ($skuOptions, $that) {
            if (isset($skuId) && isset($skuOptions[$skuId])) {
                return '<a href="'.asset('admin/products/'
                    . $skuOptions[$skuId]->product->id . '/edit').'">'
                    . $that->getProductName($skuOptions[$skuId]) . '</a>';
            }
            return '';
        })->sortable();

        $grid->column('comment', 'Комментарий')->limit(70);

        $grid->created_at('Создано');
        $grid->updated_at('Обновлено');

        $grid->filter(function (Grid\Filter $filter) use ($skuOptions) {
            $filter->equal('sku_id', 'Товар')->select($skuOptions);

            $filter->like('email', 'Email');

            $filter->like('naem', 'Имя');

            $filter->equal('notify', 'Уведомлен')->select([
                1 => 'Да',
                0 => 'Нет',
            ]);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Preorder::findOrFail($id));

        $show->id('Id');
        $show->name('Имя');
        $show->email('Email');
        $show->telephone('Телефон');
        $show->notify('Уведомлен');
        $show->comment('Комментарий');
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Preorder);

        $form->text('name', 'Имя');
        $form->email('email', 'Email')->rules('required');
        $form->text('telephone', 'Телефон');
        $form->textarea('comment', 'Комментарий');
        $form->switch('notify', 'Уведомлен')->states($this->notifyStates)->default(0);

        $form->select('sku_id', 'Товар')->options($this->skuOptions)->rules('required');

        return $form;
    }
}
