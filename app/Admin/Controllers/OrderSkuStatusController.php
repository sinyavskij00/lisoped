<?php

namespace App\Admin\Controllers;

use App\Entities\OrderSkuStatus;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class OrderSkuStatusController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Статусы товаров в заказе')
            ->description('Статусы товаров в заказе')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр статуса')
            ->description('Просмотр статуса товаров в заказе')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование статуса')
            ->description('Редактирование статуса товара в заказе')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new OrderSkuStatus);

        $grid->id('Id');
        $grid->column('color', 'Цвет')->display(function ($color) {
            if (isset($color)) {
                return '<div style="background-color: ' . $color . '; width: 100px; height: 20px;"></div>';
            }
            return '';
        })->sortable();
        $grid->name('Название');
        $grid->created_at('Создано');
        $grid->updated_at('Обновлено');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(OrderSkuStatus::findOrFail($id));

        $show->id('Id');
        $show->color('Цвет');
        $show->name('Название');
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new OrderSkuStatus);

        $form->color('color', 'Цвет');
        $form->text('name', 'Название');

        return $form;
    }
}
