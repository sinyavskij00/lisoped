<?php

namespace App\Admin\Controllers;

use App\Entities\Vendor;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class VendorController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Поставщики')
            ->description('Все поставщики')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр поставщика')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование поставщика')
            ->description('Редактирование поставщика')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание поставщика')
            ->description('Создание поставщика')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Vendor);

        $grid->column('name', 'Название')->sortable();
        $grid->column('email', 'E-mail')->setAttributes(['width' => '100px'])->sortable();
        $grid->column('email2', 'E-mail2')->setAttributes(['width' => '100px'])->sortable();
        $grid->column('email3', 'Skype')->setAttributes(['width' => '100px'])->sortable();
        $grid->column('site', 'Сайт')->sortable();
        $grid->column('comment', 'Комментарий')->display(function ($text) {
            return str_limit($text, 200, '...');
        })->setAttributes(['width' => '250px'])->sortable();
        $grid->column('telephone_1', 'Телефон 1')->setAttributes(['width' => '120px'])->sortable();
        $grid->column('telephone_2', 'Телефон 2')->setAttributes(['width' => '120px'])->sortable();
        $grid->column('address', 'Адрес')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->like('name', 'Название');

            $filter->where(function ($query) {
                $query->where('email', 'like', '%' . $this->input . '%')
                    ->orWhere('email2', 'like', '%' . $this->input . '%')
                    ->orWhere('email3', 'like', '%' . $this->input . '%');
            }, 'E-mail');

            $filter->like('address', 'Адрес');

            $filter->where(function ($query) {
                $query->where('telephone_1', 'like', "%{$this->input}%")
                    ->orWhere('telephone_2', 'like', "%{$this->input}%");
            }, 'Номер телефона');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Vendor::findOrFail($id));

        $show->name('Название');
        $show->email('E-mail');
        $show->email2('E-mail2');
        $show->email3('Skype');
        $show->comment('Комментарий');
        $show->telephone_1('Телефон 1');
        $show->telephone_2('Телефон 2');
        $show->site('Сайт');
        $show->address('Адрес');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Vendor);

        $form->text('name', 'Название');
        $form->email('email', 'E-mail');
        $form->email('email2', 'E-mail2');
        $form->text('email3', 'Skype');
        $form->textarea('comment', 'Комментарий');
        $form->text('telephone_1', 'Телефон 1');
        $form->text('telephone_2', 'Телефон 2');
        $form->text('site', 'Сайт поставщика');
        $form->textarea('address', 'Адрес');

        return $form;
    }
}
