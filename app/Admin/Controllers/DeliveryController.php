<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\SiteSetting;
use App\Http\Controllers\Controller;
use App\Library\Settings;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class DeliveryController extends Controller
{
    protected $languages;

    protected $settings;

    public function __construct(Settings $settings)
    {
        $this->languages = Language::all();
        $this->settings = $settings;
    }

    public function index(Content $content)
    {
        return $content
            ->header('Страница "Оплаты и доставки"')
            ->description('Страница "Оплаты и доставки"')

            ->row(function (Row $row) {
                $form = new Form();
                $textes_d = $this->settings->getByKey('page_delyvery');
                $textes_b = $this->settings->getByKey('page_buy');
                foreach ($this->languages as $language) {
                    $localText_d = isset($textes_d[$language->id]) ? $textes_d[$language->id] : '';
                    $localText_b = isset($textes_b[$language->id]) ? $textes_b[$language->id] : '';
                    $form->ckeditor(
                        'page_delyvery[' . $language->id . ']',
                        'Текст "Доставки" ' . $language->slug
                    )->default($localText_d);

                    $form->ckeditor(
                        'page_buy[' . $language->id . ']',
                        'Текст "Оплаты" ' . $language->slug
                    )->default($localText_b);
                }

                $form->disablePjax();
                $form->action(route('pages.delivery'));

                $row->column(12, $form);
            });
    }

    public function update(Request $request)
    {
        SiteSetting::updateByCode('page_delyvery', $request->except(['_token']));
        SiteSetting::updateByCode('page_buy', $request->except(['_token']));
        $success = new MessageBag([
            'title' => 'Изменения были применены',
        ]);
        return back()->with(compact('success'));
    }
}
