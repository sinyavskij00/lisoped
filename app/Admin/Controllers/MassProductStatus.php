<?php

namespace App\Admin\Controllers;

use App\Entities\Category;
use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;


class MassProductStatus extends Controller
{
    protected $massDiscountTable = 'mass_discounts';

    public function index(Content $content)
    {
        return $content
            ->header('Модуль массового управления статусами товаров')
            ->description('Модуль массового управления статусами товаров')
            ->row(function (Row $row) {
                $form = new Form();

                $manufacturers = Manufacturer::with('localDescription')->get();
                $form->listbox('manufacturers', 'Производители')
                    ->options($manufacturers->pluck('localDescription.name', 'id'));

                $categories = Category::with('localDescription')->get();
                $form->listbox('categories', 'Категории')
                    ->options($categories->pluck('localDescription.name', 'id'));

                $form->select('status', 'Статус')->options([
                    0 => 'Отлючить',
                    1 => 'Включить',
                ]);

                $form->disablePjax();
                $form->action('mass-status');

                $row->column(12, $form);
            });
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);

        $categoriesIds = $request->input('categories', []);
        $manufacturersIds = $request->input('manufacturers', []);
        $status = intval($request->input('status', 0));

        Product::whereIn('category_id', $categoriesIds)
            ->orWhereIn('manufacturer_id', $manufacturersIds)
            ->update(['status' => $status]);
        $success = new MessageBag([
            'title' => 'Статусы товаров были изменены',
        ]);
        return back()->with(compact('success'));
    }
}
