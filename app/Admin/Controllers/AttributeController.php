<?php

namespace App\Admin\Controllers;

use App\Entities\Attribute;
use App\Entities\AttributeGroup;
use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class AttributeController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Атрибуты')
            ->description('Атрибуты на сайте')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр атрибута')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование атрибутов')
            ->description('Редактирование атрибутов')
            ->row($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание атрибута')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Attribute);

        $grid->id('Id')->sortable();
        $grid->column('description.name', 'Название')->sortable();
        $grid->column('attribute_group_id', 'Группа атрибутов')->display(function ($attributeGroupId) {
            if (isset($attributeGroupId)) {
                $attributeGroup = AttributeGroup::find($attributeGroupId);
                if (isset($attributeGroup) && isset($attributeGroup->localDescription)) {
                    return $attributeGroup->localDescription->name;
                }
            }
            return '---';
        })->sortable();
        $grid->sort('Сортировка')->sortable();
        $grid->created_at('Создан')->sortable();
        $grid->updated_at('Обновлен')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->where(function ($query) {
                $query->whereHas('localDescription', function ($query) {
                    $query->where('name', 'like', '%' . $this->input . '%');
                });
            }, 'Название');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Attribute::findOrFail($id));

        $show->id('Id');
        $show->attribute_group_id('Группа атрибута')->as(function ($attrGroupId) {
            if (isset($attrGroupId)) {
                $attributeGroup = AttributeGroup::find($attrGroupId);
                if (isset($attributeGroup) && isset($attributeGroup->localDescription)) {
                    return $attributeGroup->localDescription->name;
                }
            }
            return '---';
        });
        $show->sort('Сортировка');
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Attribute);

        $form->tab('Основная информация', function ($form) {
            $form->select('attribute_group_id', 'Группа атрибутов')
                ->options(AttributeGroup::all()->pluck('localDescription.name', 'id'));
            $form->text('slug', 'Seo урл')
                ->rules('required|max:100|unique:attributes,slug' . (isset($form->slug) ? ',' . $form->slug : ''));
            $form->number('sort', 'Сортировка');
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')
                    ->options(Language::all()->pluck('title', 'id'));
                $form->text('name', 'Название');
            });
        });

        return $form;
    }
}
