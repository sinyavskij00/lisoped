<?php

namespace App\Admin\Controllers;

use App\Entities\Order;
use App\Entities\OrderSku;
use App\Entities\OrderSkuStatus;
use App\Entities\StockKeepingUnit;
use App\Http\Controllers\Controller;
use App\Services\OrderService;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class OrderSkuController extends Controller
{
    use HasResourceActions;

    protected $skus;

    public function __construct()
    {
        $this->skus = StockKeepingUnit::with([
            'vendor',
            'product.localDescription'
        ])->get();
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Товары заказа')
            ->description('Все товары заказа')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр товара заказа')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('Редактирование товара заказа')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание товара заказа')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    public function grid()
    {
        $grid = new Grid(new OrderSku);

        $grid->id();

        $grid->column('sku', 'Артикул на сайте');
        $grid->column('vendor_sku', 'Артикул поставщика')->editable();
//        $grid->column('vendor_sku', 'Артикул поставщика')->editable();
//        $skus = $this->skus;
//        $grid->column('vendor_name', 'Поставщик')->display(function () use ($skus) {
//            $skuId = $this->sku_id;
//            $sku = $skus->firstWhere('id', '=', $skuId);
//            if (isset($sku) && isset($sku->vendor) && isset($sku->vendor->name)) {
//                return $sku->vendor->name;
//            }
//            return '';
//        });

        $grid->column('name', 'Название товара');
        $grid->column('vendor_price', 'Цена поставщика')->editable();
        $grid->column('quantity', 'Количество');
        $grid->column('price', 'Цена');
        $grid->column('total', 'Всего');
        $grid->column('prepayment', 'Предоплата')->editable();
        $grid->column('balance', 'Прибыль')->display(function () {
            $price = $this->price;
            $vendorPrice = $this->vendor_price;
            if (isset($price) && isset($vendorPrice)) {
                return $price - $vendorPrice;
            }
            return '---';
        });

        $orderSkuStatuses = OrderSkuStatus::all();
        $grid->column('status', 'Статус')->editable('select', $orderSkuStatuses->pluck('name', 'id'));

        $grid->column('ttn', 'Накладная')->editable();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(OrderSku::findOrFail($id));

        $show->id('Id');
        $show->order_id('Order id');
//        $show->sku('Sku')->as(function () { //todo need attribute have relation
//            dd($this);
//        });
        $show->vendor_sku('Vendor sku');
        $show->sku_id('Sku id');
        $show->name('Name');
        $show->model('Model');
        $show->size('Size');
        $show->color('Color');
        $show->year('Year');
        $show->quantity('Quantity');
        $show->price('Price');
        $show->vendor_price('Vendor price');
        $show->total('Total');
        $show->vendor_total('Vendor total');
        $show->reward('Reward');
        $show->shipped_status('Shipped status');
        $show->ttn('Ttn');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new OrderSku);

        $orders = Order::get(['id']);
        $form->select('order_id', 'Номер заказа')
            ->options($orders->pluck('id', 'id'))
            ->rules('required')
            ->value(request('order_id'));
        $form->text('vendor_sku', 'Артикул производителя');

        $skuOptions = [];
        foreach ($this->skus as $sku) {
            $productName = '';
            if (isset($sku->product) && isset($sku->product->localDescription)) {
                $productName = $sku->product->localDescription->name;
            }
            $skuOptions[$sku->id] = $sku->sku . (!empty($productName) ? ' (' . $productName . ')' : '');
        }
        $form->select('sku_id', 'Артикул')->options($skuOptions)->rules('required');
        $form->number('quantity', 'Количество')->default(1);
        $form->decimal('price', 'Цена')->default(0.00);
        $form->decimal('vendor_price', 'Цена поставщика')->default(0);
        $form->decimal('prepayment', 'Предоплата')->default(0.00);
        $form->switch('shipped_status', 'Статус отправки')
            ->help('Была ли отправленна заявка поставщику');

        $orderSkuStatuses = OrderSkuStatus::all();
        $form->select('status', 'Статус')->options($orderSkuStatuses->pluck('name', 'id'));

        $form->text('ttn', 'ТТН');

        $form->saving(function (Form $form) {
            $skuId = $form->sku_id;
            $sku = $this->skus->firstWhere('id', '=', $skuId);
            if (isset($sku)) {
                $form->sku = $sku->sku;
                $form->name = isset($sku->product) && ($sku->product->localDescription)
                    ? $sku->product->localDescription->name
                    : '';
                $form->model = $sku->model;
                $form->size = isset($sku->size) ? $sku->size->value : '';
                $form->color = isset($sku->color) && isset($sku->color->localDescription)
                    ? $sku->color->localDescription->name
                    : '';
                $form->year = isset($sku->year) ? $sku->year->value : '';
            }
        });

        $form->saved(function (Form $form) {
            $orderSku = $form->model();
            $orderId = $orderSku->order_id;
            if (!empty($orderId)) {
                $orderService = app(OrderService::class);
                $orderService->countOrderTotal($orderId);

                return redirect()->route('orders.show', $orderId);
            }
        });

        return $form;
    }
}
