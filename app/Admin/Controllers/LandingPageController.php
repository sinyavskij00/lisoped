<?php

namespace App\Admin\Controllers;

use App\Entities\Category;
use App\Entities\LandingPage;
use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class LandingPageController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Посадочные страницы')
            ->description('Посадочные страницы')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр посадочной страницы')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать посадочную страницу')
            ->description('Редактировать посадочную страницу')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание посадочной страницы')
            ->description('Создание посадочной страницы')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new LandingPage);

        $grid->id('Id')->sortable();
        $grid->column('url', 'URL')->sortable();
        $grid->column('meta_title', 'Мета заголовок')->sortable();
        $grid->column('meta_h1', 'Meta H1')->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->like('url', 'URL');
            $filter->like('meta_title', 'Meta title');
            $filter->like('meta_h1', 'Meta H1');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(LandingPage::findOrFail($id));

        $show->id('Id');
        $show->url('Url');

        $show->meta_h1('meta h1');
        $show->description('Описание');
        $show->meta_title('meta title');
        $show->meta_description('meta description');
        $show->meta_keywords('meta keywords');

        $show->created_at('Создано');
        $show->updated_at('Обновленно');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new LandingPage);

        $form->text('url', 'Url');
        $form->text('meta_h1', 'meta_h1');
        $form->ckeditor('description', 'Описание');
        $form->textarea('meta_title', 'meta_title');
        $form->textarea('meta_description', 'meta_description');
        $form->textarea('meta_keywords', 'meta_keywords');

        return $form;
    }
}
