<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\OrderStatus;
use App\Entities\SiteSetting;
use App\Http\Controllers\Controller;
use App\Library\Settings;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Form;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Widgets\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\MessageBag;

class SettingsController extends Controller
{
    use HasResourceActions;

    protected $languages;

    protected $settings;

    public function __construct(Settings $settings)
    {
        $this->languages = Language::all();
        $this->settings = $settings;
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @param Settings $settings
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Настройки сайта')
            ->description('Настройки сайта')
            ->row(function (Row $row) {
                $tab = new Tab();

                $form = $this->getForm();
                $form->disablePjax();

                $tab->add('Основное', $form->render());

                $tab->add('Команды', $this->getCommandTab());

                $tab->add('Резервные копии', $this->getDumpsTab());

                $row->column(12, $tab);
            });
    }

    public function update(Request $request)
    {
        SiteSetting::updateByCode('site_settings', $request->except(['_token']));
        $success = new MessageBag([
            'title' => 'Изменения были применены',
        ]);
        return back()->with(compact('success'));
    }

    public function clearCache()
    {
        Artisan::call('cache:clear');
        $success = new MessageBag([
            'title' => 'Кэш очищен',
        ]);
        return back()->with(compact('success'));
    }

    public function makeDbDump()
    {
        Artisan::call('db:dump');
        $success = new MessageBag([
            'title' => 'Дамп базы данных сделан',
        ]);
        return back()->with(compact('success'));
    }

    public function rollbackDb(Request $request)
    {
        $fileName = $request->input('file');
        if (!empty($fileName)) {
            Artisan::call('db:rollback', ['filename' => $fileName]);
            $success = new MessageBag([
                'title' => 'База данных была восстановленна',
            ]);
            return back()->with(compact('success'));
        }
    }

    public function deleteDump(Request $request)
    {
        $fileName = $request->input('file');
        if (!empty($fileName)) {
            Artisan::call('db:delete-dump', ['filename' => $fileName]);
            $success = new MessageBag([
                'title' => 'Резервная копия удалена',
            ]);
            return back()->with(compact('success'));
        }
    }

    public function generateSitemap()
    {
        Artisan::call('generate-sitemap');
        $success = new MessageBag([
            'title' => 'Siteamp.xml сгенерирован',
        ]);
        return back()->with(compact('success'));
    }

    public function orderExport()
    {
        Artisan::call('export:order');
        $success = new MessageBag([
            'title' => 'Заказы за последние сутки были выгружены',
        ]);
        return back()->with(compact('success'));
    }

    private function getCommandTab()
    {
        // table 1
        $headers = ['Название команды', 'Обновить'];
        $rows = [
            [
                'Очистить кэш',
                '<a href="' . route('siteSettings.clearCache') . '"><i class="fa fa-cog"></i></a>'
            ],
            [
                'Сгенерировать sitemap.xml',
                '<a href="' . route('siteSettings.generateSitemap') . '"><i class="fa fa-cog"></i></a>'
            ],
            [
                'Выгрузка заказов',
                '<a href="' . route('siteSettings.orderExport') . '"><i class="fa fa-cog"></i></a>'
            ],
            [
                'Сделать резервное копирование базы данных',
                '<a href="' . route('siteSettings.db.dump') . '"><i class="fa fa-cog"></i></a>'
            ],
            [
                'Удалить все резервные копии базы данных',
                '<a href="'
                . route('siteSettings.db.delete', ['file' => 'all'])
                . '"><i class="fa fa-cog"></i></a>'
            ],
        ];

        $table = new Table($headers, $rows);
        $box = new Box('Команды', $table);
        $box->style('info');

        return $box;
    }

    private function getDumpsTab()
    {
        $dumpFiles = \Storage::disk('local')->allFiles('db_dumps');
        // table backups
        $headers = ['Название файла', 'Откатить'];
        $rows = [];

        foreach ($dumpFiles as $dumpFile) {
            $fileName = pathinfo($dumpFile)['basename'];
            $toolPanel = '<a href="' . route('siteSettings.db.rollback', ['file' => $fileName])
                . '"><i class="fa fa-cog"></i></a>'
                . '<a href="' . route('siteSettings.db.delete', ['file' => $fileName])
                . '"><i class="fa fa-trash-o"></i></a>';
            $rows[] = [$fileName, $toolPanel];
        }

        $table = new Table($headers, $rows);
        $box = new Box('Резервные копии БД', $table);
        $box->style('info');
        return $box;
    }

    private function getForm()
    {
        $siteSettings = $this->settings->getByCode('site_settings');
        $form = new Form();
        $form->action('site-settings');

        $adminEmail = isset($siteSettings['admin_email']) ? $siteSettings['admin_email'] : '';
        $form->email('admin_email', 'E-mail магазина')->default($adminEmail);

        $statuses = OrderStatus::all()->pluck('localDescription.name', 'id');

        $form->select('status_buy_one_click', 'Статус покупки в один клик')
            ->options($statuses)
            ->default(isset($siteSettings['status_buy_one_click']) ? $siteSettings['status_buy_one_click'] : 0);

        $form->select('status_sent_to_vendor', 'Статус отправлен поставщику')
            ->options($statuses)
            ->default(
                isset($siteSettings['status_sent_to_vendor'])
                    ? $siteSettings['status_sent_to_vendor']
                    : 0
            )->help('Используется в модуле собрать накладную');

        $form->text('link_youtube', 'Ссылка на Youtube')
            ->default(isset($siteSettings['link_youtube']) ? $siteSettings['link_youtube'] : '');
        $form->text('link_facebook', 'Ссылка на Facebook')
            ->default(isset($siteSettings['link_facebook']) ? $siteSettings['link_facebook'] : '');
        $form->text('link_twitter', 'Ссылка на Twitter')
            ->default(isset($siteSettings['link_twitter']) ? $siteSettings['link_twitter'] : '');
        $form->text('link_instagram', 'Ссылка на Instagram')
            ->default(isset($siteSettings['link_instagram']) ? $siteSettings['link_instagram'] : '');

        $telephones = $this->settings->getByKey('telephones');
        for ($i = 0; $i < 4; $i++) {
            $telephone = !empty($telephones[$i]) ? $telephones[$i] : '';
            $form->text('telephones[' . $i . ']', 'Номер телефона ' . ($i + 1))->default($telephone);
        }

        $location = $this->settings->getByKey('location');
        $form->text('location', 'Координаты')
            ->default($location)
            ->help('Координаты для карты на странице контакты');

        $form->elfinder('logo', 'Логотип сайта')
            ->default(isset($siteSettings['logo']) ? $siteSettings['logo'] : null);
        $form->elfinder('watermark', 'Водяной знак')
            ->default(isset($siteSettings['watermark']) ? $siteSettings['watermark'] : null);
        $form->elfinder('stamp', 'Печать для чека')
            ->default(isset($siteSettings['stamp']) ? $siteSettings['stamp'] : null);
        $form->elfinder('guarantee', 'Файл гарантии')
            ->default(isset($siteSettings['guarantee']) ? $siteSettings['guarantee'] : null);



        $workTimes = $this->settings->getByKey('work_time');
        foreach ($this->languages as $language) {
            $localWorkTime = isset($workTimes) && isset($workTimes[$language->id]) ? $workTimes[$language->id] : '';
            $form->textarea('work_time[' . $language->id . ']', 'Режим работы ' . $language->slug)
                ->default($localWorkTime);
        }

        return $form;
    }
}
