<?php

namespace App\Admin\Controllers;

use App\Entities\Attribute;
use App\Entities\AttributesValues;
use App\Entities\Filter;
use App\Entities\FilterValue;
use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class FilterValueController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Значения фильтров')
            ->description('Список всех значений фильтров')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр значения фильтра')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать значение фильтра')
            ->description('Редактироватть значение фильтра')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать значение фильтра')
            ->description('Создать значение фильтра')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    public function grid()
    {
        $grid = new Grid(new FilterValue);

        $grid->id('Id')->sortable();
        $grid->column('filter_id', 'Фильтр')->display(function ($attributeId) {
            if (isset($attributeId)) {
                $attribute = Filter::find($attributeId);
                if (isset($attribute) && isset($attribute->localDescription)) {
                    return $attribute->localDescription->name;
                }
            }
            return '---';
        })->sortable();
        $grid->column('name', 'Значение')->display(function () {
            if (isset($this->localDescription) && isset($this->localDescription->name)) {
                return $this->localDescription->name;
            }
            return '---';
        })->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $attributes = Attribute::with('localDescription')->get();
            $filter->equal('attribute_id', 'Фильтр')
                ->select($attributes->pluck('localDescription.name', 'id'));

            $filter->where(function ($query) {
                $query->whereHas('localDescription', function ($query) {
                    $query->where('value', 'like', '%' . $this->input . '%');
                });
            }, 'Название');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(FilterValue::findOrFail($id));

        $show->id('Id');
        $show->filter_id('Фильтр')->as(function ($filterId) {
            if (isset($filterId)) {
                $filter = Filter::find($filterId);
                if (isset($filter) && isset($filter->localDescription)) {
                    return $filter->localDescription->name;
                }
            }
            return '---';
        });
        $show->name('Значение')->as(function () {
            if (isset($this->localDescription) && isset($this->localDescription->name)) {
                return $this->localDescription->name;
            }
            return '---';
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new FilterValue);

        $form->tab('Основное', function ($form) {
            $form->text('slug', 'SEO-урл')->help('Используется при постороении переменной фильтра');
            $form->select('filter_id', 'Фильтр')
                ->options(Filter::all()->pluck('localDescription.name', 'id'))
                ->value(request('filter_id'));
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описание', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')
                    ->options(Language::all()->pluck('title', 'id'));
                $form->text('name', 'Значение');
                $form->text('filter_name', 'Название для фильтра')
                    ->help('Название для автогенерации методанных');
            });
        });

        $form->saved(function (Form $form) {
            $filterValue = $form->model();
            if (isset($filterValue->filter_id)) {
                $filter = Filter::find($filterValue->filter_id);
                if (isset($filter)) {
                    return redirect()->route('filters.edit', $filterValue->filter_id);
                }
            }
        });

        return $form;
    }
}
