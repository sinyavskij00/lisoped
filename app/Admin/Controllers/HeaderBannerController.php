<?php

namespace App\Admin\Controllers;

use App\Entities\HeaderBanner;
use App\Entities\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class HeaderBannerController extends Controller
{
    use HasResourceActions;

    protected $languages;

    protected $states;

    public function __construct()
    {
        $this->languages = Language::all();
        $this->states = [
            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
            'on' => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
        ];
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Сквозные баннеры')
            ->description('Все сквозные баннеры')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр сквозного баннера')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('Редактирование сквозного баннера')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание сквозного баннера')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new HeaderBanner);

        $grid->model()->with([
            'localDescription'
        ]);

        $grid->id('Id');
        $grid->column('background_color', 'Цвет заднего фона')->display(function ($color) {
            if (isset($color)) {
                return '<div style="background-color: ' . $color . '; width: 100px; height: 20px;"></div>';
            }
        });
        $grid->column('text_color', 'Цвет текста')->display(function ($textColor) {
            if (isset($textColor)) {
                return '<div style="background-color: ' . $textColor . '; width: 100px; height: 20px;"></div>';
            }
        });
        $grid->column('localDescription.text', 'Текст баннера');
        $grid->column('status', 'Статус')->switch($this->states);
        $grid->column('created_at', 'Создан');
        $grid->column('updated_at', 'Обновлен');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(HeaderBanner::findOrFail($id));

        $show->id('Id');
        $show->background_color('Цвет баннера');
        $show->text_color('Цвет текста');
        $show->status('Статус')->as(function ($status) {
            return isset($status) && $status === 1 ? 'Активный' : 'Неактивный';
        });
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new HeaderBanner);

        $form->tab('Основное', function (Form $form) {
            $form->color('background_color', 'Цвет заднего фона')->rules('required');
            $form->color('text_color', 'Цвет текста')->rules('required');
            $form->switch('status', 'Статус')->states($this->states);
        })->tab('Описания', function (Form $form) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')
                    ->options($this->languages->pluck('title', 'id'))
                    ->rules('required');
                $form->text('text', 'Текст баннера')->rules('required');
            });
        });

        return $form;
    }
}
