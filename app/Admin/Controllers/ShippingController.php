<?php

namespace App\Admin\Controllers;

use App\Entities\Shipping;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ShippingController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Методы доставки')
            ->description('Методы доставки')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать метод доставки')
            ->description('Редактировать метод доставки')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать метод доставки')
            ->description('Создать метод доставки')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Shipping);

        $grid->id('Id')->sortable();
        $grid->code('Код')->sortable();
        $grid->price('Цена')->sortable();
        $grid->column('status', 'Статус')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Активный' : 'Неактивный';
        })->sortable();
        $grid->created_at('Создан');
        $grid->updated_at('Обновлен');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Shipping::findOrFail($id));

        $show->id('Id');
        $show->code('Код');
        $show->price('Цена');
        $show->status('Статус')->as(function ($default) {
            return isset($default) && $default === 1 ? 'Активный' : 'Неактивный';
        });
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Shipping);

        $form->text('code', 'Код');
        $form->decimal('price', 'Цена')->default(0.00);
        $form->switch('status', 'Статус')->states([
            'on'  => ['value' => 1, 'text' => 'Активный', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Неактивный', 'color' => 'danger'],
        ]);

        return $form;
    }
}
