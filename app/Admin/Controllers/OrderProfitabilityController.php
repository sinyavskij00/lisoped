<?php

namespace App\Admin\Controllers;

use App\Entities\Order;
use App\Entities\OrderProfitability;
use App\Entities\OrderProfitabilityType;
use App\Entities\OrderStatus;
use App\Http\Controllers\Controller;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class OrderProfitabilityController extends Controller
{
    use HasResourceActions;

    protected $orderStatuses;

    protected $profitabilityTypes;

    public function __construct()
    {
        $this->orderStatuses = OrderStatus::with('localDescription')->get();
        $this->profitabilityTypes = OrderProfitabilityType::all();
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Статьи рендабельности заказов')
            ->description('Все статьи рендабельности заказов')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр статьи рендабельности заказа')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование статьи рендабельности заказов')
            ->description('Просмотр статьи рендабельности заказов')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание статьи рендабельности заказов')
            ->description('Создание статьи рендабельности заказов')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    public function grid()
    {
        $grid = new Grid(new OrderProfitability);

        $grid->column('order_id', 'Номер заказа')->display(function ($orderId) {
            if (isset($orderId)) {
                $content = $orderId . ' ';
                $content .= '<a href="' . route('orders.show', $orderId) . '"><i class="fa fa-eye"></i></a> ';
                $content .= '<a href="' . route('orders.edit', $orderId) . '"><i class="fa fa-edit"></i></a>';
                return $content;
            }
        })->sortable();

        $orderStatuses = $this->orderStatuses;
        $grid->column('order.order_status_id', 'Статус заказа')
            ->display(function ($orderStatusId) use ($orderStatuses) {
                if (isset($orderStatusId)) {
                    $orderStatus = $orderStatuses->firstWhere('id', '=', $orderStatusId);
                    if (isset($orderStatus) && isset($orderStatus->localDescription)) {
                        return $orderStatus->localDescription->name;
                    }
                }
                return '-';
            })->sortable();

        $grid->column('type.name', 'Тип')->sortable();
        $grid->column('price', 'Цена')->sortable();


        $grid->filter(function (Grid\Filter $filter) use ($orderStatuses) {
            $filter->disableIdFilter();
            $filter->equal('order_id', 'Номер заказа')
                ->select(Order::all()->pluck('id', 'id'));
            $filter->equal('order.order_status_id', 'Статус заказа')
                ->select($orderStatuses->pluck('localDescription.name', 'id')->put(0, '---'));
            $filter->equal('type_id', 'Тип')
                ->select($this->profitabilityTypes->pluck('name', 'id'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(OrderProfitability::findOrFail($id));

        $show->order_id('Номер заказа');
        $show->type_id('Вид расхода');
        $show->price('Цена');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new OrderProfitability);

        $form->select('order_id', 'Заказ')->options(Order::all()->pluck('id', 'id'))->value(request('order_id'));
        $form->select('type_id', 'Вид расхода')->options($this->profitabilityTypes->pluck('name', 'id'));
        $form->decimal('price', 'Цена')->default(0);

        $form->saved(function ($form) {
            $orderProfitability = $form->model();
            $orderId = $orderProfitability->order_id;
            return redirect()->route('orders.show', $orderId);
        });

        return $form;
    }
}
