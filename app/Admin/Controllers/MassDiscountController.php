<?php

namespace App\Admin\Controllers;

use App\Entities\Category;
use App\Entities\CustomerGroup;
use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Entities\SkuDiscount;
use App\Entities\StockKeepingUnit;
use App\Http\Controllers\Controller;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;


class MassDiscountController extends Controller
{
    protected $massDiscountTable = 'mass_discounts';

    public function index(Content $content)
    {
        return $content
            ->header('Модуль массового управления скидками')
            ->description('Модуль массового управления скидками')
            ->row(function (Row $row) {
                $tab = new Tab();

                $form = $this->getForm();
                $form->action('mass-discount');

                $form2 = $this->getForm();
                $form2->action('mass-discount-delete');

                $mainTabContent = $form->render()
                    . '<h1 class="text-center">Удалить скидки</h1>'
                    . $form2->render();

                $tab->add('Управление скидками', $mainTabContent);

                $discounts = DB::table($this->massDiscountTable)->get();

                $createdDiscounts = '';
                if ($discounts->count() > 0) {
                    foreach ($discounts as $discount) {
                        $form = $this->getDeleteForm($discount);
                        $form->action('mass-discount-delete');
                        $createdDiscounts .= $form->render();
                    }
                }

                $tab->add('Ранее созданные скидки', $createdDiscounts);

                $row->column(12, $tab);
            });
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'customer_group_id' => 'required',
            'quantity' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'percent' => 'required',
        ]);

        $percent = intval($request->input('percent'));

        if ($percent > 0) {
            $categoriesIds = $request->input('categories', []);
            $manufacturersIds = $request->input('manufacturers', []);
            $productsIds = Product::whereIn('category_id', $categoriesIds)
                ->orWhereIn('manufacturer_id', $manufacturersIds)->get(['id'])->pluck(['id']);
            $skus = StockKeepingUnit::whereIn('product_id', $productsIds);

            $priority = intval($request->input('priority', 0));
            $dateStart = $request->input('date_start');
            $dateEnd = $request->input('date_end');
            $customerGroupId = $request->input('customer_group_id');
            $quantity = $request->get('quantity');

            DB::table($this->massDiscountTable)->insert([
                'manufacturers' => json_encode($manufacturersIds),
                'categories' => json_encode($categoriesIds),
                'customer_group_id' => $customerGroupId,
                'priority' => $priority,
                'percent' => $percent,
                'quantity' => $quantity,
                'date_start' => $dateStart,
                'date_end' => $dateEnd
            ]);

            $skus->chunk(1000, function ($skus) use ($percent, $priority, $dateStart, $dateEnd, $customerGroupId, $quantity) {
                SkuDiscount::whereIn('unit_id', $skus->pluck('id')->toArray())
                    ->where('customer_group_id', $customerGroupId)->delete();
                $data = [];
                foreach ($skus as $sku) {
                    $data[] = [
                        'customer_group_id' => $customerGroupId,
                        'unit_id' => $sku->id,
                        'priority' => $priority,
                        'quantity' => $quantity,
                        'price' => intval($sku->price * (1 - ($percent / 100))),
                        'date_start' => $dateStart,
                        'date_end' => $dateEnd
                    ];
                }
                SkuDiscount::insert($data);
            });
        }
        $success = new MessageBag([
            'title' => 'Скидки сделаны',
//            'message' => 'Кэш очищен',
        ]);
        return back()->with(compact('success'));
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'customer_group_id' => 'required'
        ]);
        $categoriesIds = $request->input('categories', []);
        $manufacturersIds = $request->input('manufacturers', []);
        $productsIds = Product::whereIn('category_id', $categoriesIds)
            ->orWhereIn('manufacturer_id', $manufacturersIds)->get(['id'])->pluck(['id']);
        $skus = StockKeepingUnit::whereIn('product_id', $productsIds)->get()->pluck('id')->toArray();
        $customerGroupId = $request->input('customer_group_id');

        $massDiscountId = $request->input('mass_discount_id');

        if (isset($massDiscountId)) {
            DB::table($this->massDiscountTable)->where('id', '=', $massDiscountId)->delete();
        }

        SkuDiscount::where('customer_group_id', '=', $customerGroupId)
            ->whereIn('unit_id', $skus)
            ->delete();

        $success = new MessageBag([
            'title' => 'Скидки были удалены',
            //'message' => 'Siteamp.xml сгенерирован',
        ]);
        return back()->with(compact('success'));
    }

    private function getForm()
    {
        $form = new Form();

        $manufacturers = Manufacturer::with('localDescription')->get();
        $form->listbox('manufacturers', 'Производители')
            ->options($manufacturers->pluck('localDescription.name', 'id'));

        $categories = Category::with('localDescription')->get();
        $form->listbox('categories', 'Категории')
            ->options($categories->pluck('localDescription.name', 'id'));

        $form->select('customer_group_id', 'группа покупателей')
            ->options(CustomerGroup::all()->pluck('localDescription.name', 'id'));
        $form->number('priority', 'Приоритет')->default(0);
        $form->number('percent', 'Процент акции')->min(0)->max(100);
        $form->number('quantity', 'Количество')->min(1);
        $form->datetime('date_start', 'Дата начала');
        $form->datetime('date_end', 'Дата конца');

        return $form;
    }

    private function getDeleteForm($discount)
    {
        $form = new Form();
        $form->disableReset();
        $form->disablePjax();
        $form->hidden('mass_discount_id')->default($discount->id);

        $manufacturerIds = json_decode($discount->manufacturers);

        $manufacturers = Manufacturer::with('localDescription')->get();
        $form->listbox('manufacturers', 'Производители')
            ->options($manufacturers->pluck('localDescription.name', 'id'))
            ->value($manufacturerIds);

        $categoryIds = json_decode($discount->categories);
        $categories = Category::with('localDescription')->get();
        $form->listbox('categories', 'Категории')
            ->options($categories->pluck('localDescription.name', 'id'))
            ->value($categoryIds);

        $form->select('customer_group_id', 'Группа покупателей')
            ->options(CustomerGroup::all()->pluck('localDescription.name', 'id'))
            ->default($discount->customer_group_id);

        $form->display('percent', 'Процент')->default($discount->percent);
        $form->display('priority', 'Приоритет')->default($discount->priority);
        $form->display('quantity', 'Количество')->default($discount->quantity);

        $form->display('date_start', 'Дата начала')
            ->default(isset($discount->date_start) ? $discount->date_start : '');
        $form->display('date_end', 'Дата конца')
            ->default(isset($discount->date_end) ? $discount->date_end : '');
        return $form;
    }
}
