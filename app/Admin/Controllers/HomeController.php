<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->header('Админ панель')
            ->description('Lisoped админ панель')
            ->row(function (Row $row) {
                $orderGrid = app()->make(OrderController::class)->callAction('grid', []);
                $orderGrid->resource('/admin/orders');
                $orderGrid->paginate(10);
                $row->column(12, '<h1 class="text-center">Заказы</h1>');
                $row->column(12, $orderGrid);
            })
            ->row(function (Row $row) {
                $productGrid = app()->make(ProductController::class)->callAction('grid', []);
                $productGrid->resource('/admin/products');
                $productGrid->paginate(10);
                $row->column(12, '<h1 class="text-center">Товары</h1>');
                $row->column(12, $productGrid);
            });
    }
}
