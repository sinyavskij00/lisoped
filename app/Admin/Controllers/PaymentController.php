<?php

namespace App\Admin\Controllers;

use App\Entities\OrderStatus;
use App\Entities\Payment;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class PaymentController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Методы оплаты')
            ->description('Методы оплаты')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактировать метод оплаты')
            ->description('Редактировать метод оплаты')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание метода оплаты')
            ->description('Создание метода оплаты')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Payment);

        $grid->column('code', 'Код')->sortable();
        $grid->column('confirm_status_id', 'Статус заказа при покупке')->display(function ($confirmStatusId) {
            if (isset($confirmStatusId)) {
                $orderStatus = OrderStatus::find($confirmStatusId);
                if (isset($orderStatus) && isset($orderStatus->localDescription)) {
                    return $orderStatus->localDescription->name;
                }
            }
            return '---';
        })->sortable();
        $grid->column('status', 'Статус')->display(function ($default) {
            return isset($default) && $default === 1 ? 'Включен' : 'Выключен';
        })->sortable();
        $grid->created_at('Создан');
        $grid->updated_at('Обновлен');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Payment::findOrFail($id));

        $show->id('Id');
        $show->code('Код');
        $show->confirm_status_id('Статус заказа при покупке')->as(function ($confirmStatusId) {
            if (isset($confirmStatusId)) {
                $orderStatus = OrderStatus::find($confirmStatusId);
                if (isset($orderStatus) && isset($orderStatus->localDescription)) {
                    return $orderStatus->localDescription->name;
                }
            }
            return '---';
        })->sortable();
        $show->field('status', 'Статус')->as(function ($default) {
            return isset($default) && $default === 1 ? 'Включен' : 'Выключен';
        });
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Payment);

        $form->text('code', 'Код')->rules('required');
        $form->select('confirm_status_id', 'Статус заказа')
            ->options(
                OrderStatus::with('localDescription')->get()->pluck('localDescription.name', 'id')
            )
            ->rules('required');
        $form->switch('status', 'Статус')->states([
            'on'  => ['value' => 1, 'text' => 'Включен', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Выключен', 'color' => 'danger'],
        ])->default(0);

        return $form;
    }
}
