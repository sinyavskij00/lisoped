<?php

namespace App\Admin\Controllers;

use App\Entities\Color;
use App\Entities\CustomerGroup;
use App\Entities\Preorder;
use App\Entities\Product;
use App\Entities\Size;
use App\Entities\StockKeepingUnit;
use App\Entities\Vendor;
use App\Entities\Year;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Mail;

class StockKeepingUnitController extends Controller
{
    use HasResourceActions;

    protected $vendors;

    protected $sizes;

    protected $years;

    protected $colors;

    protected $products;

    protected $customerGroups;

    public function __construct()
    {
        $this->vendors = Vendor::all();
        $this->colors = Color::with(['localDescription'])->get();
        $this->products = Product::with(['localDescription'])->get();
        $this->customerGroups = CustomerGroup::with(['localDescription'])->get();
        $this->sizes = Size::with(['localDescription'])->get();
        $this->years = Year::all();
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Артикулы')
            ->description('Все артикулы')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр артикула')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование артикула')
            ->description('Редактирование артикула')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создать артикул')
            ->description('Создать артикул')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    public function grid()
    {
        $grid = new Grid(new StockKeepingUnit);

        $grid->id('Id')->sortable();
        $products = $this->products;
        $grid->column('product_id', 'Товар')->display(function ($productId) use ($products) {
            $product = $products->firstWhere('id', '=', $productId);
            if (isset($product) && isset($product->localDescription)) {
                return $product->localDescription->name;
            }
        })->sortable();
        $grid->price('Цена')->sortable();
        $grid->sku('Артикул на сайте')->sortable();

        $vendors = $this->vendors;
        $grid->column('vendor_id', 'Поставщик')->display(function ($vendorId) use ($vendors) {
            if (isset($vendorId)) {
                $vendor = $vendors->firstWhere('id', '=', $vendorId);
                if (isset($vendor) && isset($vendor->name)) {
                    return $vendor->name;
                }
            }
            return '-';
        })->sortable();
        $grid->column('vendor_sku', 'Артикул производителя')->sortable();

        $states = [
            'on' => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
        ];

        $grid->column('is_main', 'Основной')->sortable()->switch($states);
        $grid->column('size.value', 'Размер')->sortable();
        $grid->column('color.image', 'Цвет')->image()->sortable();
        $grid->column('year.value', 'Год')->sortable();
        $grid->quantity('Количество')->sortable();
        $grid->model('Модель');

        $grid->filter(function (Grid\Filter $filter) use ($vendors) {
            $filter->disableIdFilter();
            $filter->equal('vendor_id', 'Поставщик')
                ->select($vendors->pluck('name', 'id'));
            $filter->like('sku', 'Артикул (на сайте)');
            $filter->like('vendor_sku', 'Артикул (производителя)');
            $filter->equal('product_id', 'Товар')
                ->select($this->products->pluck('localDescription.name', 'id'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(StockKeepingUnit::findOrFail($id));

        $show->id('Id');

        $products = $this->products;
        $show->product_id('Товар')->as(function ($productId) use ($products) {
            $product = $products->firstWhere('id', '=', $productId);
            return isset($product) ? $product->localDescription->name : '';
        });
        $show->price('Цена');
        $show->sku('Артикул на сайте');
        $show->vendor_sku('Артикул производителя');
        $show->is_main('Основной')->as(function ($default) {
            return isset($default) && $default === 1 ? 'Да' : 'Нет';
        });
        $sizes = $this->sizes;
        $show->size_id('Размер')->as(function ($sizeId) use ($sizes) {
            if (isset($sizeId)) {
                $size = $sizes->firstWhere('id', '=', $sizeId);
                if (isset($size)) {
                    return $size->value;
                }
            }
            return '';
        });
        $colors = $this->colors;
        $show->color_id('Цвет')->as(function ($colorId) use ($colors) {
            if (isset($colorId)) {
                $color = $colors->firstWhere('id', '=', $colorId);
                if (isset($color)) {
                    return $color->localDescription->name;
                }
            }
            return '';
        });
        $years = $this->years;
        $show->year_id('Год')->as(function ($yearId) use ($years) {
            if (isset($yearId)) {
                $year = $years->firstWhere('id', '=', $yearId);
                if (isset($year) && isset($year->value)) {
                    return $year->value;
                }
            }
            return '';
        });
        $show->quantity('Количество');
        $show->model('Модель');
        $show->created_at('Создан');
        $show->updated_at('Обновлен');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        $form = new Form(new StockKeepingUnit);

        $colors = $this->colors;
        $products = $this->products;
        $customerGroups = $this->customerGroups;
        $sizes = $this->sizes;
        $vendors = $this->vendors;
        $years = $this->years;

        $form->tab('Основная информация', function (Form $form) use ($products, $vendors) {
            $form->select('product_id', 'Товар')
                ->options($products->pluck('localDescription.name', 'id'))
                ->value(request('product_id'));
            $form->text('model', 'Модель');
            $form->select('vendor_id', 'Поставщик')->options($vendors->pluck('name', 'id'));
            $form->text('sku', 'Артикул (на сайт выводится)')
                ->default(uniqid())
                ->rules('required');
            $form->text('vendor_sku', 'Артикул производителя');
            $form->number('price', 'Цена')->default(0)->min(0);
            $form->number('quantity', 'Количество')->min(0);
            $form->switch('is_main', 'Основной для товара')->states([
                'on' => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
            ]);
        })->tab('Опции', function (Form $form) use ($colors, $sizes, $years) {
            $sizeOptions = [];
            foreach ($sizes as $size) {
                $sizeOptions[$size->id] = $size->value . (isset($size->localDescription)
                        ? ' ( ' . $size->localDescription->comment . ' )' : '');
            }
            $form->select('size_id', 'Размер')->options($sizeOptions);
            $form->select('color_id', 'Цвет')
                ->options($colors->pluck('localDescription.name', 'id'));
            $form->select('year_id', 'Год')->options($years->pluck('value', 'id'));
        })->tab('Изображения', function ($form) {
            $form->hasMany('images', 'Изображения', function (Form\NestedForm $form) {
                $form->elfinder('image', 'Изображение')->addElementClass('btn-clear__hidden');
            });
        })->tab('Акции', function ($form) use ($customerGroups) {
            $form->hasMany('specials', 'Акции', function (Form\NestedForm $form) use ($customerGroups) {
                $form->select('customer_group_id', 'группа покупателей')
                    ->options($customerGroups->pluck('localDescription.name', 'id'));
                $form->number('priority', 'приоритет')->default(0);
                $form->number('price', 'Акционная цена')->min(0);
                $form->datetime('date_start', 'Дата начала');
                $form->datetime('date_end', 'Дата конца');
            });
        })->tab('Скидки', function ($form) use ($customerGroups) {
            $form->hasMany('discounts', 'Скидки', function (Form\NestedForm $form) use ($customerGroups) {
                $form->select('customer_group_id', 'группа покупателей')
                    ->options($customerGroups->pluck('localDescription.name', 'id'));
                $form->number('priority', 'Приоритет')->default(0);
                $form->number('quantity', 'Количество')->default(0);
                $form->number('price', 'Акционная цена')->min(0);
                $form->datetime('date_start', 'Дата начала');
                $form->datetime('date_end', 'Дата конца');
            });
        });

        $form->saved(function (Form $form) {
            $sku = $form->model();
            $productId = $sku->product_id;

            if (isset($productId)) {
                $product = Product::find($productId);

                if (isset($sku->is_main) && $sku->is_main === 1) {
                    StockKeepingUnit::where('product_id', '=', $productId)
                        ->where('id', '!=', $sku->id)
                        ->update(['is_main' => 0]);
                }

                if (isset($product)) {
                    Preorder::checkProductAvailability();
                    return redirect()->route('products.edit', $productId);
                }
            }

        });

        return $form;
    }
}
