<?php

namespace App\Admin\Controllers;

use App\Entities\Currency;
use App\Entities\CustomerGroup;
use App\Entities\Language;
use App\Entities\Order;
use App\Entities\OrderProfitability;
use App\Entities\OrderStatus;
use App\Entities\StockKeepingUnit;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Services\CatalogService;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Tab;
use Encore\Admin\Widgets\Table;
use Encore\Admin\Admin;

class OrderController extends Controller
{
    use HasResourceActions;

    protected $orderStatuses;

    public function __construct()
    {
        $this->orderStatuses = OrderStatus::with('localDescription')->get();
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Заказы')
            ->description('Все заказы')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Подробности заказа')
            ->description('')
            ->row(function (Row $row) use ($id) {
                $tab = new Tab();
                $mainContent = $this->detail($id)->render();
                $tab->add('Основное', $mainContent);

                $orderSkuController = app(OrderSkuController::class);
                $orderSkusGrid = $orderSkuController->grid()
                    ->setName('order_skus')
                    ->setTitle('Товары заказа')
                    ->setRelation(Order::find($id)->skus())
                    ->resource('/admin/order-skus');

                $profitabilityController = app(OrderProfitabilityController::class);
                $profitabilityGrid = $profitabilityController->grid()
                    ->setName('profitabilities')
                    ->setTitle('Оплаты')
                    ->setRelation(Order::find($id)->profitabilities())
                    ->resource('/admin/order-profitabilities');

                $tab->add('Товары заказа', $orderSkusGrid->render() . $profitabilityGrid->render());

                $orderHistoryController = app(OrderHistoryController::class);
                $orderHistoryGrid = $orderHistoryController->grid()
                    ->setName('order_history')
                    ->setTitle('История заказа')
                    ->setRelation(Order::find($id)->histories())
                    ->resource('/admin/order-histories');

                $tab->add('История', $orderHistoryGrid->render());

                $row->column(12, $tab);
            });
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование заказа')
            ->description('Редактирование заказа')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание заказа')
            ->description('Создание заказа')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Order);

        $grid->model()->with([
            'histories',
            'skus.sku.vendor'
        ]);

        $grid->model()->orderBy('id', 'desc');

        $grid->id('Id')->setAttributes(['width' => ' 60px'])->sortable();

        $grid->column('additional', 'Товары')->expand(function () {
            $orderSkus = $this->skus;
            $headers = ['Товар', 'Поставщик', 'Артикул поставщика', 'Артикул', 'Количество', 'Цена'];
            $rows = [];

            foreach ($orderSkus as $orderSku) {
                $sku = $orderSku->getRelation('sku');
                $vendorName = '';
                $vendorSku = '';
                if (isset($sku) && isset($sku->vendor) && isset($sku->vendor->name)) {
                    $vendorName = $sku->vendor->name;
                    $vendorSku = $sku->vendor_sku;
                }

                $rows[] = [
                    $orderSku->name,
                    $vendorName,
                    $vendorSku,
                    $orderSku->sku,
                    $orderSku->quantity,
                    $orderSku->price
                ];
            }
            $orderSkusTable = new Table($headers, $rows);
            return new Box('Товары', $orderSkusTable);
        }, 'Товары');

        $grid->public_id('Публичный номер')->setAttributes(['width' => ' 50px'])->sortable();
        $grid->first_name('Имя')->sortable();
        $grid->last_name('Фамимлия')->setAttributes(['width' => ' 110px'])->sortable();
        $grid->email('Email')->setAttributes(['width' => ' 150px'])->sortable();
        $grid->telephone('Телефон')->setAttributes(['width' => ' 170px'])->sortable();

        $orderStatuses = $this->orderStatuses;
        $grid->column('order_status_id', 'Статус заказа')->display(function ($orderStatusId) use ($orderStatuses) {
            if (isset($orderStatusId)) {
                $orderStatusId = $orderStatusId == 1 ? 21 : $orderStatusId;
                $orderStatus = $orderStatuses->firstWhere('id', '=', $orderStatusId);
                if (isset($orderStatus) && isset($orderStatus->localDescription)) {
                    return $orderStatus->localDescription->name;
                }
            }
            return '-';
        })->setAttributes(['width' => ' 130px'])->sortable();
        $grid->column('order_status_color', 'Цвет заказа')->display(function () use ($orderStatuses) {
            if (isset($this->order_status_id)) {
                $this->order_status_id = $this->order_status_id == 1 ? 21 : $this->order_status_id;
                $orderStatus = $orderStatuses->firstWhere('id', '=', $this->order_status_id);
                if (isset($orderStatus)) {
                    $color = !empty($orderStatus->color) ? $orderStatus->color : '';
                    return '<div style="background-color: ' . $color . '; width: 100px; height: 20px;"></div>';
                }
            }
            return '-';
        })->sortable();

        $grid->column('lastHistory.private_comment', 'Приватный комментарий');

        $grid->column('address', 'Адрес')->display(function ($address) {
            if (isset($address)) {
                return str_limit($address, 70);
            }
            return '';
        })->setAttributes(['width' => ' 200px'])->sortable();
        $grid->total('Сумма')->sortable();
        $grid->created_at('Создан')->setAttributes(['width' => ' 130px'])->sortable();

        $grid->filter(function (Grid\Filter $filter) use ($orderStatuses) {
            $orderStatusOptions = $orderStatuses->pluck('localDescription.name', 'id');
            $orderStatusOptions->put(0, '---');
            $filter->equal('order_status_id', 'Статус заказа')->select($orderStatusOptions);

            $filter->like('public_id', 'Публичный номер закза');

            $filter->like('first_name', 'Имя');

            $filter->like('last_name', 'Фамилия');

            $filter->like('telephone', 'Телефон');

            $filter->like('email', 'E-mail');

            $filter->where(function ($query) {
                $query->where('created_at', '>=', $this->input);
            }, 'Дата начала')->datetime();

            $filter->where(function ($query) {
                $query->where('created_at', '<=', $this->input);
            }, 'Дата конца')->datetime();

            $filter->where(function ($query) {
                $query->where('public_id', 'like', '%' . $this->input);
            }, 'Публичный номер (по последним цифрам)');

            $filter->like('address', 'Адрес');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $order = Order::findOrFail($id);
        $show = new Show($order);

        $show->id('Номер заказа');

        $show->public_id('Публичный номер заказа');

        $show->profitabilities('Баланс')->as(function () use ($order) {
            $orderProfitabilities = OrderProfitability::where('order_id', '=', $order->id)->get();
            $orderProfitabilityTotal = 0;
            foreach ($orderProfitabilities as $orderProfitability) {
                $orderProfitabilityTotal += $orderProfitability->price;
            }
            return $orderProfitabilityTotal - $order->total;
        });
        $show->total('Сумма');
        $show->customer_id('Зарегестрированный покупатель')->as(function ($customerId) {
            if (isset($customerId)) {
                $customer = User::find($customerId);
                if (isset($customer)) {
                    return $customer->first_name . ' ' . $customer->last_name;
                }
            }
            return '-';
        });
        $show->customer_group_id('Группа покупателей')->as(function ($customerGroupId) {
            if (isset($customerGroupId)) {
                $customerGroup = CustomerGroup::find($customerGroupId);
                if (isset($customerGroup) && isset($customerGroup->localDescription)) {
                    return $customerGroup->localDescription->name;
                }
            }
            return '-';
        });

        $show->first_name('Имя');
        $show->last_name('Фамилия');
        $show->email('Email');
        $show->telephone('Телефон');
        $show->address('Адрес');
        $show->payment_method('Способ оплаты');
        $show->shipping_method('Способ доставки');
        $show->comment('Комментарий');
        $show->order_status_id('Статус заказа')->as(function ($orderStatusId) {
            if (isset($orderStatusId)) {
                $orderStatus = OrderStatus::find($orderStatusId);
                if (isset($orderStatus) && isset($orderStatus->localDescription)) {
                    return $orderStatus->localDescription->name;
                }
            }
            return '---';
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Order);
        Admin::script($this->script());

        $shippingMethods = collect(app()->tagged('shipping_methods'));
        $paymentMethods = collect(app()->tagged('payment_methods'));

        $form->setView('admin.order_form');

        $form->tab('Основное', function (Form $form) use ($shippingMethods, $paymentMethods) {
            $form->text('public_id', 'Публичный номер заказа');
            $form->number('total', 'Сумма');
            $customers = [];
            foreach (User::all() as $user) {
                $customers[$user->id] = $user->first_name . ' ' . $user->last_name;
            }

            $form->select('customer_id', 'Покупатель')->options($customers);
            $customerGroups = CustomerGroup::with('localDescription')->get();
            $form->select('customer_group_id', 'Группа покупателей')
                ->options($customerGroups->pluck('localDescription.name', 'id'))
                ->rules('required');

            $form->text('first_name', 'Имя');
            $form->text('last_name', 'Фамимлия');
            $form->email('email', 'Email');
            $form->text('telephone', 'Телефон');
            $form->text('address', 'Адрес');

            $paymentOptions = [];
            foreach ($paymentMethods as $paymentMethod) {
                $paymentOptions[$paymentMethod->getCode()] = $paymentMethod->getName();
            }
            $form->select('payment_code', 'Метод оплаты')
                ->options($paymentOptions)
                ->rules('required');


            $shippingOptions = [];
            foreach ($shippingMethods as $shippingMethod) {
                $shippingOptions[$shippingMethod->getCode()] = $shippingMethod->getName();
            }
            $form->select('shipping_code', 'Метод доставки')
                ->options($shippingOptions)
                ->rules('required');


            $form->textarea('comment', 'Комментарий');

            $orderStatuses = OrderStatus::with(['localDescription'])->get();

            $form->select('order_status_id', 'Статус заказа')
                ->options($orderStatuses->pluck('localDescription.name', 'id'))
                ->rules('required');
            $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));

            $currencies = Currency::all();
            $form->select('currency_id', 'Валюта')->options($currencies->pluck('name', 'id'));
        });

        $form->saving(function ($form) {
            $form->customer_id = isset($form->customer_id) ? $form->customer_id : 0;
        });

        $form->saved(function ($form) use ($shippingMethods, $paymentMethods) {
            $order = $form->model();
            $catalogService = app()->make(CatalogService::class);
            $customerGroup = CustomerGroup::find($order->customer_group_id);
            $orderSubTotal = 0;
            foreach ($order->skus as $orderSku) {
                $skuId = $orderSku->getAttributeValue('sku_id');
                $sku = StockKeepingUnit::where('id', '=', $skuId)
                    ->with([
                        'color.localDescription',
                        'year',
                        'size',
                        'product.localDescription'
                    ])->first();
                $orderSku->name = isset($sku->product) && isset($sku->product->localDescription)
                    ? $sku->product->localDescription->name
                    : '';
                $orderSku->model = $sku->model;
                $orderSku->sku = $sku->sku;
                $orderSku->color = isset($sku->color) && isset($sku->color->localDescription)
                    ? $sku->color->localDescription->name
                    : '';
                $orderSku->size = $sku->size->value;
                $orderSku->year = $sku->year->value;
                $orderSku->vendor_price = $sku->vendor_price;
                $orderSku->vendor_total = $sku->vendor_price * $orderSku->quantity;
                $orderSku->price = $catalogService->getSkuPrice($sku, $orderSku->quantity, $customerGroup);
                $orderSku->total = $catalogService->getSkuPrice($sku, $orderSku->quantity, $customerGroup, true);
                $orderSubTotal += $orderSku->total;
                $orderSku->save();
            }
            $order->sub_total = $orderSubTotal;
            $shippingCode = $order->shipping_code;
            $shippingMethod = $shippingMethods->first(function ($item) use ($shippingCode) {
                return $item->getCode() === $shippingCode;
            });

            $order->shipping_payment = $shippingMethod->getCost();
            $order->shipping_method = $shippingMethod->getName();

            $order->total = $order->sub_total + $order->shipping_payment;

            $paymentCode = $order->payment_code;
            $paymentMethod = $paymentMethods->first(function ($item) use ($paymentCode) {
                return $item->getCode() === $paymentCode;
            });

            $order->payment_method = $paymentMethod->getName();

            $currency = Currency::find($order->currency_id);
            if (isset($currency)) {
                $order->currency_code = $currency->code;
                $order->currency_value = $currency->value;
            }

            $order->save();
        });

        return $form;
    }

    protected function script()
    {
        return <<<SCRIPT
    $(document).ready(function () {
       var inputNull = $('#public_id').val();
    if (inputNull !== "undefined" ) {
        $('#public_id').attr("disabled", true);
    }
    })

SCRIPT;
    }
}