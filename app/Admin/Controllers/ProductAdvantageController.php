<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\ProductAdvantage;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ProductAdvantageController extends Controller
{
    use HasResourceActions;

    protected $languages;

    protected $states;

    public function __construct()
    {
        $this->languages = Language::all();
        $this->states = [
            'off' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
            'on' => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
        ];
    }

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Преимущества товара')
            ->description('Преимущества товара')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр преимущества товара')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование')
            ->description('Редактирование преимущества товара')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание')
            ->description('Создание преимущества товара')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ProductAdvantage);

        $grid->id('Id');

        $grid->column('localDescription.title', 'Заголовок');
        $grid->status('Status');
        $grid->created_at('Создано');
        $grid->updated_at('Обновлено');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ProductAdvantage::findOrFail($id));

        $show->id('Id');
        $show->link('Link');
        $show->icon('Icon');
        $show->status('Status');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ProductAdvantage);

        $form->tab('Основное', function (Form $form) {
            $form->text('link', 'Ссылка')->help('Ссылка для кнопки подробнее');
            $form->text('icon', 'Иконка')->help('Класс иконочного шрифта');
            $form->switch('status', 'Статус')->states($this->states);
        })->tab('Описания', function (Form $form) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')
                    ->options($this->languages->pluck('title', 'id'))
                    ->rules('required');
                $form->text('title', 'Загоовок');
                $form->ckeditor('text', 'Текст');
            });
        });

        return $form;
    }
}
