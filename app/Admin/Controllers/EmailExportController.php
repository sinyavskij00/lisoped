<?php

namespace App\Admin\Controllers;

use App\Entities\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Form;
use Illuminate\Http\Request;

class EmailExportController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->header('Моудль экспорта email покупателей')
            ->description('Моудль экспорта email покупателей')
            ->row(function (Row $row) {
                $form = new Form();

                $form->radio('subscriber', 'Только подписчики')->options([
                    0 => 'Нет',
                    1 => 'Да',
                ]);

                $form->disablePjax();
                $form->action('email-export');

                $row->column(12, $form);
            });
    }

    public function export(Request $request)
    {
        $subscriber = intval($request->input('subscriber', 0));

        $query = User::select('email');

        if ($subscriber === 1) {
            $query->where('subscribe', '=', 1);
        }

        $users = $query->get();

        $emails = [];
        foreach ($users as $user) {
            $emails[] = $user->email;
        }

        $emails = array_values(array_unique($emails));
        $str = implode("\r\n", $emails);

        return response($str)->withHeaders([
            'Content-Type' => 'text/plain',
            'Cache-Control' => 'no-store, no-cache',
            'Content-Disposition' => 'attachment; filename="emails.txt',
        ]);
    }
}
