<?php

namespace App\Admin\Controllers;

use App\Entities\Language;
use App\Entities\Manufacturer;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ManufacturerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Производители')
            ->description('Все производители')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Просмотр')
            ->description('Просмотр производителя')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Редактирование производителя')
            ->description('Редактирование производителя')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание производителя')
            ->description('Создание производителя')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Manufacturer);

        $grid->id('Id')->sortable();
        $grid->column('image', 'Изображение')->image()->sortable();
        $grid->column('localDescription.name', 'Название')->sortable();
        $grid->column('status', 'Статус')->display(function ($status) {
            return isset($status) && $status === 1 ?  'Включен' : 'Отключен';
        })->sortable();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->equal('status', 'Статус')->select([
                0 => 'Отключено',
                1 => 'Включено',
            ]);

            $filter->where(function($query){
                $query->whereHas('localDescription', function ($query) {
                    $query->where('name', 'like', '%' . $this->input . '%');
                });
            }, 'Название');
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Manufacturer::findOrFail($id));

        $show->id('Id');
        $show->image('Изображение')->image();
        $show->slug('SEO-урл');
        $show->status('Статус')->as(function ($status) {
            return isset($status) && $status === 1 ?  'Включен' : 'Отключен';
        });
        $show->created_at('Создано');
        $show->updated_at('Обновлено');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Manufacturer);

        $form->tab('Основное', function ($form) {
            $form->elfinder('image', 'Изображение');
            $form->text('slug', 'SEO-урл');
            $form->switch('status', 'Статус')->states([
                'on'  => ['value' => 1, 'text' => 'Включен', 'color' => 'success'],
                'off' => ['value' => 0, 'text' => 'Выключен', 'color' => 'danger'],
            ])->default(0);
        })->tab('Описание', function ($form) {
            $form->hasMany('descriptions', 'Описания', function (Form\NestedForm $form) {
                $form->select('language_id', 'Язык')->options(Language::all()->pluck('title', 'id'));
                $form->text('name', 'Название');
                $form->ckeditor('description', 'Описание');
                $form->text('meta_h1', 'Meta h1');
                $form->textarea('meta_description', 'Meta description');
                $form->text('meta_keywords', 'Meta keywords');
                $form->text('meta_title', 'Meta title');
            });
        });

        return $form;
    }
}
