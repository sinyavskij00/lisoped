<?php

namespace App\Http\Controllers\Auth;

use App\Entities\WishProduct;
use App\Http\Controllers\Controller;
use App\Library\Checkout\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    protected $cart;

    public function __construct(Cart $cart)
    {
        $this->middleware('guest')->except(['logout', 'checkAuth']);
        $this->cart = $cart;
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');
        $oldSessionId = session()->getId();
        if (Auth::attempt($credentials)) {
            $result['status'] = 1;
            $newSessionId = session()->getId();

            //wishlist from session to db
            $wishlist = session('wishList', []);
            foreach ($wishlist as $productId) {
                if (isset($productId)) {
                    WishProduct::firstOrCreate([
                        'product_id' => $productId,
                        'customer_id' => Auth::id()
                    ]);
                }
            }
            session()->forget('wishList');


            $this->cart->updateItems($oldSessionId, $newSessionId, Auth::id());
        } else {
            $result = [
                'status' => 0,
                'message' => __('auth.login.wrong_credentials')
            ];
        }
        return response()->json($result);
    }

    public function logout()
    {
        $oldSessionId = session()->getId();
        //wishlist from session to db
        $wishlist = [];
        $wishProducts = WishProduct::where('customer_id', '=', Auth::id())->get();
        foreach ($wishProducts as $wishProduct) {
            $wishlist[] = $wishProduct->product_id;
        }
        Auth::logout();
        session()->put('wishList', $wishlist);
        $newSessionId = session()->getId();
        $this->cart->updateItems($oldSessionId, $newSessionId, Auth::id());
        return redirect()->route('home');
    }

    public function checkAuth()
    {
        return ['status' => Auth::check()];
    }
}
