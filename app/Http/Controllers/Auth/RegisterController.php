<?php

namespace App\Http\Controllers\Auth;

use App\Entities\CustomerGroup;
use App\Entities\User;
use App\Entities\WishProduct;
use App\Http\Controllers\Controller;
use App\Library\Checkout\Cart;
use App\Mail\Auth\UserRegistred;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/cabinet';

    protected $cart;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Cart $cart)
    {
        $this->middleware('guest');
        $this->cart = $cart;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->passes()) {
            $customer = $this->create($request->all());
            $oldSessionId = session()->getId();
            Auth::login($customer);
            $newSessionId = session()->getId();
            //wishlist from session to db
            $wishlist = session('wishList', []);
            foreach ($wishlist as $productId) {
                if (isset($productId)) {
                    WishProduct::firstOrCreate([
                        'product_id' => $productId,
                        'customer_id' => Auth::id()
                    ]);
                }
            }
            session()->forget('wishList');
            $this->cart->updateItems($oldSessionId, $newSessionId, Auth::id());

            $result = ['status' => 1];

            $name = $customer->first_name . ' ' . $customer->last_name;

            $action = __('mails.user_registred.actions.registration');
            $title = __('mails.user_registred.title', [
                'action' => $action
            ]);
            $subTitle = __('mails.user_registred.sub_title', [
                'name' => $name,
                'action' => $action
            ]);

            $successSubscribe = new UserRegistred($title, $subTitle, $request->all()['email'], $request->all()['password'], $name);
            $successSubscribe->subject($title);
            Mail::to($customer->email)->send($successSubscribe);
        } else {
            $result = [
                'status' => 0,
                'errors' => $validator->errors()
            ];
        }
        return response()->json($result);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User|\Illuminate\Database\Eloquent\Model
     */
    protected function create(array $data)
    {
        $defaultCustomerGroup = CustomerGroup::where('is_default', '=', 1)->first();
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'city' => isset($data['city']) ? $data['city'] : '',
            'telephone' => isset($data['telephone']) ? $data['telephone'] : '',
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'customer_group_id' => $defaultCustomerGroup->id,
            'subscribe' => 1,
        ]);
    }
}
