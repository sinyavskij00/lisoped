<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->passes()) {
            $response = $this->broker()->sendResetLink($request->only('email'));
            if ($response == Password::RESET_LINK_SENT) {
                return response()->json(['status' => 1, 'message' => __('auth.forgot_password.link_was_send')]);
            }
            return response()->json(['status' => 0, 'errors' => [__('auth.forgot_password.link_wasnt_send')]]);
        }
        return response()->json(['status' => 0, 'errors' => $validator->errors()]);
    }
}
