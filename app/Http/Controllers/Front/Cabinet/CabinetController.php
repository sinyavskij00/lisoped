<?php

namespace App\Http\Controllers\Front\Cabinet;

use App\Entities\Order;
use App\Entities\OrderSku;
use App\Entities\OrderStatus;
use App\Entities\Product;
use App\Entities\StockKeepingUnit;
use App\Library\Checkout\NewPost;
use App\Services\CatalogService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CabinetController extends Controller
{
    public function index(NewPost $newPost)
    {
        $user = Auth::user();

        $city = $newPost->getCityByRef($user->city_ref);
        $store = $newPost->getStoreByRef($user->store_ref);

        $birthday = $user->birthday;
        if (!empty($birthday)) {
            $tmp = explode('-', $birthday);
            $day = intval($tmp[2]);
            $month = intval($tmp[1]);
            $year = intval($tmp[0]);
        }

        return view('front.cabinet.personal_info', [
            'user' => $user,
            'city' => $city,
            'store' => $store,
            'day' => $day ?? null,
            'month' => $month ?? null,
            'year' => $year ?? null
        ]);
    }

    public function update(Request $request)
    {
        $data = array_filter($request->all());
        $user = Auth::user();
        if (!empty($data['day']) && !empty($data['month']) && !empty($data['year'])) {
            $day = str_pad($data['day'], 2, "0", STR_PAD_LEFT);
            $month = str_pad($data['month'], 2, "0", STR_PAD_LEFT);
            $year = $data['year'];
            $birthday = $year . '-' . $month . '-' . $day;
            if ($user->birthday !== $birthday) {
                $data['birthday'] = $birthday;
                $data['birthday_updated_at'] = now();
            }
        }
        Validator::extend('birthday_last_update', function ($field, $value, $parameters) use ($user) {
            if (!isset($user->birthday_updated_at)) {
                return true;
            }
            $diffs = Carbon::createFromFormat('Y-m-d', $user->birthday_updated_at)->diffInYears(Carbon::now());
            return $diffs > 0;
        });
        $validator = Validator::make($data, [
            'birthday' => 'nullable|date|birthday_last_update'
        ]);

        if ($validator->fails()) {
            unset($data['birthday']);
            unset($data['birthday_updated_at']);
        }

        $user->update($data);
        $subscribe = intval($request->input('subscribe', 0));
        $subscribe = $subscribe === 1 || $subscribe === 0 ? $subscribe : 0;
        $user->subscribe = $subscribe;
        $user->save();

        return redirect()->route('cabinet.index')->withErrors($validator);
    }

    public function orderHistory()
    {
        $user = Auth::user();

        return view('front.cabinet.orders_history', [
            'user' => $user
        ]);
    }

    public function password()
    {
        return view('front.cabinet.password');
    }

    public function updatePassword(Request $request)
    {
        $user = Auth::user();
        Validator::extend('old_password', function ($field, $value, $parameters) use ($user) {
            return Hash::check($value, $user->password);
        });
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed',
            'old_password' => 'old_password'
        ]);

        if ($validator->passes()) {
            $user->password = Hash::make($request->get('password'));
            $user->save();
            return response()->json(['status' => 1, 'message' => __('cabinet.password.success')]);
        }
        return response()->json(['status' => 0, 'errors' => $validator->errors()]);
    }

    public function wishlist(CatalogService $catalogService)
    {
        $customer = Auth::user();
        $productIds = $customer->wishProducts()->pluck('product_id');
        $products = Product::whereIn('products.id', $productIds)
            ->where('status', '=', 1)
            ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);
        $products = $catalogService->addProductInfo($products);
        $products->with(CatalogService::PRODUCT_ITEM_RELATIONS);
        $products = $products->get();

        return view('front.cabinet.wishlist', [
            'products' => $products
        ]);
    }

    public function purchasedGoods()
    {
        $finishedStatus = OrderStatus::where('is_finished', '=', 1)->first();
        $customerId = Auth::id();

        $orderIds = [];
        if (isset($finishedStatus)) {
            $orderIds = Order::where('customer_id', '=', $customerId)
                ->where('order_status_id', '=', $finishedStatus->id)
                ->get(['id'])
                ->pluck('id')
                ->toArray();
        }

        $skuIds = OrderSku::whereIn('order_id', $orderIds)->get(['sku_id'])->pluck('sku_id')->toArray();

        $productIds = StockKeepingUnit::whereIn('id', $skuIds)
            ->get(['product_id'])
            ->pluck('product_id')
            ->toArray();

        $currentLanguageId = config('current_language_id');
        $products = Product::leftJoin('product_descriptions', function ($join) use ($currentLanguageId) {
            $join->on('products.id', '=', 'product_descriptions.product_id')
                ->where('language_id', '=', $currentLanguageId);
        })
            ->whereIn('products.id', $productIds)
            ->select([
                'products.id',
                'products.slug',
                'products.category_id',
                'product_descriptions.name',
            ])
            ->get();

        return view('front.cabinet.purchased_goods', [
            'products' => $products
        ]);
    }
}
