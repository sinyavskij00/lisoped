<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\Category;
use App\Entities\Product;
use App\Http\Route\CategoryPath;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(CategoryPath $categoryPath)
    {
        $category = Category::where('id', '=', $categoryPath->id)
            ->where('status', '=', 1)
            ->first();

        if (!isset($category)) {
            abort(404);
        }

        $category->load(['localDescription']);

        $landingPageData = config('app.landing_page_data');
        $metaGeneratorString = config('app.meta_generator_data');
        if (isset($landingPageData)) {
            $metaH1 = isset($landingPageData['meta_h1']) ? $landingPageData['meta_h1'] : '';
            $description = isset($landingPageData['description']) ? $landingPageData['description'] : '';
            $metaTitle = isset($landingPageData['meta_title']) ? $landingPageData['meta_title'] : '';
            $metaDescription = isset($landingPageData['meta_description'])
                ? $landingPageData['meta_description'] : '';
            $metaKeywords = isset($landingPageData['meta_keywords']) ? $landingPageData['meta_keywords'] : '';
        } elseif (!empty($metaGeneratorString)) {
            $categoryName = $category->localDescription->name;
            if (empty($categoryName)) {
                $categoryName = $category->localDescription->meta_h1;
            }
            $metaTitle = $categoryName . $metaGeneratorString;
            $metaDescription = $categoryName . $metaGeneratorString;
            $metaKeywords = $categoryName . $metaGeneratorString;
            $metaH1 = $categoryName . $metaGeneratorString;
            $description = $categoryName . $metaGeneratorString . __('catalog.product_listing.toponyms');
        } else {
            $metaTitle = $category->localDescription->meta_title;
            $metaDescription = $category->localDescription->meta_description;
            $metaKeywords = $category->localDescription->meta_keywords;
            $metaH1 = !empty($category->localDescription->meta_h1)
                ? $category->localDescription->meta_h1
                : $category->localDescription->name;
            $description = $category->localDescription->description;
        }

        if ($category->layout_type === 1) {
            $category->load(['children.localDescription']);

            return view('front.catalog.category_listing', [
                'category' => $category,
                'meta_title' => $metaTitle,
                'meta_description' => $metaDescription,
                'meta_keywords' => $metaKeywords,
                'meta_h1' => $metaH1,
                'description' => $description,
            ]);
        } elseif ($category->layout_type === 0) {
            $productIds = $category->showProducts()->get(['products.id'])->pluck('id')->toArray();
            $languageId = config('current_language_id');

            return $productListingController = app(ProductListingController::class)->callAction(
                'index',
                [
                    'products' => Product::whereIn('products.id', $productIds),
                    'metaData' => [
                        'meta_title' => $metaTitle,
                        'meta_description' => $metaDescription,
                        'meta_keywords' => $metaKeywords,
                        'meta_h1' => $metaH1,
                        'description' => $description,
                    ],
                    'filterCacheKey' => 'filter_for_category_' . $languageId . '_' . $category->id
                ]
            );
        }
    }
}
