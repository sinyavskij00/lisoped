<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\Attribute;
use App\Entities\Category;
use App\Entities\Product;
use App\Http\Controllers\Controller;
use App\Library\Compare\CompareSessionDriver;
use App\Services\CatalogService;
use Illuminate\Http\Request;

class CompareController extends Controller
{
    protected $compare;
    protected $catalogService;

    public function __construct(CompareSessionDriver $compare, CatalogService $catalogService)
    {
        $this->compare = $compare;
        $this->catalogService = $catalogService;
    }

    public function categoryList()
    {
        $productIds = $this->compare->getProducts()->toArray();

        $categories = Category::where('parent_id', '=', null)
            ->with('localDescription')
            ->get();

        foreach ($categories as $rootCategory) {
            $catIds = Category::descendantsAndSelf($rootCategory->id)->pluck('id');
            $productCount = Product::whereIn('category_id', $catIds)
                ->whereIn('id', $productIds)
                ->where('status', '=', 1)
                ->count();
            $rootCategory->product_count = $productCount;
        }

        return view('front.catalog.compare_category', [
            'categories' => $categories
        ]);
    }

    public function productList($id)
    {
        $productIds = $this->compare->getProducts()->toArray();
        $catIds = Category::descendantsAndSelf($id)->pluck('id');
        $products = Product::whereIn('products.id', $productIds)
            ->whereIn('products.category_id', $catIds)
            ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS)->with([
                'productAttributes'
            ]);

        $products = $this->catalogService->addProductInfo($products);
        $products = $products->get();
        $attrIds = collect();
        foreach ($products as $product) {
            $attrIds = $attrIds->merge($product->productAttributes->pluck('attribute_id'));
        }
        $attributes = Attribute::whereIn('id', $attrIds->unique()->toArray())->with(['localDescription'])->get();

        return view('front.catalog.compare_products', [
            'products' => $products,
            'attributes' => $attributes
        ]);
    }

    public function add(Request $request)
    {
        $productId = $request->input('product_id');
        $product = Product::findOrFail($productId);
        $result = $this->compare->add($product);
        if ($result) {
            return response()->json([
                'status' => 1,
                'quantity' => $this->compare->getQuantity(),
                'message' => __('messages.compare.added')
            ]);
        }
        return response()->json(['status' => 0, 'errors' => __('messages.compare.wasnt_added')]);
    }

    public function delete(Request $request)
    {
        $productId = $request->input('product_id');
        $result = $this->compare->delete($productId);
        if ($result) {
            return response()->json([
                'status' => 1,
                'quantity' => $this->compare->getQuantity(),
                'message' => __('messages.compare.deleted')
            ]);
        }
        return response()->json(['status' => 0, 'errors' => __('messages.compare.wasnt_deleted')]);
    }
}
