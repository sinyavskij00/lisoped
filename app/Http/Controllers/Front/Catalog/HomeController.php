<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Library\Modules\Front\AdvantagesModule;
use App\Library\Modules\Front\BannerModule;
use App\Library\Modules\Front\BlogModule;
use App\Library\Modules\Front\CategoryModule;
use App\Library\Modules\Front\HtmlContentModule;
use App\Library\Modules\Front\ManufacturerModule;
use App\Library\Modules\Front\PickupBicycleModule;
use App\Library\Modules\Front\ProductTabs;
use App\Services\CatalogService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Validator;

class HomeController extends Controller
{
    protected $catalogService;

    public function __construct(CatalogService $catalogService)
    {
        $this->catalogService = $catalogService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = [
            BannerModule::class,
            CategoryModule::class,
            PickupBicycleModule::class,
            ProductTabs::class,
            AdvantagesModule::class,
            BlogModule::class,
            ManufacturerModule::class,
            HtmlContentModule::class
        ];

        $content = '';
        foreach ($modules as $module) {
            $languageId = config('current_language_id');
            $customerGroup = config('current_customer_group_id');
            $res = Cache::remember(
                'home_' . $module . '_' . $languageId . '_' . $customerGroup,
                30,
                function () use ($module) {
                    return app($module)->show();
                }
            );
            $content .= $res;
        }

        return view('front.home', [
            'content' => $content,
            'products' => []
        ]);
    }

    public function chooseBicycle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'price_range' => 'required',
            'trace_type' => 'required',
            'weight' => 'required',
            'height' => 'required',
            'gender' => 'required',
        ]);
        if ($validator->passes()) {
            $pickupModule = app()->make(PickupBicycleModule::class);
            $query = $pickupModule->filter($request);
            $product = $query->first();
            if (isset($product)) {
                return response()->json(['status' => 1, 'result' => [
                    'quantity' => $query->count(),
                    'link' => route('product_path', $product->slug)
                ]]);
            } else {
                return response()->json(['status' => 1, 'result' => [
                    'quantity' => 0,
                    'link' => '#',
                    'message' => __('messages.pickup_bicycle.not_found')
                ]]);
            }
        }
        return response()->json([
            'status' => 0,
            'errors' => $validator->errors(),
            'message' => __('messages.pickup_bicycle.not_found')
        ]);
    }
}
