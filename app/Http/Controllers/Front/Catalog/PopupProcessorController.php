<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\Preorder;
use App\Entities\StockKeepingUnit;
use App\Entities\User;
use App\Mail\Auth\UserRegistred;
use App\Mail\Info\HeaderCallback;
use App\Mail\Info\NewSubscriber;
use App\Mail\Info\SuccessSubscribe;
use App\Services\CatalogService;
use App\Services\OrderService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Entities\CustomerGroup;
use App\Library\Settings;

class PopupProcessorController extends Controller
{
    protected $settings;

    protected $adminEmail;

    protected $catalogService;

    public function __construct(CatalogService $catalogService, Settings $settings)
    {
        $this->settings = $settings;
        $this->adminEmail = \config('app.admin_email');
        $this->catalogService = $catalogService;
    }

    public function headerCallback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
        ]);
        if ($validator->passes()) {
            $telephone = $request->get('phone');
            $day = $request->input('day', '');
            $hour = $request->input('hour', '');
            $minute = $request->input('minute', '');
            $name = $request->get('name', '');

            $mail = new HeaderCallback($name, $telephone, $day, $hour, $minute);
            $mail->subject(__('mails.header_callback.subject'));

            Mail::to($this->adminEmail)->send($mail);
            return response()->json(['status' => 1, 'message' => __('messages.header_callback.success')]);
        }
        return response()->json(['status' => 0, 'errors' => $validator->errors()->all()]);
    }

    public function oneClickBuy(Request $request, OrderService $orderService, CatalogService $catalogService)
    {
        $validator = Validator::make($request->all(), [
            'sku_id' => 'required',
            'name' => 'required',
            'phone' => 'required'
        ]);

        if ($validator->passes()) {
            $customer = Auth::user();
            $currency = Config::get('current_currency');
            $customerGroup = isset($customer) && isset($customer->customerGroup)
                ? $customer->customerGroup
                : Config::get('current_customer_group');

            $sku = StockKeepingUnit::find($request->input('sku_id'));
            $quantity = $request->input('quantity', 1);

            $skuPrice = $catalogService->getSkuPrice($sku, $quantity, $customerGroup);

            $orderService->createOrder([
                'invoice' => '',
                'customer_id' => isset($customer) ? $customer->id : 0,
                'customer_group_id' => $customerGroup->id,
                'first_name' => $request->input('name'),
                'last_name' => '',
                'email' => '',
                'telephone' => $request->input('phone'),
                'address' => '',
                'comment' => $request->input('text', ''),
                'payment_method' => '',
                'payment_code' => '',
                'shipping_method' => '',
                'shipping_code' => '',
                'sub_total' => $skuPrice,
                'shipping_payment' => 0,
                'total' => $skuPrice,
                'order_status_id' => $this->settings->getByKey('status_buy_one_click') ?? 0,
                'language_id' => Config::get('current_language_id'),
                'currency_id' => $currency->id,
                'currency_code' => $currency->code,
                'currency_value' => $currency->value,
                'order_skus' => [
                    [
                        'sku_id' => $sku->getAttributeValue('id'),
                        'sku' => $sku->getAttributeValue('sku'),
                        'vendor_sku' => $sku->getAttributeValue('vendor_sku'),
                        'name' => isset($sku->product) && isset($sku->product->localDescription)
                            ? $sku->product->localDescription->getAttributeValue('name') : '',
                        'model' => $sku->getAttributeValue('model'),
                        'color' => isset($sku->color) && isset($sku->color->localDescription)
                            ? $sku->color->localDescription->getAttributeValue('name') : '',
                        'size' => isset($sku->size) ? $sku->size->getAttributeValue('value') : '',
                        'year' => isset($sku->year) ? $sku->year->getAttributeValue('value') : '',
                        'quantity' => $quantity,
                        'price' => $catalogService->getSkuPrice($sku, $quantity, $customerGroup),
                        'vendor_price' => $sku->getAttributeValue('vendor_price'),
                        'total' => $catalogService->getSkuPrice($sku, $quantity, $customerGroup, true),
                        'vendor_total' => $sku->getAttributeValue('vendor_price') * $quantity,
                        'reward' => 0
                    ]
                ]
            ]);

            return response()->json(['status' => 1, 'message' => __('messages.one_click_buy.success')]);
        }

        return response()->json(['status' => 0, $validator->errors()->all()]);
    }

    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email'
        ]);
        if ($validator->passes()) {
            $password = str_random(8);
            $userEmail = $request->input('email');
            $user = User::create([
                'first_name' => 'subscriber',
                'last_name' => 'subscriber',
                'password' => Hash::make($password),
                'email' => $userEmail,
                'subscribe' => 1,
                'customer_group_id' => CustomerGroup::where('is_default', '=', 1)->first()->id
            ]);
            $customerGroupId = Config::get('current_customer_group_id');
            $user->customer_group_id = $customerGroupId;
            $user->save();

            $action = __('mails.user_registred.actions.subcribe');
            $title = __('mails.user_registred.title', [
                'action' => $action
            ]);
            $subTitle = __('mails.user_registred.sub_title', [
                'name' => $user->first_name . ' ' . $user->last_name,
                'action' => $action
            ]);

            $successSubscribe = new UserRegistred($title, $subTitle, $user->email, $password);
            $successSubscribe->subject($title);
            $successSubscribe->from($this->adminEmail, config('app.name'));
            Mail::to($userEmail)->send($successSubscribe);

            return response()->json(['status' => 1, 'message' => __('messages.subscribe.success')]);
        }
        return response()->json(['status' => 0, 'errors' => $validator->errors()->all()]);
    }

    public function preorder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'sku_id' => 'required'
        ]);
        if ($validator->passes()) {
            $preorder = Preorder::create([
                'name' => $request->input('name', ''),
                'telephone' => $request->input('telephone', ''),
                'email' => $request->input('email'),
                'sku_id' => $request->input('sku_id'),
                'comment' => $request->input('comment', '')
            ]);

            $adminEmail = $this->adminEmail;
            Mail::send('mail.notification.admin_new_preorder', ['number' => $preorder->id], function ($message) use ($adminEmail) {
                $message->to($adminEmail)->subject(__('mails.preorder.admin.subject'));
            });
            return response()->json(['status' => 1, 'message' => __('messages.preorder.success')]);
        }
        return response()->json(['status' => 0, 'errors' => $validator->errors()->all()]);
    }
}
