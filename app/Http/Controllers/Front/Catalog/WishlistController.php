<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\Product;
use App\Http\Controllers\Controller;
use App\Services\WishListService;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    public function add(Request $request, WishListService $wishListService)
    {
        $product = Product::findOrFail($request->get('product_id'));
        if ($wishListService->add($product)) {
            return response()->json([
                'status' => 1,
                'quantity' => $wishListService->getQuantity(),
                'message' => __('messages.wishlist.added')
            ]);
        }
        return response()->json(['status' => 0]);
    }

    public function delete(Request $request, WishListService $wishListService)
    {
        $product = Product::findOrFail($request->get('product_id'));
        if ($wishListService->delete($product)) {
            return response()->json(['status' => 1, 'quantity' => $wishListService->getQuantity()]);
        }
        return response()->json(['status' => 0]);
    }
}
