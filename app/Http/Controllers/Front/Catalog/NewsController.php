<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\News;
use App\Entities\NewsReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class NewsController extends Controller
{
    public function list()
    {
        $newses = News::where('status', '=', 1)
            ->orderByDesc('id')
            ->with('localDescription')
            ->paginate(12);

        return view('front.news.list', [
            'newses' => $newses,
        ]);
    }

    public function show($slug)
    {
        $news = News::where('status', '=', 1)
            ->where('slug', '=', $slug)
            ->with(['localDescription', 'activeReviews'])
            ->first();

        if (!isset($news)) {
            abort(404);
        }

        $news->increment('views');

        $reviews = $news->reviews()->where('status', '=', 1)
            ->with('images')->withDepth()->defaultOrder()->get()->toTree();

        $previous = News::where('id', '=', $news->id + 1)
            ->where('status', '=', 1)->first();

        $next = News::where('id', '=', $news->id - 1)
            ->where('status', '=', 1)->first();

        $localDescription = $news->localDescription;
        return view('front.news.show', [
            'reviews' => $reviews,
            'news' => $news,
            'next' => $next,
            'previous' => $previous,
            'meta_title' => !empty($localDescription) ? $localDescription->meta_title : $localDescription->title,
            'meta_description' => isset($localDescription) ? $localDescription->meta_description : '',
            'meta_keywords' => isset($localDescription) ? $localDescription->meta_keywords : '',
        ]);
    }

    public function addReview(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'nullable|email',
            'text' => 'required'
        ]);
        if ($validator->passes()) {
            $newsReview = NewsReview::create($request->all());
            if ($request->has('images')) {
                $imagesNames = [];
                foreach ($request->images as $image) {
                    $imagesNames[] = [
                        'image' => $image->store('shop_images/upload/' . date('Y_m_d'))
                    ];
                }
                $newsReview->images()->createMany($imagesNames);
            }
            return response()->json(['status' => 1, 'message' => __('messages.news.review_added')]);
        }
        return response()->json(['status' => 0, $validator->errors()->all()]);
    }
}
