<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Services\CatalogService;
use App\Services\FilterService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class ProductListingController extends Controller
{
    protected $catalogService;
    protected $filterService;
    protected $request;

    protected $listingData;
    protected $query;

    public function __construct(CatalogService $catalogService, FilterService $filterService, Request $request)
    {
        $this->catalogService = $catalogService;
        $this->filterService = $filterService;

        $this->request = $request;
        $this->query = $request->input('query', '');

        $inputLimit = $request->input('limit', CatalogService::DEFAULT_LIMIT);
        $inputSort = $request->input('sort', CatalogService::DEFAULT_SORT);
        $inputOrder = $request->input('order', CatalogService::DEFAULT_ORDER);

        $limit = in_array($inputLimit, CatalogService::ALLOWED_LIMITS) ? $inputLimit : CatalogService::DEFAULT_LIMIT;
        $sort = in_array($inputSort, CatalogService::ALLOWED_SORTS) ? $inputSort : CatalogService::DEFAULT_SORT;
        $order = in_array($inputOrder, CatalogService::ALLOWED_ORDERS) ? $inputOrder : CatalogService::DEFAULT_ORDER;

        $this->listingData = [
            'limit' => $limit,
            'sort' => $sort,
            'order' => $order
        ];
    }

    public function index(Builder $products, $metaData = [], $filterCacheKey = null)
    {
        $catalog = CatalogService::PRODUCT_ITEM_SELECTED_FIELDS;
        array_push(
            $catalog,
            \DB::raw("(CASE WHEN sks.price IS NOT NULL or sks.price = '' THEN sks.price ELSE sku.price END) AS price")
        );
        $products->select($catalog)->where('status', '=', 1);

        $products = $this->catalogService->addProductInfo($products);

        $products->with(CatalogService::PRODUCT_ITEM_RELATIONS);
//        if (!empty($filterCacheKey)) {
//            $filter = Cache::remember($filterCacheKey, 120, function () use ($products) {
//                return $this->filterService->getFilterByProducts($products);
//            });
//        } else {
//            $filter = $this->filterService->getFilterByProducts($products);
//        }

        $category = explode('_', $filterCacheKey);
        $filter = $this->filterService->getFilterByProducts($products, end($category));

        $products = $this->filterService->getFilteredProducts($products);
        $filter = $this->filterService->addLinksToFilter($filter);

        $products = $this->getPagination(
            $products->orderBy('quantity', 'DESC')
                ->orderBy($this->listingData['sort'], $this->listingData['order'])
        );

        if ($this->request->ajax()) {
            $ajaxAction = $this->request->input('action', '');
            if ($ajaxAction === 'show_more') {
                $view = view('front.catalog.show_more', [
                    'products' => $products
                ]);
                return response()->json([
                    'status' => 1,
                    'template' => $view->render()
                ]);
            }

            return response()->json([
                'status' => 1,
                'filter' => $this->getFilterData($filter),
                'products_count' => $products->total()
            ]);
        }

        return view('front.catalog.product_listing', [
            'products' => $products,
            'filter' => $filter,
            'limits' => $this->catalogService->getProductListingLimits($this->listingData),
            'sorts' => $this->catalogService->getProductListingsSorts($this->listingData),
            'description' => isset($metaData['description']) ? $metaData['description'] : '',
            'meta_title' => isset($metaData['meta_title']) ? $metaData['meta_title'] : '',
            'meta_description' => isset($metaData['meta_description']) ? $metaData['meta_description'] : '',
            'meta_keywords' => isset($metaData['meta_keywords']) ? $metaData['meta_keywords'] : '',
            'meta_h1' => isset($metaData['meta_h1']) ? $metaData['meta_h1'] : '',
            'metaLinks' => [
                'prev' => $products->previousPageUrl(),
                'next' => $products->nextPageUrl(),
                'canonical' => url()->current()
            ],
            'jsonLd' => $this->getJsonLd([
                'name' => isset($metaData['meta_h1']) ? $metaData['meta_h1'] : '',
                'description' => isset($metaData['description']) ? $metaData['description'] : '',
                'quantity' => $products->total(),
                'high_price' => isset($filter['price']['max']) ? $filter['price']['max'] : '',
                'low_price' => isset($filter['price']['min']) ? $filter['price']['min'] : ''
            ])
        ]);
    }

    private function getPagination($products)
    {
        $result = $products->paginate($this->listingData['limit']);

        $this->listingData['limit'] !== CatalogService::DEFAULT_LIMIT
            ? $result->appends('limit', $this->listingData['limit'])
            : null;

        if (!($this->listingData['sort'] === CatalogService::DEFAULT_SORT
            && $this->listingData['order'] === CatalogService::DEFAULT_ORDER)
        ) {
            $result->appends('sort', $this->listingData['sort']);
            $result->appends('order', $this->listingData['order']);
        }
        $result->appends('filter', $this->request->input('filter'));
        if (!empty($this->query)) {
            $result->appends('query', $this->query);
        }
        return $result;
    }

    private function getJsonLd($data)
    {
        $jsonLd = [
            '@context' => 'http://schema.org',
            '@type' => 'Product',
            'url' => $this->request->fullUrl(),
            'name' => isset($data['name']) ? $data['name'] : '',
            'description' => isset($data['description']) ? $data['description'] : '',
            'offers' => [
                [
                    '@type' => 'AggregateOffer',
                    'offerCount' => isset($data['quantity']) ? $data['quantity'] : '',
                    'highPrice' => isset($data['high_price']) ? $data['high_price'] : '',
                    'lowPrice' => isset($data['low_price']) ? $data['low_price'] : '',
                    'priceCurrency' => 'UAH'
                ]
            ]
        ];
        return $jsonLd;
    }

    private function getFilterData($filter)
    {
        $filterData = [
            'manufacturers' => [],
            'filters' => [],
            'price' => [
                'min' => $filter['price']['min'],
                'max' => $filter['price']['max'],
            ]
        ];

        foreach ($filter['filters'] as $filterElem) {
            foreach ($filterElem->values as $filterValue) {
                $filterData['filters'][] = [
                    'id' => $filterValue->id,
                    'filter_link' => $filterValue->filter_link,
                    'name' => $filterValue->name
                ];
            }
        }

        foreach ($filter['filters_cat'] as $filterElem) {
            foreach ($filterElem->values as $filterValue) {
//                dd($filterValue);
                $filterData['filters_cat'][] = [
                    'id' => $filterValue->id,
                    'filter_link' => $filterValue->filter_link,
                    'name' => $filterValue->text,
                    'count' => $filterValue->count
                ];
            }
        }

        foreach ($filter['manufacturers'] as $manufacturer) {
            $filterData['manufacturers'][] = [
                'id' => $manufacturer->id,
                'filter_link' => $manufacturer->filter_link,
                'name' => $manufacturer->name
            ];
        }
        return $filterData;
    }
}
