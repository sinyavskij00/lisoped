<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\Product;
use App\Services\CatalogService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    protected $catalogService;

    protected $query;

    public function __construct(CatalogService $catalogService, Request $request)
    {
        $this->catalogService = $catalogService;
        $this->query = $request->input('query', '');
    }

    public function index()
    {
        $products = Product::where('status', '=', 1)
            ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);

        $products = $this->catalogService->addProductInfo($products);
        if (!empty($this->query)) {
            $products->where('status', '=', 1)
                ->where('name', 'like', '%' . $this->query . '%')
                ->orWhere('sku', 'like', '%' . $this->query . '%');
        }

        if ($products->count() === 0) {
            $recommendedProducts = Product::where('is_our_choice', '=', 1)
                ->where('status', '=', 1)
                ->orderBy(DB::raw('RAND()'))
                ->limit(6)
                ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);

            $recommendedProducts = $this->catalogService->addProductInfo($recommendedProducts);
            $recommendedProducts->with(CatalogService::PRODUCT_ITEM_RELATIONS);

            return view('front.catalog.empty_search', [
                'products' => $recommendedProducts->get(),
                'query' => $this->query
            ]);
        }

        $products = Product::where('status', '=', 1)
            ->where('name', 'like', '%' . $this->query . '%')
            ->orWhere('sku', 'like', '%' . $this->query . '%');

        $metaData = [
            'description' => '',
            'meta_h1' => __('messages.search.meta_h1') . $this->query,
            'meta_title' => __('messages.search.meta_title'),
            'meta_description' => __('messages.search.meta_description'),
            'meta_keywords' => __('messages.search.meta_keywords'),
        ];
        return $productListingController = app(ProductListingController::class)->callAction(
            'index',
            [
                'products' => $products,
                'metaData' => $metaData
            ]
        );
    }

    public function ajaxSearch()
    {
        if (empty($this->query)) {
            return '';
        }
        $result = [
            'products' => collect(),
            'total_link' => route('search', ['filter' => '', 'query' => $this->query]),
            'total_count' => 0
        ];

        $products = Product::where('status', '=', 1)
            ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);

        $products = $this->catalogService->addProductInfo($products);
        $products->with(CatalogService::PRODUCT_ITEM_RELATIONS);

        $products->where('status', '=', 1)
            ->where('name', 'like', '%' . $this->query . '%')
            ->orWhere('sku', 'like', '%' . $this->query . '%');

        $result['total_count'] = $products->count();
        $result['products'] = $products->take(3)->get();

        return view('front.catalog.ajax_search', [
            'result' => $result
        ]);
    }
}
