<?php

namespace App\Http\Controllers\Front\Catalog;

use App\Entities\AttributeGroup;
use App\Entities\Color;
use App\Entities\Product;
use App\Entities\ProductAttribute;
use App\Entities\ProductReview;
use App\Entities\SizeTable;
use App\Entities\SkuSpecial;
use App\Entities\StockKeepingUnit;
use App\Entities\Year;
use App\Services\CatalogService;
use Debugbar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Validator;

class ProductController extends Controller
{
    protected $request;
    protected $catalogService;

    protected $languageId;

    public function __construct(Request $request, CatalogService $catalogService)
    {
        $this->request = $request;
        $this->catalogService = $catalogService;
        $this->languageId = config('current_language_id');
    }

    public function index($productSlug)
    {
        $product = Product::where('slug', '=', $productSlug)->first();

        if (!isset($productSlug)) {
            abort(404);
        }
        $this->addToViewed($product);
        $languageId = $this->languageId;
        $product = Product::where('products.id', '=', $product->id)
            ->leftJoin('product_descriptions', function ($join) use ($languageId) {
                $join->on('products.id', '=', 'product_descriptions.product_id')
                    ->where('product_descriptions.language_id', '=', $languageId);
            })->leftJoin('manufacturer_descriptions', function ($join) use ($languageId) {
                $join->on('products.manufacturer_id', '=', 'manufacturer_descriptions.manufacturer_id')
                    ->where('manufacturer_descriptions.language_id', '=', $languageId);
            })->select([
                'products.*',
                'product_descriptions.name',
                'product_descriptions.description',
                'product_descriptions.meta_title',
                'product_descriptions.meta_description',
                'product_descriptions.meta_keywords',
                'manufacturer_descriptions.name as manufacturer_name'
            ])->first();

        $product->load([
            'activeReviews.images',
            'skus.year',
            'videos',
            'skus.color.localDescription',
            'skus.size.localDescription',
        ]);

        $relatedIds = DB::table('product_related')
            ->where('product_id', '=', $product->id)
            ->get(['related_id'])
            ->pluck('related_id');

        $relatedProducts = Product::where('status', '=', 1)
            ->whereIn('products.id', $relatedIds)
            ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);
        $relatedProducts = $this->catalogService->addProductInfo($relatedProducts);
        $relatedProducts->with(CatalogService::PRODUCT_ITEM_RELATIONS);

        $relatedProducts = $relatedProducts->get();

        $years = $this->getYears($product);
        $checkedYear = $years->firstWhere('checked', '=', true);

        $colors = $this->getColors($product, $checkedYear);
        $checkedColor = $colors->firstWhere('checked', '=', true);
        if (!isset($checkedColor)) {
            $checkedColor = $colors->first();
        }

        $sizes = $this->getSizes($product, $checkedYear, $checkedColor);
        $checkedSize = $sizes->firstWhere('checked', '=', true);
        if (!isset($checkedSize)) {
            $checkedSize = $sizes->first();
        }

        $activeSpecials = SkuSpecial::getActiveSpecials();

        $currentSku = $product->skus()
            ->where('product_id', '=', $product->id)
            ->leftJoinSub($activeSpecials, 'active_specials', function ($join) {
                $join->on('stock_keeping_units.id', '=', 'active_specials.unit_id');
            })->select([
                'active_specials.price as special_price',
                'stock_keeping_units.*'
            ]);

        if (!empty($checkedYear->id)) {
            $currentSku->where('year_id', '=', $checkedYear->id);
        }

        if (!empty($checkedColor->id)) {
            $currentSku->where('color_id', '=', $checkedColor->id);
        }

        if (!empty($checkedSize->id)) {
            $currentSku->where('size_id', '=', $checkedSize->id);
        }

        $currentSku = $currentSku->first();

        $tables = $this->getSizeTables($languageId, $product);
        $attributeGroups = $this->getAttributeGroups($product);
        $jsonLd = $this->getJsonLd($product);

        $templateData = [
            'years' => $years,
            'colors' => $colors,
            'sizes' => $sizes,
            'product' => $product,
            'relatedProducts' => $relatedProducts,
            'meta_title' => isset($product->meta_title) ? $product->meta_title : $product->name,
            'meta_description' => $product->meta_description,
            'meta_keywords' => $product->meta_keywords,
            'currentSku' => $currentSku,
            'sizeTables' => $tables,
            'attributeGroups' => $attributeGroups,
            'jsonLd' => $jsonLd
        ];

        if ($this->request->ajax()) {
            $template = view('front.catalog.current_sku_content', $templateData)->render();
            return response()->json(['status' => 1, 'template' => $template]);
        }

        return view('front.catalog.product_page', $templateData);
    }

    protected function addToViewed(Product $product)
    {
        $viewed = session('viewed', []);
        $viewed[] = $product->id;
        $viewed = array_slice(array_values(array_unique($viewed)), 0, 6);
        session(['viewed' => $viewed]);
    }

    public function calculateProductPrices(Request $request, CatalogService $catalogService)
    {
        $this->validate($request, [
            'sku_id' => 'required',
        ]);
        $sku = StockKeepingUnit::findOrFail($request->input('sku_id'));
        $quantity = $request->input('quantity', 1);
        $customerGroup = Config::get('current_customer_group');
        $result = $catalogService->getSkuPrices($sku, $quantity, $customerGroup);

        foreach ($result as &$value) {
            if (isset($value)) {
                $value = $catalogService->format($catalogService->calculate($value, $quantity));
            }
        }
        unset($value);

        return $result;
    }

    public function addReview(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'name' => 'required',
            'email' => 'email',
            'text' => 'required'
        ]);
        if ($validator->passes()) {
            $productReview = ProductReview::create($request->all());
            if ($request->has('images')) {
                $imagesNames = [];
                foreach ($request->images as $image) {
                    $imagesNames[] = [
                        'image' => $image->store('shop_images/upload/' . date('Y_m_d'))
                    ];
                }
                $productReview->images()->createMany($imagesNames);
            }
            return response()->json(['status' => 1, 'message' => __('messages.product.review_added')]);
        }
        return response()->json(['status' => 0, $validator->errors()->all()]);
    }

    private function getYears(Product $product)
    {
        $years = collect();
        $product->skus->each(function ($item) use ($years) {
            if ($item->year) {
                $years->push($item->year);
            }
        });
        $years = $years->unique();

        if ($years->count()) {
            //define checked year by year value
            if ($this->request->has('year')
                && $years->pluck('value')->contains($this->request->input('year'))
            ) {
                $years = $years->map(function ($item) {
                    if ($item->value == $this->request->input('year')) {
                        $item->checked = true;
                    }
                    return $item;
                });
            }

            if (!is_null($years->first()) &&!$years->contains('checked', '=', true)) {
                $years->first()->checked = true;
            }
        }

        return $years;
    }

    private function getColors(Product $product, $checkedYear)
    {
        $colors = collect();

        $where = [];
        if (isset($checkedYear->id)) {
            $where['year_id'] = $checkedYear->id;
        } else {
            $where['product_id'] = $product->id;
        }

        $skus = $product->skusProduct($where);

        if ($skus->count() > 0) {
            $skus->each(function ($item) use ($colors) {
                if ($item->color) {
                    $colors->push($item->color);
                }
            });
            $colors = $colors->unique();

            //define checked color by anchor
            if ($this->request->has('color')) {
                $colors = $colors->map(function ($item) {
                    if ($item->anchor == $this->request->input('color')) {
                        $item->checked = true;
                    }
                    return $item;
                });
            }

            if (!is_null($colors->first()) && !$colors->contains('checked', '=', true)) {
                $colors->first()->checked = true;
            }
        }

        return $colors;
    }

    private function getSizes(Product $product, $checkedYear, $checkedColor)
    {
        $sizes = collect();
        $where = [];
        if (isset($checkedYear->id)) {
            $where['year_id'] = $checkedYear->id;
        }

        if (isset($checkedColor->id)) {
            $where['color_id'] = $checkedColor->id;
        } else {
            $where['product_id'] = $product->id;
        }

        $skus = $product->skusProduct($where);

        if ($skus->count() > 0) {
            $skus->each(function ($item) use ($sizes) {

                $sizes->push($item->size);
            });
            $sizes = $sizes->unique();

            //define size by value
            $sizes = $sizes->map(function ($item) {

                if ($item) {

                    if ($item->value == $this->request->input('size')) {

                        $item->checked = true;
                    }
                }
                return $item;
            });

            if (!is_null($sizes->first()) && !$sizes->contains('checked', '=', true)) {
                $sizes->first()->checked = true;
            }
        }

        return $sizes;
    }

    private function getAttributeGroups(Product $product)
    {
        $languageId = $this->languageId;
        $productId = $product->id;
        if (isset($productId)
            && isset($languageId)
            && isset($product->productAttributes)
            && $product->productAttributes->count() > 0
        ) {
            $productAttributeIds = $product->productAttributes->pluck('id')->toArray();
            $attributeIds = $product->productAttributes->pluck('attribute_id')->toArray();

            $attributeGroups = AttributeGroup::whereHas(
                'attributes.productAttributes',
                function ($query) use ($languageId, $productAttributeIds) {
                    $query->whereIn('id', $productAttributeIds)->where('language_id', '=', $languageId);
                }
            )->with([
                'localDescription',
                'attributes' => function ($query) use ($attributeIds) {
                    $query->whereIn('id', $attributeIds);
                },
                'attributes.productAttributes' => function ($query) use ($productAttributeIds, $languageId) {
                    $query->whereIn('id', $productAttributeIds)->where('language_id', '=', $languageId);
                },
                'attributes.localDescription',
            ])->get();
            return $attributeGroups;
        }
        return collect();
    }

    private function getJsonLd($product)
    {
        $jsonLd = [
            '@context' => 'http://schema.org',
            '@type' => 'Product',
            'name' => $product->name,
            'description' => $product->description,
            'sku' => isset($product->mainSku) && !empty($product->mainSku->sku) ? $product->mainSku->sku : ''
        ];

        $productRating = $product->rating;
        if (isset($productRating) && $productRating > 0) {
            $jsonLd['aggregateRating'] = [
                '@type' => 'AggregateRating',
                'bestRating' => 5,
                'ratingValue' => $product->rating,
                'worstRating' => 1,
                'reviewCount' => $product->activeReviews->count()
            ];
        }

        if (isset($product->activeReviews) && $product->activeReviews->count() > 0) {
            $ratings = [];
            foreach ($product->activeReviews as $review) {
                $ratings[] = [
                    '@type' => 'Review',
                    'author' => $review->name,
                    'description' => $review->text,
                    'reviewRating' => [
                        '@type' => 'Rating',
                        'bestRating' => 5,
                        'ratingValue' => $review->rating,
                        'worstRating' => 1,
                    ]
                ];
            }
            $jsonLd['review'] = $ratings;
        }

        return $jsonLd;
    }

    private function getSizeTables($languageId, $product)
    {
        $tablesIds = $product->sizeTables->pluck('id')->toArray();
        $sizeTables = SizeTable::leftJoin('size_table_descriptions', function ($join) use ($languageId) {
            $join->on('size_tables.id', '=', 'size_table_descriptions.table_id')
                ->where('language_id', '=', $languageId);
        })
            ->whereIn('size_tables.id', $tablesIds)
            ->select([
                'size_tables.id',
                'size_table_descriptions.title',
                'size_table_descriptions.text'
            ])
            ->get();

        $tables = [];
        foreach ($sizeTables as $sizeTable) {
            $tableTitle = !empty($sizeTable->title) ? $sizeTable->title : '';
            $tableHeaders = [];
            $tableRows = [];
            $rows = explode(PHP_EOL, $sizeTable->text);
            foreach ($rows as $key => $row) {
                $parsedRow = str_getcsv($row);
                if ($key === 0) {
                    $tableHeaders = $parsedRow;
                } else {
                    $tableRows[] = $parsedRow;
                }
            }
            $tables[] = [
                'title' => $tableTitle,
                'headers' => $tableHeaders,
                'rows' => $tableRows
            ];
        }
        return $tables;
    }
}
