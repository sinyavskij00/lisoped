<?php

namespace App\Http\Controllers\Front;

use App\Entities\Language;
use App\Http\Middleware\BeforeLanguagePrefix;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class SwitchLanguageController extends Controller
{
    public function switchLanguage($lang, Request $request)
    {
        $referer = Redirect::back()->getTargetUrl();
        $parse_url = parse_url($referer, PHP_URL_PATH);

        $segments = explode('/', $parse_url);

        if (in_array($segments[1], BeforeLanguagePrefix::getPrefixes())) {
            unset($segments[1]);
        }

        $mainLanguage = Language::where('is_default', '=', 1)->first();

        if ($lang != $mainLanguage->slug) {
            array_splice($segments, 1, 0, $lang);
        }
        $url = $request->root() . implode("/", $segments);
        if (parse_url($referer, PHP_URL_QUERY)) {
            $url = $url . '?' . parse_url($referer, PHP_URL_QUERY);
        }
        return redirect($url);
    }
}
