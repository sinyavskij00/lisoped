<?php

namespace App\Http\Controllers\Front\Information;

use App\Library\Settings;
use App\Mail\Info\ContactForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Validator;

class ContactsController extends Controller
{
    protected $settings;

    protected $adminEmail;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
        $this->adminEmail = \config('app.admin_email');
    }

    public function index()
    {
        $settings = $this->settings->getByCode('site_settings');
        $location = isset($settings['location'])
            ? explode(',', $settings['location'])
            : [0 => '50.418540', 1 => '30.517545'];

        return view('front.information.contacts', [
            'location' => $location,
        ]);
    }

    public function question(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'text' => 'required',
        ]);

        if ($validator->passes()) {
            $mail = new ContactForm(
                $request->input('name'),
                $request->input('email'),
                $request->input('text')
            );

            $mail->subject(__('mails.contacts.subject'));
            Mail::to($this->adminEmail)->send($mail);
            return response()->json(['status' => 1, 'message' => __('messages.contacts.success')]);
        }
        return response()->json(['status' => 0, 'errors' => $validator->errors()->all()]);
    }
}
