<?php

namespace App\Http\Controllers\Front\Information;

use App\Entities\SiteSetting;
use App\Library\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutUsController extends Controller
{
    public function index(Settings $settings)
    {
        $texts = $settings->getByKey('about_us');
        $languageId = config('current_language_id');
        $text = isset($texts[$languageId]) ? $texts[$languageId] : '';

        return view('front.information.about_us', [
            'text' => $text
        ]);
    }
}
