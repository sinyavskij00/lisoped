<?php

namespace App\Http\Controllers\Front\Information;

use App\Entities\QuestionGroup;
use App\Http\Controllers\Controller;


class FaqController extends Controller
{
    public function index()
    {
        $questionGroups = QuestionGroup::with(['localDescription', 'questions.localDescription'])->get();
        return view('front.information.faq', [
            'questionGroups' => $questionGroups
        ]);
    }
}
