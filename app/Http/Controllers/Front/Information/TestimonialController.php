<?php

namespace App\Http\Controllers\Front\Information;

use App\Entities\Testimonial;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class TestimonialController extends Controller
{
    public function index()
    {
        $testimonials = Testimonial::where('status', '=', 1)
            ->where('parent_id', '=', null)
            ->with(['children.images', 'images'])
            ->paginate(3);
        return view('front.information.testimonials', [
            'testimonials' => $testimonials
        ]);
    }

    public function add(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'nullable|email',
            'text' => 'required'
        ]);
        if ($validator->passes()) {
            $testimonial = Testimonial::create($request->all());
            if ($request->has('images')) {
                $imagesNames = [];
                foreach ($request->images as $image) {
                    $imagesNames[] = [
                        'image' => $image->store('shop_images/upload/' . date('Y_m_d'))
                    ];
                }
                $testimonial->images()->createMany($imagesNames);
            }
            return response()->json(['status' => 1, 'message' => __('messages.testimonial.success')]);
        }
        return response()->json(['status' => 0, $validator->errors()->all()]);
    }
}
