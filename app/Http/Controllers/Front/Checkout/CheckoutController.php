<?php

namespace App\Http\Controllers\Front\Checkout;

use App\Library\Checkout\Checkout;
use App\Library\Checkout\Payment\LiqPayPayment;
use App\Services\OrderService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class CheckoutController extends Controller
{
    public function index(Checkout $checkout)
    {
        if ($checkout->cart->items()->count() === 0) {
            return view('front.checkout.empty_checkout');
        }

        return view('front.checkout.checkout', [
            'checkout' => $checkout,
        ]);
    }

    public function confirm(Checkout $checkout, OrderService $orderService)
    {
        $customer = Auth::user();
        $currency = Config::get('current_currency');
        $customerId = isset($customer) ? $customer->id : 0;
        $customerGroup = isset($customer) && isset($customer->customerGroup)
            ? $customer->customerGroup : Config::get('current_customer_group');
        $userInfo = $checkout->userInfo;


        $data = [
            'order_invoice' => '',
            'customer_id' => $customerId,
            'customer_group_id' => $customerGroup->id,
            'first_name' => isset($userInfo['first_name']) ? $userInfo['first_name'] : '',
            'last_name' => isset($userInfo['last_name']) ? $userInfo['last_name'] : '',
            'email' => isset($userInfo['email']) ? $userInfo['email'] : '',
            'telephone' => isset($userInfo['telephone']) ? $userInfo['telephone'] : '',
            'address' => $checkout->selectedShippingMethod->getAddress(),
            'comment' => isset($checkout->comment) ? $checkout->comment : '',
            'payment_method' => $checkout->selectedPaymentMethod->getName(),
            'payment_code' => $checkout->selectedPaymentMethod->getCode(),
            'shipping_method' => $checkout->selectedShippingMethod->getName(),
            'shipping_code' => $checkout->selectedShippingMethod->getCode(),
            'sub_total' => $checkout->subTotal,
            'shipping_payment' => $checkout->shippingPayment,
            'total' => $checkout->total,
            'language_id' => Config::get('current_language_id'),
            'currency_id' => $currency->id,
            'currency_code' => $currency->code,
            'currency_value' => $currency->value
        ];

        $orderSkus = [];

        foreach ($checkout->cart->items() as $cartItem) {
            $sku = $cartItem->sku;
            $quantity = $cartItem->getAttributeValue('quantity');
            $name = isset($sku->product) && isset($sku->product->localDescription)
                ? $sku->product->localDescription->getAttributeValue('name')
                : '';
            $colorName = isset($sku->color) && isset($sku->color->localDescription)
                ? $sku->color->localDescription->getAttributeValue('name')
                : '';
            $sizeValue = isset($sku->size) ? $sku->size->getAttributeValue('value') : '';
            $yearValue = isset($sku->year) ? $sku->year->getAttributeValue('value') : '';

            $orderSkus[] = [
                'sku_id' => $sku->getAttributeValue('id'),
                'sku' => $sku->getAttributeValue('sku'),
                'vendor_sku' => $sku->getAttributeValue('vendor_sku'),
                'name' => $name,
                'model' => $sku->getAttributeValue('model'),
                'color' => $colorName,
                'size' => $sizeValue,
                'year' => $yearValue,
                'quantity' => $quantity,
                'price' => $checkout->catalogService->getSkuPrice($sku, $quantity, $customerGroup),
                'vendor_price' => $sku->getAttributeValue('vendor_price'),
                'total' => $checkout->catalogService->getSkuPrice($sku, $quantity, $customerGroup, true),
                'vendor_total' => $sku->getAttributeValue('vendor_price') * $quantity,
                'reward' => 0
            ];
        }
        $data['order_skus'] = $orderSkus;

        $order = $orderService->createOrder($data);
        $result = [
            'status' => 1,
            'template' => $checkout->selectedPaymentMethod->confirm($order)
        ];
        session()->put('public_id', $order->public_id);
        return response()->json($result);
    }

    public function liqPayCallback(LiqPayPayment $liqPayPayment, Request $request)
    {
        $liqPayPayment->callback($request);
    }

    public function liqPayRenderStatus(LiqPayPayment $liqPayPayment, Request $request)
    {
        $liqPayPayment->renderStatus($request);
    }

    public function getShippingInfo(Request $request, Checkout $checkout)
    {
        $shippingMethods = collect(app()->tagged('shipping_methods'));
        $shippingCode = $request->input('shipping_code');
        $shippingMethod = $shippingMethods->first(function ($item) use ($shippingCode) {
            return $item->getCode() === $shippingCode;
        });
        $result = [
            'status' => 1,
            'template' => $shippingMethod->show()->render(),
            'checkout_head' => view('front.checkout.checkout_head', [
                'subTotal' => $checkout->subTotal,
                'shippingPayment' => $checkout->shippingPayment,
                'total' => $checkout->total,
                'productQuantity' => $checkout->cart->items()->count()
            ])->render()
        ];
        return response()->json($result);
    }

    public function getPaymentInfo(Request $request)
    {
        $paymentMethods = collect(app()->tagged('payment_methods'));
        $paymentCode = $request->input('payment_code');
        $paymentMethod = $paymentMethods->first(function ($item) use ($paymentCode) {
            return $item->getCode() === $paymentCode;
        });
        $result = [
            'status' => 1,
            'template' => $paymentMethod->show()->render()
        ];
        return response()->json($result);
    }
}
