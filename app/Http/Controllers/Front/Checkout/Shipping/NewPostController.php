<?php

namespace App\Http\Controllers\Front\Checkout\Shipping;

use App\Library\Checkout\Cart;
use App\Library\Checkout\NewPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class NewPostController extends Controller
{
    public $newPost;

    public function __construct(NewPost $newPost)
    {
        $this->newPost = $newPost;
    }

    public function getCity(Request $request)
    {
        $search = $request->get('city', '');
        $cities = $this->newPost->getCity($search);
        $suggestion = [];
        foreach ($cities as $city) {
            $suggestion['suggestions'][] = [
                "value" => $city->DescriptionRu,
                "data" => $city->Ref
            ];
        }
        return response()->json($suggestion);
    }

    public function getStores(Request $request, Cart $cart)
    {
        $cityRef = $request->get('cityRef');

        $cartWeight = $cart->getWeight();
        $stores = $this->newPost->getStores($cityRef);
        $result = [];
        foreach ($stores as $store) {
            if (!($store->TotalMaxWeightAllowed !== 0 && $store->TotalMaxWeightAllowed < $cartWeight)) {
                $result[] = [
                    'value' => $store->Ref,
                    'text' => $store->DescriptionRu,
                ];
            }
        }
        return response()->json($result);
    }
}
