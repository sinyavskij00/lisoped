<?php

namespace App\Http\Controllers\Front\Checkout;

use App\Entities\Order;
use App\Library\Checkout\Cart;
use App\Http\Controllers\Controller;

class SuccessController extends Controller
{
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function index(Cart $cart)
    {
        $public_id = session()->get('public_id');
//        session()->forget('public_id');
        session()->forget('orderId');
        $cart->deleteAll();
        return view('front.checkout.success', compact('public_id'));
    }
}
