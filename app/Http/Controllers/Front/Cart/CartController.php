<?php

namespace App\Http\Controllers\Front\Cart;

use App\Entities\CartItem;
use App\Library\Checkout\Cart;
use App\Library\Checkout\Checkout;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class CartController extends Controller
{
    public function add(Request $request, Cart $cart)
    {
        $validator = Validator::make($request->all(), [
            'sku_id' => 'required',
            'quantity' => 'required',
        ]);
        if ($validator->passes()) {
            $cart->add($request->input('sku_id'), $request->input('quantity', 1));
            $result = $this->getResult($cart);
            return response()->json($result);
        }
        return response()->json(['status' => 0, 'errors' => $validator->errors()]);
    }

    public function delete(Request $request, Cart $cart)
    {
        $validator = Validator::make($request->all(), [
            'cart_item_id' => 'required'
        ]);
        if ($validator->passes()) {
            $cart->delete($request->get('cart_item_id'));
            $result = $this->getResult($cart);
            return response()->json($result);
        }
        return response()->json(['status' => 0, 'errors' => $validator->errors()]);
    }

    public function changeItemQuantity(Request $request, Cart $cart)
    {
        $validator = Validator::make($request->all(), [
            'cart_item_id' => 'required',
            'quantity' => 'required'
        ]);

        $cartItemId = $request->input('cart_item_id');
        $cartItem = CartItem::findOrFail($cartItemId);
        $quantity = $request->input('quantity');

        if ($validator->passes()) {
            $cart->changeItem($cartItem, $quantity);
            $result = $this->getResult($cart);
            return response()->json($result);
        }
        return response()->json(['status' => 0, 'errors' => $validator->errors()]);
    }

    private function getResult(Cart $cart)
    {
        $checkout = app()->make(Checkout::class);
        $result = [
            'status' => 1,
            'cart_quantity' => $cart->items()->count(),
            'cart_popup' => view('front.checkout.cart_list', ['cart' => $cart])->render(),
            'checkout_list' => view('front.checkout.checkout_product_list', ['cart' => $cart])->render(),
            'checkout_head' => view('front.checkout.checkout_head', [
                'subTotal' => $checkout->subTotal,
                'shippingPayment' => $checkout->shippingPayment,
                'total' => $checkout->total,
                'productQuantity' => $checkout->cart->items()->count()
            ])->render()
        ];
        return $result;
    }
}
