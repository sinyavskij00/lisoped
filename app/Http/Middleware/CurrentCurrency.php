<?php

namespace App\Http\Middleware;

use App\Entities\Currency;
use Closure;
use Illuminate\Support\Facades\Config;

class CurrentCurrency
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentCurrencyId = session('current_currency_id');
        if (!isset($current_currency_id)) {
            $defaultCurrency = Currency::where('is_default', '=', 1)->first();
            $currentCurrencyId = $defaultCurrency->id;
            $currentCurrency = $defaultCurrency;
        } else {
            $currentCurrency = Currency::findOrFail($currentCurrencyId);
        }
        session(['current_currency_id' => $currentCurrencyId]);
        Config::set('current_currency_id', $currentCurrencyId);
        Config::set('current_currency', $currentCurrency);
        return $next($request);
    }
}
