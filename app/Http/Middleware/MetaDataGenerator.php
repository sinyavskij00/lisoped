<?php

namespace App\Http\Middleware;

use App\Entities\FilterValueDescription;
use App\Entities\LandingPage;
use App\Entities\ManufacturerDescription;
use Closure;
use Illuminate\Support\Facades\Request;

class MetaDataGenerator
{
    protected $languageId;

    public function __construct()
    {
        $this->languageId = config('current_language_id');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $resultData = config('app.filter_data');

        $currentUrl = Request::path();

        $landingPage = LandingPage::where('landing_pages.url', '=', $currentUrl)->first();

        if (isset($landingPage)) {
            config()->set('app.landing_page_data', $landingPage->toArray());
        } else {
            $metaGenerator = [];
            if (isset($resultData['filter_values'])) {
                $filterValueIds = array_slice($resultData['filter_values'], 0, 3);
                $filterValueNames = FilterValueDescription::whereIn('filter_value_id', $filterValueIds)
                    ->where('language_id', '=', $this->languageId)->select('filter_name')
                    ->get()->pluck('filter_name')->toArray();
                foreach ($filterValueNames as $filterValueName) {
                    $metaGenerator[] = ' ' . $filterValueName;
                }
            }

            if (isset($resultData['manufacturers'])) {
                $manufacturerIds = array_slice(
                    $resultData['manufacturers'],
                    0,
                    3 - count($metaGenerator)
                );
                $manufacturerNames = ManufacturerDescription::whereIn('manufacturer_id', $manufacturerIds)
                    ->where('language_id', '=', $this->languageId)->select('name')
                    ->get()->pluck('name')->toArray();
                foreach ($manufacturerNames as $manufacturerName) {
                    $metaGenerator[] = ' ' . $manufacturerName;
                }
            }
            config()->set('app.meta_generator_data', implode('', $metaGenerator));
        }

        return $next($request);
    }
}
