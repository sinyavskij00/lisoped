<?php

namespace App\Http\Middleware;

use App\Entities\Attribute;
use App\Entities\FilterValue;
use App\Entities\Manufacturer;
use Closure;
use Illuminate\Support\Facades\Request;

class Filter
{
    protected $languageId;

    protected $checkedFilterParams = 0;

    public function __construct()
    {
        $this->languageId = config('current_language_id');
    }

    protected $filterVariable = 'filter=';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentUrl = Request::path();
        if (str_contains($currentUrl, $this->filterVariable)) {
            $parts = explode('/', $currentUrl);
            foreach ($parts as $slug) {
                if (str_contains($slug, $this->filterVariable)) {
                    $slug = $this->clearSlug($slug);

                    $filterParts = $this->getFilterParts($slug);
                    config()->set('app.filter_parts', $filterParts);

                    $resultData = $this->getFilterValues($filterParts);
                    config()->set('app.filter_data', $resultData);
                }
            }
        }

        config()->set('app.is_nofollow_noindex', $this->checkedFilterParams >= 2);
        return $next($request);
    }

    private function getManufacturer($slug)
    {
        if (!empty($slug)) {
            $manufacturer = Manufacturer::where('slug', '=', $slug)->first();
            if (isset($manufacturer)) {
                return $manufacturer->getAttributeValue('id');
            }
        }
    }

    private function getFilterValue($slug)
    {
        if (!empty($slug)) {
            $filterValue = FilterValue::where('slug', '=', $slug)->first();
            if (isset($filterValue)) {
                return $filterValue->getAttributeValue('id');
            }
        }
    }

    private function clearSlug($slug)
    {
        $slug = str_replace($this->filterVariable, '', $slug);
        $slug = str_replace(';;', ';', $slug);
        $slug = str_replace('::', ':', $slug);
        $slug = str_replace(',,', ',', $slug);
        return $slug;
    }

    private function getFilterParts($slug)
    {
        $filterParameters = explode(';', $slug);
        $filterParts = [];
        foreach ($filterParameters as $filterParameter) {
            $filterParameter = explode(':', $filterParameter);
            $filterParameterName = $filterParameter[0];
            $filterParts[$filterParameterName] = [];
            $filterParameterValues = explode(',', $filterParameter[1]);

            foreach ($filterParameterValues as $filterParameterValue) {
                $filterParts[$filterParameterName][] = $filterParameterValue;
            }
        }
        return $filterParts;
    }

    private function getFilterValues($filterParts)
    {
        $attributes = Attribute::get()->pluck('id', 'slug');

        $resultData = [];
        foreach ($filterParts as $key => $value) {
            if ($key === 'price') {
                $resultData['price']['min'] = $value[0] ?? null;
                $resultData['price']['max'] = $value[1] ?? null;
            } elseif ($key === 'brand') {
                if (!isset($resultData['manufacturers'])) {
                    $resultData['manufacturers'] = [];
                }
                foreach ($value as $manufacturerSlug) {
                    if (!empty($manufacturerSlug)) {
                        $resultData['manufacturers'][] = $this->getManufacturer($manufacturerSlug);
                        $this->checkedFilterParams++;
                    }
                }
            } elseif (isset($attributes[$key])) {
                foreach ($value as $filterValueSlug) {
                    if (!empty($filterValueSlug)) {
                        if (!isset($resultData['filter_attributes'])) {
                            $resultData['filter_attributes'] = [];
                        }
                        $resultData['filter_attributes_grouped'][$key][] = $filterValueSlug;
                        $resultData['filter_attributes'][] = $filterValueSlug;
                        $this->checkedFilterParams++;
                    }
                }
            } else {
                foreach ($value as $filterValueSlug) {
                    if (!empty($filterValueSlug)) {
                        if (!isset($resultData['filter_values'])) {
                            $resultData['filter_values'] = [];
                        }

                        if ($this->getFilterValue($filterValueSlug)) {
                            $resultData['filter_values'][] = $this->getFilterValue($filterValueSlug);
                            $this->checkedFilterParams++;
                        }
                    }
                }
            }
        }
        return $resultData;
    }
}
