<?php

namespace App\Services;

use App\Entities\Order;
use App\Entities\OrderHistory;
use App\Entities\OrderSku;
use App\Entities\OrderStatus;
use App\Entities\StockKeepingUnit;
use App\Library\Settings;
use App\Mail\Orders\OrderStatusChanged;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use App\Notifications\OrderChangeStatusBySms;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Mpdf\Mpdf;

class OrderService
{
    protected $adminEmail;

    protected $settings;

    protected $mpdf;

    protected $maxPublicId = 99999999;

    public function __construct(Settings $settings)
    {
        $this->adminEmail = \config('app.admin_email');
        $this->settings = $settings;
    }

    public function addOrderHistory(Order $order, $orderStatusId, $notify = 0, $options = [])
    {
        $publicComment = isset($options['public_comment']) ? $options['public_comment'] : '';
        $privateComment = isset($options['private_comment']) ? $options['private_comment'] : '';

        $billingCartId = isset($options['billing_cart_id']) ? $options['billing_cart_id'] : null;
        $billingAccountId = isset($options['billing_account_id']) ? $options['billing_account_id'] : null;

        $orderHistory = OrderHistory::create([
            'order_id' => $order->id,
            'order_status_id' => $orderStatusId,
            'notify' => (int)$notify,
            'public_comment' => $publicComment,
            'private_comment' => $privateComment,
            'billing_cart_id' => $billingCartId,
            'billing_account_id' => $billingAccountId
        ]);

        $this->changeOrderStatus($order, $orderStatusId);

        $this->notifyCustomer($orderHistory);

        return $orderHistory;
    }

    public function changeOrderStatus(Order $order, $orderStatusId)
    {
        $orderStatusId = intval($orderStatusId);
        $order->order_status_id = $orderStatusId;
        $order->touch();
        $order->save();
        $exportOrderStatus = OrderStatus::whereIn('export_1c', array(1, 2))->pluck('id')->toArray();

        if (isset($exportOrderStatus) && is_numeric(array_search($orderStatusId, $exportOrderStatus))) {
            Artisan::call('export:order', [
                'order_id' => $order->id
            ]);
        }
    }

    public function notifyCustomer(OrderHistory $orderHistory)
    {
        $order = $orderHistory->order;

        if ($orderHistory->notify === 1) {
            if (isset($order) && !empty($order->email)) {
                $mailStatusChanged = new OrderStatusChanged($orderHistory);
                $nameLetter = request()->get('mail_template');
                if ($nameLetter == 'proof_payment') {
                    $letter = 'subject_end';
                } elseif ($nameLetter == 'product_shipped') {
                    $letter = 'subject_np';
                } else {
                    $letter = 'subject';
                }
                $mailStatusChanged->from($this->adminEmail, config('app.name'))
                    ->subject(__("mails.orders.status_changed." . $letter, ['number' => $order->public_id]))
                    ->to($order->email)
                    ->replyTo($this->adminEmail);
                Mail::send($mailStatusChanged);
            }
        }

        if ($orderHistory->notify_sms === 1 || $orderHistory->notify_viber === 1) {
            \Notification::send($orderHistory, new OrderChangeStatusBySms());
        }
    }

    public function createOrder($data)
    {
        $data['public_id'] = $this->getUniquePublicId();
        $order = Order::create($data);

        $order->skus()->createMany($data['order_skus']);

        foreach ($data['order_skus'] as $order_sku) {
            $sku = StockKeepingUnit::find($order_sku['sku_id']);
            if (isset($sku)) {
                $sku->quantity = $sku->quantity - $order_sku['quantity'];
                $sku->save();
            }
        }

        if (!empty($order->email)) {
            Notification::route('mail', $order->email)->notify(new \App\Notifications\NewOrder($order));
        }
        Notification::route('mail', $this->adminEmail)->notify(new \App\Notifications\AdminNewOrder($order));
        return $order;
    }

    public function sendCheck($orderId)
    {
        $order = Order::find($orderId);
        if (isset($order) && !empty($order->email)) {
            $email = $order->email;
            $orderSkus = OrderSku::where('order_id', '=', $orderId)->get();

            $stamp = $this->settings->getByKey('stamp');

            $view = view('layouts.check', [
                'orderSkus' => $orderSkus,
                'stamp' => $stamp,
                'orderNumber' => $order->public_id,
                'orderDate' => $order->created_at
            ])->render();

            $mpdf = app(Mpdf::class);
            $mpdf->WriteHTML($view);
            $pdf = $mpdf->Output('', 'S');

            $mailSubject = __('mails.check.subject', ['number' => $order->public_id]);
            Mail::raw($mailSubject, function ($message) use ($email, $pdf, $mailSubject) {
                $message->to($email);
                $message->subject($mailSubject);
                $message->attachData($pdf, 'check.pdf');
            });
        }
    }

    public function sendGuarantee($orderId)
    {
        $guarantee = $this->settings->getByKey('guarantee');

        if (Storage::disk('public')->exists($guarantee)) {
            $guaranteePath = Storage::disk('public')->path($guarantee);
            $order = Order::find($orderId);
            if (isset($order) && isset($order->email)) {
                $email = $order->email;
                $mailSubject = __('mails.guarantee.subject', ['number' => $order->public_id]);
                Mail::raw($mailSubject, function ($message) use ($email, $guaranteePath, $mailSubject) {
                    $message->to($email);
                    $message->subject($mailSubject);
                    $message->attach($guaranteePath);
                });
            }
        }
    }

    public function countOrderTotal($orderId)
    {
        $order = Order::find($orderId);
        if (isset($order)) {
            $orderSkus = $order->skus;
            $currencyValue = floatval($order->currency_value);
            $orderTotal = 0;
            foreach ($orderSkus as $orderSku) {
                $skuTotal = $orderSku->price * $orderSku->quantity * $currencyValue;
                $orderSku->total = $skuTotal;

                $orderSku->vendor_total = $orderSku->vendor_price * $orderSku->quantity * $currencyValue;

                $orderSku->save();
                $orderTotal += $skuTotal;
            }
            $order->total = $orderTotal + floatval($order->shipping_payment);
            $order->save();
        }
    }

    private function getUniquePublicId()
    {
        $publicId = random_int(1, $this->maxPublicId);
        if (Order::where('public_id', '=', $publicId)->count() > 0) {
            return $this->getUniquePublicId();
        }
        return $publicId;
    }
}
