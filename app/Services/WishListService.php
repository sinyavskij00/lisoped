<?php

namespace App\Services;

use App\Entities\Product;
use App\Library\Wishlist\WishlistDriver;

class WishListService
{
    protected $sessionKey = 'wishList';

    protected $driver;

    protected $wishlist;

    public function __construct(WishlistDriver $driver)
    {
        $this->driver = $driver;
        $this->wishlist = $driver->getWishlist();
    }

    public function add(Product $product)
    {
        return $this->driver->add($product);
    }

    public function delete(Product $product)
    {
        return $this->driver->delete($product);
    }

    public function getQuantity()
    {
        return $this->driver->getWishlist()->count();
    }

    public function isWished(Product $product) : bool
    {
        return $this->wishlist->contains($product->id);
    }

    protected function getWishList()
    {
        return $this->wishlist;
    }
}