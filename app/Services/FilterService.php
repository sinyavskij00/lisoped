<?php

namespace App\Services;

use App\Entities\Attribute;
use App\Entities\AttributeGroup;
use App\Entities\Filter;
use App\Entities\Manufacturer;
use App\Entities\ProductAttribute;
use App\Entities\SkuSpecial;
use App\Entities\StockKeepingUnit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class FilterService
{
    protected $filterVariable;

    protected $currentLanguageId;

    /**
     * FilterService constructor.
     */
    public function __construct()
    {
        $this->filterVariable = config('app.filter_data');
        $this->currentLanguageId = config('current_language_id');
    }

    /**
     * @param Builder $products
     * @return array
     */
    public function getFilterByProducts(Builder $products, $categoryId = false)
    {
        $products = clone $products;
        $result = [];
        $productsIds = $products->pluck('id');
        $filteredProductsIds = $this->getFilteredProducts($products)->pluck('id');

        $activeSpecials = SkuSpecial::getActiveSpecials();
        $prices = StockKeepingUnit::leftJoinSub($activeSpecials, 'active_specials', function ($join) {
            $join->on('stock_keeping_units.id', '=', 'active_specials.unit_id');
        })->whereIn('stock_keeping_units.product_id', $productsIds)->select([
            DB::raw('min(stock_keeping_units.price) as min_price'),
            DB::raw('max(stock_keeping_units.price) as max_price'),
            DB::raw('min(active_specials.price) as min_special_price'),
            DB::raw('max(active_specials.price) as max_special_price'),
        ])->where('stock_keeping_units.is_main', '=', 1)->first();

        //filter by price
        $result['price']['min'] = isset($prices->min_special_price) && $prices->min_special_price < $prices->min_price
            ? $prices->min_special_price
            : $prices->min_price;
        $result['price']['max'] = isset($prices->max_special_price) && $prices->max_special_price > $prices->max_price
            ? $prices->max_special_price
            : $prices->max_price;

        //filter by attributes
        $filterProductCounts = DB::table('filter_value_to_product')
            ->groupBy('filter_value_id')
            ->select([
                DB::raw('COUNT(product_id) as product_count'),
                'filter_value_id'
            ])
            ->whereIn('product_id', $productsIds)
            ->get();

        $filterValuesIds = $filterProductCounts->pluck('filter_value_id')->toArray();

        $result['filters'] = Filter::whereHas('values', function ($query) use ($filterValuesIds) {
            $query->whereIn('id', $filterValuesIds);
        })
            ->with(['values' => function ($query) use ($filterValuesIds) {
                $query->whereIn('id', $filterValuesIds);
            },
                'values.localDescription',
                'localDescription',
            ])->orderBy('sort')->get();

        foreach ($result['filters'] as $filter) {
            $filter->name = isset($filter->localDescription) && isset($filter->localDescription->name)
                ? $filter->localDescription->name
                : '';
            foreach ($filter->values as $filterValue) {
                $filterProductCount = $filterProductCounts
                    ->where('filter_value_id', '=', $filterValue->id)
                    ->first();
                $filterValue->products_count = isset($filterProductCount) && isset($filterProductCount->product_count)
                    ? $filterProductCount->product_count
                    : 0;
                $filterValue->name = isset($filterValue->localDescription)
                && isset($filterValue->localDescription->name)
                    ? $filterValue->localDescription->name
                    : '';
            }
        }

        //filter by category group attributes
        $filterProductCategoryCounts = DB::table('category_attributes')
            ->select([
                'category_attributes.attribute_id',
                DB::raw('(COUNT(pa.id)) as product_count'),
            ])
            ->leftJoin('product_attributes as pa', 'pa.attribute_id', '=', 'category_attributes.attribute_id')
            ->groupBy('category_attributes.attribute_id')
            ->where('category_attributes.category_id', $categoryId)
            ->whereIn('pa.product_id', $productsIds)
            ->get();

        $filterAttrValuesIds = $filterProductCategoryCounts->pluck('attribute_id')->toArray();

        $result['filters_cat'] = Attribute::select('attributes.*', 'attributes.slug')
            ->whereHas('values', function ($query) use ($filterAttrValuesIds) {
                $query->whereIn('attribute_id', $filterAttrValuesIds);
            })
            ->with(['values' => function ($query) use ($filterAttrValuesIds, $filteredProductsIds) {
                $query->whereIn('product_id', $filteredProductsIds);
            }])
            ->orderBy('sort')->get();

        foreach ($result['filters_cat'] as $filter) {
            $filter->name = isset($filter->localDescription) && isset($filter->localDescription->name)
                ? $filter->localDescription->name
                : '';
        }

        //filter by manufacturers
        $manufacturersIds = $products->select('manufacturer_id')
            ->distinct()
            ->get()
            ->pluck('manufacturer_id')
            ->toArray();
        $result['manufacturers'] = Manufacturer::leftJoin('manufacturer_descriptions', function ($join) {
            $languageId = config('current_language_id');
            $join->on('manufacturers.id', '=', 'manufacturer_descriptions.manufacturer_id')
                ->where('manufacturer_descriptions.language_id', '=', $languageId);
        })->select([
            'manufacturers.id',
            'manufacturers.slug',
            'manufacturer_descriptions.name',
        ])
            ->where('manufacturers.status', '=', 1)
            ->whereIn('manufacturers.id', $manufacturersIds)
            ->get();

        $manufacturerProductCounts = DB::table('products')->groupBy('manufacturer_id')
            ->select([
                DB::raw('COUNT(id) as product_count'),
                'manufacturer_id'
            ])
            ->whereIn('id', $productsIds)
            ->get();

        foreach ($result['manufacturers'] as $manufacturer) {
            $manufacturerProductCount = $manufacturerProductCounts
                ->where('manufacturer_id', '=', $manufacturer->id)
                ->first();
            $manufacturer->products_count = isset($manufacturerProductCount)
            && isset($manufacturerProductCount->product_count)
                ? $manufacturerProductCount->product_count
                : 0;
        }
        return $result;
    }

    public function getFilteredProducts(Builder $products)
    {
        if (empty($this->filterVariable)) {
            return $products;
        }

        $filterVariable = $this->filterVariable;
        $products->where(function($query) use ($filterVariable) {
            if (isset($filterVariable['price'])) {
                if (isset($filterVariable['price']['min'])) {
                    $priceMin = $filterVariable['price']['min'];
                    $query->where(function ($query) use ($priceMin) {
                        $query->where('sku.price', '>=', $priceMin)
                            ->orWhere('active_specials.price', '>=', $priceMin);
                    });
                }

                if (isset($filterVariable['price']['max'])) {
                    $priceMax = $filterVariable['price']['max'];
                    $query->where(function ($query) use ($priceMax) {
                        $query->where('sku.price', '<=', $priceMax)
                            ->orWhere('active_specials.price', '<=', $priceMax);
                    });
                }
            }

            if (isset($filterVariable['filter_attributes'])) {
                $query->whereHas('filterAttributes', function ($query) use ($filterVariable) {
                    $query->whereIn('product_attributes.slug', $filterVariable['filter_attributes'])
                        ->havingRaw(
                            'COUNT(DISTINCT product_attributes.attribute_id) = ?',
                            [count($filterVariable['filter_attributes_grouped'])]
                        );
                });
            }

            if (isset($filterVariable['filter_values'])) {
                $query->whereHas('filterValues', function ($query) use ($filterVariable) {
                    $query->whereIn('filter_value_id', $filterVariable['filter_values']);
                });
            }

            if (isset($filterVariable['manufacturers'])) {
                $query->whereIn('manufacturer_id', $filterVariable['manufacturers']);
            }
        });

        return $products;
    }

    public function addLinksToFilter($filter)
    {
        $clearCurrentUrl = $this->getUrlWithoutFilter();
        $filterData = [
            'query_string' => request()->getQueryString(),
            'clear_current_url' => $clearCurrentUrl,
            'filter_parts' => config('app.filter_parts')
        ];

        if (isset($filter['filters_cat'])) {
            foreach ($filter['filters_cat'] as $filterEntity) {
                foreach ($filterEntity->values as $filterValue) {
                    $filterValue->filter_link = $this->makeFilterLink(array_merge(
                        $filterData,
                        ['current_key' => $filterEntity->slug, 'current_value' => $filterValue->slug]
                    ), true);
                }
            }
        }

        foreach ($filter['filters'] as $filterEntity) {
            foreach ($filterEntity->values as $filterValue) {
                $currentKey = $filterEntity->slug;
                $currentValue = $filterValue->slug;
                $filterValue->filter_link = $this->makeFilterLink(array_merge(
                    $filterData,
                    ['current_key' => $currentKey, 'current_value' => $currentValue]
                ));
            }
        }

        foreach ($filter['manufacturers'] as $manufacturer) {
            $currentKey = 'brand';
            $currentValue = $manufacturer->slug;
            $manufacturer->filter_link = $this->makeFilterLink(array_merge(
                $filterData,
                ['current_key' => $currentKey, 'current_value' => $currentValue]
            ));
        }

        $filter['reset'] = $clearCurrentUrl;

        return $filter;
    }

    private function makeFilterLink($data)
    {
        $targetLink = $data['clear_current_url'];
        $currentKey = $data['current_key'];
        $currentValue = $data['current_value'];
        $queryString = $data['query_string'];
        $filterParts = $data['filter_parts'];

        if (isset($filterParts[$currentKey]) && in_array($currentValue, $filterParts[$currentKey])) {
            unset($filterParts[$currentKey][array_search($currentValue, $filterParts[$currentKey])]);
        } else {
            if (!isset($filterParts[$currentKey])) {
                $filterParts[$currentKey] = [];
            }
            $filterParts[$currentKey][] = $currentValue;
        }

        $linkParts = [];
        foreach ($filterParts as $valueTitle => $values) {
            if (count($values) > 0) {
                $linkParts[] = $valueTitle . ':' . implode(',', $values);
            }
        }

        if (count($linkParts) > 0) {
            $targetLink = rtrim($targetLink, '/');
            $targetLink .= '/filter=' . implode(';', $linkParts);
        }

        if (!empty($queryString)) {
            $targetLink .= '?' . $queryString;
        }

        return $targetLink;
    }

    public function getUrlWithoutFilter()
    {
        return str_before(url()->current(), 'filter=');
    }
}
