<?php

namespace App\Services;

use App\Entities\Currency;
use App\Entities\CustomerGroup;
use App\Entities\SkuDiscount;
use App\Entities\SkuSpecial;
use App\Entities\StockKeepingUnit;
use App\Library\Settings;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class CatalogService
{
    const ALLOWED_LIMITS = [12, 24, 36, 48];
    const DEFAULT_LIMIT = 12;

    const ALLOWED_SORTS = ['special_price', 'price', 'created_at', 'is_new', 'is_our_choice'];
    const DEFAULT_SORT = 'price';

    const ALLOWED_ORDERS = ['ASC', 'DESC'];
    const DEFAULT_ORDER = 'ASC';

    const PRODUCT_ITEM_SELECTED_FIELDS = [
        'products.id',
        'products.is_new',
        'products.is_hit',
        'products.is_our_choice',
        'products.slug',
        'products.created_at',
    ];

    const PRODUCT_ITEM_RELATIONS = [
        'skus.color',
        'skus.size',
        'activeReviews',
        'mainSku.images',
        'productAttributes.attribute.localDescription',
    ];

    protected $currencies;

    protected $watermarkPath;

    public function __construct(Settings $settings)
    {
        $this->watermarkPath = $settings->getByKey('watermark');
        if (Schema::hasTable('currencies')) {
            $this->currencies = Currency::all();
        }
    }

    public function resize($imgPath, $width, $height, $watermark = true, $aspectRatio = false)
    {
        if (empty($imgPath)) {
            return;
        }
        $pathInfo = pathinfo($imgPath);
        $newDir = 'cache' . DIRECTORY_SEPARATOR . $pathInfo['dirname'];
        $newPath = $newDir . DIRECTORY_SEPARATOR . $pathInfo['filename']
            . '-' . $width . '-' . $height . (!empty($watermark) ? '-marked' : '')
            . (!empty($aspectRatio) ? '-ratio' : '') . '.' . $pathInfo['extension'];
        if (!Storage::disk('public')->exists($newPath) && Storage::disk('public')->exists($imgPath)) {
            Storage::makeDirectory($newDir);
            $image = Image::make(Storage::disk('public')
                ->path($imgPath))
                ->resize($width, $height, function ($constraint) use ($aspectRatio) {
                    if ($aspectRatio) {
                        $constraint->aspectRatio();
                    }
                });
            if ($watermark && Storage::disk('public')->exists($this->watermarkPath)) {
                $watermark = Image::make(Storage::disk('public')
                    ->path($this->watermarkPath))
                    ->resize(300, 300)
                    ->opacity(50);
                $image->insert($watermark, 'center');
            }
            $image->save(Storage::disk('public')->path($newPath), 100);
        }
        return Storage::disk('public')->url($newPath);
    }

    public function format($price, $currencyId = null)
    {
        $currentCurrency = $this->currencies->find($currencyId);
        if (!isset($currentCurrency)) {
            $currentCurrency = \config('current_currency');
        }

        $price = floatval($price);
        return $currentCurrency->symbol_left
            . number_format($price, $currentCurrency->decimal_place, '.', ' ')
            . ' ' . $currentCurrency->symbol_right;
    }

    public function calculate($price, $quantity = 1)
    {
        $currentCurrency = \config('current_currency');
        return $price * $currentCurrency->value * $quantity;
    }

    public function addProductInfo(Builder $products)
    {
        $products = clone $products;
        $selectedFields = [
            'product_descriptions.name',
            'product_descriptions.description',
            'product_descriptions.meta_title',
            'product_descriptions.meta_description',
            'product_descriptions.meta_keywords',
            'sku.price',
            'sku.quantity',
            'sku.sku',
            'active_specials.price as special_price',
            'ui.image'
        ];

        $products->leftJoin('product_descriptions', function ($join) {
            $languageId = config('current_language_id');
            $join->on('products.id', '=', 'product_descriptions.product_id')->where('language_id', '=', $languageId);
        })
            ->leftJoin('stock_keeping_units as sku',
                'sku.id',
                '=',
                \DB::raw(
                    '(SELECT skusub.id
                    FROM stock_keeping_units as skusub 
                    WHERE skusub.product_id = products.id
                    ORDER BY skusub.quantity DESC LIMIT 1)'
                )
            )
            ->leftJoin('unit_images as ui', 'ui.unit_id', '=', 'sku.id')
            ->leftJoin('sku_specials AS sks', 'sks.unit_id' ,'=' ,'sku.id');

        $activeSpecials = SkuSpecial::getActiveSpecials();

        $products->leftJoinSub($activeSpecials, 'active_specials', function ($join) {
            $join->on('sku.id', '=', 'active_specials.unit_id');
        })
            ->addSelect($selectedFields);

        return $products;
    }

    public function getSkuPrice(
        StockKeepingUnit $sku,
        $quantity = 1,
        CustomerGroup $customerGroup = null,
        $calculate = false,
        $format = false
    ) {
        $customerGroup = isset($customerGroup) ? $customerGroup : config('current_customer_group');
        $result = $this->getSkuPrices($sku, $quantity, $customerGroup);

        if (isset($result['discount'])) {
            $price = $result['discount'];
        } elseif (isset($result['special'])) {
            $price = $result['special'];
        } else {
            $price = $result['price'];
        }

        $price = $calculate ? $this->calculate($price, $quantity) : $price;
        $price = $format ? $this->format($price) : $price;

        return $price;
    }

    public function getSkuPrices(StockKeepingUnit $sku, $quantity, CustomerGroup $customerGroup)
    {
        $quantity = isset($quantity) ? $quantity : 1;
        $now = now();
        $activeSpecials = SkuSpecial::getActiveSpecials($customerGroup->id);

        $activeDiscounts = SkuDiscount::where('customer_group_id', '=', $customerGroup->id)
            ->where('date_end', '>=', $now)
            ->where('date_start', '<=', $now)
            ->where('quantity', '<=', $quantity)
            ->orderBy('priority', 'desc');

        $sku = StockKeepingUnit::where('stock_keeping_units.id', '=', $sku->id)
            ->leftJoinSub($activeSpecials, 'active_specials', function ($join) {
                $join->on('stock_keeping_units.id', '=', 'active_specials.unit_id');
            })->leftJoinSub($activeDiscounts, 'active_discounts', function ($join) {
                $join->on('stock_keeping_units.id', '=', 'active_discounts.unit_id');
            })->select([
                'stock_keeping_units.price',
                'active_specials.price as special_price',
                'active_discounts.price as discount_price',
            ])->first();

        $result = [
            'discount' => $sku->discount_price,
            'special' => $sku->special_price,
            'price' => $sku->price,
        ];

        return $result;
    }

    /**
     * @param $data
     * @return array
     */
    public function getProductListingsSorts($data)
    {
        $limit = isset($data['limit']) ? $data['limit'] : self::DEFAULT_LIMIT;
        $currentSort = isset($data['sort']) ? $data['sort'] : self::DEFAULT_SORT;
        $currentOrder = isset($data['order']) ? $data['order'] : self::DEFAULT_ORDER;

        $params = array_merge(request()->query(), ['limit' => $limit !== self::DEFAULT_LIMIT ? $limit : null]);
        $stuff = [
            'price' => [
                'ASC' => __('catalog.sorts.price.asc'),
                'DESC' => __('catalog.sorts.price.desc')
            ],
            'is_new' => [
                'DESC' => __('catalog.sorts.novelty.desc')
            ],
            'special_price' => [
                'DESC' => __('catalog.sorts.special_price.desc')
            ],
            'is_our_choice' => [
                'DESC' => __('catalog.sorts.our_choice.desc')
            ]
        ];
        $sorts = [];
        foreach ($stuff as $sort => $item) {
            foreach ($item as $order => $text) {
                $sortParams = array_merge(
                    $params,
                    [
                        'sort' => $sort !== self::DEFAULT_SORT ? $sort : null,
                        'order' => $order !== self::DEFAULT_ORDER ? $order : null
                    ]
                );
                $sorts[] = [
                    'text' => $text,
                    'link' => rtrim(request()->fullUrlWithQuery($sortParams), '?'),
                    'is_active' => $currentSort === $sort && $currentOrder === $order
                ];
            }
        }
        return $sorts;
    }

    public function getProductListingLimits($data)
    {
        $limit = isset($data['limit']) ? intval($data['limit']) : self::DEFAULT_LIMIT;
        $sort = isset($data['sort']) ? $data['sort'] : self::DEFAULT_SORT;
        $order = isset($data['order']) ? $data['order'] : self::DEFAULT_ORDER;

        $params = array_merge(
            request()->query(),
            [
                'sort' => $sort !== self::DEFAULT_SORT ? $sort : null,
                'order' => $order !== self::DEFAULT_ORDER ? $order : null
            ]
        );
        $limits = [];
        foreach (self::ALLOWED_LIMITS as $item) {
            $limitParams = array_merge(
                $params,
                ['limit' => $item !== self::DEFAULT_LIMIT ? $item : null]
            );
            $limits[] = [
                'value' => $item,
                'is_active' => $limit === $item,
                'link' => rtrim(request()->fullUrlWithQuery($limitParams), '?'),
            ];
        }

        return $limits;
    }
}
