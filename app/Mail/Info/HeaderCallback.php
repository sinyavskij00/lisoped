<?php

namespace App\Mail\Info;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class HeaderCallback extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $phone;
    public $day;
    public $hour;
    public $minute;

    /**
     * Create a new message instance.
     *
     * @param $name
     * @param $phone
     * @param $day
     * @param $hour
     * @param $minute
     */
    public function __construct($name, $phone, $day, $hour, $minute)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->day = $day;
        $this->hour = $hour;
        $this->minute = $minute;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.header_callback');
    }
}
