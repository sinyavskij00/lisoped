<?php

namespace App\Mail\Notification;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgottenCart extends Mailable
{
    use Queueable, SerializesModels;

    public $skus;
    public $user;

    /**
     * Create a new message instance.
     *
     * @param $skus
     */
    public function __construct($skus, $user)
    {
        $this->skus = $skus;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.notification.letter_forgot_product');
    }
}
