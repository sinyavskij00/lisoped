<?php

namespace App\Mail\Information;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReviewAccepted extends Mailable
{
    use Queueable, SerializesModels;

    public $text;

    public $link;

    public $name;

    /**
     * Create a new message instance.
     *
     * @param $text
     * @param null $link
     */
    public function __construct($text, $link = null, $name = null)
    {
        $this->text = isset($text) ? $text : '';
        $this->link = $link;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail/notification.letter_review_published');
    }
}
