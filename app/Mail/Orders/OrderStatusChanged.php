<?php

namespace App\Mail\Orders;

use App\Entities\BillingAccount;
use App\Entities\BillingCart;
use App\Entities\OrderHistory;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderStatusChanged extends Mailable
{
    use Queueable, SerializesModels;

    public $orderHistory;

    public $publicOrderId;

    public $billingAccount;

    public $billingCart;

    public $vendorCount;

    /**
     * Create a new message instance.
     *
     * @param OrderHistory $orderHistory
     */
    public function __construct(OrderHistory $orderHistory)
    {
        $this->orderHistory = $orderHistory;
        $this->publicOrderId = isset($orderHistory->order) && isset($orderHistory->order->public_id)
            ? $orderHistory->order->public_id
            : null;

        $billingAccount = BillingAccount::find($orderHistory->getAttributeValue('billing_account_id'));
        if (isset($billingAccount)) {
            $this->billingAccount = $billingAccount;
        }

        $billingCart = BillingCart::find($orderHistory->getAttributeValue('billing_cart_id'));
        if (isset($billingCart)) {
            $this->billingCart = $billingCart;
        }

        // Vendor count for new post
        $vendors = [];
        foreach ($orderHistory->order->skus as $sku) {
            if (isset($sku->keepingUnit->vendor_id) && $sku->keepingUnit->vendor_id > 0) {
                $vendors[$sku->keepingUnit->vendor_id] = 1;
            }
        }
        $this->vendorCount = count($vendors);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = request()->get('mail_template');

        return $this->view(
            'mail/' . (
                $template && $template == 'default'
                ? 'order.order_status_changed'
                : 'templates.letter_' . $template
            )
        );
    }
}
