<?php

namespace App\Library\Modules\Front;

use App\Entities\Banner;

class BannerModule
{
    public function show()
    {
        $banners = Banner::where('status', '=', 1)->with(['localDescription'])->get();

        if ($banners->count() === 0) {
            return '';
        }

        return view('front.modules.banner', [
            'banners' => $banners
        ])->render();
    }
}
