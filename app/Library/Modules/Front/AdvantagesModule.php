<?php

namespace App\Library\Modules\Front;

class AdvantagesModule {
    public function show()
    {
        return view('front.modules.advantages')->render();
    }
}