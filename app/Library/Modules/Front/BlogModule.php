<?php

namespace App\Library\Modules\Front;

use App\Entities\News;
use App\Entities\Testimonial;

class BlogModule
{
    public function show()
    {
        $newses = News::where('status', '=', 1)
            ->take(4)
            ->with('localDescription')
            ->get();

        $testimonials = Testimonial::where('status', '=', 1)->take(3)->get();

        if ($newses->count() === 0 && $testimonials->count() === 0) {
            return '';
        }

        return view('front.modules.blog', [
            'newses' => $newses,
            'testimonials' => $testimonials
        ])->render();
    }
}
