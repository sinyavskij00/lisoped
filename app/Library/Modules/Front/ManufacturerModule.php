<?php

namespace App\Library\Modules\Front;

use App\Entities\Manufacturer;

class ManufacturerModule
{
    public function show()
    {
        $languageId = config('current_language_id');
        $manufacturers = Manufacturer::leftJoin('manufacturer_descriptions', function ($join) use ($languageId) {
            $join->on('manufacturers.id', '=', 'manufacturer_descriptions.manufacturer_id')
                ->where('manufacturer_descriptions.language_id', '=', $languageId);
        })->select([
            'manufacturers.*',
            'manufacturer_descriptions.name'
        ])
            ->where('status', '=', 1)
            ->get();

        if ($manufacturers->count() === 0) {
            return '';
        }

        return view('front.modules.manufacturer', [
            'manufacturers' => $manufacturers
        ])->render();
    }
}