<?php

namespace App\Library\Modules\Front;

use App\Entities\Product;
use App\Library\Settings;
use App\Services\CatalogService;

class ProductTabs
{
    protected $moduleSettings;

    protected $catalogService;

    public function __construct(Settings $settings, CatalogService $catalogService)
    {
        $this->moduleSettings = $settings->getByCode('home_carousel');
        $this->catalogService = $catalogService;
    }

    public function show()
    {
        $specialProducts = !empty($this->moduleSettings['special_products'])
            ? $this->moduleSettings['special_products']
            : [];
        $hitProducts = !empty($this->moduleSettings['hit_products']) ? $this->moduleSettings['hit_products'] : [];
        $newProducts = !empty($this->moduleSettings['new_products']) ? $this->moduleSettings['new_products'] : [];

        $newProducts = Product::where('status', '=', 1)
            ->whereIn('products.id', $newProducts)
            ->with(CatalogService::PRODUCT_ITEM_RELATIONS)
            ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);
        $newProducts = $this->catalogService->addProductInfo($newProducts);
        $newProducts = $newProducts->get();

        $hitProducts = Product::where('status', '=', 1)
            ->whereIn('products.id', $hitProducts)
            ->with(CatalogService::PRODUCT_ITEM_RELATIONS)
            ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);
        $hitProducts = $this->catalogService->addProductInfo($hitProducts);
        $hitProducts = $hitProducts->get();

        $specialProducts = Product::where('status', '=', 1)
            ->whereIn('products.id', $specialProducts)
            ->with(CatalogService::PRODUCT_ITEM_RELATIONS)
            ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);
        $specialProducts = $this->catalogService->addProductInfo($specialProducts);
        $specialProducts = $specialProducts->get();

        $labels = [];
        if ($newProducts->count() > 0) {
            $labels[] = [
                'title' => __('modules.product_tabs.new'),
                'active' => false
            ];
        }
        if ($hitProducts->count() > 0) {
            $labels[] = [
                'title' => __('modules.product_tabs.hit'),
                'active' => false
            ];
        }
        if ($specialProducts->count() > 0) {
            $labels[] = [
                'title' => __('modules.product_tabs.specials'),
                'active' => false
            ];
        }

        if (count($labels) > 0) {
            $labels[0]['active'] = true;
        } else {
            return '';
        }

        return view('front.modules.product_tabs', [
            'newProducts' => $newProducts,
            'hitProducts' => $hitProducts,
            'specialProducts' => $specialProducts,
            'labels' => $labels
        ])->render();
    }
}
