<?php

namespace App\Library;

use App\Entities\SiteSetting;
use Illuminate\Support\Facades\Schema;

class Settings
{
    protected $settings;

    public function __construct()
    {
        if (Schema::hasTable('site_settings')) {
            $this->settings = SiteSetting::all();
        }
    }

    public function getByCode($code)
    {
        $settings = [];
        if (!empty($this->settings)) {
            foreach ($this->settings->where('code', '=', $code) as $setting) {
                $settings[$setting->key] = ($setting->serialized === 1)
                    ? unserialize($setting->value)
                    : $setting->value;
            }
        }
        return $settings;
    }

    public function getByKey($key)
    {
        if (!empty($this->settings)) {
            $setting = $this->settings->where('key', '=', $key)->first();
            if (isset($setting)) {
                return ($setting->serialized === 1) ? unserialize($setting->value) : $setting->value;
            }
        }
        return;
    }
}
