<?php

namespace App\Library\Checkout\Payment;

use App\Entities\Order;
use App\Entities\Payment;
use App\Library\Checkout\Cart;
use App\Library\Settings;
use App\Services\OrderService;
use Illuminate\Http\Request;
use LiqPay;

class LiqPayPayment implements PaymentMethod
{
    protected $code;
    protected $name;

    protected $liqpay;
    protected $checkout;

    protected $orderService;

    protected $confirmStatus;
    protected $status;

    public function __construct(LiqPay $liqPay, Cart $cart, OrderService $orderService)
    {
        $this->liqpay = $liqPay;
        $this->cart = $cart;
        $this->orderService = $orderService;

        $this->code = 'liqpay';
        $this->name = __('checkout.library.payment.liqpay.name');

        $payment = Payment::where('code', '=', $this->code)->first();

        $this->confirmStatus = isset($payment) && isset($payment->confirm_status_id)
            ? intval($payment->confirm_status_id)
            : 0;
        $this->status = isset($payment) && isset($payment->status) ? intval($payment->status) : 0;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getName()
    {
        return $this->name;
    }

    public function confirm(Order $order)
    {
        return $this->getForm($order);
    }

    public function getForm(Order $order)
    {
        $html = '';

        if (isset($order)) {
            $html = $this->liqpay->cnb_form(array(
                'version' => '3',
                'action' => 'pay',
                'amount' => $order->total,
                'currency' => $order->currency_code,
                'description' => 'Order: ' . $order->public_id,
                'order_id' => $order->public_id,
                'language' => 'ru',
                'result_url' => route('liqpay.renderStatus'),
                'server_url' => route('liqpay.callback'),
                'sandbox' => config('app.liqpay.sunbox')
            ));
        }

        return $html;
    }

    public function callback(Request $request)
    {
        $data = $request->all();
        $this->processPaymentInfo($data);
    }

    public function renderStatus(Request $request)
    {
        $data = $request->all();
        $responseData = $this->decodeResponse($data['data']);

        $currentTime = microtime(true) * 10000;
        file_put_contents('info' . $currentTime . '.json', json_encode($request->all()));

        if (!empty($responseData['status'])
            && ($data['status'] == 'success' || $data['status'] == 'sandbox' || $data['status'] == 'wait_accept')
        ) {
            return redirect()->route('success');
        } else {
            return redirect()->route('failure');
        }
    }

    protected function processPaymentInfo($data)
    {
        $responseData = $this->decodeResponse($data['data']);
        if (!$this->checkResponseSignature($data)) {
            //fake answer from liqpay
            return false;
        }
        $order = Order::findOrFail($responseData['order_id']);
        if (!empty($responseData['status']) && $responseData['status'] === 'success') {
            $this->orderService->addOrderHistory($order, $this->confirmStatus, 1);
        }

        return true;
    }

    private function decodeResponse($data)
    {
        return $this->liqpay->decode_params($data);
    }

    private function checkResponseSignature($response)
    {
        $checkSignature = base64_encode(sha1(env('LIQPAY_PRIVATE_KEY')
            . $response['data'] . env('LIQPAY_PRIVATE_KEY'), 1));
        return $response['signature'] === $checkSignature;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getConfirmStatus()
    {
        return $this->confirmStatus;
    }

    public function checkPaymentStatus($orderId)
    {
        $res = $this->liqpay->api("request", array(
            'action' => 'status',
            'version' => '3',
            'order_id' => $orderId
        ));
        return $res;
    }
}
