<?php

namespace App\Library\Checkout\Payment;

use App\Entities\Order;

interface PaymentMethod
{
    public function getCode();

    public function getName();

    public function confirm(Order $order);

    public function getStatus();

    public function getConfirmStatus();
}
