<?php

namespace App\Library\Checkout;

use App\Services\CatalogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Checkout
{
    public $catalogService;

    public $selectedPaymentMethod;
    public $selectedShippingMethod;

    public $paymentMethods;
    public $shippingMethods;

    public $userInfo;
    public $comment;
    public $cart;

    public $subTotal;
    public $shippingPayment;
    public $total;

    /**
     * Checkout constructor.
     * @param Request $request
     * @param Cart $cart
     * @param CatalogService $catalogService
     */
    public function __construct(Request $request, Cart $cart, CatalogService $catalogService)
    {
        $this->catalogService = $catalogService;

        $this->cart = $cart;
        $this->userInfo = $this->getUserInfo($request);
        $this->comment = $this->getComment($request);

        $this->shippingMethods = collect(app()->tagged('shipping_methods'))->filter(function ($item) {
            return $item->getStatus() === 1;
        });

        $this->paymentMethods = collect(app()->tagged('payment_methods'))->filter(function ($item) {
            return $item->getStatus() === 1;
        });

        $paymentCode = $request->input('payment_code');
        $selectedPaymentMethod = $this->paymentMethods->first(function ($item) use ($paymentCode) {
            return $item->getCode() === $paymentCode;
        });
        $this->selectedPaymentMethod = $selectedPaymentMethod;

        $shippingCode = $request->input('shipping_code');
        $selectedShippingMethod = $this->shippingMethods->first(function ($item) use ($shippingCode) {
            return $item->getCode() === $shippingCode;
        });

        if (!isset($selectedShippingMethod)) {
            $selectedShippingMethod = $this->shippingMethods->first();
        }
        $this->selectedShippingMethod = $selectedShippingMethod;

        $this->subTotal = $cart->subTotal();
        $this->shippingPayment = $selectedShippingMethod->getCost();
        $this->total = $this->subTotal + $this->shippingPayment;
    }

    private function getComment(Request $request)
    {
        $comment = $request->get('order_comment', '');
        return $comment;
    }

    private function getUserInfo(Request $request)
    {
        $fields = ['first_name', 'last_name', 'telephone', 'email'];
        $result = [];
        foreach ($fields as $fieldName) {
            $result[$fieldName] = $this->getUserField($request, $fieldName);
        }
        return $result;
    }

    private function getUserField(Request $request, $variable)
    {
        $result = $request->get($variable);
        if (!isset($result)) {
            $result = Auth::check() ? Auth::user()->{$variable} : null;
            if (!isset($result)) {
                $result = session()->get('customer_info.' . $variable, '');
            }
        }
        session([$variable => $result]);
        return $result;
    }
}