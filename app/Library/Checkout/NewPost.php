<?php

namespace App\Library\Checkout;

use App\Entities\ProductToCategory;
use App\Services\CatalogService;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class NewPost
{
    public $apiKey;

    public $code = 'new_post';

    public $name = 'Новая почта';

    public $cart;

    protected $client;

    private $citiesTable = 'new_post_cities';

    private $warehousesTable = 'new_post_warehouses';


    public function __construct(Client $client, Cart $cart)
    {
        $this->apiKey = config('app.new_post.api_key');
        $this->client = $client;
        $this->cart = $cart;
    }

    public function getCitiesCount()
    {
        return DB::table($this->citiesTable)->count();
    }

    public function getWarehousesCount()
    {
        return DB::table($this->warehousesTable)->count();
    }

    public function updateCities()
    {
        $requestOptions = [
            'apiKey' => $this->apiKey,
            'modelName' => 'Address',
            'calledMethod' => 'getCities',
        ];

        $result = $this->client->request('POST', 'https://api.novaposhta.ua/v2.0/json/', [
            'body' => json_encode($requestOptions)
        ]);

        $cities = json_decode($result->getBody()->getContents())->data;
        $data = [];
        foreach ($cities as $city) {
            $data[] = [
                'Description' => $city->Description,
                'DescriptionRu' => $city->DescriptionRu,
                'Ref' => $city->Ref,
                'Area' => $city->Area,
                'SettlementType' => $city->SettlementType,
                'SettlementTypeDescription' => $city->SettlementTypeDescription,
                'SettlementTypeDescriptionRu' => $city->SettlementTypeDescriptionRu,
                'CityID' => $city->CityID,
            ];
        }
        DB::table($this->citiesTable)->delete();
        DB::table($this->citiesTable)->insert($data);
        return ['status' => 1];
    }

    public function updateWarehouses()
    {
        $requestOptions = [
            'apiKey' => $this->apiKey,
            'modelName' => 'Address',
            'calledMethod' => 'getWarehouses',
        ];

        $result = $this->client->request('POST', 'https://api.novaposhta.ua/v2.0/json/', [
            'body' => json_encode($requestOptions)
        ]);

        $warehouses = json_decode($result->getBody()->getContents())->data;
        $data = [];
        foreach ($warehouses as $warehouse) {
            $data[] = [
                'Description' => $warehouse->Description,
                'DescriptionRu' => $warehouse->DescriptionRu,
                'ShortAddress' => $warehouse->ShortAddress,
                'ShortAddressRu' => $warehouse->ShortAddressRu,
                'TypeOfWarehouse' => $warehouse->TypeOfWarehouse,
                'Ref' => $warehouse->Ref,
                'TotalMaxWeightAllowed' => $warehouse->TotalMaxWeightAllowed,
                'CityRef' => $warehouse->CityRef,
                'CityDescription' => $warehouse->CityDescription,
                'CityDescriptionRu' => $warehouse->CityDescriptionRu,
            ];
        }
        DB::table($this->warehousesTable)->delete();
        DB::table($this->warehousesTable)->insert($data);
        return ['status' => 1];
    }

    public function getCity($cityName)
    {
        return DB::table($this->citiesTable)->where('DescriptionRu', 'like', $cityName . '%')->take(11)->get();
    }

    public function getCityByRef($cityRef)
    {
        return DB::table($this->citiesTable)->where('Ref', '=', $cityRef)->take(20)->get()->first();
    }

    public function getStores($cityRef)
    {
        $cart = $this->cart;
        $sessionId = session()->getId();
        $customerId = \Auth::id();
        $cart->updateItems($sessionId, $sessionId, $customerId);
        $checkIfBicycle = false;
        foreach ($cart->items() as $checkoutProduct){
            $checkProductCategory = ProductToCategory::where([
                'product_id' => $checkoutProduct->sku()->first()->product->id,
                'category_id' => '1'
            ])->first();
            if(!empty($checkProductCategory)){
                $checkIfBicycle = true;
            }
        }
        if($checkIfBicycle){
            return DB::table($this->warehousesTable)->where('CityRef', $cityRef)->where('TotalMaxWeightAllowed','=', 0)->where('Description', 'NOT LIKE', '%до 30 кг%')->get();
        }else{
            return DB::table($this->warehousesTable)->where('CityRef', '=', $cityRef)->get();
        }
    }

    public function getStoreByRef($storeRef)
    {
        return DB::table($this->warehousesTable)->where('Ref', '=', $storeRef)->get()->first();
    }
}
