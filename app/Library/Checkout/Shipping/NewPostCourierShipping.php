<?php

namespace App\Library\Checkout\Shipping;

use App\Entities\Shipping;
use App\Http\Controllers\Front\Checkout\Shipping\ShippingMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewPostCourierShipping implements ShippingMethod
{
    protected $code;

    protected $name;

    protected $address;

    protected $cost;

    protected $status;

    public function __construct(Request $request)
    {
        $this->code = 'courier';
        $this->name = __('checkout.library.shipping.courier.name');

        $this->address = $request->input('address', Auth::check() ? Auth::user()->address : '');

        $shipping = Shipping::where('code', '=', $this->code)->first();

        $this->status = isset($shipping) && isset($shipping->status) ? intval($shipping->status) : 0;
        $this->cost = isset($shipping) && isset($shipping->price) ? floatval($shipping->price) : 0;
    }

    public function show()
    {

        return view('front.checkout.shipping.new_post_courier', [
            'address' => $this->address
        ]);
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getCostTitle()
    {
        return $this->cost >= 0 ? '+' . $this->cost : '-' . $this->cost;
    }
}
