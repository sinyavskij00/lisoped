<?php

namespace App\Http\Controllers\Front\Checkout\Shipping;

interface ShippingMethod
{
    public function show();

    public function getCode();

    public function getName();

    public function getAddress();

    public function getCost();

    public function getCostTitle();

    public function getStatus();
}
