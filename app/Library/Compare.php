<?php

namespace App\Library;

use App\Entities\Product;
use App\Library\Compare\CompareDriver;

class Compare
{
    protected $driver;

    public function __construct(CompareDriver $driver)
    {
        $this->driver = $driver;
    }

    public function add(Product $product)
    {
        return $this->driver->add($product);
    }

    public function delete($productId)
    {
        return $this->driver->delete($productId);
    }

    public function isCompared(Product $product)
    {
        return $this->driver->isCompared($product);
    }

    public function getProducts()
    {
        return $this->driver->getProducts();
    }

    public function getQuantity()
    {
        return $this->driver->getProducts()->count();
    }
}
