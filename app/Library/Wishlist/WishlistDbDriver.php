<?php

namespace App\Library\Wishlist;

use App\Entities\Product;
use App\Entities\WishProduct;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class WishlistDbDriver implements WishlistDriver
{
    /**
     * @param Product $product
     * @return bool
     */
    public function add(Product $product) : bool
    {
        WishProduct::firstOrCreate([
            'customer_id' => Auth::id(),
            'product_id' => $product->id
        ]);
        return true;
    }

    public function delete(Product $product) : bool
    {
        return WishProduct::where('product_id', '=', $product->id)
            ->where('customer_id', '=', Auth::id())
            ->delete();
    }

    public function getWishlist() : Collection
    {
        return WishProduct::where('customer_id', '=', Auth::id())
            ->get(['product_id'])
            ->pluck('product_id');
    }
}