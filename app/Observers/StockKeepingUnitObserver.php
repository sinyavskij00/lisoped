<?php

namespace App\Observers;

use App\Entities\StockKeepingUnit;
use App\Entities\User;
use App\Entities\WishProduct;
use App\Notifications\ProductBecomeCheaper;

class StockKeepingUnitObserver
{
    /**
     * Handle the stock keeping unit "created" event.
     *
     * @param  \App\Entities\StockKeepingUnit $stockKeepingUnit
     * @return void
     */
    public function created(StockKeepingUnit $stockKeepingUnit)
    {
        //
    }

    /**
     * Handle the stock keeping unit "updated" event.
     *
     * @param  \App\Entities\StockKeepingUnit $stockKeepingUnit
     * @return void
     */
    public function updated(StockKeepingUnit $stockKeepingUnit)
    {
        $newPrice = floatval($stockKeepingUnit->getAttributeValue('price'));
        $oldPrice = floatval($stockKeepingUnit->getOriginal('price'));
        if ($oldPrice - $newPrice >= 10) {
            $productId = isset($stockKeepingUnit->product) ? $stockKeepingUnit->product->id : null;
            if (isset($productId)) {
                $wishProducts = WishProduct::where('product_id', '=', $productId)
                    ->get();
                if ($wishProducts->count() > 0) {
                    $customersIds = [];
                    foreach ($wishProducts as $wishProduct) {
                        $customersIds[$wishProduct->customer_id] = $wishProduct->customer_id;
                    }
                    $customers = User::whereIn('id', $customersIds)->get();
                    if ($customers->count() > 0) {
                        \Notification::send($customers, new ProductBecomeCheaper($stockKeepingUnit));
                        //todo it doesn't work via admin
                    }
                }
            }
        }
    }

    /**
     * Handle the stock keeping unit "deleted" event.
     *
     * @param  \App\Entities\StockKeepingUnit $stockKeepingUnit
     * @return void
     */
    public function deleted(StockKeepingUnit $stockKeepingUnit)
    {
        //
    }

    /**
     * Handle the stock keeping unit "restored" event.
     *
     * @param  \App\Entities\StockKeepingUnit $stockKeepingUnit
     * @return void
     */
    public function restored(StockKeepingUnit $stockKeepingUnit)
    {
        //
    }

    /**
     * Handle the stock keeping unit "force deleted" event.
     *
     * @param  \App\Entities\StockKeepingUnit $stockKeepingUnit
     * @return void
     */
    public function forceDeleted(StockKeepingUnit $stockKeepingUnit)
    {
        //
    }
}
