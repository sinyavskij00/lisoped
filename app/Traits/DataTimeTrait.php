<?php

namespace App\Traits;

use Carbon\Carbon;

trait DataTimeTrait {

    public function getDate($date)
    {
        return Carbon::parse($date)->format(config('app.date_format'));
    }

    public function getDateTime($date)
    {
        return Carbon::parse($date)->format(config('app.date_format').' '.config('app.time_format'));
    }
}
