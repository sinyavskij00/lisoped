<?php

namespace App\Console\Commands;

use App\Entities\CustomerGroupDescription;
use App\Entities\Language;
use App\Entities\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class CustomerExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:customers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export all customers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $customers = User::select([
            'id',
            'first_name',
            'last_name',
            'patronymic',
            'telephone',
            'email',
            'customer_group_id',
        ])->get();

        $language = Language::where('is_default', '=', 1)->first();
        $languageId = $language->id;
        config()->set('current_language_id', $languageId);
        $customerGroupDescriptions = CustomerGroupDescription::where('language_id', '=', $languageId)
            ->get();

        $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><customers></customers>");

        foreach ($customers as $customer) {
            $customerNode = $xml->addChild('customer');

            $customerGroup = '';
            if (isset($customer->customer_group_id)) {
                $description = $customerGroupDescriptions
                    ->where('customer_group_id', '=', $customer->customer_group_id)->first();
                $customerGroup = isset($description) ? $description->name : '';
            }
            $customer->customer_group = $customerGroup;

            $customer = $customer->toArray();
            foreach ($customer as $key => $value) {
                $customerNode->addChild($key, $value);
            }
        }

        $xml = $xml->asXML();
        Storage::disk('1C')->put('customer/lisoped_customer_export.xml', $xml);
    }
}
