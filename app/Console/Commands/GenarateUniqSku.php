<?php

namespace App\Console\Commands;

use App\Entities\StockKeepingUnit;
use Illuminate\Console\Command;

class GenarateUniqSku extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sku:generate {--regenerate=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate unique sku';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $regenerate = intval($this->option('regenerate'));
        $bar = $this->output->createProgressBar(StockKeepingUnit::count());
        StockKeepingUnit::chunk(1000, function ($skus) use ($bar, $regenerate) {
            foreach ($skus as $sku) {
                if (!empty($sku->sku) && $regenerate === 0) {
                    continue;
                }
                $sku->sku = $this->getUniqueSku();
                $sku->save();
                $bar->advance();
            }
        });
        $bar->finish();
        echo PHP_EOL;
    }

    public function getUniqueSku()
    {
        $sku = random_int(100000, 999999);
        if (StockKeepingUnit::where('sku', '=', $sku)->count() > 0) {
            return $this->getUniqueSku();
        }
        return $sku;
    }
}
