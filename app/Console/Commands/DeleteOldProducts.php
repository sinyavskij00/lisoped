<?php

namespace App\Console\Commands;

use App\Entities\Product;
use App\Entities\StockKeepingUnit;
use Illuminate\Console\Command;

class DeleteOldProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete-old-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete old products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        config()->set('current_language_id', 1);
        //year 2015 id=4, year 2016 id=1
        $skus = StockKeepingUnit::whereIn('year_id', [1, 4])->with('product')->get();
        $productsIds = [];
        $skuIds = [];
        foreach ($skus as $sku) {
            $productsIds[] = $sku->product_id;
            $skuIds[] = $sku->id;
        }
        Product::whereIn('id', $productsIds)->delete();
        StockKeepingUnit::whereIn('id', $skuIds)->delete();
        echo 'done' . PHP_EOL;
    }
}
