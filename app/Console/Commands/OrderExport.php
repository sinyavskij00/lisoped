<?php

namespace App\Console\Commands;

use App\Entities\Language;
use App\Entities\Order;
use App\Entities\OrderHistory;
use App\Entities\StockKeepingUnit;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use SimpleXMLElement;

class OrderExport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:order {order_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Order export for 1C';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $defaultLanguage = Language::where('is_default', '=', 1)->first();
        $languageId = isset($defaultLanguage) ? $defaultLanguage->id : 1;
        config()->set('current_language_id', $languageId);

        $orderId = $this->argument('order_id');

        $order = Order::find($orderId);

      //  dd($order->status->export_1c);
        if (!isset($order)) {
            return 'Order not defined';
        }

        $orderInfo = [
            'order_id' => $order->id,
            'customer_id' => $order->customer_id,
            'first_name' => $order->first_name,
            'last_name' => $order->last_name,
            'email' => $order->email,
            'telephone' => $order->telephone,
            'address' => $order->address,
            'payment_method' => $order->payment_method,
            'shipping_method' => $order->shipping_method,
            'order_status_id' => $order->order_status_id,
            'order_status' => isset($order->status) && isset($order->status->localDescription)
                ? $order->status->localDescription->name
                : '',
        ];
        $orderInfo['products'] = [];
        foreach ($order->skus as $orderSku) {
            $vendor = '';
            $sku = StockKeepingUnit::where('id', '=', $orderSku->sku_id)
                ->with('vendor')
                ->first();

            if (isset($sku) && isset($sku->vendor)) {
                $vendor = $sku->vendor->name;
            }
            $orderInfo['products'][] = [
                'id' => $orderSku->sku_id,
                'name' => $orderSku->name,
                'sku' => $orderSku->sku,
                'vendor_sku' => $orderSku->vendor_sku,
                'color' => $orderSku->color,
                'year' => $orderSku->year,
                'size' => $orderSku->size,
                'quantity' => $orderSku->quantity,
                'price' => $orderSku->price,
                'vendor_price' => $orderSku->vendor_price,
                'prepayment' => $orderSku->prepayment,
                'vendor_ttn' => $orderSku->ttn,
                'vendor' => $vendor
            ];
        }

        $histories = OrderHistory::with(['billingCart', 'billingAccount'])->where('order_id', '=', $order->id)->get();
        $orderInfo['histories'] = [];
        foreach ($histories as $history) {
            $orderInfo['histories'][] = $history->toArray();
        }

        $orderInfo['profitabilities'] = [];
        foreach ($order->profitabilities as $profitability) {
            $profitabilityItem = [
                'id' => $profitability->id,
                'order_id' => $profitability->order_id,
                'type_id' => $profitability->type_id,
                'type_name' => isset($profitability->type) ? $profitability->type->name : '',
                'price' => $profitability->price,
                'created_at' => $profitability->created_at,
                'updated_at' => $profitability->updated_at,
            ];
            $orderInfo['profitabilities'][] = $profitabilityItem;
        }

        $ordersNode = new SimpleXMLElement("<?xml version=\"1.0\"?><orders></orders>");

        $orderNode = $ordersNode->addChild('order');

        $orderNode->addChild('order_id', $orderInfo['order_id']);
        $orderNode->addChild('customer_id', $orderInfo['customer_id']);
        $orderNode->addChild('first_name', $orderInfo['first_name']);
        $orderNode->addChild('last_name', $orderInfo['last_name']);
        $orderNode->addChild('email', $orderInfo['email']);
        $orderNode->addChild('telephone', $orderInfo['telephone']);
        $orderNode->addChild('address', $orderInfo['address']);
        $orderNode->addChild('payment_method', $orderInfo['payment_method']);
        $orderNode->addChild('shipping_method', $orderInfo['shipping_method']);
        $orderNode->addChild('order_status_id', $orderInfo['order_status_id']);
        $orderNode->addChild('order_status', $orderInfo['order_status']);

        $productsNode = $orderNode->addChild('products');
        foreach ($orderInfo['products'] as $product) {
            $productNode = $productsNode->addChild('product');
            foreach ($product as $key => $value) {
                $productNode->addChild($key, $value);
            }
        }

        $historiesNode = $orderNode->addChild('histories');
        foreach ($orderInfo['histories'] as $history) {
            $historyNode = $historiesNode->addChild('history');
            foreach ($history as $key => $value) {
                if (is_array($value)) { //todo, sometimes value is array
                    continue;
                }
                $historyNode->addChild($key, $value);
            }
        }

        $profitabilitiesNode = $orderNode->addChild('profitabilities');
        foreach ($orderInfo['profitabilities'] as $profitability) {
            $profitabilityNode = $profitabilitiesNode->addChild('profitability');
            foreach ($profitability as $key => $value) {
                $profitabilityNode->addChild($key, $value);
            }
        }

        Storage::disk('1C')->put('orders/order_' . $orderId . '_' . $order->status->export_1c . '.xml', $ordersNode->asXML());
    }
}
