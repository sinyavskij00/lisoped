<?php

namespace App\Console\Commands;

use App\Entities\Language;
use App\Entities\Manufacturer;
use App\Entities\Product;
use App\Entities\Category;
use App\Services\FilterService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class FillCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:fill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate entities paths';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param FilterService $filterService
     * @return mixed
     */
    public function handle(FilterService $filterService)
    {
        $this->info(PHP_EOL . 'categories cache' . PHP_EOL);
        $bar = $this->output->createProgressBar(Category::count());

        $categoryPaths = [];
        Category::chunk(2000, function ($categories) use (&$categoryPaths, $filterService, &$bar) {
            foreach ($categories as $category) {
                $categoryPath = Cache::rememberForever('category_path_' . $category->id, function () use ($category) {
                    return $category->getPath();
                });
                $categoryPaths[$category->id] = $categoryPath;
            }
            $bar->advance();
        });
        $bar->finish();

        Cache::remember('slug_table', 100, function () {
            $slugs = [];
            foreach (Category::get(['slug', 'id']) as $category) {
                $slugs[] = [
                    'chunk' => 'category=' . $category->id,
                    'slug' => $category->slug
                ];
            }
            return collect($slugs);
        });

        //fill filter cache
        $this->info(PHP_EOL . 'filters cache' . PHP_EOL);
        $bar = $this->output->createProgressBar((Category::count() + Manufacturer::count()) * Language::count());
        $languages = Language::all();
        foreach ($languages as $language) {
            Config::set('current_language_id', $language->id);
            $filterService = app()->make(FilterService::class);
            foreach (Category::all() as $category) {
                $productIds = $category->showProducts()->get(['products.id'])->pluck('id')->toArray();
                $products = Product::whereIn('products.id', $productIds);
                $cacheKey = 'filter_for_category_' . $language->id . '_' . $category->id;
                $this->cacheFilter($cacheKey, $filterService, $products);
                $bar->advance();
            }

            foreach (Manufacturer::all() as $manufacturer) {
                $products = Product::where('status', '=', 1)
                    ->where('manufacturer_id', '=', $manufacturer->id);
                $cacheKey = 'filter_for_manufacturer_' . $language->id . '_' . $manufacturer->id;
                $this->cacheFilter($cacheKey, $filterService, $products);
                $bar->advance();
            }
        }
        $bar->finish();
        $this->line(PHP_EOL);
    }

    private function cacheFilter($cacheKey, $filterService, $products)
    {
        Cache::remember(
            $cacheKey,
            300,
            function () use ($filterService, $products) {
                return $filterService->getFilterByProducts($products);
            }
        );
    }
}
