<?php

namespace App\Console\Commands\DbUpdates;

use App\Entities\AttributeDescription;
use App\Entities\ProductAttribute;
use Illuminate\Console\Command;
use App\Traits\DbBatchTrait;

class UpdateAttributesSlug extends Command
{
    use DbBatchTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db-update:update-attributes-slug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update attributes slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Attributes
        $attributes = AttributeDescription::where('language_id', 1)->get();
        $attributeSlug = [];
        $existSlugs = [];
        foreach ($attributes as $ky => $attr) {
            $attributeSlug[] = [
                'id' => $attr['attribute_id'],
                'slug' => (in_array(str_slug($attr['name']), $existSlugs)
                    ? str_slug($attr['name']) . $ky.rand(1, 1000)
                    : str_slug($attr['name']))
            ];

            $existSlugs[] = str_slug($attr['name']);
        }

        $this->updateBatch('attributes', $attributeSlug, 'id');

        // Product attributes
        $productAttributes = ProductAttribute::get();
        $productAttributesSlug = [];
        foreach ($productAttributes as $attr) {
            $productAttributesSlug[] = [
                'id' => $attr['id'],
                'slug' => in_array(str_slug($attr['text']), $existSlugs) ? str_slug($attr['text']) . $ky : str_slug($attr['text'])
            ];
        }

        $this->updateBatch('product_attributes', $productAttributesSlug, 'id');
    }
}
