<?php

namespace App\Console;

use App\Console\Commands\FillCache;
use App\Console\Commands\GenerateSitemap;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        FillCache::class,
        GenerateSitemap::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         //$schedule->command('inspire')->everyMinute();
//         $schedule->command('cache:clear')->command('fill-cache')->hourly();
//         $schedule->command('generate-sitemap')->daily();

        $schedule->command('notify:review-product')->dailyAt('5:00');
        $schedule->command('notify:review-store')->dailyAt('5:10');
        $schedule->command('notify:forgot-cart')->dailyAt('5:20');
        $schedule->command('notify:happy-birthday')->dailyAt('5:30');
//        $schedule->command('import:xml')->dailyAt('3:30');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
