<?php

namespace App\Providers;

use App\Entities\HeaderBanner;
use App\Entities\Product;
use App\Entities\ProductAdvantage;
use App\Http\ViewComposers\SettingsComposer;
use App\Library\Checkout\Cart;
use App\Library\Compare;
use App\Library\Settings;
use App\Services\CatalogService;
use App\Services\WishListService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @param Settings $settings
     * @param CatalogService $catalogService
     * @return void
     */
    public function boot(Settings $settings, CatalogService $catalogService)
    {
        View::composer(['front.checkout.cart_list', 'front.header'], function ($view) {
            $view->with('cart', $this->app->make(Cart::class));
        });

        View::composer(['front.header', 'front.footer'], SettingsComposer::class);

        View::share('categoryTree', get_category_tree());

        View::composer(['front.modules.viewed'], function ($view) use ($catalogService) {
            $viewed = session('viewed', []);
            if (!empty($viewed)) {
                $viewed = Product::whereIn('products.id', $viewed)
                    ->where('status', '=', 1)
                    ->select(CatalogService::PRODUCT_ITEM_SELECTED_FIELDS);
                $viewed = $catalogService->addProductInfo($viewed);
                $viewed->with(CatalogService::PRODUCT_ITEM_RELATIONS);
            }
            $view->with('viewed', $viewed->get());
        });

        View::share('catalogService', $catalogService);
        $compare = app()->make(Compare::class);
        View::share('compare', $compare);

        $telephones = $settings->getByKey('telephones');
        $telephones = !empty($telephones) ? $telephones : [];
        View::share('siteTelephones', $telephones);

        $workTimes = $settings->getByKey('work_time');
        $workTimes = !empty($workTimes) ? $workTimes : [];
        View::share('workTimes', $workTimes);

        View::composer(['front.footer'], function ($view) use ($telephones) {
            $companyJsonLd = [
                '@context' => 'http://schema.org',
                '@type' => 'Organization',
                'name' => 'Lisoped',
                'email' => config('admin_email'),
                'telephone' => !empty($telephones[0]) ? $telephones[0] : null,
                'url' => url()->current(),
                'address' => []
            ];
            $addresses = [__('information.contacts.address1'), __('information.contacts.address2')];
            foreach ($addresses as $address) {
                $companyJsonLd['address'][] = [
                    '@type' => 'PostalAddress',
                    'streetAddress' => $address
                ];
            }
            $view->with('companyJsonLd', $companyJsonLd);
        });

        View::composer(['front.footer'], function ($view) {
            $searchJsonLd = [
                "@context" => "http://schema.org",
                "@type" => "WebSite",
                "url" => url()->current(),
                'potentialAction' => [
                    '@type' => 'SearchAction',
                    'target' => route('search') . '?query={query}',
                    'query-input' => 'required name=query'
                ]
            ];

            $view->with('searchJsonLd', $searchJsonLd);
        });

        View::composer('front.catalog.current_sku_content', function ($view) {
            $productAdvantages = ProductAdvantage::with('localDescription')
                ->where('status', '=', 1)
                ->get();
            $view->with('productAdvantages', $productAdvantages);
        });

        $siteSettings = $settings->getByCode('site_settings');
        if (!empty($siteSettings['link_facebook'])) {
            View::share('linkFacebook', $siteSettings['link_facebook']);
        }
        if (!empty($siteSettings['link_facebook'])) {
            View::share('linkYoutube', $siteSettings['link_youtube']);
        }
        if (!empty($siteSettings['link_twitter'])) {
            View::share('linkTwitter', $siteSettings['link_twitter']);
        }
        if (!empty($siteSettings['link_instagram'])) {
            View::share('linkInstagram', $siteSettings['link_instagram']);
        }

        View::composer(['front.header'], function ($view) {
            $headerBanner = HeaderBanner::with('localDescription')
                ->where('status', '=', 1)
                ->first();
            $view->with('headerBanner', $headerBanner);
        });

        View::composer([
            'front.header',
            'front.catalog.product_item',
            'front.catalog.current_sku_content'
        ], function ($view) {
            $view->with('wishListService', app()->make(WishListService::class));
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
