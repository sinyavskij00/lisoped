<?php

namespace App\Providers;

use App\Entities\StockKeepingUnit;
use App\Library\Checkout\Cart;
use App\Library\Checkout\Payment\BankCartPayment;
use App\Library\Checkout\Payment\LiqPayPayment;
use App\Library\Checkout\Payment\ReceiptPayment;
use App\Library\Checkout\Shipping\NewPostCourierShipping;
use App\Library\Checkout\Shipping\NewPostOfficeShipping;
use App\Library\Compare\CompareDriver;
use App\Library\Compare\CompareSessionDriver;
use App\Library\Settings;
use App\Library\Wishlist\WishlistDbDriver;
use App\Library\Wishlist\WishlistDriver;
use App\Library\Wishlist\WishlistSessionDriver;
use App\Observers\StockKeepingUnitObserver;
use App\Services\CatalogService;
use App\Services\WishListService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param CatalogService $catalogService
     * @param Settings $settings
     * @return void
     */
    public function boot(CatalogService $catalogService, Settings $settings)
    {
        app('view')->prependNamespace('admin', resource_path('views/admin'));

        Schema::defaultStringLength(191);
        StockKeepingUnit::observe(StockKeepingUnitObserver::class);
        $siteSettings = $settings->getByCode('site_settings');
        if (!empty($siteSettings['admin_email'])) {
            config()->set('app.admin_email', $siteSettings['admin_email']);
        }
        $this->app->singleton(Settings::class, function ($app) use ($settings) {
            return $settings;
        });

        $this->app->singleton(Cart::class, function ($app) use ($catalogService) {
            return new Cart($catalogService);
        });

        $this->app->tag([
            NewPostOfficeShipping::class,
            NewPostCourierShipping::class
        ], ['shipping_methods']);

        $this->app->tag([
            LiqPayPayment::class,
            BankCartPayment::class,
            ReceiptPayment::class,
        ], ['payment_methods']);

        //wishlist
        $this->app->bind(WishlistDriver::class, function () {
            return app()->make(Auth::check() ? WishlistDbDriver::class : WishlistSessionDriver::class);
        });

        $this->app->singleton(WishListService::class, function ($app) {
            $driver = $app->make(WishlistDriver::class);
            return new WishListService($driver);
        });

        $this->app->bind(CompareDriver::class, function () {
            return app()->make(CompareSessionDriver::class);
        });

        $this->app->when(\LiqPay::class)->needs('$public_key')->give(config('app.liqpay.public_key'));
        $this->app->when(\LiqPay::class)->needs('$private_key')->give(config('app.liqpay.private_key'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
