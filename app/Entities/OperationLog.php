<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Entities\OperationLog
 *
 * @property int $id
 * @property int $user_id
 * @property string $path
 * @property string $method
 * @property string $ip
 * @property string $input
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entities\Administrator $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog whereInput($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OperationLog whereUserId($value)
 * @mixin \Eloquent
 */
class OperationLog extends BaseModel
{
    protected $fillable = ['user_id', 'path', 'method', 'ip', 'input'];

    public static $methodColors = [
        'GET'    => 'green',
        'POST'   => 'yellow',
        'PUT'    => 'blue',
        'DELETE' => 'red',
    ];

    public static $methods = [
        'GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'PATCH',
        'LINK', 'UNLINK', 'COPY', 'HEAD', 'PURGE',
    ];

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $connection = config('admin.database.connection') ?: config('database.default');

        $this->setConnection($connection);

        $this->setTable(config('admin.database.operation_log_table'));

        parent::__construct($attributes);
    }

    /**
     * Log belongs to users.
     *
     * @return BelongsTo
     */
    public function user() : BelongsTo
    {
        return $this->belongsTo(Administrator::class);
    }
}
