<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;

/**
 * App\Entities\Question
 *
 * @property int $id
 * @property int|null $question_group_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\QuestionDescription[] $descriptions
 * @property-read \App\Entities\QuestionDescription $localDescription
 * @property-read \App\Entities\QuestionGroup|null $questionGroup
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Question whereQuestionGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Question whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Question newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Question query()
 */
class Question extends BaseModel
{
    protected $fillable = ['question_group_id'];

    public function descriptions()
    {
        return $this->hasMany(QuestionDescription::class, 'question_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(QuestionDescription::class, 'question_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['answer' => '', 'question_text' => '']);
    }

    public function questionGroup()
    {
        return $this->belongsTo(QuestionGroup::class, 'question_group_id', 'id');
    }
}
