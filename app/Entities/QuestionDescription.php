<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\QuestionDescription
 *
 * @property int $id
 * @property int $question_id
 * @property int $language_id
 * @property string $answer
 * @property string $question_text
 * @property-read \App\Entities\Question $question
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionDescription whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionDescription whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionDescription whereQuestionText($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\QuestionDescription query()
 */
class QuestionDescription extends Model
{
    protected $fillable = ['language_id', 'question_id', 'answer', 'question_text'];

    public $timestamps = false;

    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id', 'id');
    }
}
