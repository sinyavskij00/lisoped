<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\CustomerGroupDescription
 *
 * @property int $id
 * @property int $customer_group_id
 * @property int $language_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroupDescription whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroupDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroupDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroupDescription whereName($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroupDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroupDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroupDescription query()
 */
class CustomerGroupDescription extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'customer_group_id', 'language_id'];
}
