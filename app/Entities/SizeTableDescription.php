<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\SizeTableDescription
 *
 * @property int $id
 * @property int $table_id
 * @property int $language_id
 * @property string|null $title
 * @property string|null $text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTableDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTableDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTableDescription whereTableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTableDescription whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTableDescription whereTitle($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTableDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTableDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTableDescription query()
 */
class SizeTableDescription extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'table_id',
        'language_id',
        'title',
        'text',
    ];
}
