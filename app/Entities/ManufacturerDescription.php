<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\ManufacturerDescription
 *
 * @property int $id
 * @property int $manufacturer_id
 * @property int $language_id
 * @property string $name
 * @property string|null $description
 * @property string|null $meta_h1
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property-read \App\Entities\Manufacturer $manufacturer
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription whereManufacturerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription whereMetaH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription whereName($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ManufacturerDescription query()
 */
class ManufacturerDescription extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'manufacturer_id',
        'language_id',
        'name',
        'description',
        'meta_h1',
        'meta_description',
        'meta_keywords',
        'meta_title'
    ];

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class, 'manufacturer_id', 'id');
    }

    public static function getManufacturersByName()
    {
        return ManufacturerDescription::select(
            'manufacturer_id',
            \DB::raw('LOWER(name) as name')
        )
            ->where(
                'language_id',
                config()->get('current_language_id')
            )
            ->get()
            ->pluck('manufacturer_id', 'name');
    }

    public static function getManufacturersBySlug()
    {
        return ManufacturerDescription::select(
            'manufacturer_id',
            \DB::raw('LOWER(name) as name')
        )
            ->where(
                'language_id',
                config()->get('current_language_id')
            )
            ->get()
            ->pluck('manufacturer_id', 'name');
    }
}
