<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Vendor
 *
 * @property int $id
 * @property string $name
 * @property string $telephone_1
 * @property string $telephone_2
 * @property string $email
 * @property string|null $address
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereTelephone1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereTelephone2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $site
 * @property string|null $comment
 * @property string|null $email2
 * @property string|null $email3
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereEmail2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereEmail3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Vendor query()
 */
class Vendor extends Model
{
    protected $fillable = [
        'name',
        'telephone_1',
        'telephone_2',
        'email',
        'email2',
        'email3',
        'address',
        'comment',
        'site',
    ];

    public static function getVendorsByName()
    {
        return Vendor::select(
            'id',
            \DB::raw('LOWER(name) as name')
        )
            ->get()
            ->pluck('id', 'name');
    }
}
