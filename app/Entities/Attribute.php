<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

/**
 * App\Entities\Attribute
 *
 * @property int $id
 * @property int|null $attribute_group_id
 * @property int $is_display
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entities\AttributeGroup|null $attributeGroup
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\AttributeDescription[] $descriptions
 * @property-read mixed $name
 * @property-read \App\Entities\AttributeDescription $localDescription
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\AttributesValues[] $values
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Attribute whereAttributeGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Attribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Attribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Attribute whereIsDisplay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Attribute whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Attribute whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $slug
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Attribute whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Attribute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Attribute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Attribute query()
 */
class Attribute extends Model
{
    protected $fillable = [
        'sort',
        'attribute_group_id'
    ];

    public function setSlugAttribute($text)
    {
        $this->attributes['slug'] = str_slug($text);
    }

    public function descriptions()
    {
        return $this->hasMany(AttributeDescription::class, 'attribute_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(AttributeDescription::class, 'attribute_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['name' => '']);
    }
    public function description()
    {

        return $this->hasOne(AttributeDescription::class, 'attribute_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['name' => '']);
    }

    public function attributeGroup()
    {
        return $this->belongsTo(AttributeGroup::class, 'attribute_group_id', 'id');
    }

    public function productAttributes()
    {
        return $this->hasMany(ProductAttribute::class, 'attribute_id', 'id');
    }

    /************ ACCESSORS START ******************/
    public function getNameAttribute()
    {
        return $this->localDescription->name;
    }

    public static function getAttributesByName()
    {
        return AttributeDescription::select(
            'attribute_id',
            \DB::raw('LOWER(name) as name')
        )
            ->where(
                'language_id',
                config()->get('current_language_id')
            )
            ->get()
            ->pluck('attribute_id', 'name');
    }

    public function values()
    {
        return $this->hasMany(ProductAttribute::class, 'attribute_id', 'id')
            ->select('product_attributes.*', \DB::raw('(COUNT(*)) as products_count'))
            ->where('language_id', '=', config()->get('current_language_id'))
            ->groupBy('text');
    }
}
