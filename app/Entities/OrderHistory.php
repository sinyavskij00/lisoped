<?php

namespace App\Entities;

/**
 * App\Entities\OrderHistory
 *
 * @property int $id
 * @property int $order_id
 * @property int $order_status_id
 * @property int $notify
 * @property string|null $comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entities\Order $order
 * @property-read \App\Entities\OrderStatus $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereNotify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereOrderStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $admin_id
 * @property int|null $billing_cart_id
 * @property int|null $billing_account_id
 * @property string|null $public_comment
 * @property string|null $private_comment
 * @property-read \App\Entities\BillingAccount $billingAccount
 * @property-read \App\Entities\BillingCart $billingCart
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereBillingAccountId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereBillingCartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory wherePrivateComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory wherePublicComment($value)
 * @property int $notify_sms
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderHistory whereNotifySms($value)
 */
class OrderHistory extends BaseModel
{
    protected $fillable = [
        'order_id',
        'order_status_id',
        'sms_template_id',
        'notify',
        'notify_sms',
        'notify_viber',
        'send_check',
        'send_guarantee',
        'public_comment',
        'private_comment',
    ];

    public function routeNotificationFor()
    {
        return $this->order->telephone;
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function status()
    {
        return $this->hasOne(OrderStatus::class, 'id', 'order_status_id');
    }

    public function smsTemplate()
    {
        return $this->hasOne(SmsTemplate::class, 'id', 'sms_template_id');
    }

    public function billingCart()
    {
        return $this->hasOne(BillingCart::class, 'id', 'billing_cart_id');
    }

    public function billingAccount()
    {
        return $this->hasOne(BillingAccount::class, 'id', 'billing_account_id');
    }
}
