<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\OrderProfitabilityType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitabilityType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitabilityType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitabilityType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitabilityType whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitabilityType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitabilityType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitabilityType query()
 */
class OrderProfitabilityType extends Model
{
    protected $fillable = ['name'];
}
