<?php

namespace App\Entities;

use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Entities\NewsReview
 *
 * @property int $id
 * @property int $news_id
 * @property string $name
 * @property string|null $email
 * @property string $text
 * @property int $status
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Kalnoy\Nestedset\Collection|\App\Entities\NewsReview[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\NewsReviewImage[] $images
 * @property-read \App\Entities\News $news
 * @property-read \App\Entities\NewsReview|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview d()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\NewsReview query()
 */
class NewsReview extends BaseModel
{
    use NodeTrait;

    protected $fillable = ['text', 'name', 'email', 'status', 'news_id', 'parent_id'];

    public function news()
    {
        return $this->belongsTo(News::class, 'news_id', 'id');
    }

    public function images()
    {
        return $this->hasMany(NewsReviewImage::class, 'news_review_id', 'id');
    }
}
