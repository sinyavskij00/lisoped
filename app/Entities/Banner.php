<?php

namespace App\Entities;

/**
 * App\Entities\Banner
 *
 * @property int $id
 * @property string|null $background_image
 * @property string|null $main_image
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\BannerDescription[] $descriptions
 * @property-read \App\Entities\BannerDescription $localDescription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Banner whereBackgroundImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Banner whereMainImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Banner whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Banner whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Banner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Banner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Banner query()
 */
class Banner extends BaseModel
{
    protected $fillable = ['background_image', 'main_image', 'status'];

    public function descriptions()
    {
        return $this->hasMany(BannerDescription::class, 'banner_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(BannerDescription::class, 'banner_id', 'id')
            ->where('language_id', '=', config('current_language_id'))
            ->withDefault(['text' => '', 'link' => '', 'link_title' => '', 'link_subtitle' => '']);
    }
}
