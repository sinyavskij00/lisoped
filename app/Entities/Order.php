<?php

namespace App\Entities;

/**
 * App\Entities\Order
 *
 * @property int $id
 * @property string|null $invoice
 * @property int $customer_id
 * @property int $customer_group_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $telephone
 * @property string|null $address
 * @property string $payment_method
 * @property string $payment_code
 * @property string $shipping_method
 * @property string $shipping_code
 * @property string|null $comment
 * @property float $total
 * @property int $order_status_id
 * @property int $language_id
 * @property int $currency_id
 * @property string $currency_code
 * @property float $currency_value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entities\CustomerGroup $customerGroup
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\OrderHistory[] $histories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\OrderProfitability[] $profitabilities
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\OrderSku[] $skus
 * @property-read \App\Entities\OrderStatus $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\OrderTotal[] $totals
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereCurrencyCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereCurrencyValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereInvoice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereOrderStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order wherePaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereShippingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereShippingMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $order_invoice
 * @property float $sub_total
 * @property float $shipping_payment
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereOrderInvoice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereShippingPayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order whereSubTotal($value)
 * @property int $public_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order wherePublicId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Order query()
 */
class Order extends BaseModel
{
    protected $fillable = [
        'public_id',
        'customer_id',
        'customer_group_id',
        'first_name',
        'last_name',
        'email',
        'telephone',
        'address',
        'payment_method',
        'payment_code',
        'shipping_method',
        'shipping_code',
        'comment',
        'sub_total',
        'shipping_payment',
        'total',
        'order_status_id',
        'language_id',
        'currency_id',
        'currency_code',
        'currency_value',
    ];

    public function skus()
    {
        return $this->hasMany(OrderSku::class, 'order_id', 'id');
    }

    public function profitabilities()
    {
        return $this->hasMany(OrderProfitability::class, 'order_id', 'id');
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function status()
    {
        return $this->hasOne(OrderStatus::class, 'id', 'order_status_id');
    }

    public function customerGroup()
    {
        return $this->belongsTo(CustomerGroup::class, 'customer_group_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany(OrderHistory::class, 'order_id', 'id');
    }

    public function lastHistory()
    {
        return $this->hasOne(OrderHistory::class, 'order_id', 'id')
            ->orderBy('id', 'DESC');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'customer_id');
    }

    public function skusPrepaymentTotal()
    {
        return $this->hasOne(OrderSku::class, 'order_id', 'id')
            ->select('order_skus.order_id', \DB::raw('SUM(prepayment) as total_prepayment'));
    }

    /***** ACCESSORS START ******/

    public function getIsFinishedAttribute()
    {
        return isset($this->status) && $this->status->is_finished === 1;
    }
}
