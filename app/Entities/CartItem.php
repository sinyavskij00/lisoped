<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\CartItem
 *
 * @property int $id
 * @property int|null $customer_id
 * @property int $sku_id
 * @property string $session_id
 * @property int $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entities\User|null $customer
 * @property-read \App\Entities\StockKeepingUnit $sku
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CartItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CartItem whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CartItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CartItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CartItem whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CartItem whereSkuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CartItem whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CartItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CartItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CartItem query()
 */
class CartItem extends Model
{
    protected $fillable = ['customer_id', 'sku_id', 'quantity', 'session_id'];

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function sku()
    {
        return $this->hasOne(StockKeepingUnit::class, 'id', 'sku_id');
    }
}
