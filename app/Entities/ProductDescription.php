<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\ProductDescription
 *
 * @property int $id
 * @property int $product_id
 * @property int $language_id
 * @property string $name
 * @property string|null $description
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property-read \App\Entities\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription whereProductId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductDescription query()
 */
class ProductDescription extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'product_id',
        'language_id',
        'name',
        'description',
        'meta_description',
        'meta_keywords',
        'meta_title'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public static function getProductsByName()
    {
        return ProductDescription::select(
            'product_id',
            \DB::raw('LOWER(name) as name')
        )
            ->where(
                'language_id',
                config()->get('current_language_id')
            )
            ->get()
            ->pluck('product_id', 'name');
    }
}
