<?php

namespace App\Entities;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;

/**
 * App\Entities\Language
 *
 * @property int $id
 * @property string $title
 * @property string|null $image
 * @property string $slug
 * @property int $is_default
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Language whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Language whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Language whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Language whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Language whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Language whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Language newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Language query()
 */
class Language extends BaseModel
{
    protected $fillable = [
        'title', 'slug', 'image'
    ];

    public function makeDefault()
    {
        Language::where('is_default', '=', 1)->update(array('is_default' => 0));
        $this->is_default = 1;
        $this->save();
    }
}
