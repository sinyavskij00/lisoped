<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;
use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Entities\Category
 *
 * @property int $id
 * @property string $slug
 * @property string|null $image
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Kalnoy\Nestedset\Collection|\App\Entities\Category[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\CategoryDescription[] $descriptions
 * @property-read \App\Entities\CategoryDescription $localDescription
 * @property-read \App\Entities\Category|null $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category d()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Category query()
 */
class Category extends BaseModel
{
    use NodeTrait;

    protected $fillable = [
        'image',
        'icon',
        'parent_id',
        'slug',
        'layout_type'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }

    public function showProducts()
    {
        return $this->belongsToMany(
            Product::class,
            'product_to_category',
            'category_id',
            'product_id'
        );
    }

    public function descriptions()
    {
        return $this->hasMany(CategoryDescription::class, 'category_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(CategoryDescription::class, 'category_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault($this->defaultLocalDescription);
    }

    public function description()
    {
        return $this->hasOne(CategoryDescription::class, 'category_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault($this->defaultLocalDescription);
    }

    public function getPath(): string
    {
        return ($this->parent ? $this->parent->getPath() . '/' : '') . $this->slug;
    }

    public function categoryAttributes()
    {
        return $this->belongsToMany(
            Attribute::class,
            'category_attributes',
            'category_id',
            'attribute_id'
        );
    }
}
