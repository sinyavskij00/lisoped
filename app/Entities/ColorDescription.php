<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\ColorDescription
 *
 * @property int $id
 * @property int $color_id
 * @property int $language_id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ColorDescription whereColorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ColorDescription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ColorDescription whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ColorDescription whereName($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ColorDescription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ColorDescription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ColorDescription query()
 */
class ColorDescription extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'language_id',
        'color_id'
    ];

    public static function getColorsByName()
    {
        return ColorDescription::select(
            'color_id',
            \DB::raw('LOWER(name) as name')
        )
            ->where(
                'language_id',
                config()->get('current_language_id')
            )
            ->get()
            ->pluck('color_id', 'name');
    }
}
