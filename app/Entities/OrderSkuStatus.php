<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderSkuStatus extends Model
{
    protected $fillable = [
        'color',
        'name'
    ];
}
