<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use App\Traits\DataTimeTrait;

/**
 * App\Entities\User
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $patronymic
 * @property string $telephone
 * @property string $address
 * @property string $city_ref
 * @property string $store_ref
 * @property int|null $customer_group_id
 * @property string $email
 * @property string $password
 * @property int $subscribe
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entities\CustomerGroup|null $customerGroup
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Order[] $orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\WishProduct[] $wishProducts
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereCityRef($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User wherePatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereStoreRef($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereSubscribe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $birthday
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\User query()
 */
class User extends Authenticatable
{
    use DataTimeTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'patronymic',
        'email',
        'birthday',
        'birthday_updated_at',
        'telephone',
        'subscribe',
        'address',
        'password',
        'city_ref',
        'store_ref'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getCreatedAtAttribute($value)
    {
        return $this->getDateTime($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return $this->getDateTime($value);
    }

    public function customerGroup()
    {
        return $this->belongsTo(CustomerGroup::class, 'customer_group_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'customer_id', 'id');
    }

    public function wishProducts()
    {
        return $this->hasMany(WishProduct::class, 'customer_id', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /***********************  ACCESSORS ***********************************************/

    public function getHasPurchasedGoodsAttribute()
    {
        $finishedStatus = OrderStatus::where('is_finished', '=', 1)->first();
        $customerId = $this->id;

        $orderIds = [];
        if (isset($finishedStatus)) {
            $orderIds = Order::where('customer_id', '=', $customerId)
                ->where('order_status_id', '=', $finishedStatus->id)
                ->get(['id'])
                ->pluck('id')
                ->toArray();
        }
        $skuIds = OrderSku::whereIn('order_id', $orderIds)->get(['sku_id'])->pluck('sku_id')->toArray();
        $productIds = StockKeepingUnit::whereIn('id', $skuIds)
            ->get(['product_id'])
            ->pluck('product_id')
            ->toArray();
        return count($productIds) > 0;
    }
}
