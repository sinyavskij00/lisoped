<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;

/**
 * App\Entities\AttributeGroup
 *
 * @property int $id
 * @property int $sort
 * @property string|null $image
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Attribute[] $attributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\AttributeGroupDescription[] $descriptions
 * @property-read \App\Entities\AttributeGroupDescription $localDescription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroup whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroup whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\AttributeGroup query()
 */
class AttributeGroup extends BaseModel
{
    protected $fillable = [
        'id',
        'sort',
        'image'
    ];

    public function descriptions()
    {
        return $this->hasMany(AttributeGroupDescription::class, 'attribute_group_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(AttributeGroupDescription::class, 'attribute_group_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['name' => '']);
    }

    public function attributes()
    {
        return $this->hasMany(Attribute::class, 'attribute_group_id', 'id');
    }
}
