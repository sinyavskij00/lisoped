<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;

/**
 * App\Entities\CustomerGroup
 *
 * @property int $id
 * @property int $is_default
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\CustomerGroupDescription[] $descriptions
 * @property-read \App\Entities\CustomerGroupDescription $localDescription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroup whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\CustomerGroup query()
 */
class CustomerGroup extends BaseModel
{
    protected $fillable = ['is_default'];

    public function descriptions()
    {
        return $this->hasMany(CustomerGroupDescription::class, 'customer_group_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(CustomerGroupDescription::class, 'customer_group_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['name' => '']);
    }

    public function description()
    {
        return $this->hasOne(CustomerGroupDescription::class, 'customer_group_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['name' => '']);
    }

    public function makeDefault()
    {
        CustomerGroup::where('is_default', '=', 1)->update(['is_default' => 0]);
        $this->is_default = 1;
        $this->save();
    }
}
