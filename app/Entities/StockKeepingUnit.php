<?php

namespace App\Entities;

/**
 * App\Entities\StockKeepingUnit
 *
 * @property int $id
 * @property int|null $product_id
 * @property float $price
 * @property float $vendor_price
 * @property int|null $vendor_id
 * @property string $sku
 * @property string|null $manufacturer_sku
 * @property int $is_main
 * @property int $size_id
 * @property int $color_id
 * @property int $year_id
 * @property int $quantity
 * @property string $model
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entities\Color $color
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\SkuDiscount[] $discounts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\UnitImage[] $images
 * @property-read \App\Entities\Product|null $product
 * @property-read \App\Entities\Size $size
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\SkuSpecial[] $specials
 * @property-read \App\Entities\Vendor|null $vendor
 * @property-read \App\Entities\Year $year
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereColorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereIsMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereManufacturerSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereSizeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereVendorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereVendorPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereYearId($value)
 * @mixin \Eloquent
 * @property string|null $vendor_sku
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit whereVendorSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\StockKeepingUnit query()
 */
class StockKeepingUnit extends BaseModel
{
    protected $fillable = [
        'price',
        'vendor_price',
        'vendor_id',
        'sku',
        'vendor_sku',
        'model',
        'is_main',
        'quantity',
        'product_id',
        'color_id',
        'size_id',
        'year_id'
    ];

    public function vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor_id', 'id');
    }

    public function images()
    {
        return $this->hasMany(UnitImage::class, 'unit_id', 'id');
    }

    public function color()
    {
        return $this->hasOne(Color::class, 'id', 'color_id');
    }

    public function year()
    {
        return $this->hasOne(Year::class, 'id', 'year_id');
    }

    public function size()
    {
        return $this->hasOne(Size::class, 'id', 'size_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function specials()
    {
        return $this->hasMany(SkuSpecial::class, 'unit_id', 'id');
    }

    public function activeSpecials()
    {
        $now = now();
        return $this->specials()
            ->where('date_start', '<=', $now)
            ->where(function($where) use($now) {
                $where->where('date_end', '>=', $now)
                    ->orWhere('date_end', '=', '0000-00-00 00:00:00');
            })
            ->orderBy('priority', 'desc');
    }

    public function currentActiveSpecials()
    {
        $customerGroupId = config('current_customer_group_id');
        return $this->activeSpecials()->where('customer_group_id', '=', $customerGroupId);
    }

    public function discounts()
    {
        return $this->hasMany(SkuDiscount::class, 'unit_id', 'id');
    }

    public function activeDiscounts()
    {
        $now = now();
        return $this->discounts()
            ->where('date_start', '<=', $now)
            ->where(function($where) use($now) {
                $where->where('date_end', '>=', $now)
                    ->orWhere('date_end', '=', '0000-00-00 00:00:00');
            })
            ->orderBy('priority', 'desc');
    }

    public function currentActiveDiscounts()
    {
        return $this->activeDiscounts()
            ->where('customer_group_id', '=', config('current_customer_group_id'));
    }

    public static function getCountVendorUnit($product)
    {
        return StockKeepingUnit::where([
            'vendor_id' => $product['vendor_id'],
            'vendor_sku' => $product['vendor_sku']
        ])->count();
    }

    public static function getSkuByVendorSku()
    {
        $units = [];
        foreach (StockKeepingUnit::get() as $unit) {
            $units[$unit->vendor_sku] = [
                'id' => $unit['id'],
                'vendor_id' => $unit['vendor_id'],
                'product_id' => $unit['product_id']
            ];
        }

        return $units;
    }
}
