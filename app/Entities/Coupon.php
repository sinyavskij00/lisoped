<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\Coupon
 *
 * @property int $id
 * @property string $title
 * @property string $code
 * @property int $type
 * @property float $sum
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Coupon query()
 */
class Coupon extends Model
{
    protected $fillable = ['title', 'code', 'type', 'sum', 'status'];
}
