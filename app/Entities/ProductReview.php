<?php

namespace App\Entities;

/**
 * App\Entities\ProductReview
 *
 * @property int $id
 * @property int $product_id
 * @property int $status
 * @property int $rating
 * @property string $name
 * @property string $email
 * @property string $text
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\ProductReviewImage[] $images
 * @property-read \App\Entities\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductReview query()
 */
class ProductReview extends BaseModel
{
    protected $fillable = ['product_id', 'status', 'name', 'text', 'email', 'rating'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function images()
    {
        return $this->hasMany(ProductReviewImage::class, 'product_review_id', 'id');
    }
}
