<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\OrderProfitability
 *
 * @property int $id
 * @property int $order_id
 * @property int $type_id
 * @property float $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Entities\Order $order
 * @property-read \App\Entities\OrderProfitabilityType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitability whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitability whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitability whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitability wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitability whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitability whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitability newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitability newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\OrderProfitability query()
 */
class OrderProfitability extends Model
{
    protected $fillable = ['order_id', 'type_id', 'price'];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function type()
    {
        return $this->hasOne(OrderProfitabilityType::class, 'id', 'type_id');
    }
}
