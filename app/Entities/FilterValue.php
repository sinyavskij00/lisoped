<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FilterValue extends Model
{
    public $timestamps = false;

    protected $fillable = ['filter_id', 'slug', 'sort'];

    public function products()
    {
        return $this->belongsToMany(
            Product::class,
            'filter_value_to_product',
            'filter_value_id',
            'product_id'
        );
    }

    public function filter()
    {
        return $this->belongsTo(Filter::class, 'filter_id', 'id');
    }

    public function descriptions()
    {
        return $this->hasMany(FilterValueDescription::class, 'filter_value_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(FilterValueDescription::class, 'filter_value_id', 'id')
            ->where('language_id', '=', config('current_language_id'))
            ->withDefault(['name' => '', 'filter_name' => '']);
    }
}
