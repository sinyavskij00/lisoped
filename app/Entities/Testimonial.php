<?php

namespace App\Entities;

use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Entities\Testimonial
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string $text
 * @property int $rating
 * @property int $status
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Kalnoy\Nestedset\Collection|\App\Entities\Testimonial[] $children
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\TestimonialImage[] $images
 * @property-read \App\Entities\Testimonial|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial d()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Testimonial query()
 */
class Testimonial extends BaseModel
{
    use NodeTrait;

    protected $fillable = ['text', 'name', 'email', 'status', 'rating', 'parent_id'];

    public function images()
    {
        return $this->hasMany(TestimonialImage::class, 'testimonial_id', 'id');
    }
}
