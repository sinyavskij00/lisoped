<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\SiteSetting
 *
 * @property int $id
 * @property string $code
 * @property string $key
 * @property string $value
 * @property int $serialized
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SiteSetting whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SiteSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SiteSetting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SiteSetting whereSerialized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SiteSetting whereValue($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SiteSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SiteSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SiteSetting query()
 */
class SiteSetting extends Model
{
    public $timestamps = false;

    protected $fillable = ['code', 'key', 'value', 'serialized'];

    public static function updateByCode($code, $data)
    {
        self::where('code', '=', $code)->delete();
        $settings = [];
        foreach ($data as $key => $value) {
            $settings[] = [
                'code' => $code,
                'key' => $key,
                'value' => is_array($value) ? serialize($value) : $value,
                'serialized' => is_array($value) ? 1 : 0
            ];
        }
        self::insert($settings);
    }
}
