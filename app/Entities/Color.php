<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;

/**
 * App\Entities\Color
 *
 * @property int $id
 * @property string $image
 * @property string $anchor
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\ColorDescription[] $descriptions
 * @property-read \App\Entities\ColorDescription $localDescription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Color whereAnchor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Color whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Color whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Color whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Color whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Color newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Color newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Color query()
 */
class Color extends BaseModel
{
    protected $fillable = ['image', 'anchor'];

    public function descriptions()
    {
        return $this->hasMany(ColorDescription::class, 'color_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(ColorDescription::class, 'color_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['name' => '']);
    }
}
