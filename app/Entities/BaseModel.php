<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DataTimeTrait;

/**
 * App\Entities\BaseModel
 *
 * @property-read mixed $created_at
 * @property-read mixed $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BaseModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BaseModel query()
 * @mixin \Eloquent
 */
class BaseModel extends Model
{
    use DataTimeTrait;

    public $defaultLocalDescription = [
        'meta_h1' => '---',
        'name' => '---',
        'description' => '---',
        'meta_title' => '',
        'meta_keywords' => '',
        'meta_description' => '',
    ];

    public function getCreatedAtAttribute($value)
    {
        return $this->getDateTime($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return $this->getDateTime($value);
    }
}
