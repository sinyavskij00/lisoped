<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\SizeTable
 *
 * @property int $id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\SizeTableDescription[] $descriptions
 * @property-read \App\Entities\SizeTableDescription $localDescription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTable whereId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SizeTable query()
 */
class SizeTable extends Model
{
    public $timestamps = false;

    public function descriptions()
    {
        return $this->hasMany(SizeTableDescription::class, 'table_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(SizeTableDescription::class, 'table_id', 'id')
            ->where('language_id', '=', config('current_language_id'))
            ->withDefault(['title' => '', 'text' => '']);
    }

    public static function getSizeTablesById()
    {
        return SizeTable::get()->pluck('id')->toArray();
    }
}
