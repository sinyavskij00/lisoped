<?php

namespace App\Entities;

/**
 * App\Entities\BillingCart
 *
 * @property int $id
 * @property string $title
 * @property string $name
 * @property string $number
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingCart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingCart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingCart whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingCart whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingCart whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingCart whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingCart newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingCart newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\BillingCart query()
 */
class BillingCart extends BaseModel
{
    protected $fillable = ['title', 'name', 'number'];
}
