<?php

namespace App\Entities;

/**
 * App\Entities\Shipping
 *
 * @property int $id
 * @property string $code
 * @property float $price
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Shipping whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Shipping whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Shipping whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Shipping wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Shipping whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Shipping whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Shipping newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Shipping newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Shipping query()
 */
class Shipping extends BaseModel
{
    protected $fillable = ['price', 'code', 'status'];
}
