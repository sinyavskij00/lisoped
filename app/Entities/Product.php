<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

/**
 * App\Entities\Product
 *
 * @property int $id
 * @property string $slug
 * @property string|null $image
 * @property int $is_preorder
 * @property int $is_new
 * @property int $is_hit
 * @property int $is_our_choice
 * @property int $points
 * @property int $points_percent
 * @property int|null $category_id
 * @property int|null $manufacturer_id
 * @property int $status
 * @property int|null $min_weight
 * @property int|null $max_weight
 * @property int|null $min_height
 * @property int|null $max_height
 * @property int|null $gender
 * @property int|null $trace_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Product[] $activeRelated
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\ProductReview[] $activeReviews
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\AttributesValues[] $attributeValues
 * @property-read \App\Entities\Category|null $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\ProductDescription[] $descriptions
 * @property-read mixed $attribute_groups
 * @property-read mixed $colors
 * @property-read mixed $is_available
 * @property-read mixed $manufacturer_name
 * @property-read mixed $price
 * @property-read mixed $rating
 * @property-read mixed $special_price
 * @property-read mixed $years
 * @property-read \App\Entities\ProductDescription $localDescription
 * @property-read \App\Entities\StockKeepingUnit $mainSku
 * @property-read \App\Entities\Manufacturer|null $manufacturer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Product[] $related
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\ProductReview[] $reviews
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\ProductReward[] $rewards
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\StockKeepingUnit[] $skus
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\ProductVideo[] $videos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereIsHit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereIsNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereIsOurChoice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereIsPreorder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereManufacturerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereMaxHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereMaxWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereMinHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereMinWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product wherePointsPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereTraceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $attribute_value_1
 * @property int|null $attribute_value_2
 * @property int|null $attribute_value_3
 * @property-read mixed $product_item_attributes
 * @property-read mixed $sizes_range
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereAttributeValue1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereAttributeValue2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product whereAttributeValue3($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\SizeTable[] $sizeTables
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Product query()
 */
class Product extends BaseModel
{
    protected $fillable = [
        'category_id',
        'manufacturer_id',
        'points',
        'points_percent',
        'slug',
        'is_preorder',
        'is_new',
        'is_hit',
        'is_our_choice',
        'status',
        'weight'
    ];

    public function descriptions()
    {
        return $this->hasMany(ProductDescription::class, 'product_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(ProductDescription::class, 'product_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault($this->defaultLocalDescription);
    }

    public function description()
    {
        return $this->hasOne(ProductDescription::class, 'product_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault($this->defaultLocalDescription);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function showCategories()
    {
        return $this->belongsToMany(
            Category::class,
            'product_to_category',
            'product_id',
            'category_id'
        );
    }

    public function getPath(): string
    {
        return ($this->category_id ? $this->category->getPath() . '/' : '') . $this->slug;
    }

    public function filterValues()
    {
        return $this->belongsToMany(
            FilterValue::class,
            'filter_value_to_product',
            'product_id',
            'filter_value_id'
        );
    }

    public function filterAttributes()
    {
        return $this->belongsToMany(
            Attribute::class,
            'product_attributes',
            'product_id',
            'attribute_id'
        );
    }

    public function sizeTables()
    {
        return $this->belongsToMany(
            SizeTable::class,
            'size_table_to_product',
            'product_id',
            'table_id'
        );
    }

    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class, 'manufacturer_id', 'id');
    }

    public function related()
    {
        return $this->belongsToMany(Product::class, 'product_related', 'product_id', 'related_id');
    }

    public function activeRelated()
    {
        return $this->belongsToMany(Product::class, 'product_related', 'product_id', 'related_id')
            ->where('status', '=', 1);
    }

    public function skus()
    {
        return $this->hasMany(StockKeepingUnit::class, 'product_id', 'id')
            ->orderBy('quantity', 'DESC');
    }

    public function skusProduct($where = [])
    {
        return $this->hasMany(StockKeepingUnit::class, 'product_id', 'id')
            ->where($where)->orderBy('quantity', 'DESC')->get();
    }

    public function mainSku()
    {
        return $this->hasOne(StockKeepingUnit::class, 'product_id', 'id')
            ->where('is_main', '=', 1);
    }

    public function reviews()
    {
        return $this->hasMany(ProductReview::class, 'product_id', 'id');
    }

    public function activeReviews()
    {
        return $this->hasMany(ProductReview::class, 'product_id', 'id')
            ->where('status', '=', 1);
    }

    public function rewards()
    {
        return $this->hasMany(ProductReward::class, 'product_id', 'id');
    }

    public function videos()
    {
        return $this->hasMany(ProductVideo::class, 'product_id', 'id');
    }

    public function productAttributes()
    {
        return $this->hasMany(ProductAttribute::class, 'product_id', 'id');
    }

    /***** ACCESSORS START ******/
    public function getImageAttribute($image)
    {
        if (is_null($image) && isset($this->mainSku) && isset($this->mainSku->images)) {
            $image = $this->mainSku->images->first();
            if (isset($image)) {
                return $image->image;
            }
        }

        return $image;
    }

    public function getRatingAttribute()
    {
        if ($this->activeReviews->count() > 0) {
            $rating = 0;
            foreach ($this->activeReviews as $review) {
                $rating += $review->rating;
            }
            $rating = (int)($rating / $this->activeReviews->count());
            return $rating;
        }
        return 0;
    }

    public function getColorsAttribute()
    {
        $colors = collect();
        foreach ($this->skus as $sku) {
            if ($sku->color) {
                $colors->push($sku->color);
            }
        }
        return $colors->unique();
    }

    public function getSizesRangeAttribute()
    {
        $sizes = [];
        foreach ($this->skus as $sku) {
            if (isset($sku->size) && isset($sku->size->value)) {
                $sizes[] = $sku->size->value;
            }
        }
        $sizes = array_unique($sizes);
        return $sizes;
    }

    public function getYearsAttribute()
    {
        $years = collect();
        $this->skus->each(function ($item) use ($years) {
            if ($item->year) {
                $years->push($item->year);
            }
        });
        return $years->unique();
    }

    public function getProductItemAttributesAttribute()
    {
        $languageId = config('current_language_id');
        $result = collect();
        if (isset($this->productAttributes) && $this->productAttributes->count() > 0) {
            return $this->productAttributes->where('language_id', '=', $languageId)->sortBy('sort')->take(3);
        }
        return $result;
    }
}
