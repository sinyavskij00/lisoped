<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SizeTableToProduct extends Model
{
    public $table = 'size_table_to_product';

    public $timestamps = false;

    protected $fillable = [
        'product_id',
        'table_id',
    ];

    public static function getSizeTablesByProductId()
    {
        $existSizeTable = [];
        $sizeTables = SizeTableToProduct::select('product_id', 'table_id')->get();
        foreach ($sizeTables as $size) {
           $existSizeTable[$size['product_id']][] = $size['table_id'];
        }

        return $existSizeTable;
    }
}
