<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\ProductVideo
 *
 * @property int $id
 * @property int $product_id
 * @property string $video
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductVideo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductVideo whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductVideo whereVideo($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductVideo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductVideo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\ProductVideo query()
 */
class ProductVideo extends Model
{
    public $timestamps = false;

    protected $fillable = ['product_id', 'video'];
}
