<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;

/**
 * App\Entities\Special
 *
 * @property int $id
 * @property string $image
 * @property int $status
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\SpecialDescription[] $descriptions
 * @property-read \App\Entities\SpecialDescription $localDescription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $date_start
 * @property string|null $date_end
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Special query()
 */
class Special extends BaseModel
{
    protected $fillable = ['image', 'status', 'slug', 'date_start', 'date_end'];

    public function getDateStartAttribute($value)
    {
        return $this->getDate($value);
    }

    public function getDateEndAttribute($value)
    {
        return $this->getDate($value);
    }

    public function descriptions()
    {
        return $this->hasMany(SpecialDescription::class, 'special_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(SpecialDescription::class, 'special_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault(['title' => '', 'text' => '']);
    }
}
