<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\SkuSpecial
 *
 * @property int $id
 * @property int $customer_group_id
 * @property int $unit_id
 * @property int $priority
 * @property float $price
 * @property string $date_start
 * @property string $date_end
 * @property-read \App\Entities\StockKeepingUnit $sku
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuSpecial whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuSpecial whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuSpecial whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuSpecial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuSpecial wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuSpecial wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuSpecial whereUnitId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuSpecial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuSpecial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\SkuSpecial query()
 */
class SkuSpecial extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'customer_group_id',
        'unit_id',
        'priority',
        'price',
        'date_start',
        'date_end'
    ];

    public function sku()
    {
        return $this->belongsTo(StockKeepingUnit::class, 'unit_id', 'id');
    }

    public static function getActiveSpecials($customerGroupId = false)
    {
        $now = now();
        $customerGroupId = $customerGroupId ? $customerGroupId : config('current_customer_group_id');

        return self::where('customer_group_id', '=', $customerGroupId)
            ->where('date_start', '<=', $now)
            ->where(function($where) use($now) {
                $where->where('date_end', '>=', $now)
                    ->orWhere('date_end', '=', '0000-00-00 00:00:00');
            })
            ->orderBy('priority', 'desc');
    }

    public static function getSkuSpecials()
    {
        return SkuSpecial::select(
            'unit_id', 'id'
        )
            ->get()
            ->pluck('id', 'unit_id');
    }
}
