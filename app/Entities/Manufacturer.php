<?php

namespace App\Entities;

use Illuminate\Support\Facades\Config;

/**
 * App\Entities\Manufacturer
 *
 * @property int $id
 * @property string|null $image
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\ManufacturerDescription[] $descriptions
 * @property-read mixed $name
 * @property-read \App\Entities\ManufacturerDescription $localDescription
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Manufacturer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Manufacturer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Manufacturer whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Manufacturer whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Manufacturer whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Manufacturer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Manufacturer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Manufacturer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\Manufacturer whereStatus($value)
 */
class Manufacturer extends BaseModel
{
    protected $fillable = [
        'image',
        'slug',
        'status'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'manufacturer_id', 'id');
    }

    public function descriptions()
    {
        return $this->hasMany(ManufacturerDescription::class, 'manufacturer_id', 'id');
    }

    public function localDescription()
    {
        return $this->hasOne(ManufacturerDescription::class, 'manufacturer_id', 'id')
            ->where('language_id', '=', Config::get('current_language_id'))
            ->withDefault($this->defaultLocalDescription);
    }

}
