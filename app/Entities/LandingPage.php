<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Entities\LandingPage
 *
 * @property int $id
 * @property string $url
 * @property int $category_id
 * @property string $filter
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Entities\LandingPageDescription[] $descriptions
 * @property-read \App\Entities\LandingPageDescription $localDescription
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereFilter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereUrl($value)
 * @mixin \Eloquent
 * @property string|null $meta_h1
 * @property string|null $description
 * @property string|null $meta_title
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereMetaH1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Entities\LandingPage query()
 */
class LandingPage extends Model
{
    protected $fillable = [
        'url',
        'meta_h1',
        'description',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];
}
