<?php

namespace App\Notifications;

use NotificationChannels\SmscRu\Exceptions\CouldNotSendNotification;
use NotificationChannels\SmscRu\SmscRuChannel;

class SmscRuViberChanel extends SmscRuChannel
{
    protected function sendMessage($recipients, \NotificationChannels\SmscRu\SmscRuMessage $message)
    {
        if (\mb_strlen($message->content) > 800) {
            throw CouldNotSendNotification::contentLengthLimitExceeded();
        }

        $params = [
            'phones' => \implode(',', $recipients),
            'mes' => $message->content,
            'sender' => $message->from,
            'viber' => 1,
        ];

        if ($message->sendAt instanceof \DateTimeInterface) {
            $params['time'] = '0' . $message->sendAt->getTimestamp();
        }

        $this->smsc->send($params);
    }
}
