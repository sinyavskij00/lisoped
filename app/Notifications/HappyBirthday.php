<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class HappyBirthday extends Notification
{
    use Queueable;

    protected $adminEmail;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->adminEmail = config('app.admin_email');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \App\Mail\Notification\HappyBirthday
     */
    public function toMail($notifiable)
    {
        $mail = new \App\Mail\Notification\HappyBirthday($notifiable);
        $mail->from($this->adminEmail, config('app.name'))
            ->replyTo($this->adminEmail, config('app.name'))
            ->to($notifiable->email)
            ->subject(__('mails.notifications.happy_birthday'));
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
