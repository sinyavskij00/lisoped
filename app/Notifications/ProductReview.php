<?php

namespace App\Notifications;

use App\Entities\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProductReview extends Notification
{
    use Queueable;

    public $order;

    protected $adminEmail;

    /**
     * Create a new notification instance.
     * @param Order $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;

        $this->adminEmail = config('app.admin_email');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \App\Mail\Notification\HappyBirthday
     */
    public function toMail($notifiable)
    {
        $mail = new \App\Mail\Notification\ProductReview($this->order);
        $mail->from($this->adminEmail, config('app.name'))
            ->replyTo($this->adminEmail, config('app.name'))
            ->to($this->order->email)
            ->subject(__('mails.notifications.product_review'));
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
