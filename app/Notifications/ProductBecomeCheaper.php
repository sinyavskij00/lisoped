<?php

namespace App\Notifications;

use App\Entities\StockKeepingUnit;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProductBecomeCheaper extends Notification
{
    use Queueable;

    protected $sku;

    protected $adminEmail;

    /**
     * Create a new notification instance.
     *
     * @param StockKeepingUnit $sku
     */
    public function __construct(StockKeepingUnit $sku)
    {
        $this->sku = $sku;
        $this->adminEmail = config('app.admin_email');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \App\Mail\Notification\ProductBecomeCheaper
     */
    public function toMail($notifiable)
    {
        $mail = new \App\Mail\Notification\ProductBecomeCheaper($notifiable, $this->sku);
        $mail->to($notifiable->email)
            ->from($this->adminEmail, config('app.name'))
            ->replyTo($this->adminEmail, config('app.name'))
            ->subject(__('mails.notifications.product_become_cheaper'));
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
