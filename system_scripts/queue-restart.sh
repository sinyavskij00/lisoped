#!/bin/bash

. $PWD/system_scripts/common/config.sh

cd $conf_basePath/$conf_laradock_dirName
docker-compose exec workspace bash -e ./system_scripts/install/queue-restart.sh
