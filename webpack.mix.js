let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css').version();

// mix.styles([
//     'resources/assets/admin/bootstrap/css/bootstrap.min.css',
//     'resources/assets/admin/font-awesome/4.5.0/css/font-awesome.min.css',
//     'resources/assets/admin/ionicons/2.0.1/css/ionicons.min.css',
//     'resources/assets/admin/plugins/iCheck/minimal/_all.css',
//     'resources/assets/admin/plugins/datepicker/datepicker3.css',
//     'resources/assets/admin/plugins/datetimepicker/bootstrap-datetimepicker.min.css',
//     'resources/assets/admin/plugins/select2/select2.min.css',
//     'resources/assets/admin/plugins/datatables/dataTables.bootstrap.css',
//     'resources/assets/admin/dist/css/AdminLTE.min.css',
//     'resources/assets/admin/dist/css/skins/_all-skins.min.css',
//     'resources/assets/admin/colorbox.css',
//     'resources/assets/admin/jquery-ui.css',
//     'resources/assets/admin/custom.css'
// ], 'public/css/admin.css').version();
//
// mix.scripts([
//     'resources/assets/admin/plugins/jQuery/jquery-2.2.3.min.js',
//     'resources/assets/admin/bootstrap/js/bootstrap.min.js',
//     'resources/assets/admin/plugins/select2/select2.full.min.js',
//     // 'resources/assets/admin/plugins/datepicker/bootstrap-datepicker.js',
//     'resources/assets/admin/plugins/datetimepicker/moment.js',
//     'resources/assets/admin/plugins/datetimepicker/bootstrap-datetimepicker.min.js',
//     'resources/assets/admin/plugins/datatables/jquery.dataTables.min.js',
//     'resources/assets/admin/plugins/datatables/dataTables.bootstrap.min.js',
//     'resources/assets/admin/plugins/slimScroll/jquery.slimscroll.min.js',
//     'resources/assets/admin/plugins/fastclick/fastclick.js',
//     'resources/assets/admin/plugins/iCheck/icheck.min.js',
//     'resources/assets/admin/dist/js/app.min.js',
//     'resources/assets/admin/dist/js/demo.js',
//     'resources/assets/admin/dist/js/scripts.js',
//     'resources/assets/admin/jquery.colorbox-min.js'
// ], 'public/js/admin.js').version();
//
// mix.copy('resources/assets/admin/bootstrap/fonts', 'public/fonts').version();
// mix.copy('resources/assets/admin/dist/fonts', 'public/fonts').version();
// mix.copy('resources/assets/admin/dist/img', 'public/img').version();
// mix.copy('resources/assets/admin/plugins/iCheck/minimal/blue.png', 'public/css').version();


/****************************FRONT***********************************************/
mix.scripts([
    // 'resources/assets/front/libs/jquery/dist/jquery.min.js',
    'resources/assets/front/libs/glasscase/js/modernizr.custom.js',
    'resources/assets/front/libs/slick/slick.min.js',
    'resources/assets/front/libs/equalHeights/equalheights.js',
    'resources/assets/front/libs/glasscase/js/jquery.glasscase.min.js',
    'resources/assets/front/libs/freezeframe/build/js/freezeframe.pkgd.min.js',
    'resources/assets/front/libs/idle/jquery.idle.min.js',
    'resources/assets/front/libs/Magnific-Popup/jquery.magnific-popup.min.js',
    'resources/assets/front/libs/scrollbar-pages/jquery.scrollbar.min.js',
    'resources/assets/front/libs/inputmask/jquery.inputmask.bundle.min.js',
    'resources/assets/front/libs/mmenu/dist/jquery.mmenu.all.js',
    'resources/assets/front/libs/selectize/js/selectize.min.js',
    'resources/assets/front/libs/noUiSlider/nouislider.min.js',
    'resources/assets/front/libs/inputmask/jquerymask.min.js',
    'resources/assets/front/libs/datepicker/datepicker.min.js',
    'resources/assets/front/libs/waypoints/waypoints.min.js',
    'resources/assets/front/libs/greensock-js/src/minified/TweenMax.min.js',
    // 'resources/assets/front/libs/autocomplete/jquery.autocomplete.js',
    'resources/assets/front/js/common.js', // Always at the end
], 'public/front/js/scripts.min.js').version();

mix.copy('resources/assets/front/libs/jquery/dist/jquery.min.js', 'public/front/js/jquery.min.js').version();
mix.copy('resources/assets/front/libs/autocomplete/jquery.autocomplete.min.js', 'public/front/js/jquery.autocomplete.min.js').version();

mix.sass('resources/assets/front/sass/main.sass', 'public/front/css/main.min.css').version().options({
    postCss: [
        require('postcss-discard-comments')({
            removeAll: true
        })
    ]
});

mix.copy('resources/assets/front/img', 'public/assets/img').version();
mix.copy('resources/assets/front/gif', 'public/assets/gif').version();
mix.copy('resources/assets/front/fonts', 'public/assets/fonts').version();

mix.copy('resources/assets/admin/jquery.colorbox-min.js', 'public/assets/js/jquery.colorbox-min.js').version();
mix.copy('resources/assets/admin/colorbox.css', 'public/assets/css/colorbox.css').version();