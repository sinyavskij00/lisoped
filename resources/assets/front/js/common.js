(function ($) {
    'use strict';

    $(function () {

        var windowWidth = $(window).innerWidth(),
            windowHeight = $(window).height(),
            laptop = 1200,
            tabletDesktop = 992,
            largeMobDesktop = 767,
            mobileDesktop = 576,
            removeMd = $('.remove-md'),
            removeSm = $('.remove-sm'),
            removeXs = $('.remove-xs'),
            headerHeight = parseInt($('#my-header').innerHeight()),
            filterHeight = windowHeight - headerHeight;


        // media
        if (windowWidth <= tabletDesktop) {
            removeMd.remove();

            $('#mobile-catalog').removeClass('hidden').mmenu({
                extensions: ['position-front', 'effect-menu-slide', 'pagedim-black', 'theme-white'],
                setSelected: true,
                navbar: {
                    title: 'Каталог' //todo
                }
            });

            $('.filter__wrapper').css('height', filterHeight);
            $('.filter__wrapper').css('top', headerHeight);
            $('.filter-trigger').css('top', headerHeight);
        }


        if (windowWidth <= largeMobDesktop) {
            removeSm.remove();
        }

        if (windowWidth <= mobileDesktop) {
            removeXs.remove();
        }

        if (windowWidth >= mobileDesktop) {
            $('.blog-item__title').equalHeights();
            $('.product-item').equalHeights();
            $('.category-item__name').equalHeights();
            $('.share-item__info').equalHeights();
            $('.faq-list .accordion-item__title').equalHeights();
        }

        $('#my-content').css('margin-top', headerHeight);
        $(window).resize(function () {
            $('#my-content').css('margin-top', headerHeight);
        });

        function animate() {
            $('.fade-right, .fade-left, .fade-top, .fade-bottom, .transform-bottom, .magic-animate, .up-animate, .slide-down, .items-animate').each(function () {
                var self = this;
                var slideDown = new Waypoint({
                    element: $(self),
                    handler: function (direction) {
                        if (direction == 'down') {
                            if (!$(self).hasClass("show") && $(self).hasClass("transform-bottom")) {
                                // console.log($(self).outerHeight());
                                TweenMax.fromTo(self, 1.4, {
                                    opacity: 1,
                                    y: $(self).outerHeight()
                                }, {
                                    y: 0
                                });
                                $(self).addClass("show");
                            }
                            if (!$(self).hasClass("show") && $(self).hasClass("magic-animate")) {
                                // console.log($(self).outerHeight());
                                TweenMax.staggerFromTo(".magic-animate>div>div", 1, {
                                    y: $(".magic-animate>div>div").outerHeight()
                                }, {
                                    y: 0
                                }, 0.2);
                                $(self).addClass("show");
                            }
                            if (!$(self).hasClass("show") && $(self).hasClass("up-animate")) {
                                // console.log($(self).outerHeight());
                                TweenMax.staggerFromTo($(self).find(".animate-item"), 0.4, {
                                    opacity: 0,
                                    y: 30
                                }, {
                                    opacity: 1,
                                    y: 0
                                }, 0.1);
                                $(self).addClass("show");
                            }
                            if (!$(self).hasClass("show") && $(self).hasClass("items-animate")) {
                                var tl = new TimelineMax();
                                tl.staggerFromTo($(self).find(".item-animate"), 0.6, {
                                    opacity: 0,
                                    y: 100
                                }, {
                                    opacity: 1,
                                    y: 0
                                }, 0.2);
                                // .staggerFromTo($(self).find(".item-animate .rellax"), 0.6, { opacity:0, x:"100%"}, { opacity:1, x:"0%"}, 0.2, "-=0.4")

                                $(self).addClass("show");
                            }
                            if (!$(self).hasClass("show")) {
                                $(self).addClass("show");
                            }
                        }
                    },
                    offset: '90%'
                });
            });

        }

        animate();


        $('.menu .has-dropdown>div, .menu .has-dropdown>span').on('click', function (e) {
            e.preventDefault();
            if ($(this).parent().find('.dropdown').is(':visible')) {
                $(this).parent().find('.dropdown').hide();
                $(this).closest('.has-dropdown').removeClass('active');
            } else {
                $(this).parent().find('.dropdown').show();
                $(this).closest('.has-dropdown').addClass('active');
            }
        });


        $('.h-phones-more').on('click', function (e) {
            e.preventDefault();
            if ($(this).next('.dropdown').is(':visible')) {
                $(this).next('.dropdown').hide();
                $(this).closest('.has-dropdown').removeClass('active');
            } else {
                $(this).next('.dropdown').show();
                $(this).closest('.has-dropdown').addClass('active');
            }
        });


        $('.search-btn').on('click', function () {
            $(this).closest('.search').toggleClass('active');
            $(this).closest('.search').find('input[type="search"]').focus();
        });

        // tooltip init
        var $tooltip = $('.tooltip');
        $tooltip.each(function () {
            var dataInfo = $(this).data("tooltip");
            $(this).append('<span class="tooltip-content">' + dataInfo + '</span>');
        });

        // product zoom img
        if (windowWidth >= largeMobDesktop) {
            $('#glasscase').glassCase({
                zoomPosition: 'inner',
                thumbsPosition: 'bottom',
                widthDisplay: 654,
                heightDisplay: 370,
                autoInnerZoom: true,
                nrThumbsPerRow: 4,
                isZoomDiffWH: true,
                isShowAlwaysIcons: true,
                isDownloadEnabled: false,
                zoomMargin: 10,
                colorActiveThumb: '#c6c6c6',
                thumbsMargin: 25
            });
        } else {
            $('#glasscase').glassCase({
                isOverlayEnabled: false,
                zoomPosition: 'inner',
                thumbsPosition: 'bottom',
                widthDisplay: 654,
                heightDisplay: 370,
                autoInnerZoom: true,
                nrThumbsPerRow: 4,
                isZoomDiffWH: true,
                isShowAlwaysIcons: true,
                isDownloadEnabled: false,
                zoomMargin: 10,
                colorActiveThumb: '#c6c6c6',
                thumbsMargin: 25
            });
        }

        // Product Grid
        if ($('#filter').length == 0) {
            $('.products-row .product-item').parent().attr('class', 'col-sm-6 col-lg-4');
        }

        // init main banner slider
        if ($('.main-banner').length) {
            $('.main-banner').on('init', function (event, slick) {
                $('.main-banner .banner-item').equalHeights();
            }).slick({
                slidesToShow: 1,
                lazyLoad: 'progressive',
                dots: true,
                arrows: false,
                speed: 500,
                adaptiveHeight: false,
                autoplay: true,
                fade: true,
                cssEase: 'linear',
                autoplaySpeed: 10000
            });
        }


        // trigger to mobile filter
        $('[data-toggle="filter"]').click(function () {
            $(this).addClass('active');
            $('.filter__wrapper').addClass('active');
            $('html').addClass('__fixed __show-overlay');
        });

        // custom select
        $('.select').selectize();


        // mobile menu trigger
        $('[data-toggle="mobile-menu"]').click(function (e) {
            $(this).toggleClass('active');
            $('#mobile-menu').toggleClass('active');
            $('html').toggleClass('__fixed __show-overlay');
        });
        $('.mobile-menu .btn-close').on('click', function () {
            $('.mobile-menu').removeClass('active');
            $('#mobile-menu').removeClass('active');
            $('html').removeClass('__fixed __show-overlay');
        });

        // on page-overlay click
        $('.page-overlay').on('click', function () {
            if ($('html').hasClass('__fixed __show-overlay')) {
                $('html').removeClass('__fixed __show-overlay');
                $('#mobile-menu').removeClass('active');
                $('[data-toggle="filter"]').removeClass('active');
                $('.filter__wrapper').removeClass('active');
            }
            if ($('html').hasClass('__character-active')) {
                $('html').removeClass('__character-active');
                $('.character-wrapper').removeClass('active');
                $('.character-wrapper').find('.character-text__inner').removeClass('active');
            }
            if ($(this).hasClass('__overlay-blue')) {
                $(this).removeClass('__overlay-blue');
                $('.character-wakeup').addClass('hidden');
            }
        });


        $('.tab-products').slick({
            dots: false,
            arrows: true,
            infinite: false,
            speed: 700,
            adaptiveHeight: false,
            rows: 2,
            slidesPerRow: 3,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesPerRow: 2,
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        rows: 1,
                        slidesPerRow: 1,
                    }
                }
            ]
        });


        // tabs
        $.fn.minimalTabs = function () {
            var tabWrapper = $(this),
                tabNav = tabWrapper.find('.tab-nav'),
                tabNavItem = tabWrapper.find('.tab-nav > li'),
                tabContent = tabWrapper.find('.tab-cont'),
                tabContentItem = tabWrapper.find('.tab-cont > .tab-pane');

            tabNavItem.each(function (i) {
                $(this).attr('data-tab', 'tab' + i);
            });
            tabContentItem.each(function (i) {
                $(this).attr('data-tab', 'tab' + i);
            });

            var activeTab = tabNav.find('li.active').data('tab');

            tabContentItem.not('[data-tab=' + activeTab + ']').hide();

            tabNav.on("click", 'li', function (event) {
                event.preventDefault();
                if ($(this).hasClass('active') === false) {
                    tabNavItem.removeClass('active');
                    $(this).addClass('active');

                    activeTab = $(this).data('tab');

                    tabContent.fadeOut('fast', function () {
                        tabContentItem.hide();
                        tabContentItem.filter('[data-tab=' + activeTab + ']').show();
                        tabContent.fadeIn('fast');
                    });

                    // addition for slider in tab
                    var tabSlider = tabContentItem.filter('[data-tab='+activeTab+']').find('.tab-products');
                    if (tabSlider.length){
                        setTimeout(function(){
                            tabSlider.slick('reinit');
                        }, 250);
                    }


                    // if($('.tab-products').length){
                    //     setTimeout(function(){
                    //         $('.tab-products .tab-pane:not(.slider__inited) .tab-products').slick('reinit');
                    //     }, 200);
                    //     activeTab.not('.slider__inited').addClass('.slider__inited');
                    // }
                }
            });
        };
        // init tabs
        if ($('.products-tabs').length) {
            $('.products-tabs').minimalTabs();
        }
        if ($('.blog-tabs').length) {
            $('.blog-tabs').minimalTabs();
        }
        if ($('.product-info__tabs').length) {
            $('.product-info__tabs').minimalTabs();
        }

        if (window.location.hash.substr(1) === 'to-review') {
            $(document).off("scroll");

            $('html, body').stop().animate({
                'scrollTop': $('#product-info__tabs').offset().top - 100
            }, 700, 'swing');

            $('.product-info__tabs [data-tab="tab2"]').click();
        }
        // /tabs


        // init manufacturer slider
        $('.manufacturer-slider').slick({
            dots: false,
            arrows: true,
            speed: 1000,
            adaptiveHeight: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
        $('.s-manufacturer .manufacturer-item__img').equalHeights();


        //  text more
        function showText() {
            var heightTextWrap = $(".show-text .text-wrap").innerHeight(),
                heightText = $(".show-text .text-wrap .text").innerHeight(),
                textMoreBtn = $(".show-text .btn-wrap .btn__text-more");

            if (heightText > heightTextWrap) {
                $(".show-text .text-wrap").addClass('shadow');
                $(".show-text .btn-wrap").removeClass('hidden');
                textMoreBtn.on('click', function () {
                    var heightTextWrapAfterClick = $(this).closest(".show-text").find(".text-wrap").innerHeight();
                    var textWrap = $(this).closest(".show-text").find(".text-wrap");
                    var heightInside = textWrap.find(".text").innerHeight() + 20;

                    if (heightTextWrap == heightTextWrapAfterClick) {
                        $(".show-text").addClass('active');
                        textWrap.innerHeight(heightInside);
                        $(".show-text .text-wrap").css("max-height", heightInside);
                    }
                    else {
                        $(".show-text").removeClass('active');
                        textWrap.innerHeight(heightTextWrap);
                    }
                });
            }
        }

        if ($('.show-text').length) {
            showText();
        }


        // init popups
        $(document).magnificPopup({
            delegate: '.to-popup',
            type: 'inline',
            fixedContentPos: true,
            fixedBgPos: true,
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            mainClass: 'mfp-fade',
            removalDelay: 300,
        });
        $('#cart-popup').on('click', '.btn-continue', function (e) {
            e.preventDefault();
            $.magnificPopup.close();
        });


        // lost password trigger
        $('.login-form__lost').on('click', function () {
            $('[data-form="login"]').hide();
            $('[data-form="lost"]').show();
        });
        $('.lost-form__login').on('click', function () {
            $('[data-form="lost"]').hide();
            $('[data-form="login"]').show();
        });


        // input type file customization
        $(document).on('change', 'input[type="file"]', function () {
            var files = $(this).parent().find("input").prop("files");
            var names = $.map(files, function (val) {
                return val.name;
            });
            $(this).parent().find(".reviews-form__file-name").text(names);
        });


        // init products slider
        $('.slider-products').slick({
            dots: false,
            arrows: true,
            speed: 700,
            adaptiveHeight: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });


        // FAQ accordion
        var accordionItemTitle = $('.accordion-item__title'),
            accordionItemContent = $('.accordion-item__content');

        function closeAccordionItem() {
            $(accordionItemTitle).removeClass('active');
            $(accordionItemContent).slideUp(300);
        }

        $(accordionItemTitle).on('click', function (e) {
            e.preventDefault();

            if ($(this).hasClass('active') == false) {
                closeAccordionItem();
                $(this).addClass('active');
                $(this).next('.accordion-item__content').slideDown(300);
            } else {
                closeAccordionItem();
            }
        });


        // filter price slider init
        var keypressSlider = document.getElementById('keypress');
        var input0 = document.getElementById('input-with-keypress-0');
        var input1 = document.getElementById('input-with-keypress-1');
        var input0Start = +$('#input-with-keypress-0').attr("data-start");
        var input1Start = +$('#input-with-keypress-1').attr("data-start");
        var input1Min = +$('#input-with-keypress-1').attr("data-min");
        var input1Max = +$('#input-with-keypress-1').attr("data-max");
        var inputs = [input0, input1];

        if (document.getElementById('keypress')) {
            noUiSlider.create(keypressSlider, {
                start: [input0Start, input1Start],
                connect: true,
                tooltips: true,
                range: {
                    'min': [input1Min],
                    // '10%': [10, 10],
                    // '50%': [80, 50],
                    // '80%': 150,
                    'max': input1Max
                }
            });

            keypressSlider.noUiSlider.on('update', function (values, handle) {
                inputs[handle].value = Math.round(values[handle]);
            });
            keypressSlider.noUiSlider.on('change', function (values, handle) {
                $(input0).trigger('change');
                $(input1).trigger('change');
            });
            // /filter price slider init

            // FILTER price slide
            function setSliderHandle(i, value) {
                var r = [null, null];
                r[i] = value;
                keypressSlider.noUiSlider.set(r);
            }

            // Listen to keydown events on the input field.
            inputs.forEach(function (input, handle) {

                input.addEventListener('change', function () {
                    setSliderHandle(handle, this.value);
                });

                input.addEventListener('keydown', function (e) {

                    var values = keypressSlider.noUiSlider.get();
                    var value = Number(values[handle]);

                    // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
                    var steps = keypressSlider.noUiSlider.steps();

                    // [down, up]
                    var step = steps[handle];

                    var position;

                    // 13 is enter,
                    // 38 is key up,
                    // 40 is key down.
                    switch (e.which) {

                        case 13:
                            setSliderHandle(handle, this.value);
                            break;

                        case 38:

                            // Get step to go increase slider value (up)
                            position = step[1];

                            // false = no step is set
                            if (position === false) {
                                position = 1;
                            }

                            // null = edge of slider
                            if (position !== null) {
                                setSliderHandle(handle, value + position);
                            }

                            break;

                        case 40:

                            position = step[0];

                            if (position === false) {
                                position = 1;
                            }

                            if (position !== null) {
                                setSliderHandle(handle, value - position);
                            }

                            break;
                    }
                });
            });
        }
        // /FILTER price slide

        // reviews quantity
        var reviewsQnty = $('.reviews-list .reviews-item').length;
        $('.reviews-section__head .reviews-qnty').html('(' + reviewsQnty + ')');

        $('.reviews-list__item').each(function () {
            var reviewsLevel1 = $(this).find('.reviews-item.level1'),
                reviewsLevel1Qnty = reviewsLevel1.length,
                reviewsLevel1QntyWithoutFirst = reviewsLevel1Qnty - 1,
                reviewsLevelFirst = reviewsLevel1.eq(0);

            if (reviewsLevel1Qnty > 0) {
                reviewsLevelFirst.removeClass('hidden');
            }
            if (reviewsLevel1Qnty > 1) {
                reviewsLevelFirst.after('<span class="show__all-reviews">Показать еще ответы (' + reviewsLevel1QntyWithoutFirst + ')</span>');
                $('.show__all-reviews').on('click', function () {
                    $(this).addClass('hidden');
                    $(this).closest('.reviews-list__item').find('.reviews-item.hidden').removeClass('hidden');
                });
            }
        });

        /**
         * Function NumberEnd
         **/
        function NumberEnd(mNumber, mEnds) {
            var cases = [2, 0, 1, 1, 1, 2];
            return mEnds[(mNumber % 100 > 4 && mNumber % 100 < 20) ? 2 : cases[Math.min(mNumber % 10, 5)]];
        }

        // console.log("В корзине: <span>"+5+"</span> товар"+NumberEnd(5, ["", "а", "ов"]));


        // map
        var mapLat = +$('.map').attr("data-lat");
        var mapLng = +$('.map').attr("data-lng");
        var mapMarker = $('.map').attr("data-marker");
        var mapTitle = $('.map').attr("data-title");
        // var mapAddress = $('.contacts-item__title address').text();
        var mapPhones = $('.phones-list').html();

        function initMap() {

            var mapOptions = {
                zoom: 17,
                center: new google.maps.LatLng(mapLat, mapLng),
                mapTypeControl: false,
                styles: [
                    {stylers: [{saturation: -80}, {gamma: 1}]},
                    {
                        featureType: "transit.station",
                        elementType: "labels.icon",
                        stylers: [{gamma: 1}, {saturation: 50}]
                    }
                ]
            };

            var mapElement = document.querySelector('.map');
            var map = new google.maps.Map(mapElement, mapOptions);

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(mapLat, mapLng),
                map: map,
                title: mapTitle
            });

            var infowindow = new google.maps.InfoWindow({
                content: '<div class="info-window"><p class="info-window-title">' + mapTitle + '</p>' +
                '<ul class="info-window-phones">' + mapPhones + '</ul></div>'
            });

            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
        }

        if (document.querySelector('.map')) {
            initMap();
        }
        // /map


        // comparison
        if (!$('.comparison-index__item').length) {
            $('.compare-empty').removeClass('hidden');
        }

        function comparisonTableWidth(elLength) {
            if (elLength >= 3 && comparisonTable.hasClass('full-width') == false) {
                comparisonTable.addClass('full-width');
            } else {
                comparisonTable.removeClass('full-width');
            }
        }

        if (windowWidth >= laptop) {
            if ($('.comparison').length) {
                var comparisonTable = $('.comparison');
                var thLength = comparisonTable.find('thead tr th').length;
                comparisonTableWidth(thLength);
            }
        }

        $('.comparison .comparison-product__close').on('click', function (e) {
            e.preventDefault();
            var n = $(this).parent().parent().index();
            $('.comparison tr').find('td:eq(' + n + '),th:eq(' + n + ')').remove();
        });
        $('.comparison-product .comparison-product__info').equalHeights();


        // history orders trigger
        $('[data-toggle="history-order__descr-table"]').click(function () {
            if ($(this).hasClass('active') == true) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
            $(this).next('.history-order__descr-table').slideToggle(500);
        });


        // product tables qnty
        if ($('.sizes-popup__tables .sizes-popup__table--wrapper').length == 2) {
            $('.sizes-popup').removeClass('sizes-popup__col-1');
            $('.sizes-popup__tables').addClass('sizes-popup__tables-2');
        }


        // to top
        $('.top').click(function () {
            $('html, body').stop().animate({scrollTop: 0}, 'slow', 'swing');
        });
        $(window).scroll(function () {
            if ($(this).scrollTop() > $(window).height()) {
                $('.top').addClass("active");
            } else {
                $('.top').removeClass("active");
            }

            // fixed site header
            if ($(this).scrollTop() >= 50) {
                $("#my-header").addClass("sticky");
            } else {
                $("#my-header").removeClass("sticky");
            }
        });


        // same width for product sort blocks
        if ($('.product-sort__item').length) {
            // if (windowWidth >= mobileDesktop) {
            //
            // }
            $('.product-sort__group .product-sort__item').each(function () {
                var $sortItemSelectedWidth = $(this).find('.product-sort__item-selected'),
                    $sortItems = $(this).find('.product-sort__item-select').innerWidth(),
                    sortItemsWidth = parseInt($sortItems) + 2 + 'px';
                $sortItemSelectedWidth.css("width", sortItemsWidth);
            });
        }


        // input mask init
        $('input[type=tel]').mask('+38 (000) 000 00 00');


        // to product reviews
        $('.product-info .reviews-number').on('click', function (e) {
            e.preventDefault();
            $(document).off("scroll");

            var target = this.hash,
                $target = $(target);

            $('html, body').stop().animate({
                'scrollTop': $target.offset().top - 100
            }, 700, 'swing');

            $('.product-info__tabs [data-tab="tab2"]').click();
        });

        $('.has-dropdown__list .product-sort__item-selected').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.has-dropdown__list').toggleClass('active');
        });


        // compare
        function compareItemsHeights() {
            var compareTable = $('.table-compare');
            compareTable.each(function(){
                var compareItem = $(this).find('.table-compare__item');
                compareItem.each(function(i){
                    $(this).attr('data-item','item-'+i);
                });
                setTimeout(function(){
                    for(var i = 0; i < compareItem.length; i++){
                        $('[data-item=item-'+i+']').equalHeights();
                    }
                }, 200);
            });
        }

        $('.compare-page__slider').on("init", function(event, slick){
            $('.compare__block .one-goods__image').equalHeights();
            setTimeout(function(){
                $('.compare__block .one-goods__first').equalHeights();
            }, 50);
            compareItemsHeights();
        });

        $('.compare-page__slider').slick({
            infinite: false,
            arrows: true,
            speed: 700,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        adaptiveHeight: true
                    }
                }
            ]
        });


        // animate text
        $.fn.animate_Text = function () {
            var string = this.html();
            return this.each(function () {
                var $this = $(this);

                $this.html(string.replace(/<\s*a[^>]*>(.*?)<\/\s*a>/gi, '<span class="new">$&</span>'));
                // $this.html(string.replace(/./g, '<span class="new">$&</span>'));
                $this.find('span.new').each(function (i, el) {
                    setTimeout(function () {
                        $(el).addClass('div_opacity');
                    }, 35 * i);
                });
            });
        };


        // character
        function characterShow(el) {
            var $this = el;
            $this.addClass('active');
            $this.find($('.character-text__inner')).addClass('active');
        }

        function characterHide(el) {
            var $this = el;
            $this.removeClass('active');
            $this.find($('.character-text__inner')).removeClass('active');
            setTimeout(function () {
                $this.not('.character-lenguage').addClass('hidden');
            }, 500);
        }

        function characterLogic(el, t) {
            setTimeout(function () {
                characterShow(el);
                characterHide(el);
            }, t);
        }

        var timeBeforeShow = null;

        if ($('.character-wakeup').length) {
            timeBeforeShow = parseInt($('.character-wakeup').attr("data-time"));
            $(document).idle({
                onIdle: function () {
                    if ($('.character-wrapper:not(.character-wakeup)').hasClass("active")) {
                        characterHide($('.character-wrapper.active'));
                    }
                    if (!$('body').hasClass("no-overlay")) {
                        characterShow($('.character-wakeup'));
                        $('.page-overlay').addClass('__overlay-blue');
                        $('html').addClass('__fixed __show-overlay __character-active');
                        $('body').addClass('no-overlay');
                    }
                },
                idle: timeBeforeShow,
                events: 'mousemove keydown mousedown touchstart',
                keepTracking: true,
                startAtIdle: false,
                recurIdleCall: false
            });
        }

        if ($('.character-welcome').length) {
            timeBeforeShow = parseInt($('.character-welcome').attr("data-time"));
            $(document).idle({
                onIdle: function () {
                    if (!$('.character-welcome').hasClass('hidden') && !$('body').hasClass('stop') && !$('html').hasClass('__show-overlay __character-active')) {
                        characterShow($('.character-welcome'));
                        $(document).off("scroll");
                        $('html, body').stop().animate({
                            'scrollTop': $('#pickup_bicycle').offset().top - 110
                        }, 700, 'swing');
                        $('body').addClass('stop');
                    }
                },
                idle: timeBeforeShow,
                events: 'mousemove keydown mousedown touchstart',
                keepTracking: true,
                startAtIdle: false,
                recurIdleCall: false
            });
        }


        if ($('.character-help').length) {
            timeBeforeShow = parseInt($('.character-help').attr("data-time"));
            $(document).idle({
                onIdle: function () {
                    characterShow($('.character-help'));
                },
                idle: timeBeforeShow,
                events: 'mousemove keydown mousedown touchstart',
                keepTracking: true,
                startAtIdle: false,
                recurIdleCall: false
            });
        }

        function getCookie(name) {
            var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        }



        if ($('.character-blog').length) {
            var characterCook = getCookie("characterCook");
            if (characterCook !== "no") {
                characterShow($('.character-blog'));
                var now = new Date();
                now.setTime(now.getTime() + 1 * 3600 * 1000);
                document.cookie = "characterCook=no; expires=" + now.toUTCString() + "; path=/";
                $(window).on('scroll', function () {
                    setTimeout(function () {
                        characterHide($('.character-blog'));
                    }, 1500);
                });
            }
        }

        if ($('.character-purchased').length) {
            characterShow($('.character-purchased'));
        }

        if (windowWidth >= tabletDesktop) {
            if ($('.character-filter--help').length) {
                timeBeforeShow = parseInt($('.character-filter--help').attr("data-time"));
                setTimeout(function () {
                    if(!$('.filter-option__label.checked').length) {
                        characterShow($('.character-filter--help'));
                    }
                }, timeBeforeShow);
            }
        }

        $('.character-wrapper .close').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.character-wrapper').removeClass('active');
            $(this).closest('.character-wrapper').find('.character-text__inner').removeClass('active');
            if ($('.page-overlay').hasClass('__overlay-blue')) {
                $('.page-overlay').removeClass('__overlay-blue');
            }
            if ($('html').hasClass('__fixed __show-overlay __character-active')) {
                $('html').removeClass('__fixed __show-overlay __character-active');
            }
            $(this).closest('.character-wrapper:not(.character-lenguage)').addClass('hidden');
        });


        // success page character position
        function successImgPosition() {
            var footerHeight = parseInt($('#my-footer').innerHeight());
            var successImgPosition = parseInt(footerHeight) + 2 + 'px';
            $('.success-content__img').css('bottom', successImgPosition);
        }
        successImgPosition();
        if ($('.success-content__img').length) {
            $(window).resize(function () {
                successImgPosition();
            });
        }


        // lenguage gag
        $('.language .language-item:nth-child(2)').on('click', function (e) {
            e.preventDefault();
            $('html').addClass('__fixed __show-overlay __character-active');
            characterShow($('.character-lenguage'));
        });

    });

    $(document).on('click touchstart', function (e) {

        var dropdown = $(".dropdown"),
            search = $('.search');

        if (dropdown.is(':visible') && dropdown.parent().has(e.target).length === 0) {
            dropdown.hide();
            $('.has-dropdown').removeClass('active');
        }

        if (search.hasClass('active') == true && search.has(e.target).length === 0) {
            search.removeClass('active');
        }

        if (!$(e.target.closest('.has-dropdown__list')).is(".has-dropdown__list") && $('.has-dropdown__list').hasClass('active')) {
            $(".has-dropdown__list").removeClass('active');
        }

    });

})(jQuery);
