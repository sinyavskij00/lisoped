<?php
return [
    'notifications' => [
        'forgotten_cart' => 'Кажется, ты что-то забыл',
        'happy_birthday' => 'С днем рождения!',
        'product_become_cheaper' => 'Товары из списка желания стали дешевле',
        'product_review' => 'Поделись впечатлениями',
        'store_review' => 'Как тебе Lisoped?'
    ],
    'header_callback' => [
        'subject' => 'Заказали обратный звонок',
        'title' => 'Заказали обратный звонок',
        'name' => 'Имя',
        'telephone' => 'Телефон',
        'date' => 'Дата:',
        'hours' => 'Часы:',
        'minutes' => 'Минуты:',
    ],
    'specials' => [
        'subject' => 'Вопрос со страницы акции',
        'name' => 'Имя:',
        'email' => 'Email:',
        'question' => 'Вопрос:',
    ],
    'contacts' => [
        'subject' => 'Задали вопрос',
        'title' => 'Сообщение с контактной формы',
        'name' => 'Имя:',
        'email' => 'E-mail:',
        'message' => 'Сообщение:'
    ],
    'orders' => [
        'status_changed' => [
            'subject' => 'Реквизиты для оплаты заказа №:number',
            'subject_np' => 'Ура! К тебе едет посылка из заказа №:number',
            'subject_end' => 'Оплата получена. Заказ №:number',
            'title' => 'Статус заказа изменен',
            'sub_title' => 'Заказ №',
            'status' => 'Статус',
            'comment' => 'Комментарий:',
            'billing_account' => [
                'title' => 'Расчетный счет:',
                'name' => 'ФЛП',
                'employer_number' => 'УГРПОУ',
                'bank_number' => 'МФО',
                'checking_account' => 'Р/С'
            ],
            'billing_cart' => [
                'title' => 'Платежная карта:',
                'name' => 'ФИО',
                'number' => 'Номер'
            ],
        ],
        'new_order' => [
            'subject' => 'Заказ №:number оформлен',
            'consultation_by_phone' => 'Консультация по телефону:',
            'thanks_for_buy' => 'Благодарим Вас, :name, за покупку в нашем интернет - магазине',
            'order_number' => 'Номер Вашего заказа:',
            'status' => 'Статус:',
            'product_name' => 'Наименование',
            'product_statistic' => 'Характеристики',
            'product_price' => 'Цена',
            'product_quantity' => 'К-ство',
            'product_total' => 'Сумма',
            'size' => 'Размер:',
            'color' => 'Цвет:',
            'year' => 'Год:',
            'shipping_price' => 'Стоимость доставки',
            'total' => 'Итого',
            'product' => 'товара',
            'payment' => 'Оплата:',
            'shipping' => 'Доставка:',
            'billing_account' => [
                'title' => 'Расчетный счет:',
                'name' => 'ФЛП',
                'employer_number' => 'УГРПОУ',
                'bank_number' => 'МФО',
                'checking_account' => 'Р/С'
            ],
            'billing_cart' => [
                'title' => 'Платежная карта:',
                'name' => 'ФИО',
                'number' => 'Номер'
            ],
            'regards' => 'С уважением, магазин'
        ]
    ],
    'information' => [
        'review_accepted' => [
            'title' => 'Твой отзыв был одобрен',
            'subject' => 'Твой отзыв опубликован',
            'review' => 'Отзыв:',
            'link_text' => 'перейти к отзыву'
        ]
    ],
    'user_registred' => [
        'actions' => [
            'registration' => 'регистрацию',
            'subcribe' => 'подписку',
        ],
        'title' => 'СПАСИБО ЗА :action!',
        'sub_title' => 'Благодарим Вас, :name, за :action.',
        'telephone_title' => 'Консультация по телефону:',
        'regards' => 'С уважением, магазин',
        'login_title' => 'Логин:',
        'password_title' => 'Пароль:',
        'footer_text' => '<p>Одна из ценностей нашей подписки — уникальные акции и подарки только для подписчиков.</p>'
            . '<p>Читайте популярные статьи нашего сайта, подписывайтесь на интересные блоги экспертов или принимайте '
            .'участие в самых дискуссионных обсуждениях.</p>'
    ],
    'check' => [
        'subject' => 'Чек за заказ №:number',
        'content' => [
            'head_text' => 'Текст в заголовке',
            'title' => 'Чек',
            'order_number' => 'Заказ №:number',
            'product' => 'Товар',
            'price' => 'Цена',
            'quantity' => 'Количество',
            'total' => 'Сумма',
            'date' => 'Дата:'
        ]
    ],
    'guarantee' => [
        'subject' => 'Гарантия на заказ №:number',
    ],
    'preorder' => [
        'admin' => [
            'subject' => 'Заявка на подобрать похожий',
            'text' => 'У вас новая заявка на подобрать похожий, номер заявки :number'
        ]
    ]
];
