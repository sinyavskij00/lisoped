<?php
return [
    'ajax_search' => [
        'available' => 'Есть в наличии',
        'not_available' => 'Нет в наличии',
        'show_all' => 'Показать все результаты',
    ],
    'search' => [
        'title' => 'Результат поиска по слову: :query',
        'sub_title' => 'Результаты поиска по слову ":query" не найдены,'
            . 'но не расстраивайтесь, посмотрите что мы для Вас нашли',
        'carousel_title' => 'Рекомендуемые товары'
    ],
    'compare' => [
        'category' => [
            'title' => 'Категории сравнения'
        ],
        'product' => [
            'title' => 'Сравнение товаров',
            'products' => 'Товары',
            'price' => 'Цена',
            'sku' => 'Артикул:',
            'btn_delete' => 'Удалить товар',
            'btn_more' => 'Подробнее',
        ]
    ],
    'sorts' => [
        'price' => [
            'asc' => 'От дешевых к дорогим',
            'desc' => 'От дорогих к дешевым',
        ],
        'special_price' => [
            'asc' => 'В конце акционные',
            'desc' => 'Акционные',
        ],
        'novelty' => [
            'asc' => 'В конце новинки',
            'desc' => 'Новинки',
        ],
        'our_choice' => [
            'asc' => 'В конце наш выбор',
            'desc' => 'Наш выбор',
        ],
    ],
    'current_sku' => [
        'reviews' => 'отзывов',
        'sku' => 'Артикул',
        'manufacturer' => 'Производитель',
        'year' => 'Год',
        'color' => 'Цвет',
        'size' => 'Размер',
        'size_table' => 'Таблица размеров',
        'available' => 'Есть в наличии',
        'not_available' => 'Подобрать похожий товар',
        'buy_one_click' => 'Купить в 1 клик',
        'btn_buy' => 'Купить',
        'product_advantages' => [
            'btn_more' => 'Подробнее'
        ],
        'one_click_buy_form' => [
            'success' => '<span>Спасибо за заявку!</span><span>В ближайшее время с тобой свяжется наш менеджер</span>',
            'title' => 'Быстрый заказ',
            'name' => 'Имя*:',
            'telephone' => 'Номер телефона*:',
            'comment' => 'Комментарий к заказу:',
            'submit' => 'Заказать',
        ],
        'preorder' => [
            'title' => 'Предзаказ',
            'name' => 'Имя:*',
            'telephone' => 'Номер телефона*:',
            'email' => 'Email*:',
            'comment' => 'Комментарий:',
            'submit' => 'Уведомить',
        ]
    ],
    'labels' => [
        'our_choice' => 'Наш выбор',
        'hit' => 'Hit',
        'new' => 'New',
        'sale' => 'Sale'
    ],
    'filter' => [
        'reset_filter' => 'Сбросить все фильтры',
        'price' => 'Цена',
        'from' => 'от',
        'to' => 'до',
        'manufacturers' => 'Производители'
    ],
    'manufacturer_listing' => [
        'title' => 'Производители'
    ],
    'product_item' => [
        'available' => 'В наличии',
        'not_available' => 'Нет в наличии',
        'btn_buy' => 'подробнее',
        'reviews' => 'отзывов',
        'text_compare' => 'В сравнение',
        'text_wishlist' => 'В закладки',
        'sizes' => 'Размеры:'
    ],
    'product_listing' => [
        'filters' => 'фильтры',
        'sort_title' => 'Сортировать:',
        'limit_title' => 'Показывать по:',
        'read_more' => 'Читать полностью',
        'character_text' => 'С фильтрами ты быстрее найдешь нужный товар',
        'toponyms' => ' купить в Харьков, Киев, Львов'
    ],
    'product_page' => [
        'description' => 'Описание',
        'specifications' => 'Характеристики',
        'reviews' => 'Отзывы',
        'videos' => 'Видеообзор',
        'leave_comment' => 'Оставить отзыв',
        'text_comment' => 'Комментарий...',
        'text_name' => 'Имя',
        'text_email' => 'Email (не будет отображаться)',
        'submit' => 'Отправить',
        'related_products' => 'Похожие товары',
        'character_text' => 'Остались вопросы? <a href="#callback-popup" class="btn__callback to-popup">Позвони нам</a>, поможем с выбором.',
    ]
];
