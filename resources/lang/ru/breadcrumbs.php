<?php
return [
    'home' => 'Главная',
    'about_us' => 'О нас',
    'specials_list' => 'Акции',
    'search' => 'Поиск',
    'new' => 'Новинки',
    'contacts' => 'Контакты',
    'credit' => 'Рассрочка',
    'delivery' => 'Оплата и доствка',
    'news_list' => 'Блог',
    'compare_category' => 'Категории сравнения',
    'manufacturers' => 'Производители',
    'guarantee' => 'Гарантии',
    'faq' => 'Вопрос-ответ',
    'testimonials' => 'Отзывы о магазине',
];
