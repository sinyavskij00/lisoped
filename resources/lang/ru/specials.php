<?php
return [
    'list' => [
        'title' => 'Акции',
        'from' => 'С',
        'to' => 'до'
    ],
    'show' => [
        'form' => [
            'title' => 'Остались вопросы?',
            'placeholder_name' => 'Имя*',
            'placeholder_email' => 'Email*',
            'placeholder_question' => 'Вопрос*',
            'submit' => 'Отправить',
        ]
    ]
];