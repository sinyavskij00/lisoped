<?php
return [
    'header' => [
        'for_customers' => 'Покупателям',
        'delivery' => 'Оплата и доставка',
        'testimonials' => 'Отзывы о магазине',
        'faq' => 'Вопрос-ответ',
        'credit' => 'Рассрочка',
        'manufacturers' => 'Производители',
        'about_us' => 'О нас',
        'blog' => 'Блог',
        'contacts' => 'Контакты',
        'specials' => 'Акции',
        'work_time_title' => 'Режим работы',
        'back_call' => 'Перезвонить?',
        'text_cabinet' => 'Личный кабинет',
        'text_my_cabinet' => 'Мой кабинет',
        'text_auth_reg' => 'Авторизация/регистрация',
        'text_compare' => 'Сравнение',
        'text_wishlist' => 'Список желаний',
        'text_cart' => 'Корзина',
        'text_catalog' => 'Каталог товаров',
        'text_search' => 'Поиск',
        'placeholder_search' => 'Что будешь искать?',
        'text_enter' => 'Войти',
        'text_register' => 'Зарегистрироваться',
        'text_order_by_phone' => 'Заказы по телефонам:',
    ],
    'footer' => [
        'address1' => 'Киев, ул. Казимира Малевич 89',
        'address2' => 'Харьков, пр. Науки 30',
        'telephone' => '(067) 62-06-611',
        'dialing_telephone' => '676206611',
        'category_title' => 'Продукция',
        'about_title' => 'О магазине',
        'about_us' => 'О нас',
        'manufacturers' => 'Производители',
        'testimonials' => 'Отзывы о магазине',
        'faq' => 'Вопрос-ответ',
        'guarantee' => 'Гарантия',
        'blog' => 'Блог',
        'delivery' => 'Оплата и доставка',
        'contacts' => 'Контакты',
        'credit' => 'Рассрочка',
        'specials' => 'Акции',
        'social_title' => 'Мы здесь',
        'subscribe_placeholder' => 'Укажи свой email',
        'subscribe_submit' => 'Подписаться',
        'copyright' => '&copy; :year Все права защищены',
        'developed' => 'Разработано в:',
        'wishlist_popup' => [
            'text' => 'Чтобы понравившиеся товары не потерялись, они сохраняются в специальном списке личного кабинета',
            'btn_register' => 'Регистрация'
        ],
        'login_popup' => [
            'title' => 'Вход в личный кабинет',
            'label_login' => 'Логин*:',
            'placeholder_email' => 'I-Ivanov@gmail.com',
            'label_password' => 'Пароль*:',
            'placeholder_password' => '*********',
            'remember_me' => 'Запомнить меня',
            'forgot_password' => 'Забыл пароль?',
            'submit' => 'Вход',
            'social_title' => 'Войти через социальные сети',
            'register' => 'Регистрация',
        ],
        'reset_password' => [
            'success' => 'Ссылку для восстановления пароля отправил на Email. Проверь почту.',
            'title' => 'Восстановление пароля',
            'subtitle' => 'Напиши Email указанный при регистрации.',
            'back_link' => 'Назад',
            'submit' => 'Далее',
        ],
        'callback_popup' => [
            'success' => '<span>Спасибо за заявку!</span><span>В ближайшее время с Вами свяжется наш менеджер</span>',
            'title' => 'Обратный звонок',
            'telephone_label' => 'Номер телефона:*',
            'telephone_placeholder' => 'Телефон*',
            'name_label' => 'Имя:*',
            'name_placeholder' => 'Иван*',
            'today' => 'Сегодня',
            'tomorrow' => 'Завтра',
            'submit' => 'Жду звонка',
        ],
        'review_popup' => [
            'success' => 'Спасибо за вопрос!',
            'title' => 'Задать вопрос',
            'placeholder_name' => 'Имя*',
            'placeholder_email' => 'Email*',
            'placeholder_question' => 'Ваш вопрос*',
            'submit' => 'Отправить',
        ]
    ],
    'home' => [
        'character_text' => 'Привет, я Илья. Помогу тебе с выбором велосипеда, это просто, если заполнить поля - вот здесь. Или почитать статью в <a href="https://lisoped.ua/news">блоге</a>.'
    ],
    'size_popup' => [
        'title' => '<p>Если вы сомневаетесь, какой размер вам лучше подойдет – обращайтесь к нам, будем рады помочь.</p>
            <p>Если ваш рост находится на границе размеров рам, мы рекомендуем воспользоваться таким правилом:</p>
            <ul>
                <li>-  для более спортивной езды, мы рекомендуем выбрать раму меньшего размера;</li>
                <li>-  для более умеренной, комфортной езды, следует выбирать раму большего размера.</li>
            </ul>'
    ]
];
