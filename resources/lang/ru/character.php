<?php
return [
  'article' => [
    'phrase' => 'Предлагай темы, которые ты хотел бы почитать. Просто напиши нам: <a href="mailto::email">:email</a>'
  ],
  'faq' => [
    'phrase' => 'Остались вопросы? <a href="#callback-popup" class="btn__callback to-popup">Позвони нам</a>.'
  ],
  'wakeup' => [
    'phrase' => 'Эй! Ты где?'
  ],
  'lenguage' => [
    'phrase' => 'Вибач, українська версія сайту ще в розробцi. Скоро буде :)'
  ],
  'purchased' => [
    'phrase' => 'Как покупка? Оставь свой отзыв'
  ]
];