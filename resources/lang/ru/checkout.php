<?php
return [
    'shipping' => [
        'new_post_courier' => [
            'placeholder' => 'Адрес для доставки*'
        ],
        'new_post_office' => [
            'placeholder_city' => 'Город*',
            'choose_office' => 'Выбери отделение',
            'invalid_select' => 'Пожалуйста, выбери город из выпадающего списка',
        ]
    ],
    'cart_list' => [
        'title' => 'Корзина',
        'color' => 'Цвет:',
        'size' => 'Размер:',
        'total' => 'Сумма:',
        'text_continue' => 'Продолжить покупки',
        'text_checkout' => 'Оформить заказ',
    ],
    'checkout' => [
        'title' => 'Корзина',
        'making_order' => 'Оформление заказа',
        'customer' => 'Я новый покупатель',
        'text_account' => 'У меня есть аккаунт',
        'placeholder_first_name' => 'Имя*',
        'placeholder_last_name' => 'Фамилия*',
        'placeholder_email' => 'Email',
        'placeholder_telephone' => 'Телефон*',
        'placeholder_comment' => 'Комментарий к заказу',
        'choose_delivery' => 'Выбери способ доставки*',
        'choose_payment' => 'Выбери способ оплаты*',
        'make_order' => 'Оформить заказ',
        'text_continue' => 'Продолжить покупки',
        'character_text' => 'После оформления заказа, с тобой свяжется наш менеджер.',
    ],
    'checkout_head' => [
        'subtotal' => 'Сумма',
        'delivery' => 'Доставка',
        'total' => 'К оплате',
        'products_quantity' => 'Товаров в корзине:',
        'text_free_delivery' => '*При заказе от :sum доставка бесплатно',
    ],
    'product_list' => [
        'product' => 'Товар',
        'price' => 'Цена',
        'quantity' => 'Количество',
        'total' => 'Сумма',
        'color' => 'Цвет:',
        'size' => 'Размер:',
        'year' => 'Год:',
    ],
    'empty_checkout' => [
        'text' => 'Корзина пуста'
    ],
    'success' => [
        'title' => 'ЗАКАЗ ОТПРАВЛЕН, И МЫ УЖЕ НАЧИНАЕМ РАБОТУ НАД НИМ',
        'sub_title' => 'Номер Вашего заказа:',
        'text' => 'Тебе позвонит наш сотрудник, что бы подтвердить заказ'
    ],
    'library' => [
        'payment' => [
            'bank_cart' => [
                'name' => 'Перевод на карту ПриватБанк'
            ],
            'liqpay' => [
                'name' => 'Оплата онлайн (LiqPay)'
            ],
            'receipt' => [
                'name' => 'Наложенный платеж'
            ],
        ],
        'shipping' => [
            'courier' => [
                'name' => 'Доставка курьером'
            ],
            'office' => [
                'name' => 'Самовывоз из отделения'
            ],
        ]
    ]
];
