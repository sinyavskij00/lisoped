<?php
return [
    'list' => [
        'title' => 'Блог',
        'read_more' => 'Читать'
    ],
    'show' => [
        'comments' => 'Комментарии',
        'answer' => 'Ответить',
        'gave_answer' => 'ответил(а)',
        'form' => [
            'leave_review' => 'Оставить комментарий',
            'placeholder_comment' => 'Комментарий*...',
            'placeholder_name' => 'Имя*',
            'placeholder_email' => 'Email*',
            'submit' => 'Отправить',
        ],
        'to_news_list' => 'К списку статей',
        'cancel' => 'Отмена',
        'placeholder_name' => 'Твое имя*',
        'placeholder_answer' => 'Ответ*...',
    ]
];