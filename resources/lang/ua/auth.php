<?php
return [
    'forgot_password' => [
        'link_was_send' => 'Ссылка для восстановления пароля была отправлена на твой Email.',
        'link_wasnt_send' => 'Произошла ошибка, ссылка не была отправлена.'
    ],
    'login' => [
        'wrong_credentials' => 'Логин и/или пароль неправильный'
    ]
];