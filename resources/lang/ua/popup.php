<?php
return [
    'subscribe' => [
        'success' => 'Спасибо за подписку',
        'new_subscriber' => [
            'subject' => 'У Вас новый подписчик',
            'letter_title' => 'У Вас новый подпичик',
            'text_email' => 'Почта:'
        ],
        'success_subscribe' => [
            'subject' => 'Круто! Теперь ты подписан на нас',
            'letter_title' => 'Круто! Теперь ты подписан на нас',
            'text_password' => 'Твой пароль:',
            'text_login' => 'Твой login:'
        ]
    ],
    'header_callback' => [
        'success' => 'С тобой свяжутся ближайшее время',
        'subject' => 'Заказали обратный звонок',
        'letter_title' => 'Заказали звонок',
        'text_name' => 'Имя:',
        'text_phone' => 'Телефон:',
        'text_comment' => 'Комментарий:',
    ],
    'one_click_buy' => [
        'success' => 'Твой заказ принят. С тобой скоро свяжутся.',
        'subject' => 'Покупка в один клик',
        'letter_title' => 'Заказ в один клик',
        'text_phone' => 'Телефон:',
        'text_product_name' => 'Название товара:',
        'text_product_model' => 'Модель товара:',
        'text_product_sku' => 'Артикул товара:'
    ]
];