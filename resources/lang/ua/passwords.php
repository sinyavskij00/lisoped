<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен содержать не менее 6 символов и быть правильно повторен.',
    'reset' => 'Ваш пароль был изменен!',
    'sent' => 'Мы отправили тебе на email письмо с ссылкой для смены пароля!',
    'token' => 'Токен изменения пароля истек.',
    'user' => "Нет пользователя с таким email.",

];
