<?php
return [
    'header_callback' => [
        'subject' => 'Заказали обратный звонок',
        'title' => 'Заказали обратный звонок',
        'name' => 'Имя',
        'telephone' => 'Телефон',
        'date' => 'Дата:',
        'hours' => 'Часы:',
        'minutes' => 'Минуты:',
    ],
    'subscribe' => [
        'to_admin' => [
            'subject' => 'У Вас новый подписчик',
            'title' => 'У Вас новый подписчик',
            'email' => 'email',
        ],
        'to_subscriber' => [
            'subject' => 'СПАСИБО ЗА ПОДПИСКУ!',
            'contacts_title' => 'Консультация по телефону:',
            'text_thanks_title' => 'СПАСИБО ЗА ПОДПИСКУ!',
            'text_thanks_subtitle' => 'Благодарим Вас за подписку.',
            'text_login' => 'Логин от твоего акаунта -',
            'text_password' => 'Пароль от твоего акаунта -',
            'description' => '<p>Одна из ценностей нашей подписки — уникальные акции и подарки только для подписчиков.</p>
            <p>Читайте популярные статьи нашего сайта, подписывайтесь на интересные блоги экспертов или принимайте
                участие в самых дискуссионных обсуждениях.</p>',
            'text_thanks' => 'Спасибо за подписку',
            'text_footer' => 'С уважением, магазин'
        ]
    ],
    'specials' => [
        'subject' => 'Вопрос со страницы акции',
        'name' => 'Имя:',
        'email' => 'Email:',
        'question' => 'Вопрос:',
    ],
    'contacts' => [
        'subject' => 'Задали вопрос',
        'title' => 'Сообщение с контактной формы',
        'name' => 'Имя:',
        'email' => 'E-mail:',
        'message' => 'Сообщение:'
    ],
    'orders' => [
        'status_changed' => [
            'title' => 'Статус заказа изменен'
        ],
        'new_order' => [
            'consultation_by_phone' => 'Консультация по телефону:',
            'thanks_for_buy' => 'Благодарим Вас, :name, за покупку в нашем интернет - магазине',
            'order_number' => 'Номер Вашего заказа:',
            'status' => 'Статус:',
            'product_name' => 'Наименование',
            'product_statistic' => 'Характеристики',
            'product_price' => 'Цена',
            'product_quantity' => 'К-ство',
            'product_total' => 'Сумма',
            'size' => 'Размер:',
            'color' => 'Цвет:',
            'year' => 'Год:',
            'shipping_price' => 'Стоимость доставки',
            'total' => 'Итого',
            'product' => 'товара'
        ]
    ],
    'information' => [
        'review_accepted' => [
            'title' => 'Твой отзыв был одобрен',
            'review' => 'Отзыв:'
        ]
    ]
];