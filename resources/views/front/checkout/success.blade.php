@extends('front.layout')
@section('content')
		<main id="my-content" class="page-content page-success p-padding">

        <div class="container">
            <div class="success-content">
                <div class="text-holder">
                    <div class="success-content__title">{{__('checkout.success.title')}}</div>
                    <p class="success-content__text">{{__('checkout.success.sub_title')}} {{$public_id}}</p>
                    <div class="success-content__text">{{__('checkout.success.text')}}</div>
                </div>
                <img class="success-content__img" src="images/charackter/Lisoped_man_ok.svg" alt="{{__('checkout.success.title')}}">
            </div>
        </div><!-- /container -->
        <div class="page-overlay"></div>
    </main>
@endsection