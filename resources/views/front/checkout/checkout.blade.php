@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding page-checkout">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="p-title">{{__('checkout.checkout.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">

                @include('front.checkout.checkout_product_list', ['cart' => $checkout->cart])


                <div class="col-md-5 col-lg-4">
                    <div class="checkout-title">{{__('checkout.checkout.making_order')}}</div>

                    <div class="checkout">
                        @include('front.checkout.checkout_head', [
                        'subTotal' => $checkout->subTotal,
                        'shippingPayment' => $checkout->shippingPayment,
                        'total' => $checkout->total,
                        'productQuantity' => $checkout->cart->items()->count()])

                        <form class="checkout-form">
                            @csrf
                            @if(!Auth::check())
                                <div class="checkout-form__head">
                                    <span>{{__('checkout.checkout.customer')}}</span>
                                    <a href="#log-in-popup" class="to-popup">{{__('checkout.checkout.text_account')}}</a>
                                </div>
                            @endif
                            <div class="checkout-row">
                                <input type="text" name="first_name" required="" class="input"
                                       value="{{$checkout->userInfo['first_name']}}"
                                       placeholder="{{__('checkout.checkout.placeholder_first_name')}}">
                            </div>
                            <div class="checkout-row">
                                <input type="text" name="last_name" required="" class="input"
                                       value="{{$checkout->userInfo['last_name']}}" placeholder="{{__('checkout.checkout.placeholder_last_name')}}">
                            </div>
                            <div class="checkout-row">
                                <input type="tel" name="telephone" required="" class="input"
                                       value="{{$checkout->userInfo['telephone']}}" placeholder="{{__('checkout.checkout.placeholder_telephone')}}">
                            </div>
                            <div class="checkout-row">
                                <input type="email" name="email" class="input"
                                       value="{{$checkout->userInfo['email']}}" placeholder="{{__('checkout.checkout.placeholder_email')}}">
                            </div>

                            <div class="shipping-block">
                                <div class="checkout-row">
                                    <select name="shipping_code" id="order-shipping">
                                        <option value="">{{__('checkout.checkout.choose_payment')}}</option>
                                        @foreach($checkout->shippingMethods as $shippingMethod)
                                            <option {!! $shippingMethod->getCode() === $checkout->selectedShippingMethod->getCode() ? 'selected' : '' !!}
                                                    value="{{$shippingMethod->getCode()}}">{{$shippingMethod->getName()}} ({{$shippingMethod->getCostTitle()}})</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div id="current-shipping-info">
                                    @if($checkout->selectedShippingMethod)
                                        {!! $checkout->selectedShippingMethod->show() !!}
                                    @endif
                                </div>
                            </div>

                            <div class="payment-block">
                                <select name="payment_code" id="order-payment" class="select">
                                    <option value="">Выберите способ оплаты</option>
                                    @foreach($checkout->paymentMethods as $paymentMethod)
                                        <option value="{{$paymentMethod->getCode()}}">{{$paymentMethod->getName()}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="checkout__row">
                                <textarea name="order_comment" rows="1" class="input"
                                          placeholder="{{__('checkout.checkout.placeholder_comment')}}">{{$checkout->comment}}</textarea>
                            </div>

                            <div class="btn-wrap">
                                <button type="submit" class="btn"><span>{{__('checkout.checkout.make_order')}}</span></button>
                                <a href="{{route('home')}}" class="btn-continue">{{__('checkout.checkout.text_continue')}}</a>
                            </div>

                        </form>

                    </div>
                    <!-- /.checkout-form -->

                </div><!-- /.col -->

            </div>
        </div><!-- /container -->

        <div class="page-overlay"></div>
    </main>

    <script type="text/javascript">
        $(function () {
            var shippingSelect = $('#order-shipping').selectize();

            shippingSelect[0].selectize.on('change', function (code) {
                getShippingInfo(code)
            });

            function getShippingInfo(code) {
                $.ajax({
                    url: `{{route('checkout.shippingInfo')}}`,
                    data: {
                        shipping_code: code
                    },
                    success: function (result) {
                        if(result['status'] && result['status'] === 1){
                            $('#current-shipping-info').html(result['template']);
                            $('.checkout .checkout-head').replaceWith(result['checkout_head']);
                        }
                    }
                });
            }
        });


        $('.checkout-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: `{{route('checkout.confirm')}}`,
                data: $(this).serialize(),
                type: 'POST',
                success: function (result) {
                    if(result['status'] && result['status'] === 1){
                        var html = '<div class="hidden" id="confirm-form">' + result['template'] + '</div>';
                        $('body').append(html);
                        $('#confirm-form form').submit();
                    }
                }
            });
        });
    </script>

@endsection