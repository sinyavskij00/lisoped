<div class="popup-header">{{__('checkout.cart_list.title')}}</div>

<div class="cart-product__list">
    @foreach($cart->items() as $cartItem)
        <div class="cart-product">
            <div class="cart-product__col">
                @if(isset($cartItem->sku) && isset($cartItem->sku->product) && isset($cartItem->sku->product->image))
                    <div class="cart-product__img">
                        <img src="{{$catalogService->resize($cartItem->sku->product->image, 90, 60)}}">
                    </div>
                @endif
                <div class="cart-product__descr">
                    <h6 class="cart-product__name">
                        @if (isset($cartItem->sku->product->slug))
                            <a href="{{route('product_path', $cartItem->sku->product->slug)}}">
                                {{$cartItem->sku->product->localDescription->name}}
                            </a>
                        @endif
                    </h6>
                    <div class="cart-product__options">
                        @if($cartItem->sku->color)
                            <div class="option-item">
                                <span class="option-item__name">{{__('checkout.cart_list.color')}}</span>
                                <span class="option-item__value">{{$cartItem->sku->color->localDescription->name}}</span>
                            </div>
                        @endif
                        @if($cartItem->sku->size)
                            <div class="option-item">
                                <span class="option-item__name">{{__('checkout.cart_list.size')}}</span>
                                <span class="option-item__value">{{$cartItem->sku->size->value}}</span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="cart-product__col cart-product__info">
                <div class="cart-product__price">{{$catalogService->getSkuPrice($cartItem->sku, $cartItem->quantity, null, false, true)}}</div>
                <div class="cart-product__count">
                    <div class="cart-product__count-decrease" onclick="changeCartItemQuantity({{$cartItem->id}}, -1)"><i class="icon-caret-left"></i></div>
                    <input type="text" readonly="" name="count" value="{{$cartItem->quantity}}">
                    <div class="cart-product__count-increase" onclick="changeCartItemQuantity({{$cartItem->id}}, 1)"><i class="icon-caret-right"></i></div>
                </div>
                <div class="cart-product__count-price">{{$catalogService->getSkuPrice($cartItem->sku, $cartItem->quantity, null, true, true)}}</div>
                <button class="remove" onclick="deleteCartItem({{$cartItem->id}})"></button>
            </div>
        </div><!-- /.cart-product -->
    @endforeach
</div><!-- /.cart-product__list -->

<div class="popup-footer">
    <div class="cart-total">
        <span class="text">{{__('checkout.cart_list.total')}}</span>
        <span class="sum">{{$catalogService->format($catalogService->calculate($cart->subTotal()))}}</span>
    </div>
    <div class="btn-wrap">
        <a href="#" class="btn-continue">{{__('checkout.cart_list.text_continue')}}</a>
        @if($cart->items()->count() > 0)
        <a href="{{route('checkout')}}" class="btn">
            <span>{{__('checkout.cart_list.text_checkout')}}</span>
        </a>
        @endif
    </div>
</div>
