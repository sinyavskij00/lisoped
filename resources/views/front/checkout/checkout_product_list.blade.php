<div id="checkout-product-list" class="col-md-7 col-lg-8">
    <div class="cart-product__list-header d-flex align-items-center justify-content-between">
        <div class="cart-product__list-item cart-product__list-item--name">
            <span>{{__('checkout.product_list.product')}}</span>
        </div>
        <div class="cart-product__list-item cart-product__list-item--info">
            <span>{{__('checkout.product_list.price')}}</span>
            <span>{{__('checkout.product_list.quantity')}}</span>
            <span>{{__('checkout.product_list.total')}}</span>
            <span></span>
        </div>
    </div>

    <div class="cart-product__list cart-product__list--checkout">
        @foreach($cart->items() as $cartItem)
            <div class="cart-product">
                <div class="cart-product__col">
                    @if($cartItem->sku->images->count() > 0)
                        <div class="cart-product__img">
                            <img src="{{$catalogService->resize($cartItem->sku->images->first()->image, 90, 60)}}">
                        </div>
                    @elseif($cartItem->sku->product->image)
                        <div class="cart-product__img">
                            <img src="{{$catalogService->resize($cartItem->sku->product->image, 90, 60)}}">
                        </div>
                    @endif
                    <div class="cart-product__descr">
                        <h6 class="cart-product__name"><a
                                    href="{{route('product_path', $cartItem->sku->product->slug)}}">{{$cartItem->sku->product->localDescription->name}}</a>
                        </h6>
                        <div class="cart-product__options">
                            @if(isset($cartItem->sku->color->localDescription))
                            <div class="option-item">
                                <span class="option-item__name">{{__('checkout.product_list.color')}}</span>
                                <span class="option-item__value">{{$cartItem->sku->color->localDescription->name}}</span>
                            </div>
                            @endif
                            @if(isset($cartItem->sku->size))
                                <div class="option-item">
                                    <span class="option-item__name">{{__('checkout.product_list.size')}}</span>
                                    <span class="option-item__value">{{$cartItem->sku->size->value}}</span>
                                </div>
                            @endif
                            @if(isset($cartItem->sku->year))
                                <div class="option-item">
                                    <span class="option-item__name">{{__('checkout.product_list.year')}}</span>
                                    <span class="option-item__value">{{$cartItem->sku->year->value}}</span>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="cart-product__col cart-product__info">
                    <div class="cart-product__price">{{$catalogService->getSkuPrice($cartItem->sku, $cartItem->quantity, null, false, true)}}</div>
                    <div class="cart-product__count">
                        <div class="cart-product__count-decrease" onclick="changeCartItemQuantity({{$cartItem->id}}, -1)"><i class="icon-caret-left"></i></div>
                        <input type="text" readonly="" name="count" value="{{$cartItem->quantity}}">
                        <div class="cart-product__count-increase" onclick="changeCartItemQuantity({{$cartItem->id}}, 1)"><i class="icon-caret-right"></i></div>
                    </div>
                    <div class="cart-product__count-price">{{$catalogService->getSkuPrice($cartItem->sku, $cartItem->quantity, null, true, true)}}</div>
                    <button class="remove" onclick="deleteCartItem({{$cartItem->id}})"></button>
                </div>
            </div><!-- /.cart-product -->
        @endforeach
    </div><!-- /.cart-product__list -->
</div>