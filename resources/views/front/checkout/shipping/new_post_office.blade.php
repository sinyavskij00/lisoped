<div class="checkout__row checkout__row-city">
    <input id="city" type="text" name="city" class="input" autocomplete="off" value="{{$cityName}}"
           data-search-url="{{route('newpost.city')}}" placeholder="{{__('checkout.shipping.new_post_office.placeholder_city')}}" required="required">
    <input id="city-id-selected" name="city_ref" required="" type="hidden" value="{{$cityRef}}">
    <div id="invalidate-selection" class="error"></div>

    <div class="search-value__container">
        <ul class="search-value__list hidden">
        </ul>
    </div>
</div>

<div class="checkout__row">
    <select name="store_ref" id="order-store" required="">
        <option value="">{{__('checkout.shipping.new_post_office.choose_office')}}</option>
        @if(isset($storeRef) && isset($storeName) && isset($currentCityStores))
            @foreach($currentCityStores as $store)
                <option value="{{$store->Ref}}" {{ $store->Ref === $storeRef ? 'selected' : '' }} >{{$store->DescriptionRu}}</option>
            @endforeach
        @endif
    </select>
</div>
<script type="text/javascript">
    $(function () {
        var storeSelect = $('#order-store').selectize();
        var storeSelectize = storeSelect[0].selectize;
        // Initialize ajax autocomplete:
        $('#city').autocomplete({
            serviceUrl: `{{route('newpost.city')}}`,
            dataType: 'json',
            paramName: 'city',
						appendTo: '.checkout__row-city',
						triggerSelectOnValidInput: false,
            onSelect: function (suggestion) {
                $('#city-id-selected').val(suggestion.data);
                $('#invalidate-selection').hide();
                $.ajax({
                    url: `{{route('newpost.stores')}}`,
                    data: {
                        cityRef: suggestion.data
                    },
                    success: function (result) {
                        storeSelectize.clearOptions();
                        if (result) {
                            for (var i = 0; i < result.length; i++) {
                                storeSelectize.addOption({
                                    value: result[i]['value'],
                                    text: result[i]['text']
                                });
                            }
                            storeSelectize.refreshOptions();
                        }
                    }
                });

            },
            onInvalidateSelection: function () {
                $('#invalidate-selection').show().html('{{__('checkout.shipping.new_post_office.invalid_select')}}');
            }
        });
    });
</script>