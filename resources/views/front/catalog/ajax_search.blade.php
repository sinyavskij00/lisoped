@if($result['products']->count() > 0)
    <div id="live-search" class="live-search">
        <div class="cart-product__list">
            @foreach($result['products'] as $product)
                <div class="cart-product">
                    @if($product->image)
                        <div class="cart-product__img">
                            <img src="{{$catalogService->resize($product->image, 90, 60)}}"
                                 alt="{{$product->name}}">
                        </div>
                    @endif
                    <div class="cart-product__descr-wrapper">
                        <div class="cart-product__descr">
                            <h6 class="cart-product__name">
                                <a href="{{route('product_path', $product->slug)}}">{{$product->name}}</a>
                            </h6>
                            @if(isset($product->quantity) && $product->quantity > 0)
                                <span class="product-info__availability product-info__availability--available">{{__('catalog.ajax_search.available')}}</span>
                            @else
                                <span class="product-item__availability availability not-available">{{__('catalog.ajax_search.not_available')}}</span>
                            @endif
                        </div>
                        <div class="cart-product__price">{{$catalogService->format($catalogService->calculate($product->price))}}</div>
                    </div>
                </div><!-- /.cart-product -->
            @endforeach
        </div><!-- /.cart-product__list -->

        <div class="result-text">
            <a href="{{$result['total_link']}}">{{__('catalog.ajax_search.show_all')}} ({{$result['total_count']}})</a>
        </div>
    </div>
@endif
