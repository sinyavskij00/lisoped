@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h2 class="p-title">{{$meta_h1}}</h2>
                </div>
            </div>
        </div>

        <div class="categories-grid">
            <div class="container">
                <div class="row">
                    @foreach($category->children->where('status', '=', 1) as $child)
                        <div class="col-xl-3 col-lg-4 col-sm-6">
                            <a href="{{route('category_path', category_path($child->id))}}" class="category-item">
                                @if(Storage::disk('public')->exists($child->image))
                                    <span class="category-item__img">
                                <img src="{{$catalogService->resize($child->image, 200, 125, false)}}"
                                     alt="{{$child->localDescription->name}}">
                                    </span>
                                @endif
                                @if(isset($child->localDescription))
                                    <span class="category-item__name">
                                        <span class="line">{{ $child->localDescription->name }}</span>
                                    </span>
                                @endif
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @if(!empty($description))
            <section class="description">
                <div class="container">

                    <div class="text-description show-text">
                        <div>
                            <div class="text-wrap">
                                <div class="text">
                                    {!! $description !!}
                                </div>
                            </div>
                            <div class="btn-wrap hidden">
                                <span class="btn__text-more">{{__('catalog.product_listing.read_more')}}</span>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- /.description ===================================== -->
        @endif

        <div class="page-overlay"></div>

    </main>
@endsection
