@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding__sm">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('catalog.manufacturer_listing.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">

            <div class="manufacturers-list d-flex flex-wrap">
                @foreach($manufacturers as $manufacturer)
                <a href="{{route('manufacturer.show', $manufacturer->slug)}}" class="manufacturer-item">
                    <div class="manufacturer-item__img">
                        <img src="{{Storage::disk('public')->url($manufacturer->image)}}" alt="{{$manufacturer->localDescription->name}}">
                    </div>
                </a>
                @endforeach
            </div>

        </div><!-- /container -->


        <div class="page-overlay"></div>

    </main>
@endsection