@extends('front.layout')
@section('content')
    <div id="my-content" class="page-content p-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
            </div>
        </div>

        <div class="catalog-content">
            <div class="container">
                <div class="text empty-search">
                    <div class="h3">{{__('catalog.search.title', ['query' => isset($query) ? $query : ''])}}</div>
                    <p>{{__('catalog.search.sub_title', ['query' => isset($query) ? $query : ''])}}</p>
                </div>
                <div class="slider-section">
                    <h2 class="slider-title">{{__('catalog.search.carousel_title')}}</h2>
                    <div class="slider-products">
                        @foreach($products as $product)
                            <div class="slider-products__item">
                                @include('front.catalog.product_item', ['product' => $product])
                            </div>
                            <!-- /.slider-products__item -->
                        @endforeach
                    </div>
                    <!-- /.slider-products -->

                </div>
                <!-- /.slider-section -->
            </div>
        </div>

        @if(!empty($description))
            <section class="description">
                <div class="container">

                    <div class="text-description show-text">
                        <div>
                            <div class="text-wrap">
                                <div class="text">
                                    {!! $description !!}
                                </div>
                            </div>
                            <div class="btn-wrap hidden">
                                <span class="btn__text-more">{{__('catalog.product_listing.read_more')}}</span>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- /.description ===================================== -->
    @endif

        <div class="page-overlay">
        </div>
    </div>
    <!-- /.page-content ===================================== -->
@endsection