@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content page-compare p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('catalog.compare.category.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            @if($categories->count() > 0)
                <ul class="comparison-index ul">
                    @foreach($categories as $category)
                        @if(isset($category->product_count) && $category->product_count > 0)
                        <li class="comparison-index__item">
                            <a class="comparison-index__link"
                               href="{{route('compare.productList', ['id' => $category->id])}}">
                                {{$category->localDescription->name}} ({{$category->product_count}})
                            </a>
                        </li>
                        @endif
                    @endforeach
                </ul>

                <div class="compare-empty hidden">
                    <p class="compare-empty__text">Товары для сравнения не выбраны. Чтобы это сделать отметь специальным значком товары, которые хочешь сравнить</p>
                    <div class="compare-empty__img">
                        <img src="images/charackter/Lisoped_man_compare.png" alt="Илья">
                    </div>
                </div>
            @endif
        </div><!-- /container -->
        <div class="page-overlay"></div>
    </main>
@endsection