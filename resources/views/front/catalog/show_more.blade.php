@foreach($products as $product)
    <div class="col-sm-6">
    @include('front.catalog.product_item', ['product' => $product])
    <!-- /.product-item -->
    </div>
@endforeach