<div id="current-sku-content">
    <div class="row align-items-md-center align-items-lg-start">
        <div class="col-md-6 col-lg-7">
            <div class="product-images">

                <div class="product-item__stickers">
                    @if($currentSku->getAttributeValue('special_price'))
                        <div class="product-item__sticker sticker_sale">
                            <span class="sticker_text">{{__('catalog.labels.sale')}}</span>
                            <span class="sticker_percent">-{{ (int) round((1 - ($currentSku->getAttributeValue('special_price') / $currentSku->getAttributeValue('price'))) * 100)}} %</span>
                        </div>
                    @endif
                    @if($product->getAttributeValue('is_our_choice'))
                        <div class="product-item__sticker sticker_our-choice">
                            <span class="sticker_text">{{__('catalog.labels.our_choice')}}</span>
                        </div>
                    @endif
                    @if($product->getAttributeValue('is_new'))
                        <div class="product-item__sticker sticker_new">
                            <span class="sticker_text">{{__('catalog.labels.new')}}</span>
                        </div>
                    @endif
                    @if($product->getAttributeValue('is_hit'))
                        <div class="product-item__sticker sticker_hit">
                            <span class="sticker_text">{{__('catalog.labels.hit')}}</span>
                        </div>
                    @endif
                </div>
                <ul id="glasscase" class="gc-start">
                    @foreach($currentSku->images as $image)
                        <li><img src="{{$catalogService->resize($image->getAttributeValue('image'), 1500, 900)}}"
                                 title="{{$product->getAttributeValue('name')}}"
                                 alt="{{$product->getAttributeValue('name')}}">
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- /.product-images ====================-->
        <div class="col-md-6 col-lg-5">
            <div class="product-info">
                <div class="product-info__top d-flex align-items-center justify-content-between">
                    <div class="rating-wrap">
                        <div class="product-info__rating rating">
                            @for ($i = 1; $i <= 5; $i++)
                                @if($product->rating >= $i)
                                    <i class="icon-star-full i_stack-2x"></i>
                                @else
                                    <i class="icon-star-full i_stack-1x"></i>
                                @endif
                            @endfor
                        </div>
                        <a href="#product-info__tabs" class="reviews-number">
                            ({{$product->activeReviews->count()}} {{__('catalog.current_sku.reviews')}})
                        </a>
                    </div>
                    <div class="product-info__btns">
                        @if($compare->isCompared($product))
                            <button type="button" onclick="compare.delete({{$product->getAttributeValue('id')}}, this)"
                                    class="product-item__btn" title="В сравнение">
                                <i class="i i-lawyer accent"></i>
                            </button>
                        @else
                            <button type="button" onclick="compare.add({{$product->getAttributeValue('id')}}, this)"
                                    class="product-item__btn" title="В сравнение">
                                <i class="i i-lawyer"></i>
                            </button>
                        @endif

                        @if($wishListService->isWished($product))
                            <button type="button" onclick="wishlist.delete({{$product->getAttributeValue('id')}}, this)"
                                    class="product-item__btn" title="В закладки">
                                <i class="i icon-heart"></i>
                            </button>
                        @else
                            <button type="button" onclick="wishlist.add({{$product->getAttributeValue('id')}}, this)"
                                    class="product-item__btn" title="В закладки">
                                <i class="i i-heart"></i>
                            </button>
                        @endif
                    </div>
                </div>

                <div class="product-info__d-list">
                    <div class="product-info__d-item">
                        <span class="product-info__d-item--name">{{__('catalog.current_sku.sku')}}</span>
                        <span class="product-info__d-item--value">{{substr($currentSku->getAttributeValue('sku'), 0, iconv_strlen($currentSku->getAttributeValue('sku')) - 6) . '-' . substr($currentSku->getAttributeValue('sku'), iconv_strlen($currentSku->getAttributeValue('sku')) - 6, iconv_strlen($currentSku->getAttributeValue('sku')))}}</span>
                    </div>
                    @if($product->getAttributeValue('manufacturer_name'))
                        <div class="product-info__d-item">
                            <span class="product-info__d-item--name">{{__('catalog.current_sku.manufacturer')}}</span>
                            <span class="product-info__d-item--value">{{$product->getAttributeValue('manufacturer_name')}}</span>
                        </div>
                    @endif
                </div>
                <div class="product-info__options">
                    @if($years->count() > 0 && !is_null($years[0]))
                        <div class="product-info__option product-info__option--size">
                            <div class="product-info__option-name">{{__('catalog.current_sku.year')}}</div>
                            <div class="product-info__option--flex">
                                <div class="product-info__option-values">
                                    @foreach($years as $year)
                                        @if(isset($year->value))
                                        <label class="product-info__option-value">
                                            <input type="radio" name="year"
                                                   value="{{$year->value}}"
                                                   {{$year->checked ? 'checked' : ''}}
                                                   class="product-info__option-value--input">
                                            <span class="product-info__option-value--size">{{$year->value}}</span>
                                        </label>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    @if($colors->count() > 0 && !is_null($colors[0]))
                        <div class="product-info__option product-info__option--color">
                            <div class="product-info__option-name">{{__('catalog.current_sku.color')}}</div>
                            <div class="product-info__option-values">
                                @foreach($colors as $color)
                                    @if(Storage::disk('public')->exists($color->image))
                                        @if(isset($color->anchor))
                                        <label class="product-info__option-value">
                                            <input type="radio" name="color"
                                                   value="{{$color->anchor}}"
                                                   {{$color->checked ? 'checked' : ''}}
                                                   class="product-info__option-value--input">
                                            <img class="product-info__option-value--color"
                                                 src="{{Storage::disk('public')->url($color->image)}}"
                                                 title="{{$color->localDescription->name}}">
                                        </label>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    @endif
                    @if($sizes->count() > 0 && !is_null($sizes[0]))
                        <div class="product-info__option product-info__option--size">
                            <div class="product-info__option-name">{{__('catalog.current_sku.size')}}</div>
                            <div class="product-info__option--flex">
                                <div class="product-info__option-values">
                                    @foreach($sizes as $size)
                                        @if(isset($size->value))
                                            <label class="product-info__option-value">
                                                <input type="radio" name="size"
                                                       value="{{$size->value}}"
                                                       {{$size->checked ? 'checked' : ''}}
                                                       class="product-info__option-value--input">
                                                <span class="product-info__option-value--size">{{$size->value}} </span>
                                                @if($size->comment)
                                                    <span class="product-info__option-value--tooltip">
                                                    <span class="accent">{{$size->value}}</span>
                                                    ({{$size->comment}})
                                                </span>
                                                @endif
                                            </label>
                                        @endif
                                    @endforeach
                                </div>
                                @if(!empty($sizeTables))
                                <a href="#sizes-popup" class="product-info__sizes-link to-popup">{{__('catalog.current_sku.size_table')}}</a>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>

                <div class="product-info__price">
                    @if($currentSku->getAttributeValue('special_price'))
                        <div class="main-price">{{$catalogService->format($catalogService->calculate($currentSku->getAttributeValue('special_price')))}}</div>
                        <div class="old-price">{{$catalogService->format($catalogService->calculate($currentSku->getAttributeValue('price')))}}</div>
                    @else
                        <div class="main-price">{{$catalogService->format($catalogService->calculate($currentSku->getAttributeValue('price')))}}</div>
                    @endif
                </div>

                <div class="product-info__main-controls">
                    @if($currentSku->getAttributeValue('quantity') > 0)
                        <div class="product-info__main-controls--col">
                            <div class="product-info__count">
                                <div class="product-info__button-decrease" onclick="changeProductQuantity(-1)">-</div>
                                <input class="product-info__count-value" type="text" readonly="" id="quantity"
                                       name="quantity"
                                       value="1">
                                <div class="product-info__button-increase" onclick="changeProductQuantity(1)">+</div>
                            </div>
                            <span class="product-info__availability product-info__availability--available">{{__('catalog.current_sku.available')}}</span>
                        </div>
                    @endif
                    <div class="product-info__main-controls--col">
                        @if($currentSku->getAttributeValue('quantity') > 0)
                            <a href="#cart-popup" id="addProductToCart"
                               data-sku-id="{{$currentSku->id}}"
                               class="btn to-popup"><span>{{__('catalog.current_sku.btn_buy')}}</span></a>
                            <a href="#one-click-buy__popup" class="buy-one-click-btn to-popup">
                                <span>{{__('catalog.current_sku.buy_one_click')}}</span>
                            </a>
                        @else
                            <a id="notAvailability" href="#preorder" class="btn btn__accent btn__notavailable to-popup">
                                <span>{{__('catalog.current_sku.not_available')}}</span>
                            </a>

                            <div class="product-item__availability availability not-available">
                                {{__('catalog.product_item.not_available')}}
                            </div>
                        @endif
                    </div>
                </div>
                @if(!empty($productAdvantages) && $productAdvantages->count() > 0)
                <div class="product-info__bottom">
                    @foreach($productAdvantages as $productAdvantage)
                    <a href="#product-advantage-{{$productAdvantage->id}}" class="product-info__bottom-link to-popup">
                        <span class="{{$productAdvantage->icon}}"></span>
                        <span class="text">{{$productAdvantage->localDescription->title}}</span>
                    </a>
                    @endforeach
                </div>

                <div class="hidden">
                    @foreach($productAdvantages as $productAdvantage)
                    <div id="product-advantage-{{$productAdvantage->id}}" class="product-info__popup">
                        {!! $productAdvantage->localDescription->text !!}
                        <div class="t-a-right">
                            <a style="border-bottom: 1px dashed #656565;" href="{{$productAdvantage->link}}">{{__('catalog.current_sku.product_advantages.btn_more')}}</a>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
        <!-- /.product-info ====================-->
    </div>

    <!-- buy popups =================== -->
    <div class="hidden">
        <div id="one-click-buy__popup" class="one-click__popup popup">
            <form action="" class="popup-form">
                @csrf
                <div class="success">
                    <div class="s-inner">
                        {!! __('catalog.current_sku.one_click_buy_form.success') !!}
                    </div>
                </div>
                <div class="formhead">{{__('catalog.current_sku.one_click_buy_form.title')}}</div>
                <input type="hidden" name="sku_id" value="{{$currentSku->getAttributeValue('id')}}">
                <label>
                    <span>{{__('catalog.current_sku.one_click_buy_form.name')}}</span>
                    <input type="text" name="name" required
                           value="{{Auth::check() ? Auth::user()->first_name . ' ' . Auth::user()->last_name : ''}}">
                </label>
                <label>
                    <span>{{__('catalog.current_sku.one_click_buy_form.telephone')}}</span>
                    <input type="tel" name="phone" required value="{{ Auth::check() ? Auth::user()->telephone : '' }}">
                </label>
                <label>
                    <span>{{__('catalog.current_sku.one_click_buy_form.comment')}}</span>
                    <textarea name="text" rows="1" maxlength="300"></textarea>
                </label>
                <input type="hidden" name="product_id" value="{{$product->getAttributeValue('name')}}">
                <div class="btn-submit__wrap">
                    <button type="submit" class="btn">
                        <span>{{__('catalog.current_sku.one_click_buy_form.submit')}}</span>
                    </button>
                </div>
            </form>
        </div><!-- /#one-click-buy__popup -->

        <div id="preorder" class="one-click__popup popup">
            <form class="popup-form">
                @csrf
                <input type="hidden" name="sku_id" value="{{$currentSku->getAttributeValue('id')}}">
                <label>
                    <span>{{__('catalog.current_sku.preorder.name')}}</span>
                    <input type="text" name="name" required
                           value="{{Auth::check() ? Auth::user()->first_name . ' ' . Auth::user()->last_name : ''}}">
                </label>
                <label>
                    <span>{{__('catalog.current_sku.preorder.telephone')}}</span>
                    <input type="tel" name="telephone" required value="{{ Auth::check() ? Auth::user()->telephone : '' }}">
                </label>
                <label>
                    <span>{{__('catalog.current_sku.preorder.email')}}</span>
                    <input type="email" name="email" required value="{{ Auth::check() ? Auth::user()->email : '' }}">
                </label>
                <label>
                    <span>{{__('catalog.current_sku.preorder.comment')}}</span>
                    <textarea name="comment" maxlength="300"></textarea>
                </label>
                <div class="btn-submit__wrap">
                    <button type="submit" class="btn">
                        <span>{{__('catalog.current_sku.preorder.submit')}}</span>
                    </button>
                </div>
            </form>
            <script>
                $('#preorder form').on('submit', function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: '{{route('popupProcessor.preorder')}}',
                        type: 'POST',
                        data: $(this).serialize(),
                        success: function (result) {
                            if (result['status'] && result['status'] === 1) {
                                $('#preorder').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                                $('#preorder form').addClass('hidden');
                                setTimeout(function () {
                                    $('#preorder .success').remove();
                                    $.magnificPopup.close();
                                    $('#preorder form')[0].reset().removeClass('hidden');
                                }, 4000);
                            }
                        }
                    });
                });
            </script>
        </div>
    </div>
    <!-- /buy popups =================== -->
</div>
