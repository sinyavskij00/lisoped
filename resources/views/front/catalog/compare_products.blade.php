@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="p-title">{{__('catalog.compare.product.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">

            <div class="compare__block">

                <div class="page-compare__left">
                    <div class="one-goods__first table-compare__item"><span class="title">{{__('catalog.compare.product.products')}}</span></div>
                    <div class="table-compare">
                        <div class="table-compare__item"><span class="title">{{__('catalog.compare.product.price')}}</span></div>
                        @foreach($attributes as $attribute)
                        <div class="table-compare__item"><span class="title">{{$attribute->localDescription->name}}</span></div>
                        @endforeach
                    </div>
                </div>

                <div class="page-compare__right">
                    <div class="compare-page__slider">

                        @foreach($products as $product)
                        <div class="item compare-product-{{$product->id}}">
                            <div class="one-goods one-goods__first">
                                <button class="action delete" onclick="compare.delete({{$product->id}})" title="{{__('catalog.compare.product.btn_delete')}}"><i class="icon-close2"></i></button>
                                <a class="one-goods__image" href="{{route('product_path', $product->slug)}}" title="{{$product->name}}">
                                    <img class="product-image-photo" src="{{$catalogService->resize($product->image, 200, 200)}}" title="{{$product->name}}" alt="{{$product->name}}">
                                </a>
                                <a href="{{route('product_path', $product->slug)}}" class="one-goods__name" title="{{$product->name}}">{{$product->name}}</a>
                                <span class="one-goods__code">{{__('catalog.compare.product.sku')}} {{$product->sku}}</span>
                            </div>
                            <div class="table-compare">
                                <div class="table-compare__item">
                                    <span class="comparison-product__prices">
                                        <span class="comparison-product__price comparison-product__price">{{$catalogService->format($catalogService->calculate($product->price))}}</span>
                                    </span>
                                    <span class="comparison-product__btn-wrapper">
										<a href="{{route('product_path', $product->slug)}}" class="btn"><span>{{__('catalog.compare.product.btn_more')}}</span></a>
                                    </span>
                                </div>
                                @foreach($attributes as $attribute)
                                    @foreach($product->productAttributes as $productAttribute)
                                        @if($productAttribute->attribute_id === $attribute->id)
                                            <div class="table-compare__item">{{$productAttribute->text}}</div>
                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                        </div><!-- /.item -->
                        @endforeach
                    </div>
                </div>
            </div><!-- /.compare__block -->
        </div><!-- /container -->


        <div class="page-overlay"></div>

    </main>
@endsection
