@extends('front.layout')
@section('content')
    <div id="my-content" class="page-content p-padding">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
            </div>
        </div>

        <div class="catalog-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col filter__wrapper">
                        @include('front.catalog.filter')
                    </div>
                    <!-- /.filter ========================= -->

                    <div class="col-lg-8 col-12">
                        <main class="catalog__main-content">

                            <div class="filter-trigger" data-toggle="filter">
                                <i class="icon-filter"></i>
                                <span>{{__('catalog.product_listing.filters')}}</span>
                            </div>

                            <h1 class="p-title">{{$meta_h1}}</h1>

                            <div class="product-sort__group d-sm-flex flex-sm-row align-items-sm-center justify-content-sm-between">

                                <div class="product-sort__item d-sm-flex align-items-center has-dropdown__list">
                                    <span class="product-sort__item-name">{{__('catalog.product_listing.sort_title')}}</span>
                                    @foreach($sorts as $sort)
                                        @if($sort['is_active'])
                                            <span class="product-sort__item-selected">{{$sort['text']}}</span>
                                        @endif
                                    @endforeach
                                    <ul class="product-sort__item-select dropdown-list">
                                        @foreach($sorts as $sort)
                                            @if(!$sort['is_active'])
                                                <li><a href="{{$sort['link']}}">{{$sort['text']}}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>

                                <div class="product-sort__item d-sm-flex align-items-center has-dropdown__list">
                                    <span class="product-sort__item-name">{{__('catalog.product_listing.limit_title')}}</span>
                                    @foreach($limits as $limit)
                                        @if($limit['is_active'])
                                            <span class="product-sort__item-selected">{{$limit['value']}}</span>
                                        @endif
                                    @endforeach
                                    <ul class="product-sort__item-select dropdown-list">
                                        @foreach($limits as $limit)
                                            @if(!$limit['is_active'])
                                                <li><a href="{{$limit['link']}}">{{$limit['value']}}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>

                                </div>
                            </div>

                            <div class="products-row">
                                <div class="row">
                                    @foreach($products as $product)
                                        <div class="col-sm-6">
                                        @include('front.catalog.product_item', ['product' => $product])
                                        <!-- /.product-item -->
                                        </div>
                                    @endforeach
                                    <div class="col-12">
                                        @if($products->hasMorePages())
                                            <a id="load-content" class="btn-more">
                                                <span>Показать еще</span>
                                            </a>
                                            <script type="text/javascript">
                                                var currentPage = parseInt({{$products->currentPage()}});
                                                var lastPage = parseInt({{$products->lastPage()}});

                                                $('#load-content').on('click', function (e) {
                                                    e.preventDefault();
                                                    $.ajax({
                                                        url: `{{Request::fullUrl()}}`,
                                                        data: {
                                                            page: ++currentPage,
                                                            action: 'show_more'
                                                        },
                                                        success: function (result) {
                                                            if (result['status'] && result['status'] === 1) {
                                                                var html = result['template'];
                                                                $('#load-content').parent().before(html);
                                                                if (currentPage === lastPage) {
                                                                    $('#load-content').parent().remove();
                                                                }
                                                            }
                                                        }
                                                    });
                                                });
                                            </script>
                                        @endif
                                        {{$products->links('front.paginate')}}
                                    </div>
                                </div>
                            </div><!-- /.products-row -->

                        </main>
                    </div>
                    <!-- /.catalog-content ==========================-->

                </div>
            </div>
        </div>

        @if(!empty($description))
            <section class="description">
                <div class="container">

                    <div class="text-description show-text">
                        <div>
                            <div class="text-wrap">
                                <div class="text">
                                    {!! $description !!}
                                </div>
                            </div>
                            <div class="btn-wrap hidden">
                                <span class="btn__text-more">{{__('catalog.product_listing.read_more')}}</span>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- /.description ===================================== -->
        @if(!empty($jsonLd))
            <script type="application/ld+json">
                @json($jsonLd)
            </script>
        @endif
    @endif

        <!-- character -->
        <div class="character-wrapper character-filter--help character-left" data-time="15000">
            <div class="character">
                <div class="character-text">
                    <div class="holder">
                        <div class="character-text__inner">{{__('catalog.product_listing.character_text')}}</div>
                        <button class="close"><i class="icon-close"></i></button>
                    </div>
                </div>
                <div class="character-img__holder">
                    <img src="images/charackter/Lisoped_man_11.svg" alt="character" class="character-img">
                </div>
            </div>
        </div>

        <div class="page-overlay">
        </div>
    </div>
        <!-- /.page-content ===================================== -->
@endsection