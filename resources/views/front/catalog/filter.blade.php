<aside id="filter" class="filter">

    <div class="filter-head filter-tab__toggle hidden">
        <span class="chosen-filter__clear __active">
            <a href="{{$filter['reset']}}">
                <i class="icon-close2"></i>
                <span>{{__('catalog.filter.reset_filter')}}</span>
            </a>
        </span>
    </div><!-- /.filter-head -->

    <div class="filter-tab">

        <div class="filter-tab__head">
            <span>{{__('catalog.filter.price')}}</span>
        </div>

        <div class="filter-tab__content open">
            <div class="filter-price__slider">
                <div id="keypress"></div>
                <div class="d-flex align-items-center justify-content-between">
                    <span class="filter-price__currency-text">{{__('catalog.filter.from')}}</span>
                    <div class="filter-price__input-wrap">
                        <input type="number" id="input-with-keypress-0"
                               data-min="{{$filter['price']['min']}}"
                               data-start="{{config('app.filter_data.price.min') !== null ? config('app.filter_data.price.min') : $filter['price']['min']}}"
                               data-max="{{$filter['price']['max']}}"
                               name="price-min" value="{{config('app.filter_data.price.min')}}">
                        <span class="filter-price__currency">грн</span>
                    </div>
                    <span class="filter-price__currency-text">{{__('catalog.filter.to')}}</span>
                    <div class="filter-price__input-wrap">
                        <input type="number" id="input-with-keypress-1"
                               data-start="{{config('app.filter_data.price.max') !== null ? config('app.filter_data.price.max') : $filter['price']['max']}}"
                               data-min="{{$filter['price']['min']}}" data-max="{{$filter['price']['max']}}"
                               name="price-max" value="{{config('app.filter_data.price.max')}}">
                        <span class="filter-price__currency">грн</span>
                    </div>
                </div>
            </div>

        </div>
    </div><!-- /.filter-tab -->

    @if(isset($filter['filters_cat']) && $filter['filters_cat']->count() > 0)
        @foreach($filter['filters_cat'] as $filterEntity)
            <div class="filter-tab filter-tab__closed filters-filter-cat-tab">

                <div class="filter-tab__head filter-tab__toggle">
                    <span>{{$filterEntity->name}}</span>
                </div>

                <div class="filter-tab__content">
                    <ul class="filter-tab__options-list">
                        @foreach($filterEntity->values as $filterValue)
                            <li data-id="{{$filterValue->id}}" {!! in_array($filterValue->slug, config('app.filter_data.filter_attributes', [])) ? 'class="filter-option__label checked"' : 'class="filter-option__label"' !!} >
                                <a class="filter-option"
                                   {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                   href="{{$filterValue->filter_link}}">
                                    <span class="filter-option__checkbox input-checkbox"></span>
                                    <span class="filter-option__name">{{$filterValue->text}}</span>
                                    <span class="filter-option__qnty">({{$filterValue->products_count}})</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div><!-- /.filter-tab -->
        @endforeach
    @endif

    @if(isset($filter['filters']) && $filter['filters']->count() > 0)
        @foreach($filter['filters'] as $filterEntity)
            <div class="filter-tab filter-tab__closed filters-filter-tab">

                <div class="filter-tab__head filter-tab__toggle">
                    <span>{{$filterEntity->name}}</span>
                </div>

                <div class="filter-tab__content">
                    <ul class="filter-tab__options-list">
                        @foreach($filterEntity->values as $filterValue)
                            <li data-id="{{$filterValue->id}}" {!! in_array($filterValue->id, config('app.filter_data.filter_values', [])) ? 'class="filter-option__label checked"' : 'class="filter-option__label"' !!} >
                                <a class="filter-option"
                                   {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                                   href="{{$filterValue->filter_link}}">
                                    <span class="filter-option__checkbox input-checkbox"></span>
                                    <span class="filter-option__name">{{$filterValue->getAttributeValue('name')}}</span>
                                    <span class="filter-option__qnty">({{$filterValue->getAttributeValue('products_count')}})</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div><!-- /.filter-tab -->
        @endforeach
    @endif

    @if(isset($filter['manufacturers']) && $filter['manufacturers']->count() > 0)
        <div class="filter-tab filter-tab__closed manufacturer-filter-tab">

            <div class="filter-tab__head filter-tab__toggle">
                <span>{{__('catalog.filter.manufacturers')}}</span>
            </div>

            <div class="filter-tab__content">
                <ul class="filter-tab__options-list">
                    @foreach($filter['manufacturers'] as $manufacturer)
                        <li data-id="{{$manufacturer->id}}" {!! in_array($manufacturer->id, config('app.filter_data.manufacturers', [])) ? 'class="filter-option__label checked"' : 'class="filter-option__label"' !!} >
                            <a class="filter-option"
                               {!! config('app.is_nofollow_noindex', false) ? 'rel="nofollow, noindex"' : ''!!}
                               href="{{$manufacturer->filter_link}}">
                                <span class="filter-option__checkbox input-checkbox"></span>
                                <span class="filter-option__name">{{$manufacturer->getAttributeValue('name')}}</span>
                                <span class="filter-option__qnty">({{$manufacturer->getAttributeValue('products_count')}})</span>
                            </a>

                        </li>
                    @endforeach
                </ul>
            </div>
        </div><!-- /.filter-tab -->
    @endif
</aside>
<script type="text/javascript">
    $('.chosen-filter__clear').on('click', function (e) {
        $('.filter input').each(function () {
            this.checked = false;
        });
    });

    // filter item hide toggle
    $('.filter-tab .filter-tab__head').on('click', function () {
        $(this).closest('.filter-tab').toggleClass('filter-tab__closed');
        $(this).next('.filter-tab__content').slideToggle(300);
    });

    var checkedOptions = '.filter-tab .filter-option__label.checked';
    $(checkedOptions)
        .closest('.filter-tab__content').addClass('open')
        .closest('.filter-tab').removeClass('filter-tab__closed');

    if ($(checkedOptions).length) {
        $('#filter .filter-head').removeClass('hidden');
    }
</script>
<script type="text/javascript">
    filterObj = {
        manufacturers: {},
        filters: {},
        filters_cat: {},
        priceMin: null,
        priceMax: null,
        lastKlickedElement: null,
        updateFilter: function (e) {
        //this - li
            e.preventDefault();
            var href = $(this).find('a').attr('href');
            filterObj.lastKlickedElement = $(this);
            $(this).toggleClass('checked');
            window.history.pushState({state: 1}, '', href);
            filterObj.makeRequest(href);
        },
        changePrice: function (e) {
        //this - input
            var priceMin = $(filterObj.priceMin).val();
            var priceMax = $(filterObj.priceMax).val();

            filterObj.lastKlickedElement = $(this).closest('.filter-tab');

            var url = document.createElement('a');
            url.href = window.location;

            var targetUrl = '';

            var path = url.pathname;
            var filterStartIndex = path.indexOf('filter=');
            if (filterStartIndex === -1) {
                path += '/filter=price:' + priceMin + ',' + priceMax;
            } else {
                var priceStartIndex = path.indexOf('price:', filterStartIndex);
                if (priceStartIndex === -1) {
                    path += ';price:' + priceMin + ',' + priceMax;
                } else {
                    var priceEndIndex = path.indexOf(';', priceStartIndex);
                    var path1 = path.substring(0, priceStartIndex);
                    var path2 = priceEndIndex === -1 ? '' : path.substring(priceEndIndex);
                    path = path1 + 'price:' + priceMin + ',' + priceMax + path2;
                }
            }
            targetUrl += (path + url.search);
            filterObj.makeRequest(targetUrl);
        },
        makeRequest: function (href) {
            window.history.pushState({state: 1}, '', href);

            var response = $.ajax({url: href, async: false});

            if (response.responseJSON && response.responseJSON['status'] === 1) {
                var productsCount = response.responseJSON['products_count'];

                var filter = response.responseJSON['filter'];
                if (filter.manufacturers) {
                    for (var manufacturer of filter.manufacturers) {
                        if (manufacturer.id && filterObj.manufacturers[manufacturer.id]) {
                            $(filterObj.manufacturers[manufacturer.id])
                                .find('a')
                                .attr('href', manufacturer.filter_link);
                        }
                    }
                }
                if (filter.filters) {
                    for (var filterElement of filter.filters) {
                        if (filterElement.id && filterObj.filters[filterElement.id]) {
                            $(filterObj.filters[filterElement.id])
                                .find('a')
                                .attr('href', filterElement.filter_link);
                        }
                    }
                }
                if (filter.filters_cat) {
                    for (var filterElementCat of filter.filters_cat) {
                        if (filterElementCat.id && filterObj.filters_cat[filterElementCat.id]) {
                            $(filterObj.filters_cat[filterElementCat.id])
                                .find('a')
                                .attr('href', filterElementCat.filter_link);
                        }
                    }
                }
                filterObj.makeButton(productsCount, window.location);
            }
        },
        makeButton: function (productsCount, href) {
            var span = filterObj.lastKlickedElement;
            var windowWidth = jQuery(window).width();
            var layer = $("#filter-ask-layer");
            if (layer.length == 0) {
                layer = $("<div id='filter-ask-layer' style='display: none;'><a class='btn'><b>Показать</b></a><span></span></div>");
                if (windowWidth < 992) {
                    $("#filter").append(layer);
                } else {
                    $(".catalog-content").append(layer);
                }
            }
            layer.css("display", "none");
            layer.find("a").attr("href", href);

            var pos = span.offset();
            var name = span.find('.filter-option__name');
            var size = name.outerWidth();
            var priceSliderWidth = $('.filter-price__slider').outerWidth();

            if (productsCount == 0) {
                if (windowWidth < 992) {
                    var text = "0 товаров"
                } else {
                    var text = "Выбрано 0 товаров"
                }
                layer.find("a").css("display", "none")
            } else {
                if (windowWidth < 992) {
                    var text = productsCount + " товар" + NumberEnd(productsCount, ["", "а", "ов"])
                } else {
                    var text = "Выбран" + NumberEnd(productsCount, ["", "о", "о"]) + " " + productsCount + " товар" + NumberEnd(productsCount, ["", "а", "ов"])
                }
                layer.find("a").css("display", "inline-block");
            }
            layer.find("span").text(text);
            if (windowWidth >= 992) {
                if(size){
                    layer.css("left", pos.left + size + 60 + "px").css("top", pos.top + 3 + "px");
                } else {
                    layer.css("left", pos.left + priceSliderWidth + "px").css("top", pos.top + 57 + "px");
                }
            }
            layer.css("display", "block");
        }
    };

    function NumberEnd(mNumber, mEnds) {
        var cases = [2, 0, 1, 1, 1, 2];
        return mEnds[(mNumber % 100 > 4 && mNumber % 100 < 20) ? 2 : cases[Math.min(mNumber % 10, 5)]]
    }

    filterObj.priceMin = $('.filter-price__input-wrap [name=price-min]');
    filterObj.priceMax = $('.filter-price__input-wrap [name=price-max]');
    $(filterObj.priceMin).on('change', filterObj.changePrice);
    $(filterObj.priceMax).on('change', filterObj.changePrice);


    $('.filters-filter-tab li').each(function (key, link) {
        $(link).on('click', filterObj.updateFilter);
        var id = $(link).attr('data-id');
        filterObj.filters[id] = link;
    });

    $('.filters-filter-cat-tab li').each(function (key, link) {
        $(link).on('click', filterObj.updateFilter);
        var id = $(link).attr('data-id');
        filterObj.filters_cat[id] = link;
    });

    $('.manufacturer-filter-tab li').each(function (key, link) {
        $(link).on('click', filterObj.updateFilter);
        var id = $(link).attr('data-id');
        filterObj.manufacturers[id] = link;
    });

</script>
