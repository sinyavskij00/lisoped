<footer id="my-footer" class="footer">
    <div class="container">
        <div class="d-flex footer-row flex-wrap justify-content-between">

            <div class="footer-col footer-col__contacts">
                @if(isset($logo))
                    @if(Route::currentRouteName() === 'home')
                        <span class="logo">
                            <img src="{{$logo}}" alt="{{config('app.name')}}">
                        </span>
                    @else
                        <a href="{{route('home')}}" class="logo">
                            <img src="{{$logo}}" alt="{{config('app.name')}}">
                        </a>
                    @endif
                @endif
                <ul class="contact-list">
                    <li>
                        <address>{{__('common.footer.address1')}}</address>
                    </li>
                    <li>
                        <address>{{__('common.footer.address2')}}</address>
                    </li>
                    @if(!empty($siteTelephones))
                        @foreach($siteTelephones as $telephone)
                            <li class="phones">
                                <span class="a">
                                    <a href="tel:{{preg_replace("/[^0-9]/", "", $telephone)}}"><span>{{$telephone}}</span></a>
                                    @if($loop->index === 0)
                                        <a href="https://t.me/Lisoped"><span class="icon icon_telegram"></span></a>
                                        <a href="viber://chat?number=+380676206611"><span class="icon icon_viber"></span></a>
                                        <a href="https://wa.me/380676206611"><span class="icon icon_whatsapp"></span></a>
                                    @endif
                                </span>
                            </li>
                        @endforeach
                    @endif
                    <li><a href="mailto:info@lisoped.ua">{{config('app.admin_email')}}</a></li>
                </ul>
            </div>

            <div class="footer-col footer-col__products">
                <div class="title">{{__('common.footer.category_title')}}</div>
                <ul class="links-list">
                    @foreach($categoryTree as $mainCategory)
                        @if($mainCategory->status === 1 && isset($mainCategory->localDescription))
                            <li>
                                <a href="{{route('category_path', category_path($mainCategory->getAttributeValue('id')))}}">{{$mainCategory->localDescription->getAttributeValue('name')}}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>

            <div class="footer-col footer-col__about">
                <div class="title">{{__('common.footer.about_title')}}</div>
                <ul class="links-list two-columns">
                    <li><a href="{{route('about_us')}}">{{__('common.footer.about_us')}}</a></li>
                    <li><a href="{{route('manufacturer.list')}}">{{__('common.footer.manufacturers')}}</a></li>
                    <li><a href="{{route('testimonials')}}">{{__('common.footer.testimonials')}}</a></li>
                    <li><a href="{{route('faq')}}">{{__('common.footer.faq')}}</a></li>
                    <li><a href="{{route('guarantee')}}">{{__('common.footer.guarantee')}}</a></li>
                    <li><a href="{{route('news.list')}}">{{__('common.footer.blog')}}</a></li>
                    <li><a href="{{route('delivery')}}">{{__('common.footer.delivery')}}</a></li>
                    <li><a href="{{route('contacts')}}">{{__('common.footer.contacts')}}</a></li>
                    <li><a href="{{route('credit')}}">{{__('common.footer.credit')}}</a></li>
                    <li><a href="{{route('special.list')}}">{{__('common.footer.specials')}}</a></li>
                </ul>
            </div>

            <div class="footer-col t-a-right footer-col__subscribe">
                <div class="footer-col__inner t-a-left">
                    <div class="title">{{__('common.footer.social_title')}}</div>
                    <ul class="socials d-flex align-items-center flex-wrap">
                        @if(isset($linkFacebook))
                            <li><a href="{{$linkFacebook}}"><i class="i-facebook"></i></a></li>
                        @endif
                        @if(isset($linkInstagram))
                            <li><a href="{{$linkInstagram}}"><i class="i-instagram"></i></a></li>
                        @endif
                        @if(isset($linkTwitter))
                            <li><a href="{{$linkTwitter}}"><i class="i-twitter"></i></a></li>
                        @endif
                        @if(isset($linkYoutube))
                            <li><a href="{{$linkYoutube}}"><i class="i-youtube"></i></a></li>
                        @endif
                    </ul>
                    <form class="subscribe">
                        @csrf
                        <input id="footer_subscribe_email" name="email" type="email"
                               placeholder="{{__('common.footer.subscribe_placeholder')}}" required>
                        <button type="submit" class="btn"><span>{{__('common.footer.subscribe_submit')}}</span></button>
                    </form>
                </div>
            </div>

        </div>

        <div class="footer__bottom-line">
            <div class="copy">{{__('common.footer.copyright', ['year' => date('Y')])}}</div>
            <div class="dev">
                <p>{{__('common.footer.developed')}}</p>
                <a rel="nofollow" href="http://art-marks.net/" class="cont" target="_blank">
                    <img alt="alt" src="assets/img/ArtMarks_logo_black.svg">
                </a>
            </div>
        </div>

    </div>
</footer>
<!-- /.footer ===================================== -->

<div class="character-wrapper character-lenguage character-center" data-time="0">
    <div class="character">
        <div class="character-text">
            <div class="holder">
                <div class="character-text__inner">{!!__('character.lenguage.phrase')!!}</div>
                <button class="close"><i class="icon-close"></i></button>
            </div>
        </div>
        <div class="character-img__holder">
            <img src="images/charackter/Lisoped_man_10.svg" alt="character" class="character-img">
        </div>
    </div>
</div>

<div class="character-wrapper character-wakeup character-center" data-time="120000">
    <div class="character">
        <div class="character-text">
            <div class="holder">
                <div class="character-text__inner">{!!__('character.wakeup.phrase')!!}</div>
                <button class="close"><i class="icon-close"></i></button>
            </div>
        </div>
        <div class="character-img__holder">
            <img src="images/charackter/Lisoped_man_9.svg" alt="character" class="character-img">
        </div>
    </div>
</div>

</div>

<div class="hidden">
    <div id="wishlist-popup" class="product-info__popup">
        <p class="text-center">{{__('common.footer.wishlist_popup.text')}}</p>
        <div class="text-center">
            <a href="{{route('register')}}" class="btn">{{__('common.footer.wishlist_popup.btn_register')}}</a>
        </div>
    </div>

    <div id="log-in-popup" class="log-in-popup popup">
        <form action="" class="log-in-form popup-form" data-form="login" id="login-form">
            @csrf
            <div class="formhead">{{__('common.footer.login_popup.title')}}</div>
            <div class="form-row">
                <label>
                    <span>{{__('common.footer.login_popup.label_login')}}</span>
                    <input type="email" name="email" required
                           placeholder="{{__('common.footer.login_popup.placeholder_email')}}">
                </label>
            </div>
            <div class="form-row">
                <label>
                    <span>{{__('common.footer.login_popup.label_password')}}</span>
                    <input type="password" name="password" required
                           placeholder="{{__('common.footer.login_popup.placeholder_password')}}">
                </label>
            </div>
            <div class="form-row form-row_cols-2">
                <label class="label-check">
                    <input type="checkbox">
                    <span>{{__('common.footer.login_popup.remember_me')}}</span>
                </label>
                <div class="login-form__lost link"><span>{{__('common.footer.login_popup.forgot_password')}}</span>
                </div>
            </div>

            <div class="btn-submit__wrap">
                <button type="submit" class="btn">
                    <span>{{__('common.footer.login_popup.submit')}}</span>
                </button>
            </div>
        </form>

        <form action="" class="lost-form popup-form" data-form="lost" id="reset-password">
            @csrf
            <div class="success">
                <span>{{__('common.footer.reset_password.success')}}</span>
            </div>
            <div class="formhead">{{__('common.footer.reset_password.title')}}</div>
            <div class="formdescr">{{__('common.footer.reset_password.subtitle')}}</div>
            <input type="email" name="email" maxlength="50" required placeholder="E-mail*">
            <span class="lost-form__login link"><i
                        class="icon-arrow-thin-left"></i><span>{{__('common.footer.reset_password.back_link')}}</span></span>
            <div class="btn-submit__wrap">
                <button type="submit" class="btn">
                    <span>{{__('common.footer.reset_password.submit')}}</span>
                </button>
            </div>
        </form>

        <div class="login-socials">
            <div class="formdescr">{{__('common.footer.login_popup.social_title')}}</div>
            <div class="ulogin-buttons-container">
                <a href="{{route('socialize.redirect', ['service' => 'facebook'])}}" class="ulogin-button" role="button"
                   title="Facebook"><i class="icon-facebook2"></i></a>
                <a href="{{route('socialize.redirect', ['service' => 'google'])}}" class="ulogin-button" role="button"
                   title="Google+"><i class="icon-google-plus2"></i></a>
            </div>
        </div>
        <div class="if-has-uccount">
            <a href="{{route('register')}}">{{__('common.footer.login_popup.register')}}</a>
        </div>
    </div><!-- /.log-in-popup -->

    <div id="callback-popup" class="callback popup">
        <form action="" class="callback-form popup-form">
            @csrf
            <div class="success">
                <div class="s-inner">
                    {!! __('common.footer.callback_popup.success') !!}
                </div>
            </div>
            <div class="formhead">{{__('common.footer.callback_popup.title') }}</div>
            <div class="form-row form-row_cols-2">
                <label>
                    <span>{{__('common.footer.callback_popup.telephone_label') }}</span>
                    <input type="tel" name="phone" required>
                </label>
                <label>
                    <span>{{__('common.footer.callback_popup.name_label')}}</span>
                    <input type="text" name="name">
                </label>
            </div>
            <div class="form-row">
                <div class="time-selects">
                    <div class="select-day">
                        <select name="day" class="select">
                            <option value="{{\Illuminate\Support\Carbon::today()}}">{{__('common.footer.callback_popup.today')}}</option>
                            <option value="{{\Illuminate\Support\Carbon::tomorrow()}}">{{__('common.footer.callback_popup.tomorrow')}}</option>
                        </select>
                    </div>
                    <div class="select-hour">
                        <select name="hour" class="select">
                            @for($i = 0; $i < 24; $i++)
                                <option value="{{str_pad($i, 2, "0", STR_PAD_LEFT)}}">{{str_pad($i, 2, "0", STR_PAD_LEFT)}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="select-min">
                        <select name="minute" class="select">
                            @for($i = 0; $i < 60; $i++)
                                <option value="{{str_pad($i, 2, "0", STR_PAD_LEFT)}}">{{str_pad($i, 2, "0", STR_PAD_LEFT)}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
            </div>

            <div class="btn-submit__wrap">
                <button type="submit" class="btn">
                    <span>{{__('common.footer.callback_popup.submit')}}</span>
                </button>
            </div>
        </form>
    </div><!-- /.callback-popup -->

    <div id="reviews-popup" class="reviews-popup popup">
        <form class="reviews-popup__form">
            @csrf
            <input type="hidden" name="parent_id">
            <div class="success"><span>Спасибо за отзыв!</span></div>
            <div class="formhead">Оставить отзыв</div>
            <input type="text" name="name" placeholder="Имя*" required="">
            <input type="text" name="email" placeholder="E-mail*" required="">
            <textarea name="text" rows="6" placeholder="Ваш отзыв*" required=""></textarea>
            <div class="btn-wrap">
                <button type="submit" class="btn">
                    <span>Отправить</span>
                    <svg class="angle-icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-angle-left"></use>
                    </svg>
                </button>
            </div>
        </form>
    </div><!-- /#reviews-popup -->

    <div id="faq-popup" class="faq-popup popup">
        <form action="" class="faq-popup__form">
            <div class="success"><span>{{__('common.footer.review_popup.success')}}</span></div>
            <div class="formhead">{{__('common.footer.review_popup.title')}}</div>
            <input type="text" name="Name" placeholder="{{__('common.footer.review_popup.placeholder_name')}}"
                   required="">
            <input type="text" name="E-mail" placeholder="{{__('common.footer.review_popup.placeholder_email')}}"
                   required="">
            <textarea name="Question" rows="6" placeholder="{{__('common.footer.review_popup.placeholder_question')}}"
                      required=""></textarea>
            <div class="btn-wrap">
                <button type="submit" class="btn">
                    <span>{{__('common.footer.review_popup.submit')}}</span>
                    <svg class="angle-icon">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-angle-left"></use>
                    </svg>
                </button>
            </div>
        </form>
    </div><!-- /#faq-popup -->

    <div id="wishlist-popup" class="wishlist-popup popup">
        <div class="inner"></div>
        <div class="btn-wrap">
            <a href="#log-in-popup" class="to-popup btn">Войти</a>
            <a href="{{route('register')}}" class="btn">Зарегистрироваться</a>
        </div>
    </div><!-- /.wishlist-popup -->

    <div id="compare-popup" class="wishlist-popup popup">
        <div class="inner"></div>
    </div><!-- /.wishlist-popup -->

</div>

<div class="top" title="Наверх">
    <i class="icon-chevron-thin-up"></i>
</div>
<script type="text/javascript">
    $('form.subscribe').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: `{{route('subscribe')}}`,
            data: $(this).serialize(),
            type: 'POST',
            success: function (result) {
                if (result['status'] && result['status'] === 1) {
                    $('form.subscribe .success').remove();
                    $('form.subscribe .error').remove();
                    $('form.subscribe').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                    setTimeout(function () {
                        $('form.subscribe .success.active').removeClass('active');
                        $.magnificPopup.close();
                        $('form.subscribe')[0].reset();
                    }, 3000);
                } else {
                    $('form.subscribe .error').remove();
                    $('form.subscribe').append('<div class="error"><span>' + result['errors'] + '</span></div>');
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $('#callback-popup form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: `{{route('popupProcessor.headerCallback')}}`,
            type: 'POST',
            data: $(this).serialize(),
            success: function (result) {
                if (result['status'] === 1) {
                    $('#callback-popup form').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                    setTimeout(function () {
                        $('#callback-popup .success.active').removeClass('active');
                        $.magnificPopup.close();
                        $('#callback-popup form')[0].reset();
                    }, 4000);
                }
            }
        });
    });
</script>
<script type="text/javascript">
    var wishlist = {
        add: function (productId, button) {
            var type = 'PUT';
            var url = '{{route('wishlist.add')}}';
            var callback = function (result) {
                if (result['status'] && result['status'] === 1) {
                    $('#wishlist-popup .inner').text(result['message']);
                    wishlist.changeQuantity(result['quantity']);
                    $(button).find('.i-heart').removeClass('i-heart').addClass('icon-heart');
                    $(button).attr('onclick', 'wishlist.delete(' + productId + ', this)');
                    @if(!Auth::check())
                    $.magnificPopup.open({
                        items: {
                            src: '#wishlist-popup',
                            type: 'inline'
                        }
                    });
                    @endif
                }
            };
            wishlist.sendRequest(url, productId, callback, type);
        },
        delete: function (productId, button) {
            var url = '{{route('wishlist.delete')}}';
            var type = 'DELETE';
            var callback = function (result) {
                if (result['status'] && result['status'] === 1) {
                    $('#wishlist-popup .inner').text(result['message']);
                    wishlist.changeQuantity(result['quantity']);
                    $(button).find('.icon-heart').removeClass('icon-heart').addClass('i-heart');
                    $(button).attr('onclick', 'wishlist.add(' + productId + ', this)');
                    $('.wish-product-' + productId).remove();
                }
            };
            wishlist.sendRequest(url, productId, callback, type);
        },
        changeQuantity: function (quantity) {
            $('#wishlist-link .num-total').remove();
            if (quantity) {
                quantity = parseInt(quantity);
                if (quantity > 0) {
                    $('#wishlist-link').append('<span class="num-total">' + quantity + '</span>');
                }
            }
        },
        sendRequest: function (url, productId, callback, type) {
            $.ajax({
                url: url,
                type: type,
                data: {
                    product_id: productId,
                    _token: '{{csrf_token()}}'
                },
                success: callback
            });
        }
    };
    var compare = {
        add: function (productId, button) {
            var callback = function (result) {
                if (result['status'] && result['status'] === 1) {
                    $('#compare-popup .inner').text(result['message']);
                    $(button).find('.i-lawyer').addClass('accent');
                    $(button).attr('onclick', 'compare.delete(' + productId + ', this)');
                    compare.changeQuantity(result['quantity']);
                }
            };
            var url = '{{route('compare.add')}}';
            var type = 'PUT';
            compare.sendRequest(url, productId, callback, type);
        },
        delete: function (productId, button) {
            var url = '{{route('compare.delete')}}';
            var callback = function (result) {
                if (result['status'] && result['status'] === 1) {
                    $(button).find('.i-lawyer').removeClass('accent');
                    $(button).attr('onclick', 'compare.add(' + productId + ', this)');
                    compare.changeQuantity(result['quantity']);
                    $('.compare-product-' + productId).closest('.slick-slide').remove();
                }
            };
            var type = 'DELETE';
            compare.sendRequest(url, productId, callback, type);
        },
        sendRequest: function (url, productId, callback, type) {
            $.ajax({
                url: url,
                data: {
                    product_id: productId,
                    _token: '{{csrf_token()}}'
                },
                type: type,
                success: callback
            });
        },
        changeQuantity: function (quantity) {
            var compareButton = $('#compare-link');
            compareButton.find('.num-total').remove();
            if (quantity) {
                quantity = parseInt(quantity);
                if (quantity > 0) {
                    compareButton.append('<span class="num-total">' + quantity + '</span>');
                }
            }
        }
    };
</script>
<script type="text/javascript">
    $('#login-form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: `{{route('login')}}`,
            data: $(this).serialize(),
            type: 'POST',
            success: function (result) {
                if (result['status'] === 1) {
                    @if(Route::currentRouteName() === 'checkout')
                    window.location.reload();
                    @else
                        window.location = `{{route('cabinet.index')}}`
                    @endif
                } else {
                    $('#log-in-popup .error').remove();
                    $('#log-in-popup form').append('<div class="error"><span>' + result['message'] + '</span></div>');
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $('#reset-password').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: `{{route('password.email')}}`,
            type: 'POST',
            data: $(this).serialize(),
            success: function (result) {
                $('#reset-password .success').addClass('active');
                setTimeout(function () {
                    $('#reset-password .success').removeClass('active');
                    $.magnificPopup.close();
                    $('[data-form="lost"]').hide();
                    $('[data-form="login"]').show();
                }, 4000);
            }
        });
    });
</script>
<script type="text/javascript">
    function changeCartItemQuantity(cartItemId, quantity) {
        $.ajax({
            url: `{{route('cart.changeQuantity')}}`,
            type: 'POST',
            data: {
                cart_item_id: cartItemId,
                quantity: quantity,
                _token: `{{csrf_token()}}`
            },
            success: function (result) {
                replaceHtml(result);
            }
        });
    }

    function deleteCartItem(cartItemId) {
        $.ajax({
            url: `{{route('cart.delete')}}`,
            type: 'POST',
            data: {
                cart_item_id: cartItemId,
                _token: `{{csrf_token()}}`
            },
            success: function (result) {
                replaceHtml(result);
            }
        })
    }

    function replaceHtml(result) {
        if (result['status'] && result['status'] === 1) {
            if (result['cart_popup']) {
                $('#cart-popup').html(result['cart_popup']);
            }
            if (result['checkout_list']) {
                $('#checkout-product-list').replaceWith(result['checkout_list']);
            }
            if (result['cart_quantity']) {
                $('.hub-i__cart .num-total').text(result['cart_quantity']);
            }
            if (result['checkout_head']) {
                $('.checkout .checkout-head').replaceWith(result['checkout_head']);
            }
        }
    }
</script>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDJ79uJPujvlv4hzNzKoOsq9umUqTU_JiE"></script>
<script src="{{mix('front/js/scripts.min.js')}}"></script>
@if(!empty($companyJsonLd))
<script type="application/ld+json">
    @json($companyJsonLd)
</script>
@endif
@if(!empty($searchJsonLd))
<script type="application/ld+json">
    @json($searchJsonLd)
</script>
@endif
</body>
</html>
