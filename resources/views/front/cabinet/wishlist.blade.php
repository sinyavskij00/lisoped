@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content page-wishlist p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="p-title">{{__('cabinet.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
								@include('front.cabinet.sidebar')

                <div class="col-md-9">

                    <div class="cart-product__list-header d-sm-flex align-items-center">
                        <span class="cart-product__list-header--photo">{{__('cabinet.wishlist.image')}}</span>
                        <span class="cart-product__list-header--name">{{__('cabinet.wishlist.product')}}</span>
                        <span class="cart-product__list-header--price">{{__('cabinet.wishlist.price')}}</span>
                    </div>
                    @if($products->count() > 0)
                        <div class="cart-product__list cart-product__list--checkout">
                            @foreach($products as $wishProduct)
                                @if($wishProduct)
                                    <div class="cart-product wish-product-{{$wishProduct->id}}">
                                        @if($wishProduct->image)
                                            <div class="cart-product__img">
                                                @if(isset($wishProduct->image) && Storage::disk('public')->exists($wishProduct->image))
                                                    <img src="{{$catalogService->resize($wishProduct->image, 90, 60)}}"
                                                         alt="{{$wishProduct->name}}">
                                                @endif
                                            </div>
                                        @endif
                                        <div class="cart-product__descr">
                                            <h6 class="cart-product__name">
                                                <a href="{{route('product_path', $wishProduct->slug)}}">{{$wishProduct->name}}</a>
                                            </h6>
                                            @if(isset($wishProduct->quantity) && $wishProduct->quantity > 0)
                                                <span class="product-info__availability product-info__availability--available">{{__('cabinet.wishlist.text_available')}}</span>
                                            @else
                                                <span class="product-info__availability product-info__availability--notavailable">{{__('cabinet.wishlist.text_not_available')}}</span>
                                            @endif
                                        </div>
                                        <div class="cart-product__price">{{$catalogService->format($catalogService->calculate($wishProduct->price))}}</div>
                                        <button class="remove"
                                                onclick="wishlist.delete({{$wishProduct->id}}, this)"></button>
                                    </div><!-- /.cart-product -->
                                @endif
                            @endforeach
                        </div><!-- /.cart-product__list -->
                    @endif
                </div>

            </div>
        </div><!-- /container -->
        <div class="page-overlay"></div>
    </main>
    <!-- /main ===================================== -->
@endsection