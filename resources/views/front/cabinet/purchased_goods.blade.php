@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="p-title">{{__('cabinet.purchased_goods.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                @include('front.cabinet.sidebar')

                <div class="col-md-9">

                    <div class="cart-product__list cart-product__list--checkout purchased-goods__list">
                        @foreach($products as $product)
                        <div class="cart-product">
                            @if(!empty($product->image) && Storage::disk('public')->exists($product->image))
                            <div class="cart-product__img" style="margin-right: 15px;">
                                <img src="{{Storage::disk('public')->url($product->image)}}" alt="{{$product->name}}">
                            </div>
                            @endif
                            <div class="cart-product__descr">
                                <h6 class="cart-product__name">{{$product->name}}</h6>
                            </div>
                            <div>
                                <a class="btn" href="{{route('product_path', $product->getAttributeValue('slug'))}}#to-review">
                                    {{__('cabinet.purchased_goods.btn_feedback')}}
                                </a>
                            </div>
                        </div><!-- /.cart-product -->
                       @endforeach
                    </div><!-- /.cart-product__list -->

                </div>

            </div>
        </div><!-- /container -->

        <div class="page-overlay"></div>

    </main>
    <!-- /main ===================================== -->
@endsection