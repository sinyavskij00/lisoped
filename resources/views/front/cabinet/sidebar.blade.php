<div class="col-md-3">
    <div class="personal-area__nav--wrapper">
        <ul class="personal-area__nav">
            <li><a {!! url()->current() === route('cabinet.index') ? ' class="active" ' : '' !!}href="{{route('cabinet.index')}}">{{__('cabinet.sidebar.personal_info')}}</a></li>
            <li><a {!! url()->current() === route('cabinet.changePassword') ? ' class="active" ' : '' !!}href="{{route('cabinet.changePassword')}}">{{__('cabinet.sidebar.change_password')}}</a></li>
            <li><a {!! url()->current() === route('cabinet.orderHistory') ? ' class="active" ' : '' !!}href="{{route('cabinet.orderHistory')}}">{{__('cabinet.sidebar.order_history')}}</a></li>
            <li><a {!! url()->current() === route('cabinet.wishlist') ? ' class="active" ' : '' !!}href="{{route('cabinet.wishlist')}}">{{__('cabinet.sidebar.wishlist')}}</a></li>
            <li><a {!! url()->current() === route('cabinet.purchasedGoods') ? ' class="active" ' : '' !!}href="{{route('cabinet.purchasedGoods')}}">{{__('cabinet.sidebar.purchased_goods')}}</a></li>
            <li><a href="{{route('logout')}}">{{__('cabinet.sidebar.logout')}}</a></li>
        </ul>

        @if(Auth::user()->has_purchased_goods)
            <div class="character-wrapper character-purchased character-left" data-time="0">
                <div class="character">
                    <div class="character-text">
                        <div class="holder">
                            <div class="character-text__inner">{!!__('character.purchased.phrase')!!}</div>
                            <button class="close"><i class="icon-close"></i></button>
                        </div>
                    </div>
                    <div class="character-img__holder">
                        <img src="images/charackter/Lisoped_man_11.svg" alt="character" class="character-img">
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>