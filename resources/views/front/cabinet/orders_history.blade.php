@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="p-title">{{__('cabinet.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
								@include('front.cabinet.sidebar')

                <div class="col-md-9">

                    <div class="history-table__wrapper">
                        <div class="history-table">
                            <div class="thead">
                                <div class="tr">
                                    <span class="th">{{__('cabinet.orders_history.order_number')}}</span>
                                    <span class="th"></span>
                                    <span class="th">{{__('cabinet.orders_history.order_sum')}}</span>
                                    <span class="th">{{__('cabinet.orders_history.order_status')}}</span>
                                    <span class="th">{{__('cabinet.orders_history.order_date')}}</span>
                                </div>
                            </div>

                            <div class="tbody">
                                @if($user->orders->count() > 0)
                                    @foreach($user->orders as $order)
                                        <div class="history-table__row">
                                            <div class="tr history-order" data-toggle="history-order__descr-table">
                                                <span class="td{{!$order->isFinished ? ' accent' : ''}}">
                                                    <span>{{__('cabinet.orders_history.order_number_2', ['number' => $order->public_id])}}</span>
                                                    <i class="icon-caret-down"></i>
                                                </span>
                                                <span class="td"></span>
                                                <span class="td">{{$catalogService->format($order->total * $order->currency_value, $order->currency_id)}}</span>
                                                @if($order->status = '21')
                                                    <span class="td">{{\App\Entities\OrderStatus::find(21)->localDescription->name}}</span>
                                                    @else
                                                    <span class="td">{{$order->status->localDescription->name}}</span>
                                                @endif
                                                <span class="td">{{$order->created_at}}</span>
                                            </div>
                                            <div class="history-order__descr-table">
                                                <div class="thead">
                                                    <div class="tr">
                                                        <span class="th">{{__('cabinet.orders_history.product_name')}}</span>
                                                        <span class="th">{{__('cabinet.orders_history.product_ttn')}}</span>
                                                        <span class="th">{{__('cabinet.orders_history.product_sum')}}</span>
                                                        <span class="th">{{__('cabinet.orders_history.product_quantity')}}</span>
                                                        <span class="th">{{__('cabinet.orders_history.product_price')}}</span>
                                                    </div>
                                                </div>
                                                <div class="tbody">
                                                    @foreach($order->skus as $orderSku)
                                                        <div class="tr">
                                                            @php
                                                            $sku = \App\Entities\StockKeepingUnit::where('id', '=', $orderSku->sku_id)->first();
                                                            $product = isset($sku) ? $sku->product : null;
                                                            @endphp
                                                            @if(isset($product))
                                                                <span class="td">
                                                                    <a href="{{route('product_path', $product->slug)}}">{{$orderSku->name}}</a>
                                                                </span>
                                                            @else
                                                                <span class="td">{{$orderSku->name}}</span>
                                                            @endif
                                                            <span class="td">{{$orderSku->ttn}}</span>
                                                            <span class="td">{{$catalogService->format($orderSku->price * $orderSku->quantity, $order->currency_id) }}</span>
                                                            <span class="td">{{$orderSku->quantity}}</span>
                                                            <span class="td">{{$catalogService->format($orderSku->price, $order->currency_id) }}</span>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.history-table__row -->
                                    @endforeach
                                @endif
                            </div><!-- /.tbody -->
                        </div><!-- /.history-table -->
                    </div>    <!-- /.history-table__wrapper -->

                </div>
            </div>
        </div><!-- /container -->
        <div class="page-overlay"></div>
    </main>
    <!-- /main ===================================== -->
@endsection
