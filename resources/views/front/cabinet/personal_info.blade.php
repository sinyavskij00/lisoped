@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="p-title">{{__('cabinet.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
								@include('front.cabinet.sidebar')
								
                <div class="col-md-9">
                    <form id="personal-info" class="personal-info personal-area__inputs"
                          action="{{route('cabinet.index')}}" method="POST">
                        @csrf
                        <div class="personal-info__row">
                            <div class="personal-info__inputs personal-info__col">
                                <input type="text" name="first_name"
                                       placeholder="{{__('cabinet.personal_info.fields.first_name')}}"
                                       value="{{$user->first_name}}">
                                <input type="text" name="last_name"
                                       placeholder="{{__('cabinet.personal_info.fields.last_name')}}"
                                       value="{{$user->last_name}}">
                            </div>
                            <div class="personal-info__descr personal-info__col">{{__('cabinet.personal_info.descriptions.name')}}</div>
                        </div>

                        <div class="personal-info__row">
                            <input type="email" name="email" placeholder="{{__('cabinet.personal_info.fields.email')}}"
                                   class="personal-info__input personal-info__col" value="{{$user->email}}">
                            <div class="personal-info__descr personal-info__col">
                                {{__('cabinet.personal_info.descriptions.email')}}
                            </div>
                        </div>

                        <div class="personal-info__row">
                            <input type="tel" name="telephone" placeholder="+38 (0"
                                   class="personal-info__input personal-info__col" value="{{$user->telephone}}">
                            <div class="personal-info__descr personal-info__col">{{__('cabinet.personal_info.descriptions.telephone')}}</div>
                        </div>

                        <div class="personal-info__row" style="position: relative">
                            <div>
                                <label for="select-day" style="display: block">День</label>
                                <select name="day" id="select-day">
                                    @for($i = 1; $i <= 31; $i++)
                                        <option value="{{$i}}" {{$day === $i ? 'selected' : ''}}>{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div>
                                <label for="select-month" style="display: block">Месяц</label>
                                <select name="month" id="select-month">
                                    @for($i = 1; $i <= 12; $i++)
                                        <option value="{{$i}}" {{$month === $i ? 'selected' : ''}}>{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div>
                                <label for="select-year" style="display: block">Год</label>
                                <select name="year" id="select-year">
                                    @for($i = 1960; $i <= intval(date('Y')); $i++)
                                        <option value="{{$i}}" {{$year === $i ? 'selected' : ''}}>{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <span class="danger error">{{$errors->first('birthday')}}</span>
                            <div class="personal-info__descr personal-info__col">{{__('cabinet.personal_info.descriptions.birthday')}}</div>
                        </div>

                        <div class="personal-info__row">
                            <input type="text" name="address"
                                   placeholder="{{__('cabinet.personal_info.fields.address')}}"
                                   class="personal-info__input personal-info__col" value="{{$user->address}}">
                            <div class="personal-info__descr personal-info__col">{{__('cabinet.personal_info.descriptions.address')}}</div>
                        </div>

                        <div class="personal-info__row">
                            <div class="personal-info__col">
                                <div class="city__row">
                                    <input id="city" type="text" name="city" class="input" autocomplete="off"
                                           data-search-url="{{route('newpost.city')}}"
                                           placeholder="{{__('cabinet.personal_info.fields.city')}}"
                                           value="{{isset($city) ? $city->DescriptionRu : ''}}">
                                    <input id="city-id-selected" name="city_ref" type="hidden"
                                           value="{{isset($city) ? $city->Ref : ''}}">
                                    <div id="invalidate-selection" class="error"></div>
                                </div>
                                <select name="store_ref" id="order-store">
                                    <option value="">{{__('cabinet.personal_info.choose_store')}}</option>
                                    @if($store)
                                        <option selected value="{{$store->Ref}}">{{$store->DescriptionRu}}</option>
                                    @endif
                                </select>
                            </div>
                            <div class="personal-info__descr personal-info__col">{{__('cabinet.personal_info.descriptions.new_post')}}</div>
                        </div>

                        <div class="personal-info__row personal-info__label">
                            <span class="personal-info__label-descr">{{__('cabinet.personal_info.subscribe.title')}}</span>
                            <div class="personal-info__label-values">
                                <label class="personal-info__label-value">
                                    <input type="radio" name="subscribe" checked 
                                           value="1" {{$user->subscribe == 1 ? 'checked' : ''}}>
                                    <span class="personal-info__label-text">{{__('cabinet.personal_info.subscribe.yes')}}</span>
                                </label>
                                <label class="personal-info__label-value">
                                    <input type="radio" name="subscribe"
                                           value="0" {{$user->subscribe != 1 ? 'checked' : ''}}>
                                    <span class="personal-info__label-text">{{__('cabinet.personal_info.subscribe.no')}}</span>
                                </label>
                            </div>
                        </div>

                        <div class="btn-wrap">
                            <button type="submit" class="btn"><span>{{__('cabinet.personal_info.submit')}}</span>
                            </button>
                        </div>

                    </form>
                </div>

            </div>
        </div><!-- /container -->
        <div class="page-overlay"></div>
    </main>
    <!-- /main ===================================== -->
    <script type="text/javascript">
        $(function () {
            var storeSelect = $('#order-store').selectize();
            var storeSelectize = storeSelect[0].selectize;
            // Initialize ajax autocomplete:
            $('#city').autocomplete({
                serviceUrl: `{{route('newpost.city')}}`,
                dataType: 'json',
                paramName: 'city',
                appendTo: '.city__row',
                triggerSelectOnValidInput: false,
                onSelect: function (suggestion) {
                    $('#city-id-selected').val(suggestion.data);
                    $('#invalidate-selection').hide();
                    $.ajax({
                        url: `{{route('newpost.stores')}}`,
                        data: {
                            cityRef: suggestion.data
                        },
                        success: function (result) {
                            storeSelectize.clearOptions();
                            if (result) {
                                for (var i = 0; i < result.length; i++) {
                                    storeSelectize.addOption({
                                        value: result[i]['value'],
                                        text: result[i]['text']
                                    });
                                }
                                storeSelectize.refreshOptions();
                            }
                        }
                    });
                },
                onInvalidateSelection: function () {
                    $('#invalidate-selection').show().html('Пожалуйста, выберите город из выпадающего списка');
                }
            });


            if ($('.my-datepicker').length) {
                var $myPicker = $('.my-datepicker');

                $.fn.datepicker.language['ru'] = {
                    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                    daysShort: ['Вос', 'Пон', 'Вто', 'Сре', 'Чет', 'Пят', 'Суб'],
                    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
                    today: 'Сегодня',
                    clear: 'Очистить',
                    dateFormat: 'yyyy-mm-dd',
                    timeFormat: 'hh:ii',
                    firstDay: 1
                };

                $.fn.datepicker.language['ua'] = {
                    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                    daysShort: ['Вос', 'Пон', 'Вто', 'Сре', 'Чет', 'Пят', 'Суб'],
                    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
                    today: 'Сегодня',
                    clear: 'Очистить',
                    dateFormat: 'yyyy-mm-dd',
                    timeFormat: 'hh:ii',
                    firstDay: 1
                };

                $myPicker.datepicker({
                    language: 'ru',
                    autoClose: true
                });
            }

        });
    </script>
@endsection