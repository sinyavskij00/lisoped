@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content page-password p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="p-title">{{__('cabinet.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
								@include('front.cabinet.sidebar')

                <div class="col-md-9">
                    <form id="password" class="personal-info personal-area__inputs">
                        @csrf
                        <div class="personal-info__row">
                            <input type="password" name="old_password" placeholder="Пароль*" class="personal-info__input personal-info__col" required>
                            <div class="personal-info__descr personal-info__col">{{__('cabinet.password.enter_password')}}</div>
                        </div>

                        <div class="personal-info__row">
                            <input type="password" name="password" placeholder="Пароль*" class="personal-info__input personal-info__col" required>
                            <div class="personal-info__descr personal-info__col">{{__('cabinet.password.enter_new_password')}}</div>
                        </div>

                        <div class="personal-info__row">
                            <input type="password" name="password_confirmation" placeholder="Пароль*" class="personal-info__input personal-info__col" required>
                            <div class="personal-info__descr personal-info__col">{{__('cabinet.password.confirm_password')}}</div>
                        </div>

                        <div class="btn-wrap">
                            <button type="submit" class="btn"><span>{{__('cabinet.password.submit')}}</span></button>
                        </div>

                    </form>
                </div>

            </div>
        </div><!-- /container -->


        <div class="page-overlay"></div>

    </main>
    <!-- /main ===================================== -->
    <script type="text/javascript">
        $('#password').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{route('cabinet.changePassword')}}',
                type: 'POST',
                data: $(this).serialize(),
                success: function (result) {
                    if(result['status'] && result['status'] === 1){												
												$('#password .error').remove();
												$('#password').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
												setTimeout(function () {
														$('#password .success.active').removeClass('active');
														$('#password')[0].reset();
												}, 6000);
										} else {
											$('#password .error').remove();

											var passwordError = '';
											passwordError += '<ol class="text-danger">';

											$.each(result,function(key,data) {
													$.each(data, function(index, value) {
															passwordError += '<li>' + value + '</li>';
													});
											});

											passwordError += '</ol>';

											$('#password .btn-wrap').before('<div class="error">' + passwordError + '</div>');
										}										
                }
            });
        });
    </script>
@endsection