@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{$special->localDescription->title}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="content-text">
                @if(!empty($special->image))
                    <img src="{{$catalogService->resize($special->image, 360, 210)}}"
                         alt="{{$special->localDescription->title}}"
                         style="margin: 8px 40px 15px 0; float: left;">
                @endif
                {!! $special->localDescription->text !!}
            </div>

            <form class="question-form">
                <div class="question-form__head">{{__('specials.show.form.title')}}</div>
                <div class="question-form__body">
                    @csrf
                    <input class="input-name" type="text" name="name" required
                           placeholder="{{__('specials.show.form.placeholder_name')}}">
                    <input class="input-email" type="email" name="email" required
                           placeholder="{{__('specials.show.form.placeholder_email')}}">
                    <input class="input-text" type="text" name="text" required
                           placeholder="{{__('specials.show.form.placeholder_question')}}">
                    <div class="btn-wrap">
                        <button type="submit" class="btn"><span>{{__('specials.show.form.submit')}}</span></button>
                    </div>
                </div>
            </form>

        </div><!-- /container -->

        <div class="page-overlay"></div>

    </main>
    <script type="text/javascript">
        $('.question-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: `{{route('special.question')}}`,
                data: $(this).serialize(),
                type: 'POST',
                success: function (result) {
                    if (result['status'] && result['status'] === 1) {
                        $('.question-form .success').remove();

                        var html = '<div class="success active"><div class="s-inner"><span>'
                            + result['message'] + '</span></div></div>';

                        $('.question-form .question-form__body').append(html);
                        setTimeout(function () {
                            $('.question-form .success').remove();
                            $('.question-form')[0].reset();
                        }, 4000);
                    }
                }
            });
        });
    </script>
@endsection
