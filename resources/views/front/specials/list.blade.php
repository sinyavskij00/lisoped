@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content page-shares">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('specials.list.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                @foreach($specials as $special)
                    <div class="col-lg-4 col-sm-6">
                        <a href="{{route('special.show', $special->slug)}}" class="share-item">
                            @if($special->image)
                                <div class="share-item__img">
                                    <img src="{{$catalogService->resize($special->image, 360, 210)}}"
                                         alt="{{$special->localDescription->title}}">
                                </div>
                            @endif
                            <div class="share-item__info">
                                <div class="share-item__title">{{$special->localDescription->title}}</div>
                                @if(!empty($special->date_start) && !empty($special->date_end))
                                    <div class="share-item__period">
                                        {{__('specials.list.from')}} {{$special->date_start}} {{__('specials.list.to')}} {{$special->date_end}}
                                    </div>
                                @elseif(!empty($special->date_start))
                                    <div class="share-item__period">{{$special->date_start}}</div>
                                @elseif(!empty($special->date_end))
                                    <div class="share-item__period">{{__('specials.list.to')}} {{$special->date_end}}</div>
                                @endif
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div><!-- /container -->
        <div class="page-overlay"></div>

    </main>
@endsection
