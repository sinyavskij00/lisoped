@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content home-content">
        {!! $content !!}

        <!-- character -->
        <!-- data-time - time before character show -->
        <div class="character-wrapper character-welcome" data-time="25000">
            <div class="character">
                <div class="character-text">
                    <div class="holder">
                        <div class="character-text__inner">{!!__('common.home.character_text')!!}</div>
                        <button class="close"><i class="icon-close"></i></button>
                    </div>
                </div>
                <div class="character-img__holder">
                    <img src="images/charackter/Lisoped_man_7.svg" alt="character" class="character-img">
                </div>
            </div>
        </div>
        <!-- /character -->

        <div class="page-overlay"></div>

    </main><!-- /.page-content -->
@endsection