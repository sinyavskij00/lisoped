<section class="advantages">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="s-title">{{__('modules.advantages.title')}}</h2>
            </div>
            @php
            $items = __('modules.advantages.items');
            @endphp
            @if(!empty($items))
            <div class="col-12">
                <div class="advantages-list d-flex flex-wrap align-items-start justify-content-between items-animate">
                    @foreach($items as $item)
                    <div class="advantages-item item-animate">
                        <div class="advantages-item__icon">
                            <i class="{{!empty($item['icon']) ? $item['icon'] : ''}}"></i>
                        </div>
                        <div class="advantages-item__info">
                            <h6 class="advantages-item__title">{{!empty($item['title']) ? $item['title'] : ''}}</h6>
                            <p class="advantages-item__descr">{{!empty($item['description']) ? $item['description'] : ''}}</p>
                        </div>
                    </div>
                    <!-- /.advantages-item -->
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
<!-- /.advantages ===================================== -->