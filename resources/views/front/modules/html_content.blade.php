@if(!empty($text))
<section class="description">
    <div class="container">

        <div class="text-description show-text">
            <div>
                <div class="text-wrap">
                    <div class="text">
                        {!! $text !!}
                    </div>
                </div>
                <div class="btn-wrap hidden">
                    <span class="btn__text-more">{{__('modules.html_content.read_more')}}</span>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- /.description ===================================== -->
@endif