<section class="s__blog-tabs">
    <div class="container">
        <div class="tab-block blog-tabs">

            <div class="row">
                <div class="col-12">
                    <ul class="tab-nav d-flex align-items-center flex-wrap">
                        @if($newses->count() > 0)
                            <li class="active">{{__('modules.blog.labels.blog')}}</li>
                        @endif
                        @if($testimonials->count() > 0)
                            <li {!! $newses->count() === 0 ? 'class="active"' : '' !!}>{{__('modules.blog.labels.testimonials')}}</li>
                        @endif
                    </ul>
                </div>
            </div>
            <!-- /.tab-nav -->

            <div class="tab-cont">
                @if($newses->count() > 0)
                    <div class="tab-pane tab-blog items-animate">
                        <div class="row">
                            @foreach($newses as $news)
                                <div class="col-lg-3 col-sm-6">
                                    <div class="blog-item item-animate">
                                        @if(Storage::disk('public')->exists($news->image))
                                            <a href="{{route('news.show', $news->slug)}}" rel="nofollow"
                                               class="blog-item__img">
                                                <img src="{{$catalogService->resize($news->image, 270, 305, false)}}"
                                                     alt="blog-item__title">
                                            </a>
                                        @endif
                                        <h6 class="blog-item__title">{{$news->localDescription->title}}</h6>
                                        <a href="{{route('news.show', $news->slug)}}" class="more">
                                            <span>{{__('modules.blog.read_more')}}</span>
                                            <i class="i-next"></i>
                                        </a>
                                    </div>
                                </div><!-- /.blog-item -->
                            @endforeach
                            <div class="col-12">
                                <div class="all">
                                    <a href="{{route('news.list')}}" class="btn">{{__('modules.blog.see_all')}}</a>
                                </div>
                            </div>

                        </div>

                    </div>
                    <!-- /.tab-pane -->
                @endif
                @if($testimonials->count() > 0)
                    <div class="tab-pane tab-reviews">
                        <div class="row">

                            <div class="col-12">
                                @foreach($testimonials as $testimonial)
                                    <div class="reviews-item">
                                        <div class="reviews-item__reviewer">{{$testimonial->name}}</div>
                                        <div class="reviews-item__date">{{$testimonial->created_at}}</div>
                                        <div class="rating reviews-item__rating">
                                            @for($i = 1; $i <= 5; $i++)
                                                @if($testimonial->rating > $i)
                                                    <i class="icon-star-full i_stack-2x"></i>
                                                @else
                                                    <i class="icon-star-full i_stack-1x"></i>
                                                @endif
                                            @endfor
                                        </div>
                                        <div class="reviews-item__descr">
                                            <p>{{$testimonial->text}}</p>
                                        </div>
                                    </div><!-- /.reviews-item -->
                                @endforeach
                            </div>

                            <div class="col-12">
                                <div class="all">
                                    <a href="{{route('testimonials')}}" class="btn">{{__('modules.blog.see_all')}}</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.tab-pane -->
                @endif
            </div>
            <!-- /.tab-cont -->

        </div><!-- /.tab-block -->
    </div>
</section>
<!-- /.s__blog-tabs ===================================== -->
