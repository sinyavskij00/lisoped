@if(isset($banners) && $banners->count() > 0)
<section class="s-banner">
    <div class="main-banner">
        @foreach($banners as $banner)
            @if(isset($banner->main_image))
                    <div class="banner-item__wrapper">
                        <div class="banner-item banner-item__with-img"
                             style="background-image: url({{Storage::disk('public')->url($banner->background_image)}});">
                            <div class="d-flex">
                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="col-lg-7 col-sm-6 order-sm-last banner-p__img-wrap">
                                            <a href="{{$banner->localDescription->link}}" class="banner-p__img">
                                                <img src="{{Storage::disk('public')->url($banner->main_image)}}" alt="bicycle">
                                            </a>
                                        </div>
                                        <div class="col-lg-5 col-sm-6 banner-item_descr-wrap">
                                            <div class="banner-item_descr">
                                                @if($banner->localDescription->text)
                                                <h2 class="h2">{{$banner->localDescription->text}}</h2>
                                                @endif
                                                <div class="banner-item_links">
                                                    @if($banner->localDescription->link_title)
                                                    <a href="{{$banner->localDescription->link}}" class="banner-c__link btn_accent">
                                                        <span class="text">{{$banner->localDescription->link_title}}</span>
                                                        <span class="i i-next"></span>
                                                    </a>
                                                    @endif
                                                    @if($banner->localDescription->link_subtitle)
                                                    <a href="{{$banner->localDescription->link}}" class="banner-p__link">
                                                        {{$banner->localDescription->link_subtitle}}
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.banner-item -->
                    </div>
            @else
                    <div class="banner-item__wrapper">
                        <div class="banner-item" style="background-image: url({{Storage::disk('public')->url($banner->background_image)}});">
                            <div class="d-flex">
                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="col-lg-5 col-md-6 banner-item_descr-wrap">
                                            <div class="banner-item_descr">
                                                @if($banner->localDescription->text)
                                                <h2 class="h2">{{$banner->localDescription->text}}</h2>
                                                @endif
                                                <div class="banner-item_links">
                                                    @if($banner->localDescription->link_title)
                                                    <a href="{{$banner->localDescription->link}}" class="banner-c__link btn_accent">
                                                        <span class="text">{{$banner->localDescription->link_title}}</span>
                                                        <span class="i i-next"></span>
                                                    </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.banner-item -->
                    </div>
            @endif
        @endforeach
    </div>
</section>
<!-- /.s-banner ===================================== -->
@endif
