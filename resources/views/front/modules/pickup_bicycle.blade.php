<section id="pickup_bicycle" class="choice-help s-padding">
    <div class="container">
        <h2 class="s-title">{{__('modules.pickup_bicycle.title')}}</h2>

        <form id="choose-bicycle" class="ch-help__form">
            @csrf
            <div class="ch-help__form-col ch-help__form-col_1">
                <label for="#ch-help__inp1">{{__('modules.pickup_bicycle.field_titles.gender')}}</label>
                <select name="gender" id="ch-help__inp1" class="select">
                    @foreach($genderOptions as $genderId => $genderName)
                        <option value="{{$genderId}}">{{$genderName}}</option>
                    @endforeach
                </select>
            </div>

            <div class="ch-help__form-col ch-help__form-col_2">
                <label for="#help__inp2">{{__('modules.pickup_bicycle.field_titles.height')}}</label>
                <input id="help__inp2" type="number" name="height" placeholder="170" required>
            </div>

            <div class="ch-help__form-col ch-help__form-col_3">
                <label for="#help__inp3">{{__('modules.pickup_bicycle.field_titles.weight')}}</label>
                <input id="help__inp3" type="number" name="weight" placeholder="54" required>
            </div>

            <div class="ch-help__form-col ch-help__form-col_4">
                <label for="help__inp4">{{__('modules.pickup_bicycle.field_titles.trace_type')}}</label>
                <select id="help__inp4" name="trace_type" class="select">
                    @foreach($traceTypeOptions as $traceId => $traceName)
                        <option value="{{$traceId}}">{{$traceName}}</option>
                    @endforeach
                </select>
            </div>

            <div class="ch-help__form-col ch-help__form-col_5">
                <label for="help__inp5">{{__('modules.pickup_bicycle.field_titles.price')}}</label>
                <select name="price_range" id="help__inp5" class="select">
                    @foreach($priceOptions as $priceOption)
                        <option value="{{$priceOption}}">{{$priceOption}} грн</option>
                    @endforeach
                </select>
            </div>

            <button class="btn"><span>{{__('modules.pickup_bicycle.submit')}}</span></button>

        </form>
    </div>
    <script type="text/javascript">
        $('#choose-bicycle').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: `{{route('module.pickupBicycle')}}`,
                type: 'POST',
                data: $('#choose-bicycle').serialize(),
                success: function (result) {
                    if (result['status'] && result['status'] === 1 && result['result']['quantity'] > 0) {
                        window.location = result['result']['link'];
                    } else {
                        $('#choose-bicycle .text-danger').remove();
                        $('#choose-bicycle').append('<div class="text-danger"><span>' + result['result']['message'] + '</span></div>');
                        setTimeout(function () {
                            $('#choose-bicycle .text-danger').remove();
                        }, 5500);
                    }
                }
            });
        });
    </script>
</section>
<!-- /.choice-help ===================================== -->