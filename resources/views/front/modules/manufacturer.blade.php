@if($manufacturers->count() > 0)
    <section class="s-manufacturer s-padding">
        <div class="container">

            <h3 class="s-title">{{__('modules.manufacturers.title')}}</h3>

            <div class="manufacturer-slider">
                @foreach($manufacturers as $manufacturer)
                    @if(Storage::disk('public')->exists($manufacturer->getAttributeValue('image')))
                        <div class="manufacturer-item">
                            <a href="{{route('manufacturer.show', $manufacturer->slug)}}" class="manufacturer-item__img">
                                <img src="{{Storage::disk('public')->url($manufacturer->getAttributeValue('image'))}}"
                                     alt="{{$manufacturer->getAttributeValue('name')}}">
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

        </div>
    </section>
    <!-- /.s-manufacturer ===================================== -->
@endif