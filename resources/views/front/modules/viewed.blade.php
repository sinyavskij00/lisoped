@if($viewed->count())
<div class="slider-section">
    <h2 class="slider-title">{{__('modules.viewed.title')}}</h2>
    <div class="slider-products">
        @foreach($viewed as $product)
        <div class="slider-products__item">
            @include('front.catalog.product_item', ['product' => $product])
        </div>
        <!-- /.slider-products__item -->
        @endforeach
    </div>
    <!-- /.slider-products -->

</div>
<!-- /.slider-section -->
@endif