<section class="s-categories">
    <div class="container">
        <div class="categories d-flex flex-wrap items-animate">
            @if($firstCategory)
                <div class="categories-col categories-col__left">
                    <a href="{{route('category_path', category_path($firstCategory->id))}}" class="categories-item item-animate">
                        @if(isset($firstCategory->localDescription))
                            <span class="categories-item__title">{{$firstCategory->localDescription->name}}</span>
                        @endif
                        @if(Storage::disk('public')->exists($firstCategory->image))
                            <span class="categories-item__img">
                                <img src="{{$catalogService->resize($firstCategory->image, 400, 315, false, true)}}"
                                 alt="{{$firstCategory->localDescription->name}}">
                            </span>
                        @endif
                    </a>
                </div>
            @endif
            <div class="categories-col categories-col__right">
                @foreach($categories as $category)
                    <a href="{{route('category_path', category_path($category->id))}}" class="categories-item item-animate">
                        @if(isset($category->localDescription))
                            <span class="categories-item__title">{{$category->localDescription->name}}</span>
                        @endif
                        @if(Storage::disk('public')->exists($category->image))
                            <span class="categories-item__img">
                                <img src="{{$catalogService->resize($category->image, 290, 150, false, true)}}"
                                 alt="{{$category->localDescription->name}}">
                            </span>
                        @endif
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- /.s-categories ===================================== -->
