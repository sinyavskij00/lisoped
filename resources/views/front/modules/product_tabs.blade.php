<section class="s-products__tabs s-padding">
    <div class="container">
        <div class="tab-block products-tabs">

            <div class="row">
                <div class="col-12">
                    <ul class="tab-nav d-flex align-items-center flex-wrap">
                        @foreach($labels as $label)
                        <li {!! $label['active'] ? 'class="active"' : '' !!}>{{$label['title']}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="tab-cont">

                <div class="tab-pane">
                    <div class="tab-products">
                        @foreach($newProducts as $product)
                            <div class="slider-products__item">
                                @include('front.catalog.product_item')
                            </div>
                        @endforeach
                    </div>

                    {{--<div class="row">--}}
                        {{--@foreach($newProducts as $product)--}}
                            {{--<div class="col-lg-4 col-sm-6">--}}
                            {{--@include('front.catalog.product_item')--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                </div><!-- /.tab-pane -->

                <div class="tab-pane">
                    <div class="tab-products">
                        @foreach($hitProducts as $product)
                            <div class="slider-products__item">
                                @include('front.catalog.product_item')
                            </div>
                        @endforeach
                    </div>

                    {{--<div class="row">--}}
                        {{--@foreach($hitProducts as $product)--}}
                            {{--<div class="col-lg-4 col-sm-6">--}}
                            {{--@include('front.catalog.product_item')--}}
                            {{--<!-- /.product-item -->--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                </div>

                <div class="tab-pane">
                    <div class="tab-products">
                        @foreach($specialProducts as $product)
                            <div class="slider-products__item">
                                @include('front.catalog.product_item')
                            </div>
                        @endforeach
                    </div>

                    {{--<div class="row">--}}
                        {{--@foreach($specialProducts as $product)--}}
                            {{--<div class="col-lg-4 col-sm-6">--}}
                            {{--@include('front.catalog.product_item')--}}
                            {{--<!-- /.product-item -->--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                </div>

            </div>
        </div>
    </div>
</section>
<!-- /.s-products__tabs ===================================== -->