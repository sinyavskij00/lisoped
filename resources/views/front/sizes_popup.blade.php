@if(!empty($tables))
    <!-- sizes popups =================== -->
    <div class="hidden">
        <div id="sizes-popup" class="sizes-popup sizes-popup__col-1 popup">
            <div class="sizes-popup__text content-text">
                {!! __('common.size_popup.title') !!}
            </div>
            <div class="sizes-popup__tables">
                @foreach($tables as $table)
                    <div class="sizes-popup__table--wrapper">
                        <div class="sizes-popup__table-head">{{isset($table['title']) ? $table['title'] : ''}}</div>
                        <table class="sizes-popup__table">
                            @if(!empty($table['headers']))
                                <thead>
                                <tr>
                                    @foreach($table['headers'] as $header)
                                        <th>{{$header}}</th>
                                    @endforeach
                                </tr>
                                </thead>
                            @endif
                            @if(!empty($table['rows']))
                                <tbody>
                                @foreach($table['rows'] as $row)
                                    <tr>
                                        @foreach($row as $value)
                                            <td>{{$value}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            @endif
                        </table>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- /sizes popups =================== -->
@endif