@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('information.credit.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            @if(!empty($text))
                <div class="content-text">
                    {!! $text !!}
                </div>
            @endif
            @include('front.information.any_questions_form')
        </div><!-- /container -->
        <div class="page-overlay"></div>
    </main>
@endsection