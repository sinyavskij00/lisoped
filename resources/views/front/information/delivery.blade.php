@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content page-delivery p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('information.delivery.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">

            <div class="content-text delivery-oreder__content-text">
                @if (!empty($delivery))
                    {!! $delivery !!}

                @endif

                @if (!empty($pay))
                    {!! $pay !!}

                @endif
                @include('front.information.any_questions_form')

            </div><!-- /container -->


            <div class="page-overlay"></div>
        </div>

    </main>

@endsection