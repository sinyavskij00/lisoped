@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('information.contacts.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <div class="contacts">
                        @php
                            $languageId = config('current_language_id');
                            $workTime = '';
                            if (isset($workTimes) && !empty($workTimes[$languageId])) {
                                $workTime = explode(PHP_EOL, $workTimes[$languageId]);
                                $workTime = implode('<br />', $workTime);
                            }
                        @endphp
                        @if(!empty($workTime))
                            <div class="contacts-item">
                                <h6 class="contacts-item__title">{{__('information.contacts.work_time_title')}}</h6>
                                <div class="timework">{!! $workTime !!}</div>
                            </div>
                        @endif
                        @if(!empty($siteTelephones))
                            <div class="contacts-item">
                                <h6 class="contacts-item__title">{{__('information.contacts.text_phones')}}</h6>
                                <ul class="phones-list ul">
                                    @foreach($siteTelephones as $telephone)
                                        <li>
                                            <span class="a">
                                                <a href="tel:{{preg_replace("/[^0-9]/", "", $telephone)}}"><span>{{$telephone}}</span></a>
                                                @if($loop->index === 0)
                                                    <a href="https://t.me/Lisoped"><span class="icon icon_telegram"></span></a>
                                                    <a href="viber://chat?number=+380676206611"><span class="icon icon_viber"></span></a>
                                                    <a href="https://wa.me/380676206611"><span class="icon icon_whatsapp"></span></a>
                                                @endif
                                            </span>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="contacts-item">
                            <h6 class="contacts-item__title">{{__('information.contacts.text_email')}}</h6>
                            <a href="mailto:{{config('app.admin_email')}}">{{config('app.admin_email')}}</a>
                        </div>

                        <div class="contacts-item">
                            <h6 class="contacts-item__title">{{__('information.contacts.text_addresses')}}</h6>
                            <address>{{__('information.contacts.address1')}}</address>
                            <address>{{__('information.contacts.address2')}}</address>
                            <p class="attention">{{__('information.contacts.address_attention')}}</p>
                        </div>

                    </div>
                </div>
                <div class="col-md-8 order-md-first">
                    <div class="map"
                         data-lat="{{isset($location[0]) ? $location[0] : ''}}"
                         data-lng="{{isset($location[1]) ? $location[1] : ''}}"
                         data-title="{{__('information.contacts.map_title')}}"></div>
                </div>

            </div>

            @include('front.information.any_questions_form')

        </div><!-- /container -->

        <div class="page-overlay"></div>

    </main>
@endsection