<form class="question-form">
    @csrf
    <div class="question-form__head">{{__('information.any_questions.title')}}</div>
    <div class="question-form__body">
        <input class="input-name" type="text" name="name" required placeholder="{{__('information.any_questions.placeholder_name')}}">
        <input class="input-email" type="email" name="email" required placeholder="{{__('information.any_questions.placeholder_email')}}">
        <input class="input-text" maxlength="600" type="text" name="text" required placeholder="{{__('information.any_questions.placeholder_question')}}">
        <div class="btn-wrap">
            <button type="submit" class="btn"><span>{{__('information.any_questions.submit')}}</span></button>
        </div>
    </div>
</form>
<script type="text/javascript">
    $('.question-form').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: `{{route('contacts')}}`,
            type: 'POST',
            data: $(this).serialize(),
            success: function (result) {
                if (result['status'] && result['status'] === 1) {
                    $('.question-form .success').remove();
                    $('.question-form .question-form__body').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                    setTimeout(function () {
                        $('.question-form .success').remove();
                        $('.question-form')[0].reset();
                    }, 4000);
                }
            }
        });
    })
</script>