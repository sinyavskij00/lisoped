@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('information.about_us.title')}}</h1>
                </div>
            </div>
        </div>
        <style>
            .about-us__content strong {
                color: #42cf9e;
            }
        </style>
        <div class="container">
            @if(isset($text))
            <div class="about-us__content">
                {!! $text !!}
            </div>
            @endif
        </div><!-- /container -->

        <div class="page-overlay"></div>

    </main>
@endsection