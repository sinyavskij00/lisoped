@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content page-reviews p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('information.testimonials.title')}}</h1>
{{--                    <p class="p-descr">{{__('information.testimonials.subtitle')}}</p>--}}
                </div>
            </div>
        </div>

        <div class="container">

            <section class="reviews-section">

                <div class="reviews-list">
                    @foreach($testimonials as $testimonial)
                        <div class="reviews-list__item">
                            <div class="reviews-item">
                                <div class="reviews-item__reviewer">{{$testimonial->name}}</div>
                                <div class="reviews-item__date">{{$testimonial->created_at}}</div>
                                <div class="rating reviews-item__rating">
                                    @for($i = 1; $i <= 5; $i++)
                                        @if($testimonial->rating > $i)
                                            <i class="icon-star-full i_stack-2x"></i>
                                        @else
                                            <i class="icon-star-full i_stack-1x"></i>
                                        @endif
                                    @endfor
                                </div>
                                <div class="reviews-item__descr">
                                    <p>{{$testimonial->text}}</p>
                                    @if($testimonial->images->count() > 0)
                                        @foreach($testimonial->images as $image)
                                            <img src="{{$catalogService->resize($image->image, 100, 100, false)}}">
                                        @endforeach
                                    @endif
                                    <div class="answer">
                                        <span class="to-answer" data-form="answer"
                                              data-parent-id="{{$testimonial->id}}">Ответить</span>
                                    </div>
                                </div>
                            </div><!-- /.reviews-item -->
                            @if($testimonial->children->count() > 0)
                                @foreach($testimonial->children as $child)
                                    <div class="reviews-item level1 hidden">
                                        <div class="reviews-item__reviewer">{{$child->name}}
                                            <span>ответил(а)</span> {{$testimonial->name}}</div>
                                        <div class="reviews-item__date">{{$child->created_at}}</div>
                                        <div class="reviews-item__descr">
                                            <p>{{$child->text}}</p>
                                        </div>
                                    </div><!-- /.reviews-item -->
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>
                <!-- /.reviews-list -->

                <form class="reviews-form" enctype="multipart/form-data">
                    @csrf
                    <div class="formhead">{{__('information.testimonials.form.title')}}</div>
                    <div class="d-flex justify-content-sm-between flex-wrap">
                        <div class="reviews-form__content">
                            <textarea name="text" cols="30" rows="7" required maxlength="3000"
                                      placeholder="{{__('information.testimonials.form.placeholder_comment')}}"></textarea>
                            <label class="reviews-form__file">
                                <input type="file" name="images[]" multiple>
                                <span class="reviews-form__file-name"></span>
                                <span class="reviews-form__file-icon"><i class="icon-attach_file"></i></span>
                            </label>
                        </div>
                        <div class="reviews-form__info">
                            <div class="rating-wrapper clearfix">
                                <input type="radio" class="rating-input" id="rating-input-1-5" name="rating" value="5"/>
                                <label for="rating-input-1-5" class="rating-star"></label>

                                <input type="radio" class="rating-input" id="rating-input-1-4" name="rating" value="4"/>
                                <label for="rating-input-1-4" class="rating-star"></label>

                                <input type="radio" class="rating-input" id="rating-input-1-3" name="rating" value="3"/>
                                <label for="rating-input-1-3" class="rating-star"></label>

                                <input type="radio" class="rating-input" id="rating-input-1-2" name="rating" value="2"/>
                                <label for="rating-input-1-2" class="rating-star"></label>

                                <input type="radio" class="rating-input" id="rating-input-1-1" name="rating" value="1"/>
                                <label for="rating-input-1-1" class="rating-star"></label>
                            </div>
                            <input type="text" name="name" required placeholder="{{__('information.testimonials.form.placeholder_name')}}">
                            <input type="email" name="email" required placeholder="{{__('information.testimonials.form.placeholder_email')}}">
                            <button type="submit" class="btn"><span>Отправить</span></button>
                        </div>
                    </div>
                </form>
            </section>
            <!-- /.reviews-section -->

            {{$testimonials->links('front.paginate')}}

        </div><!-- /container -->


        <div class="page-overlay"></div>

    </main>
    <script type="text/javascript">
        $('.reviews-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: `{{route('testimonials.add')}}`,
                //data: $(this).serialize(),
                data: new FormData(this),
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (result) {
                    if (result['status'] && result['status'] === 1) {
                        $('.reviews-form').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                        setTimeout(function () {
                            $('.reviews-form .success').remove();
                            $('.reviews-form')[0].reset();
                        }, 4000);
                    }
                }
            });
        });


        // answer comment form
        $('[data-form="answer"]').on('click', function () {
            var toAnswerBtn = $(this);
            var div = $(this).parent().parent();
            var reviewerName = toAnswerBtn.closest('.reviews-item').find('.reviews-item__reviewer').text();
            var parentCommentId = $(this).attr('data-parent-id');

            var html = "<form class='answer-form' enctype='multipart/form-data'>";
            html += "<div class='answer-form__head'>{{__('information.testimonials.answer')}} <strong>" + reviewerName + "</strong></div>";
            html += "<div class='d-flex justify-content-sm-between flex-wrap'>";
            html += "<div class='answer-form__content'>";
            html += "<textarea maxlength=\"3000\" name='text' cols='30' rows='2' required placeholder='{{__('information.testimonials.placeholder_answer')}}'></textarea>";
            html += "<label class='reviews-form__file'>";
            html += "<input type='file' name='images[]' multiple><span class='reviews-form__file-name'></span><span class='reviews-form__file-icon'><i class='icon-attach_file'></i></span>";
            html += "<input type='hidden' name='_token' value='{{csrf_token()}}'>";
            if (parentCommentId) {
                html += "<input type='hidden' name='parent_id' value='" + parentCommentId + "'>";
            }
            html += "</label>";
            html += "</div>";
            html += "<div class='answer-form__info'>";
            html += "<input type='text' name='name' required placeholder='Ваше имя'><button type='submit' class='btn butt1'><span>{{__('information.testimonials.answer')}}</span></button><button class='butt2'><span>Отмена</span></button>";
            html += "</div>";
            html += "</div>";
            html += "</form>";

            var block = $(html);

            block.find(".butt2").click(function () {
                toAnswerBtn.css("display", "block");
                block.remove();
            });

            block.on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    url: `{{route('testimonials.add')}}`,
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        if (result['status'] && result['status'] === 1) {
                            $('.answer-form').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                            setTimeout(function () {
                                $('.answer-form .success').remove();
                                $('.answer-form')[0].reset();
                            }, 4000);
                        }
                    }
                });
            });

            div.after(block);
            toAnswerBtn.css("display", "none");
        });
    </script>
@endsection
