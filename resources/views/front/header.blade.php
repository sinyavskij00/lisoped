<!DOCTYPE html>
<html lang="ru">

<head>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WBMGL3X');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">

    <title>{{!empty($meta_title) ? $meta_title : config('app.name')}}</title>
    @if(isset($meta_description))
        <meta name="description" content="{{ $meta_description }}">
    @endif
    @if(isset($meta_keywords))
        <meta name="keywords" content="{{$meta_keywords}}">
    @endif
    @if(!empty($metaLinks))
        @foreach($metaLinks as $rel => $link)
            @if(!empty($rel) && !empty($link))
                <link href="{{$link}}" rel="{{$rel}}"/>
            @endif
        @endforeach
    @endif
    <base href="/">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Template Basic Images Start -->
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="icon" href="assets/img/favicon/favicon-lisoped.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-touch-icon.png">
    <!-- Template Basic Images End -->

    <!-- Custom Browsers Color Start -->
    <meta name="theme-color" content="#000">
    <!-- Custom Browsers Color End -->

    <link rel="stylesheet" href="{{mix('front/css/main.min.css')}}">
    <script type="text/javascript" src="{{mix('front/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{mix('front/js/jquery.autocomplete.min.js')}}"></script>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WBMGL3X"
            height="0" width="0" style="display:none;visibility:hidden">
    </iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="my-wrapper" class="wrapper">

    <header id="my-header" class="header">
        @if(isset($headerBanner))
        <a href="#" class="top-banner" style="background-color: {{$headerBanner->background_color}}; color: {{$headerBanner->text_color}}">{{$headerBanner->text}}</a>
        @endif
        <div class="container">

            <div class="topline d-flex align-items-center justify-content-between flex-wrap">

                <ul class="h-item menu d-lg-flex align-items-center flex-wrap remove-md">
                    <li class="has-dropdown">
                        <span>{{__('common.header.for_customers')}}</span>
                        <div></div>
                        <ul class="dropdown">
                            <li><a href="{{route('delivery')}}">{{__('common.header.delivery')}}</a></li>
                            <li><a href="{{route('testimonials')}}">{{__('common.header.testimonials')}}</a></li>
                            <li><a href="{{route('faq')}}">{{__('common.header.faq')}}</a></li>
                            <li><a href="{{route('credit')}}">{{__('common.header.credit')}}</a></li>
                            <li><a href="{{route('manufacturer.list')}}">{{__('common.header.manufacturers')}}</a></li>
                            <li><a href="{{route('about_us')}}">{{__('common.header.about_us')}}</a></li>
                        </ul>
                    </li>
                    <li><a href="{{route('news.list')}}">{{__('common.header.blog')}}</a></li>
                    <li><a href="{{route('contacts')}}">{{__('common.header.contacts')}}</a></li>
                    <li><a href="{{route('special.list')}}">{{__('common.header.specials')}}</a></li>
                </ul>

                <span data-toggle="mobile-menu" class="hamburger hamburger--3dx">
						<div class="hamburger-box">
							<div class="hamburger-inner"></div>
						</div>
                </span>

                @if(Route::currentRouteName() === 'home')
                    @if(isset($logo))
                        <span class="logo"><img src="{{$logo}}" alt="{{config('app.name')}}"></span>
                    @endif
                @else
                    @if(isset($logo))
                        <a href="{{route('home')}}" class="logo"><img src="{{$logo}}" alt="{{config('app.name')}}"></a>
                    @endif
                @endif
                <div class="h-item h-phones hidden-sm remove-sm">
                    @if(!empty($siteTelephones))
                        <div class="h-phones__list has-dropdown">
                            @if(!empty($siteTelephones[0]))
                                <a href="tel:{{preg_replace("/[^0-9]/", "", $siteTelephones[0])}}">{{$siteTelephones[0]}}</a>
                            @endif
                            <div class="h-phones-more" data-toggle="dropdown">
                                <i class="icon-caret-down"></i>
                            </div>
                            <div class="dropdown h-phones__dropdown">
                                <ul>
                                    @foreach($siteTelephones as $telephone)
                                        <li>
                                            <span class="a">
                                                <a href="tel:{{preg_replace("/[^0-9]/", "", $telephone)}}"><span>{{$telephone}}</span></a>
                                                @if($loop->index === 0)
                                                    <a href="https://t.me/Lisoped"><span class="icon icon_telegram"></span></a>
                                                    <a href="viber://chat?number=+380676206611"><span class="icon icon_viber"></span></a>
                                                    <a href="https://wa.me/380676206611"><span class="icon icon_whatsapp"></span></a>
                                                @endif
                                            </span>
                                        </li>
                                    @endforeach
                                </ul>
                                @php
                                    $languageId = config('current_language_id');
                                    $workTime = '';
                                    if (isset($workTimes) && !empty($workTimes[$languageId])) {
                                        $workTime = explode(PHP_EOL, $workTimes[$languageId]);
                                        $workTime = implode('<br />', $workTime);
                                    }
                                @endphp
                                @if(!empty($workTime))
                                    <strong class="h6">{{__('common.header.work_time_title')}}</strong>
                                    <div class="timework">{!! $workTime !!}</div>
                                @endif
                            </div>
                        </div>
                    @endif
                    <a href="#callback-popup" class="btn__callback to-popup">{{__('common.header.back_call')}}</a>
                </div>

                <div class="h-item header__user-buttons">
                    <ul class="hub-list d-flex justify-content-center justify-content-sm-end">

                        <li class="hub-i hub-i__user hidden-md">
                            @if(Auth::check())
                                <a href="{{route('cabinet.index')}}" class="hub-i__link accent tooltip"
                                   data-tooltip="{{__('common.header.text_cabinet')}}">
                                    <span class="i i-user"></span>
                                    {{--<span class="user-logged__text">{{__('common.header.text_my_cabinet')}}</span>--}}
                                </a>
                            @else
                                <a href="#log-in-popup" class="hub-i__link to-popup tooltip"
                                   data-tooltip="{{__('common.header.text_auth_reg')}}">
                                    <span class="i i-user"></span>
                                </a>
                            @endif
                        </li>

                        <li class="hub-i hub-i__compare">
                            <a id="compare-link" href="{{route('compare.categoryList')}}"
                               class="hub-i__link tooltip" data-tooltip="{{__('common.header.text_compare')}}">
                                <span class="i i-lawyer"></span>
                                @if($compare->getQuantity() > 0)
                                    <span class="num-total">{{$compare->getQuantity()}}</span>
                                @endif
                            </a>
                        </li>

                        <li class="hub-i hub-i__wishlist">
                            @if(Auth::check())
                                <a id="wishlist-link" href="{{route('cabinet.wishlist')}}"
                                   class="hub-i__link tooltip" data-tooltip="{{__('common.header.text_wishlist')}}">
                                    <span class="i i-heart"></span>
                                    @if($wishListService->getQuantity() > 0)
                                        <span class="num-total">{{$wishListService->getQuantity()}}</span>
                                    @endif
                                </a>
                            @else
                                <a id="wishlist-link" href="#wishlist-popup"
                                   class="hub-i__link tooltip to-popup"
                                   data-tooltip="{{__('common.header.text_wishlist')}}">
                                    <span class="i i-heart"></span>
                                    @if($wishListService->getQuantity() > 0)
                                        <span class="num-total">{{$wishListService->getQuantity()}}</span>
                                    @endif
                                </a>
                            @endif
                        </li>
                        <li class="hub-i hub-i__cart">
                            <a href="#cart-popup" class="hub-i__link to-popup tooltip"
                               data-tooltip="{{__('common.header.text_cart')}}">
                                <span class="i i-shopping-cart"></span>
                                @if($cart->items()->count() > 0)
                                    <span class="num-total">{{$cart->items()->count()}}</span>
                                @endif
                            </a>
                        </li>

                    </ul>
                </div>

            </div><!-- /.topline -->
        </div>

        <div class="botline">
            <div class="container">

                <div class="botline-inner d-flex align-items-center justify-content-between flex-wrap">

                    <a href="#mobile-catalog" id="trigger-btn" class="btn-accent">
                        <span>{{__('common.header.text_catalog')}}</span>
                    </a>

                    <ul class="language ul">
                        @foreach($languagesSwitcher as $item)
                            <li class="language-item {{$item['is_active'] ? 'active' : ''}}"><a
                                        href="{{$item['link']}}">{{$item['text']}}</a></li>
                        @endforeach
                    </ul>
                    @if(isset($headerCategoryMenu))
                        {!! $headerCategoryMenu !!}
                    @endif
                    <div class="search">
                        <div class="search-btn">
                            <span class="text">{{__('common.header.text_search')}}</span>
                            <span class="i i-search"></span>
                        </div>
                        <div class="search-inp">
                            <form action="{{route('search')}}">
                                <input autocomplete="off" name="query" type="search"
                                       placeholder="{{__('common.header.placeholder_search')}}">
                            </form>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $('.search input').on('input', function (e) {
                            $.ajax({
                                url: `{{route('search.ajax')}}`,
                                data: {
                                    query: $('.search input').val()
                                },
                                success: function (result) {
                                    $('#live-search').remove();
                                    $('.search input').parent().after(result);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div><!-- /.botline -->


        <!-- .mobile-menu ================== -->
        <div id="mobile-menu" class="mobile-menu">

            <div class="btn-close">
                <i class="icon-close"></i>
            </div>

            <div class="mobile-item mobile-item__user">
                @if(Auth::check())
                    <span class="i i-user accent"></span>
                    <a href="{{route('cabinet.index')}}">{{__('common.header.text_cabinet')}}</a>
                @else
                    <span class="i i-user accent"></span>
                    <a href="#log-in-popup" class="to-popup">{{__('common.header.text_enter')}}</a> /
                    <a href="{{route('register')}}">{{__('common.header.text_register')}}</a>
                @endif
            </div>

            <ul class="mobile-list mobile-item mobile-list__phones">
                <li><span class="h6">{{__('common.header.text_order_by_phone')}}</span></li>
                @foreach($siteTelephones as $telephone)
                    <li>
                        <span class="a">
                            <a href="tel:{{preg_replace("/[^0-9]/", "", $telephone)}}"><span>{{$telephone}}</span></a>
                            @if($loop->index === 0)
                                <a href="https://t.me/Lisoped"><span class="icon icon_telegram"></span></a>
                                <a href="viber://chat?number=+380676206611"><span class="icon icon_viber"></span></a>
                                <a href="https://wa.me/380676206611"><span class="icon icon_whatsapp"></span></a>
                            @endif
                                </span>
                    </li>
                @endforeach
                <li>
                    <a href="#callback-popup" class="btn__callback to-popup">{{__('common.header.back_call')}}</a>
                </li>
            </ul>

            <ul class="mobile-list mobile-item">
                <li><a href="{{route('delivery')}}">{{__('common.header.delivery')}}</a></li>
                <li><a href="{{route('testimonials')}}">{{__('common.header.testimonials')}}</a></li>
                <li><a href="{{route('faq')}}">{{__('common.header.faq')}}</a></li>
                <li><a href="{{route('credit')}}">{{__('common.header.credit')}}</a></li>
                <li><a href="{{route('manufacturer.list')}}">{{__('common.header.manufacturers')}}</a></li>
                <li><a href="{{route('about_us')}}">{{__('common.header.about_us')}}</a></li>
                <li><a href="{{route('news.list')}}">{{__('common.header.blog')}}</a></li>
                <li><a href="{{route('contacts')}}">{{__('common.header.contacts')}}</a></li>
                <li><a href="{{route('special.list')}}">{{__('common.header.specials')}}</a></li>
            </ul>
            @if(!empty($workTime))
            <div class="mobile-item mobile-item__timework">
                <span class="h6">{{__('common.header.work_time_title')}}</span>
                <div class="timework">{!! $workTime !!}</div>
            </div>
            @endif
        </div>
        <!-- /.mobile-menu ================== -->


        @if(isset($headerCategoryMobile))
            {!! $headerCategoryMobile !!}
        @endif
        <div class="hidden">
            <div id="cart-popup" class="cart-popup popup">
                @include('front.checkout.cart_list')
            </div><!-- /.cart-popup -->
        </div>

    </header>
    <!-- /.header ===================================== -->
