@if(isset($paginator) && $paginator->hasPages())
    <!-- pagination -->
    <ul class="pagination d-flex align-items-center justify-content-center flex-wrap ul">
        @if(!$paginator->onFirstPage())
            <li><a class="prev" href="{{$paginator->previousPageUrl() }}"></a></li>
        @endif

        @if($paginator->currentPage() <= 3)
            @if($paginator->currentPage() == 1)
                @for($i = 1; $i <= 3 && $i <= $paginator->lastPage(); $i++)
                    <li{!! $paginator->currentPage() == $i ? ' class="active"' : '' !!}><a
                                href="{{$paginator->url($i)}}">{{$i}}</a></li>
                @endfor
            @else
                @for($i = 1; $i <= $paginator->currentPage() + 1 && $i <= $paginator->lastPage(); $i++)
                    <li{!! $paginator->currentPage() == $i ? ' class="active"' : '' !!}><a
                                href="{{$paginator->url($i)}}">{{$i}}</a></li>
                @endfor
            @endif
            @if($paginator->lastPage() - 2 > $paginator->currentPage())
                <li><span>...</span></li>

                <li><a href="{{$paginator->url($paginator->lastPage())}}">{{$paginator->lastPage()}}</a></li>
            @endif
        @elseif($paginator->currentPage() <= ($paginator->lastPage() - 3))
            <li><a href="{{$paginator->url(1)}}">1</a></li>

            <li><span>...</span></li>
            @for($i = $paginator->currentPage() - 1; $i <= ($paginator->currentPage() + 1); $i++)
                <li{!! $paginator->currentPage() == $i ? ' class="active"' : '' !!}><a
                            href="{{$paginator->url($i)}}">{{$i}}</a></li>
            @endfor

            <li><span>...</span></li>

            <li><a href="{{$paginator->url($paginator->lastPage())}}">{{$paginator->lastPage()}}</a></li>
        @else
            <li><a href="{{$paginator->url(1)}}">1</a></li>

            <li><span>...</span></li>
            @for($i = $paginator->lastPage() - 3; $i <= $paginator->lastPage(); $i++)
                <li{!! $paginator->currentPage() == $i ? ' class="active"' : '' !!}><a
                            href="{{$paginator->url($i)}}">{{$i}}</a></li>
            @endfor
        @endif

        @if($paginator->hasMorePages())
            <li><a class="next" href="{{$paginator->nextPageUrl()}}"></a></li>
        @endif
    </ul><!-- /.pagination -->
@endif