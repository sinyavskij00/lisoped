@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content page-article p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{$news->localDescription->title}}</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="article">
                @if($news->image)
                    <div class="article-img">
                        <img src="{{$catalogService->resize($news->image, 390, 440, false)}}"
                             alt="{{$news->localDescription->title}}">
                    </div>
                @endif
                <div class="article-content">
                    <div class="article-info d-flex align-items-center">
                        <div class="date">{{$news->updated_at}}</div>
                        <div class="social__share">
                            <a href="https://plus.google.com/share?url={{url()->current()}}"
                               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="social-gp tooltip" data-tooltip="Поделится">
                                <i class="icon-google-plus2"></i>
                            </a>
                        </div>
                        <div class="social__share">
                            <a href="https://www.facebook.com/sharer.php?u={{url()->current()}}&amp;t={{$news->localDescription->title}}" class="social-fb tooltip" target="_blank" data-tooltip="Поделиться">
                                <i class="icon-facebook2"></i>
                            </a>
                        </div>
                        <div class="views">{{$news->activeReviews->count()}}</div>
                    </div>
                    <div class="article-descr descr">
                        {!! $news->localDescription->text !!}
                    </div>
                </div>
            </div>

            <section class="reviews-section">

                <div class="reviews-section__head">
                    {{__('news.show.comments')}} <span class="reviews-qnty">({{$news->activeReviews->count()}})</span>
                </div>

                <div class="reviews-list">
                    @foreach($reviews as $review)
                        <div class="reviews-list__item">

                            <div class="reviews-item">
                                <div class="reviews-item__reviewer">{{$review->name}}</div>
                                <div class="reviews-item__date">{{$review->updated_at}}</div>
                                <div class="reviews-item__descr">
                                    <p>{{$review->text}}</p>
                                    @if($review->images->count() > 0)
                                        @foreach($review->images as $image)
                                            @if(isset($image->image))
                                            <img src="{{$catalogService->resize($image->image, 100, 100, false)}}">
                                            @endif
                                        @endforeach
                                    @endif
                                    <div class="answer">
                                        <span class="to-answer" data-form="answer"
                                              data-parent-id="{{$review->id}}">{{__('news.show.answer')}}</span>
                                    </div>
                                </div>
                            </div><!-- /.reviews-item -->
                            @if($review->children)
                                @foreach($review->children as $child)
                                    <div class="reviews-item level1 hidden">
                                        <div class="reviews-item__reviewer">{{$child->name}} <span>{{__('news.show.gave_answer')}}</span> {{$review->name}}</div>
                                        <div class="reviews-item__date">{{$child->updated_at}}</div>
                                        <div class="reviews-item__descr">
                                            <p>{{$child->text}}</p>
                                        </div>
                                    </div><!-- /.reviews-item -->
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>
                <!-- /.reviews-list -->

                <form class="reviews-form" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="news_id" value="{{$news->id}}">
                    <div class="formhead">{{__('news.show.form.leave_review')}}</div>
                    <div class="d-flex justify-content-sm-between flex-wrap">
                        <div class="reviews-form__content">
                            <textarea name="text" cols="30" rows="5" required maxlength="3000"
                                      placeholder="{{__('news.show.form.placeholder_comment')}}"></textarea>
                            <label class="reviews-form__file">
                                <input type="file" name="images[]" multiple>
                                <span class="reviews-form__file-name"></span>
                                <span class="reviews-form__file-icon"><i class="icon-attach_file"></i></span>
                            </label>
                        </div>
                        <div class="reviews-form__info">
                            <input type="text" name="name" required placeholder="{{__('news.show.form.placeholder_name')}}">
                            <input type="email" name="email" required placeholder="{{__('news.show.form.placeholder_email')}}">
                            <button type="submit" class="btn"><span>{{__('news.show.form.submit')}}</span></button>
                        </div>
                    </div>
                </form>
            </section>
            <!-- /.reviews-section -->

            <div class="article-nav d-flex align-items-center justify-content-center">
                @if($previous)
                    <a href="{{route('news.show', $previous->slug)}}" class="article-prev">
                        <i class="icon-chevron-thin-left"></i>
                    </a>
                @endif
                <a href="{{route('news.list')}}" class="btn">{{__('news.show.to_news_list')}}</a>
                @if($next)
                    <a href="{{route('news.show', $next->slug)}}" class="article-next">
                        <i class="icon-chevron-thin-right"></i>
                    </a>
                @endif
            </div>
            <!-- /.article-nav -->

        </div><!-- /container -->

        <div class="page-overlay"></div>

    </main>
    <script type="text/javascript">
        $('.reviews-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: `{{route('news.addReview')}}`,
                data: new FormData(this),
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (result) {
                    if (result['status'] && result['status'] === 1) {
                        $('.reviews-form').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                        setTimeout(function () {
                            $('.reviews-form .success').remove();
                            $('.reviews-form')[0].reset();
                        }, 4000);
                    }
                }
            });
        });


        // answer comment form
        $('[data-form="answer"]').on('click', function () {
            var toAnswerBtn = $(this);
            var div = $(this).parent().parent();
            var reviewerName = toAnswerBtn.closest('.reviews-item').find('.reviews-item__reviewer').text();
            var parentCommentId = $(this).attr('data-parent-id');

            var html = "<form class='answer-form' enctype='multipart/form-data'>";
            html += "<div class='answer-form__head'>{{__('news.show.answer')}} <strong>" + reviewerName + "</strong></div>";
            html += "<div class='d-flex justify-content-sm-between flex-wrap'>";
            html += "<div class='answer-form__content'>";
            html += "<textarea maxlength=\"3000\" name='text' cols='30' rows='2' required placeholder='{{__('news.show.placeholder_answer')}}'></textarea>";
            html += "<label class='reviews-form__file'>";
            html += "<input type='file' name='images[]' multiple><span class='reviews-form__file-name'></span><span class='reviews-form__file-icon'><i class='icon-attach_file'></i></span>";
            html += "<input type='hidden' name='_token' value='{{csrf_token()}}'>";
            html += "<input type='hidden' name='news_id' value='{{$news->id}}'>";
            if (parentCommentId) {
                html += "<input type='hidden' name='parent_id' value='" + parentCommentId + "'>";
            }
            html += "</label>";
            html += "</div>";
            html += "<div class='answer-form__info'>";
            html += "<input type='text' name='name' required placeholder='{{__('news.show.placeholder_name')}}'><button type='submit' class='btn butt1'><span>{{__('news.show.answer')}}</span></button><button class='butt2'><span>{{__('news.show.cancel')}}</span></button>";
            html += "</div>";
            html += "</div>";
            html += "</form>";

            var block = $(html);

            block.find(".butt2").click(function () {
                toAnswerBtn.css("display", "block");
                block.remove();
            });

            block.on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    url: `{{route('news.addReview')}}`,
                    data: new FormData(this),
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (result) {
                        if (result['status'] && result['status'] === 1) {
                            $('.answer-form').append('<div class="success active"><div class="s-inner"><span>' + result['message'] + '</span></div></div>');
                            setTimeout(function () {
                                $('.answer-form .success').remove();
                                $('.answer-form')[0].reset();
                            }, 4000);
                        }
                    }
                });
            });

            div.after(block);
            toAnswerBtn.css("display", "none");
        });
    </script>
@endsection
