@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    @include('partials.breadcrumbs', ['breadcrumbs' => Breadcrumbs::generate()])
                </div>
                <div class="col-12">
                    <h1 class="p-title">{{__('news.list.title')}}</h1>
                </div>
            </div>
        </div>


        <div class="blog-items items-animate">
            <div class="container">
                <div class="row">
                    @foreach($newses as $news)
                    <div class="col-lg-3 col-sm-6">
                        <div class="blog-item item-animate">
                            <a href="{{route('news.show', $news->slug)}}" class="blog-item__img">
                                <img src="{{$catalogService->resize($news->image, 270, 305, false)}}" alt="blog-item__title">
                            </a>
                            <h6 class="blog-item__title">
                                <a href="{{route('news.show', $news->slug)}}">{{$news->localDescription->title}}</a>
                            </h6>
                            <a href="{{route('news.show', $news->slug)}}" class="more">
                                <span>{{__('news.list.read_more')}}</span>
                                <i class="i-next"></i>
                            </a>
                        </div>
                    </div><!-- /.blog-item -->
                    @endforeach

                    <div class="col-12">
                        {{$newses->links('front.paginate')}}
                    </div>

                </div>
            </div>
        </div>

        <!-- character -->
        <div class="character-wrapper character-blog character-left" data-time="0">
            <div class="character">
                <div class="character-text">
                    <div class="holder">
                        <div class="character-text__inner">{!! __('character.article.phrase', ['email' => config('app.admin_email')] ) !!}</div>
                        <button class="close"><i class="icon-close"></i></button>
                    </div>
                </div>
                <div class="character-img__holder">
                    <img src="images/charackter/Lisoped_man_2.svg" alt="character" class="character-img">
                </div>
            </div>
        </div>
        <!-- /character -->


        <div class="page-overlay"></div>

    </main>
@endsection
