<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    .table-bordered td {
        border: 1px solid black;
    }

    h1 {
        font-size: 15px;
    }

    .text-center {
        text-align: center;
    }

    .invoice-head {
        font-size: 15px;
    }

    .invoice-products {
        margin-bottom: 40px;
    }
</style>
<div class="invoice-head text-center">
    <h1>Отчет с {{$dateStart}} по {{$dateEnd}}</h1>
    Шапка документа
</div>
<div class="invoice-products">
    <h1 class="text-center">Товары</h1>
    <table class="text-center table-bordered">
        <thead>
        <tr>
            <td>Название</td>
            <td>Артикул</td>
            <td>Количество</td>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>{{$product['name']}}</td>
                <td>{{$product['sku']}}</td>
                <td>{{$product['quantity']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<div class="invoice-information">
    <table>
        <tbody>
        <tr>
            <td style="width: 50%;">
                <table class="table-bordered">
                    <thead>
                    <tr>
                        <td colspan="2"><h1>Поставщик</h1></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Название</td>
                        <td>{{$vendor->name}}</td>
                    </tr>
                    <tr>
                        <td>Телефон 1</td>
                        <td>{{$vendor->telephone_1}}</td>
                    </tr>
                    <tr>
                        <td>Телефон 2</td>
                        <td>{{$vendor->telephone_2}}</td>
                    </tr>
                    <tr>
                        <td>E-mail</td>
                        <td>{{$vendor->email}}</td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td style="width: 50%;">
                <table class="table-bordered">
                    <thead>
                    <tr>
                        <td colspan="2"><h1>Lisoped информация</h1></td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Название</td>
                        <td>Lisoped</td>
                    </tr>
                    <tr>
                        <td>Телефон 1</td>
                        <td>050 999 99 99</td>
                    </tr>
                    <tr>
                        <td>Телефон 2</td>
                        <td>066 654 99 99</td>
                    </tr>
                    <tr>
                        <td>E-mail</td>
                        <td>test@gmail.com</td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>