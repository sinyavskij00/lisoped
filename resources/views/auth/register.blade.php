@extends('front.layout')
@section('content')
    <main id="my-content" class="page-content p-padding">

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="p-title">{{__('auth.register.title')}}</h1>
                </div>
            </div>
        </div>

        <div class="container">

            <form method="POST" class="registration-form personal-area__inputs">
                @csrf
                <div class="registration-form__label">
                    <div class="registration-form__label-head">{{__('auth.register.form.title')}}</div>
                    <div class="registration-form__inputs">
                        <input type="text" name="first_name"
                               placeholder="{{__('auth.register.form.placeholder_first_name')}}" required
                                {{$errors->first('first_name')}}>
                        <input type="text" name="last_name"
                               placeholder="{{__('auth.register.form.placeholder_last_name')}}" required>
                        <input type="email" name="email" placeholder="{{__('auth.register.form.placeholder_email')}}" required>
                        <input type="tel" name="telephone"
                               placeholder="{{__('auth.register.form.placeholder_telephone')}}" required>
                    </div>
                </div>
                <div class="registration-form__label">
                    <div class="registration-form__label-head">{{__('auth.register.form.password_title')}}</div>
                    <div class="registration-form__inputs">
                        <input type="password" name="password"
                               placeholder="{{__('auth.register.form.placeholder_password')}}" required>
                        <input type="password" name="password_confirmation"
                               placeholder="{{__('auth.register.form.placeholder_confirm_password')}}" required>
                    </div>
                </div>
                <div class="btn-wrap">
                    <button type="submit" class="btn"><span>{{__('auth.register.form.btn_submit')}}</span></button>
                </div>
            </form>

        </div><!-- /container -->


        <div class="page-overlay"></div>

    </main>
    <script type="text/javascript">
        $('.registration-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: `{{route('register')}}`,
                type: 'POST',
                data: $(this).serialize(),
                success: function (result) {
                    if (result['status'] && result['status'] === 1) {
                        window.location = `{{route('cabinet.index')}}`;
                        $('.registration-form .text-danger').remove();
                    } else {
                        $('.registration-form .text-danger').remove();

                        var registrationError = '';
                        registrationError += '<ol class="text-danger">';

                        $.each(result,function(key,data) {
                            $.each(data, function(index, value) {
                                registrationError += '<li>' + value + '</li>';
                            });
                        });

                        registrationError += '</ol>';

                        $('.registration-form .btn-wrap').before(registrationError);
                    }
                }
            });
        });
    </script>
@endsection