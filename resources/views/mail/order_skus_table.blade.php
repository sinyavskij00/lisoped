<table class="main table order-description" style="box-sizing: border-box; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
    <thead style="box-sizing: border-box;">
    <tr class="t-a-l" style="box-sizing: border-box; text-align: left;" align="left">
        <th style="box-sizing: border-box; white-space: nowrap; font-size: .9em; border: 1px solid #ccc; padding: 5px;">Фото</th>
        <th style="box-sizing: border-box; white-space: nowrap; font-size: .9em; border: 1px solid #ccc; padding: 5px;">Товар</th>
        <th style="box-sizing: border-box; white-space: nowrap; font-size: .9em; border: 1px solid #ccc; padding: 5px;">Характеристики</th>
        <th style="box-sizing: border-box; white-space: nowrap; font-size: .9em; border: 1px solid #ccc; padding: 5px;">Цена</th>
        <th style="box-sizing: border-box; white-space: nowrap; font-size: .9em; border: 1px solid #ccc; padding: 5px;">К-ство</th>
        <th style="box-sizing: border-box; white-space: nowrap; font-size: .9em; border: 1px solid #ccc; padding: 5px;">Сумма</th>
    </tr>
    </thead>
    <tbody style="box-sizing: border-box;">
    @foreach($orderSkus as $orderSku)
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">
                <a href="{{route('product_path', $orderSku->keepingUnit->product->slug)}}" rel="nofollow" target="_blank" style="box-sizing: border-box; color: #333;">
                    @php
                        $skuImage = $orderSku->keepingUnit->images->first();
                    @endphp
                    @if($skuImage && Storage::disk('public')->exists($skuImage->image))
                        <img src="{{$catalogService->resize($orderSku->keepingUnit->images->first()->image, 90, 60)}}" alt="{{isset($orderSku->product)
                             && isset($orderSku->product->localDescription)
                             ? $orderSku->product->localDescription->name
                             : ''}}" style="box-sizing: border-box; display: block; max-width: 100%; height: auto;">
                    @endif
                </a>
            </td>
            <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">
                <div class="product-name" style="box-sizing: border-box; max-width: 150px;">
                    <a href="{{route('product_path', $orderSku->keepingUnit->product->slug)}}" rel="nofollow" target="_blank" style="box-sizing: border-box; color: #333;">
                        {{isset($orderSku->name) ? $orderSku->name : '---'}}
                    </a>
                </div>
                @if($orderSku->ttn)
                    <div class="ttn" style="box-sizing: border-box; color: #666; font-size: .9em; max-width: 150px; margin-top: 5px;">Номер ТТН Новой Почты: {{ $orderSku->ttn }}</div>
                @endif
            </td>
            <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">
                <table class="attributes" style="box-sizing: border-box; border-collapse: collapse; width: 100%;" width="100%">
                    <tr class="attribute" style="box-sizing: border-box; color: #666; font-size: .9em;">
                        <td class="attribute-name" style="box-sizing: border-box; color: #333; border: none; padding: 0;">Размер:</td>
                        <td class="attribute-value" style="box-sizing: border-box; border: none; padding: 0;">{{$orderSku->size}}</td>
                    </tr>
                    <tr class="attribute" style="box-sizing: border-box; color: #666; font-size: .9em;">
                        <td class="attribute-name" style="box-sizing: border-box; color: #333; border: none; padding: 0;">Цвет:</td>
                        <td class="attribute-value" style="box-sizing: border-box; border: none; padding: 0;">{{$orderSku->color}}</td>
                    </tr>
                    <tr class="attribute" style="box-sizing: border-box; color: #666; font-size: .9em;">
                        <td class="attribute-name" style="box-sizing: border-box; color: #333; border: none; padding: 0;">Год:</td>
                        <td class="attribute-value" style="box-sizing: border-box; border: none; padding: 0;">{{$orderSku->year}}</td>
                    </tr>
                </table>
            </td>
            <td class="price t-a-c" style="box-sizing: border-box; text-align: center; border: 1px solid #ccc; padding: 5px;" align="center"><span style="box-sizing: border-box;">{{$catalogService->format($orderSku->price)}}</span></td>
            <td class="t-a-c qnty" style="box-sizing: border-box; text-align: center; border: 1px solid #ccc; padding: 5px;" align="center"><span style="box-sizing: border-box;">{{$orderSku->quantity}}</span></td>
            <td class="sum t-a-c" style="box-sizing: border-box; text-align: center; border: 1px solid #ccc; padding: 5px;" align="center"><span style="box-sizing: border-box;">{{$catalogService->format($orderSku->price * $orderSku->quantity)}}</span></td>
        </tr>
    @endforeach
    </tbody>
</table>
