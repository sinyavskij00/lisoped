@extends('mail.layout')
@section('content')
    <table class="table">
        <tbody>
        <tr>
            <td><b>У Вас новый заказ, № {{$order->id}}</b></td>
        </tr>
        </tbody>
    </table>

    <table class="main table order-description">
        <tbody>
        <tr>
            <td class="t-a-c">Имя</td>
            <td>{{$order->getFullName()}}</td>
        </tr>
        <tr>
            <td class="t-a-c">Адрес</td>
            <td>{{$order->address}}</td>
        </tr>
        <tr>
            <td class="t-a-c">Телефон</td>
            <td>{{$order->telephone}}</td>
        </tr>
        <tr>
            <td class="t-a-c">E-mail</td>
            <td>{{$order->email}}</td>
        </tr>
        </tbody>
    </table>

    <table class="table">
        <tbody>
        <tr>
            <td><b>Данные о заказе</b></td>
        </tr>
        </tbody>
    </table>
    @if(isset($order->status) && isset($order->status->localDescription))
        <table class="main table order-description">
            <tbody>
            <tr>
                <td>{{__('mails.orders.status_changed.status')}}</td>
                <td>{{ $order->status->localDescription->name }}</td>
            </tr>
            </tbody>
        </table>
    @endif

    @include('mail.order_skus_table', ['orderSkus' => $order->skus])
@endsection