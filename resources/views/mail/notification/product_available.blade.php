@extends('mail.layout)
@section('content')
    <table class="table">
        <tbody>
        <tr>
            <td><b>{{__('mails.preorder.title')}}</b></td>
        </tr>
        </tbody>
    </table>
    <table class="table">
        <tbody>
        <tr>
            <td><a href="{{$link}}">{{$productName}}</a></td>
        </tr>
        </tbody>
    </table>
@endsection