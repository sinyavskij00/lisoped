@extends('mail.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px 0; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;"><td style="box-sizing: border-box; padding: 20px 0 8px;"><h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;"><b style="box-sizing: border-box;">Привет!</b></h6></td></tr>
    </table>

    <table class="main character table" style="box-sizing: border-box; border-collapse: collapse; width: calc(100% - 70px); margin: 6px 35px 0;" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <div class="row row--2" style="box-sizing: border-box;">
                    <div class="col text" style="box-sizing: border-box; display: inline-block; width: 66%; padding-right: 4%; vertical-align: top;">
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">Недавно ты делал у нас заказ. Тебе всё понравилось?</p>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">Мы рады обратной связи - опиши свой опыт, оставь свой отзыв о магазине.</p>
                        <br style="box-sizing: border-box;">
                        <p class="t-a-c" style="box-sizing: border-box; text-align: center; margin: 0 0 1.3em;">
                            <a href="{{asset('testimonials')}}" rel="nofollow" target="_blank" class="btn" style="box-sizing: border-box; padding: 10px 30px; background-color: #11ad62; display: inline-block; text-decoration: none; line-height: 1; color: #fff; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -webkit-transition: all .3s ease; -moz-transition: all .3s ease; -ms-transition: all .3s ease; -o-transition: all .3s ease; transition: all .3s ease;">Рассказать о впечатлениях</a>
                        </p>
                        <br style="box-sizing: border-box;">
                        <br style="box-sizing: border-box;">
                        <p class="f-z-small" style="box-sizing: border-box; font-size: .82em; line-height: 1.55; margin: 0 0 1.3em;">Можешь оставить замечания, предложения и пожелания в письме нашему директору. Его почта:
                            <a href="mailto:{{config('app.admin_email')}}" style="box-sizing: border-box; color: #333;">{{config('app.admin_email')}}</a></p>
                    </div>
                    <div class="col img" style="box-sizing: border-box; display: inline-block; text-align: right; margin: 0 0 0 auto; width: calc(33% - 4px); vertical-align: bottom;">
                        <img src="{{asset('assets/img/letters/character_gotcha.png')}}" alt="Илья" style="box-sizing: border-box; display: block; max-width: 100%; height: auto; margin: 0 0 0 auto;">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection

