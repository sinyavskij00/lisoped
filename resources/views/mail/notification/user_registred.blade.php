@extends('mail.layout')
@section('content')
    <table class="welcome table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px 0; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td class="pdd-b-0 " style="box-sizing: border-box; padding: 20px 0 8px; margin-bottom: 0;">
                @if(isset($name))
                    <h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;">Здравствуй, <span class="accent" style="box-sizing: border-box; color: #11ad62;">{{$name}}!</span></h6>
                @else
                    <h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;">Здравствуй!</h6>
                @endif
            </td>
        </tr>
    </table>

    <table class="main registration table" style="box-sizing: border-box; border-collapse: collapse; width: calc(100% - 70px); margin: 6px 35px 0;" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <div class="row row--2" style="box-sizing: border-box;">
                    <div class="col text" style="box-sizing: border-box; display: inline-block; width: 60%; vertical-align: top;">
                        <h3 style="font-size: 1.2em; margin: 0 0 1.3em; box-sizing: border-box;">Спасибо за регистрацию!</h3>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">Теперь у тебя есть личный кабинет в магазине Lisoped. Это очень удобно: отслеживай статус заказа, сохраняй товары в список желаний, оставляй отзывы.</p>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;"><b style="box-sizing: border-box;">Твои данные для входа:</b></p>
                        <table class="log-pass__table" style="box-sizing: border-box; border-collapse: collapse; width: 100%; margin: 0 0 25px;" width="100%">
                            <tbody style="box-sizing: border-box;">
                            <tr style="box-sizing: border-box;">
                                <td class="accent" style="box-sizing: border-box; color: #11ad62;">Логин:</td>
                                <td style="box-sizing: border-box;">{{$login}}</td>
                            </tr>
                            <tr style="box-sizing: border-box;">
                                <td class="accent" style="box-sizing: border-box; color: #11ad62;">Пароль:</td>
                                <td style="box-sizing: border-box;">{{$password}}</td>
                            </tr>
                            </tbody>
                        </table>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">В нашем <a href="{{asset('news')}}" target="_blank" rel="nofollow" style="box-sizing: border-box; color: #333;">блоге</a> много полезных статей.</p>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">Подписывайся на наши соцсети к сообществу велолюбителей, там иногда разыгрывают велосипеды :)</p>
                        <br style="box-sizing: border-box;">
                        <br style="box-sizing: border-box;">
                    </div>
                    <div class="col img" style="box-sizing: border-box; display: inline-block; width: calc(39% - 4px); vertical-align: bottom; margin: 0 0 0 auto; text-align: right;">
                        <img src="{{asset('assets/img/letters/character_hello.png')}}" alt="Илья" style="box-sizing: border-box; display: block; max-width: 100%; height: auto; margin: 0 0 0 auto;">
                    </div>
                </div>
            </td>
        </tr>
    </table>
@endsection