@extends('mail.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="main character table" style="box-sizing: border-box; border-collapse: collapse; width: calc(100% - 70px); margin: 6px 35px 0;" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <br style="box-sizing: border-box;">
                <div class="row row--2" style="box-sizing: border-box;">
                    <div class="col text" style="box-sizing: border-box; display: inline-block; width: 66%; padding-right: 4%; vertical-align: top;">
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">Здравствуй, <span class="accent" style="box-sizing: border-box; color: #11ad62;">{{$user->first_name . ' ' . $user->last_name}}!</span></p>
                        <p class="m-0" style="box-sizing: border-box; margin: 0;"><b style="box-sizing: border-box;">С Днем Рождения!</b></p>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">Желаем дорог без кочек и верных спутников рядом. Покорения новых вершин и прекрасных горизонтов впереди.</p>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">Мы подготовили тебе подарок :)</p>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">В следующем заказе любой добавленный товар до 100 грн будет твоим подарком*. Просто укажи выбранный подарок в комментарии к заказу или менеджеру при звонке.</p>
                        <p class="f-z-small" style="box-sizing: border-box; font-size: .82em; line-height: 1.55; margin: 0 0 1.3em;">*Под подарком имеется ввиду покупка за 0.01 грн. Подарок можно получить при общей сумме заказа от 300 грн.</p>
                    </div>
                    <div class="col img" style="box-sizing: border-box; display: inline-block; text-align: right; margin: 0 0 0 auto; width: calc(33% - 4px); vertical-align: bottom;">
                        <img src="http://lisoped.ua/assets/img/letters/character_HappyBirthday.png" alt="Илья" style="box-sizing: border-box; display: block; max-width: 100%; height: auto; margin: 0 0 0 auto;">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection
