@extends('mail.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px 0; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;"><td style="box-sizing: border-box; padding: 20px 0 8px;"><h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;"><b style="box-sizing: border-box;">Привет!</b></h6></td></tr>
    </table>

    <table class="main table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <p style="box-sizing: border-box; margin: 0 0 1.3em;">Расскажи другим покупателям о товарах, которые ты купил.</p>
                <p class="m-0" style="box-sizing: border-box; margin: 0;">Оставь отзыв:</p>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="main table table__revies-products" style="box-sizing: border-box; border: 1px solid #ddd; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        @foreach($data->skus as $orderSku)
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; padding: 10px 7px; background-color: #fafafa;" bgcolor="#fafafa">
                    <a href="{{route('product_path', $orderSku->keepingUnit->product->slug)}}" rel="nofollow" target="_blank" style="box-sizing: border-box; color: #333;">
                    @php
                        $skuImage = $orderSku->keepingUnit->images->first();
                    @endphp
                    @if($skuImage && Storage::disk('public')->exists($skuImage->image))
                            <img src="{{$catalogService->resize($orderSku->keepingUnit->images->first()->image, 90, 60)}}" alt="{{isset($orderSku->product)
                             && isset($orderSku->product->localDescription)
                             ? $orderSku->product->localDescription->name
                             : ''}}" style="box-sizing: border-box; display: block; max-width: 100%; height: auto;">
                        @endif
                    </a>
                </td>
                <td style="box-sizing: border-box; padding: 10px 7px; background-color: #fafafa;" bgcolor="#fafafa">
                    <p style="box-sizing: border-box; margin: 0 0 1.3em; margin-bottom: 5px;"><a href="https://lisoped.ua/" rel="nofollow" target="_blank" style="box-sizing: border-box; color: #333;">{{isset($orderSku->name) ? $orderSku->name : '---'}}</a></p>
                    <table class="attributes" style="box-sizing: border-box; border-collapse: collapse; width: 100%; line-height: 1.4;" width="100%">
                        <tr class="attribute" style="box-sizing: border-box; color: #666; font-size: .9em;">
                            <td class="attribute-name" style="box-sizing: border-box; background-color: #fafafa; color: #333;" bgcolor="#fafafa">Размер:</td>
                            <td class="attribute-value" style="box-sizing: border-box; background-color: #fafafa;" bgcolor="#fafafa">{{$orderSku->size}}</td>
                        </tr>
                        <tr class="attribute" style="box-sizing: border-box; color: #666; font-size: .9em;">
                            <td class="attribute-name" style="box-sizing: border-box; background-color: #fafafa; color: #333;" bgcolor="#fafafa">Цвет:</td>
                            <td class="attribute-value" style="box-sizing: border-box; background-color: #fafafa;" bgcolor="#fafafa">{{$orderSku->color}}</td>
                        </tr>
                        <tr class="attribute" style="box-sizing: border-box; color: #666; font-size: .9em;">
                            <td class="attribute-name" style="box-sizing: border-box; background-color: #fafafa; color: #333;" bgcolor="#fafafa">Год:</td>
                            <td class="attribute-value" style="box-sizing: border-box; background-color: #fafafa;" bgcolor="#fafafa">{{$orderSku->year}}</td>
                        </tr>
                    </table>
                </td>
                <td class="t-a-c" style="box-sizing: border-box; text-align: center; padding: 10px 7px; background-color: #fafafa;" align="center" bgcolor="#fafafa">
                    <a href="{{route('product_path', $orderSku->keepingUnit->product->slug)}}" rel="nofollow" target="_blank" class="btn" style="box-sizing: border-box; background-color: #11ad62; display: inline-block; text-decoration: none; line-height: 1; color: #fff; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -webkit-transition: all .3s ease; -moz-transition: all .3s ease; -ms-transition: all .3s ease; -o-transition: all .3s ease; transition: all .3s ease; padding: 9px 20px;">
                        <span style="box-sizing: border-box;">Оставить отзыв</span>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <table class="table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <p class="m-t" style="box-sizing: border-box; margin: 0 0 1.3em; margin-top: 1.3em;">Спасибо!</p>
            </td>
        </tr>
        </tbody>
    </table>
    <!-- Main == end ==============================================-->
@endsection


