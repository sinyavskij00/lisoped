@extends('mail.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px 0; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;"><td style="box-sizing: border-box; padding: 20px 0 8px;"><h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;">Здравствуй, <span class="accent" style="box-sizing: border-box; color: #11ad62;">{{$name}}!</span></h6></td></tr>
    </table>

    <table class="main table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <p style="box-sizing: border-box; margin: 0 0 1.3em;">Твой <a href="{{$link}}" rel="nofollow" target="_blank" style="box-sizing: border-box; color: #333;">отзыв</a> опубликован.</p>
                <div class="review" style="box-sizing: border-box; padding: 15px 20px; border: 1px solid #ccc; border-radius: 10px; margin-bottom: 1.3em; font-style: italic; background: #fafafa; text-align: center;">
                    <span class="line" style="box-sizing: border-box;"></span>
                    <p class="m-0" style="box-sizing: border-box; margin: 0;">"{{$text}}"</p>
                </div>
                <p style="box-sizing: border-box; margin: 0 0 1.3em;">Спасибо за обратную связь!</p>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection
