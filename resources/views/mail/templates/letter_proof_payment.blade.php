@extends('mail.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px 0; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;"><td style="box-sizing: border-box; padding: 20px 0 8px;"><h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;">Здравствуй, <span class="accent" style="box-sizing: border-box; color: #11ad62;">{{$orderHistory->order->first_name . ' ' . $orderHistory->order->last_name}}!</span></h6></td></tr>
    </table>

    <table class="main character table" style="box-sizing: border-box; border-collapse: collapse; width: calc(100% - 70px); margin: 6px 35px 0;" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <div class="row row--2" style="box-sizing: border-box;">
                    <div class="col text" style="box-sizing: border-box; display: inline-block; width: 66%; padding-right: 4%; vertical-align: top; padding-top: 7px;">
                        <h3 style="font-size: 1.2em; margin: 0 0 1.3em; box-sizing: border-box;">Оплата получена. Заказ <b class="accent" style="box-sizing: border-box; color: #11ad62;">№{{$publicOrderId}}</b></h3>
                        <p class="m-0" style="box-sizing: border-box; margin: 0;">Мы получили оплату по твоему заказу.</p>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">Отправим посылку в течении 1-3 рабочих дней.</p>
                        <br style="box-sizing: border-box;">
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">Присоединяйся к нам на <a href="{{!empty($linkFacebook) ? $linkFacebook : 'https://facebook.com'}}" target="_blank" rel="nofollow" style="box-sizing: border-box; color: #333;">Facebook</a> :)</p>
                    </div>
                    <div class="col img" style="box-sizing: border-box; display: inline-block; text-align: right; margin: 0 0 0 auto; width: calc(33% - 4px); vertical-align: bottom;">
                        <img src="{{asset('assets/img/letters/character_ok.png')}}" alt="Илья" style="box-sizing: border-box; display: block; max-width: 100%; height: auto; margin: 0 0 0 auto;">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection
