@extends('mail.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px 0; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;"><td style="box-sizing: border-box; padding: 20px 0 8px;"><h6 style="font-size: 1em; margin: 0 0 1.3em; box-sizing: border-box; margin-bottom: 0; font-weight: 600;">Здравствуй, <span class="accent" style="box-sizing: border-box; color: #11ad62;">{{$orderHistory->order->first_name . ' ' . $orderHistory->order->last_name}}!</span></h6></td></tr>
    </table>

    <table class="main character table" style="box-sizing: border-box; border-collapse: collapse; width: calc(100% - 70px); margin: 6px 35px 0;" width="calc(100% - 70)">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;">
                <div class="row row--2" style="box-sizing: border-box;">
                    <div class="col text" style="box-sizing: border-box; display: inline-block; width: 66%; padding-right: 4%; vertical-align: top;">
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">Ты выбрал оплату заказа на расчетный счет.</p>
                        <p class="m-0" style="box-sizing: border-box; margin: 0;">Сумма коплате: <b style="box-sizing: border-box;">5678 грн</b></p>
                        <p class="m-0" style="box-sizing: border-box; margin: 0;">р/с 11112222333344 в ПАО ПриватБанк МФО 777777</p>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;"><b style="box-sizing: border-box;">ФОП Костылев П.В. ЕДРПОУ 555555</b></p>
                        <br style="box-sizing: border-box;">

                        <p class="f-z-small" style="box-sizing: border-box; font-size: .82em; line-height: 1.55; margin: 0 0 1.3em;">Оплатить можно в приложении Приват24 или в терминале самообслуживания ПриватБанк. Это самый простой способ, особенно если ты владелец карты ПриватБанк.</p>
                        <p class="f-z-small" style="box-sizing: border-box; font-size: .82em; line-height: 1.55; margin: 0 0 1.3em;">Также перевод можно осуществить в кассе любого банка или других платежных онлайн - сервисах (например: Portmone, LigPay или онлайн - банкинг другого банка).</p>
                        <p class="f-z-small" style="box-sizing: border-box; font-size: .82em; line-height: 1.55; margin: 0 0 1.3em;">Обрати внимание, платежный сервис может взимать комиссию, её оплачивает покупатель.</p>
                        <p style="box-sizing: border-box; margin: 0 0 1.3em;">А мы в это время будем упаковывать твою посылку.</p>
                        <br style="box-sizing: border-box;">
                    </div>
                    <div class="col img" style="box-sizing: border-box; display: inline-block; text-align: right; margin: 0 0 0 auto; width: calc(33% - 4px); vertical-align: bottom;">
                        <img src="http://lisoped.ua/assets/img/letters/character_with_tablet.png" alt="Илья" style="box-sizing: border-box; display: block; max-width: 100%; height: auto; margin: 0 0 0 auto;">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection
