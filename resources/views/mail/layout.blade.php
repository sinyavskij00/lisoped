<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name')}}</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=cyrillic" rel="stylesheet">
    <style type="text/css">
        .content,.content__wrapper,.letter table{border-collapse:collapse}.categories,.categories li,.socials,.socials li{list-style-type:none}.order-description th,.table__revies-products .price{white-space:nowrap}.letter *{box-sizing:border-box}.letter{background:#d6e3ec;color:#333;font:600 13px/18px 'Open Sans',Helvetica,Arial,Verdana,sans-serif;padding:50px 15px}.letter img{display:block;max-width:100%;height:auto}.categories a,.categories li,.socials a,.socials li{display:inline-block}.letter table{width:100%}.letter .table{margin:6px 35px;width:calc(100% - 70px)}.letter .table.character,.letter .table.registration,.letter .welcome{margin:6px 35px 0}.letter .header{background:#001421;color:#11ad62;width:100%}.letter .header__content td{padding:11px 35px 10px}.letter .welcome{width:calc(100% - 70px)}.letter .footer__content{margin:35px 35px 25px;width:calc(100% - 70px);font-size:.93em}.letter a{color:#333}.letter p{margin:0 0 1.3em}.letter .m-0{margin:0}.pdd-b-0{margin-bottom:0!important}.letter .log-pass__table{margin:0 0 25px}.letter .m-b{margin:0 0 1.3em}.letter .m-b-xs{margin:0 0 .9em}.letter .m-t{margin-top:1.3em}.letter .m-auto{margin-left:auto;margin-right:auto}.content__wrapper{max-width:650px;margin:0 auto}.content{width:100%;background:#fff}.v-a-t{vertical-align:top}.categories a,.v-a-m{vertical-align:middle}.t-a-c{text-align:center}.t-a-l{text-align:left}.categories,.character .row--2 .img,.phones,.t-a-r,.thank{text-align:right}.t-a-r img{margin:0 0 0 auto}.pdd-0{padding:0}.f-s-i{font-style:italic}.f-w-n{font-weight:400}.accent{color:#11ad62}.attribute,.ttn{color:#666}.f-z-small{font-size:.82em;line-height:1.55}address{margin:0;font-style:normal}.categories{padding:0;margin:0}.categories li{margin:0 2px}.categories a{padding:3px 7px}.categories a img{width:auto;max-height:36px}h1,h2,h3{font-size:1.2em;margin:0 0 1.3em}h4,h5,h6{font-size:1em;margin:0 0 1.3em}.attribute,.order-description,.order-description th,.table__bordered,.ttn{font-size:.9em}.welcome td{padding:20px 0 8px}.welcome h6{margin-bottom:0;font-weight:600}.footer{background:#f6f6f6}.footer table{width:100%}.footer__content td,.footer__content>div{padding:0 15px 10px}.socials{margin:12px 0 0;padding:0;line-height:1}.socials li{line-height:1}.socials a{width:25px;height:25px;margin:0 2px}.socials img{display:block;max-width:100%;height:auto}.row--2 .img,.row--2 .text{display:inline-block;vertical-align:middle}.phones p{margin:0}.character .row--2 .img,.character .row--2 .img img,.registration .row--2 .img img{margin:0 0 0 auto}.row--2 .text{width:60%}.row--2 .img{width:35%}.registration .row--2 .text{width:60%;vertical-align:top}.registration .row--2 .img{width:calc(39% - 4px);vertical-align:bottom;margin:0 0 0 auto;text-align:right}.character .row--2 .text{width:66%;padding-right:4%;vertical-align:top}.order-description td,.order-description th,.table__bordered td,.table__bordered th{border:1px solid #ccc;padding:5px}.character .row--2 .img{width:calc(33% - 4px);vertical-align:bottom}.thank{max-width:190px}.thank span{display:block}.order-description .attribute td{border:none;padding:0}.order__description{padding-top:10px}.order__description p{margin:0 0 5px}.order__description p:last-child{margin-bottom:2.5em}.product-name,.ttn{max-width:150px}.ttn{margin-top:5px}.attribute .attribute-name{color:#333}.btn,.letter .btn{padding:10px 30px;background-color:#11ad62;display:inline-block;text-decoration:none;line-height:1;color:#fff;-webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-ms-transition:all .3s ease;-o-transition:all .3s ease;transition:all .3s ease}.btn:hover,.letter .btn:hover{background-color:#00904b}.table__revies-products{border:1px solid #ddd;font-size:.9em}.letter .table__revies-products .btn{padding:9px 20px}.table__revies-products p{margin-bottom:5px}.table__revies-products .attributes{line-height:1.4}.table__revies-products>tbody>tr>td{padding:10px 7px}.table__revies-products>tbody>tr:nth-child(odd) td{background-color:#fafafa}.review{padding:15px 20px;border:1px solid #ccc;border-radius:10px;margin-bottom:1.3em;font-style:italic;background:#fafafa;text-align:center}.pdd-l{padding-left:2em}@media (max-width:620px){.letter .footer__content,.letter .table,.letter .welcome{width:calc(100% - 40px)!important}.letter .header__content td{padding:11px 10px 10px!important}.categories li{margin:0!important}.categories a{padding:3px!important}.categories a img{max-height:20px!important}.letter-logo img{max-width:85px!important}.letter .table{margin:11px 20px!important}.letter .table.character,.letter .table.registration{margin:10px 20px 0!important;width:calc(100% - 40px)!important}.letter .welcome{margin:11px 20px 0!important}.letter .footer__content{margin:30px 20px 15px!important}.character .row--2 .img,.character .row--2 .text,.registration .row--2 .text,.row--2 .img,.row--2 .text{width:100%!important}.address{max-width:140px!important}.footer__content td,.footer__content>div{padding:0 0 10px!important}.registration .row--2 .img,.row--2 .img{width:100%!important;text-align:center!important;margin:25px auto 0!important}.registration .row--2 .img img{margin:0 auto!important;max-width:180px!important}.character .row--2 .img,.row--2 .img{text-align:center!important;margin:25px auto 0!important}.character .row--2 .img img{margin:0 auto!important;max-width:180px!important}.thank{max-width:150px!important;text-align:left!important}.thank span{display:inline!important}.order-description{font-size:.6em!important}.table__revies-products{font-size:.8em!important}.table__revies-products .btn{padding:7px 10px!important}.table__revies-products .name{line-height:1.4!important;font-size:.9em!important}.order__description p:last-child{margin-bottom:2em!important}.socials a{width:18px!important;height:18px!important;margin:0 1px!important}}
    </style>
</head>
<body>

<div class="letter" style="background: #d6e3ec; color: #333; font: 600 13px/18px 'Open Sans',Helvetica,Arial,Verdana,sans-serif; padding: 50px 15px;">
    <table class="content__wrapper" style="box-sizing: border-box; max-width: 650px; margin: 0 auto; border-collapse: collapse; width: 100%;" width="100%">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td class="pdd-0" style="box-sizing: border-box; padding: 0;">
                <table class="content" style="box-sizing: border-box; background: #fff; border-collapse: collapse; width: 100%;" width="100%">
                    <tbody style="box-sizing: border-box;">
                    <tr style="box-sizing: border-box;">
                        <td class="pdd-0" style="box-sizing: border-box; padding: 0;">

                            <table class="header" style="box-sizing: border-box; border-collapse: collapse; background: #001421; color: #11ad62; width: 100%;" width="100%">
                                <tr style="box-sizing: border-box;">
                                    <td style="box-sizing: border-box;">
                                        <table class="header__content" style="box-sizing: border-box; border-collapse: collapse; width: 100%;" width="100%">
                                            <tr style="box-sizing: border-box;">
                                                <td style="box-sizing: border-box; padding: 11px 35px 10px;">
                                                    <a href="{{asset('/')}}" rel="nofollow" target="_blank" class="letter-logo" style="box-sizing: border-box; color: #333;">
                                                        <img src="{{asset('assets/img/letters/lisoped_logo.png')}}" alt="Интернет-магазин {{config('app.name')}}" title="Интернет-магазин {{config('app.name')}}" style="box-sizing: border-box; display: block; max-width: 100%; height: auto;">
                                                    </a>
                                                </td>
                                                @if(!empty($categoryTree) && $categoryTree->count() > 0)
                                                    <td class="t-a-r" style="box-sizing: border-box; text-align: right; padding: 11px 35px 10px;" align="right">
                                                        <ul class="categories" style="list-style-type: none; box-sizing: border-box; text-align: right; padding: 0; margin: 0;">
                                                            @foreach($categoryTree as $mainCategory)
                                                                <li style="box-sizing: border-box; list-style-type: none; display: inline-block; margin: 0 2px;">
                                                                    <a href="{{route('category_path', category_path($mainCategory->id))}}" rel="nofollow" target="_blank" style="box-sizing: border-box; display: inline-block; color: #333; vertical-align: middle; padding: 3px 7px;">
                                                                        @if(Storage::disk('public')->exists($mainCategory->icon))
                                                                            <img src="{{Storage::disk('public')->url($mainCategory->icon)}}" alt="{{isset($mainCategory->localDescription) ? $mainCategory->localDescription->name : ''}}" title="{{isset($mainCategory->localDescription) ? $mainCategory->localDescription->name : ''}}" style="box-sizing: border-box; display: block; max-width: 100%; height: auto; margin: 0 0 0 auto; width: auto; max-height: 36px;">
                                                                        @endif
                                                                    </a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </td>
                                                @endif
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            @yield('content')

                            <table class="footer" style="box-sizing: border-box; background: #f6f6f6; border-collapse: collapse; width: 100%;" width="100%">
                                <tr style="box-sizing: border-box;">
                                    <td style="box-sizing: border-box;">
                                        <table class="footer__content" style="box-sizing: border-box; border-collapse: collapse; margin: 35px 35px 25px; width: calc(100% - 70px); font-size: .93em;" width="calc(100% - 70)">
                                            <tr class="v-a-t" style="box-sizing: border-box; vertical-align: top;" valign="top">
                                                <td style="box-sizing: border-box; padding: 0 15px 10px;">
                                                    <div class="address" style="box-sizing: border-box;">
                                                        <address style="margin: 0; font-style: normal; box-sizing: border-box;">{{__('common.footer.address1')}}</address>
                                                        <address style="margin: 0; font-style: normal; box-sizing: border-box;">{{__('common.footer.address2')}}</address>
                                                    </div>
                                                    <ul class="socials" style="list-style-type: none; box-sizing: border-box; margin: 12px 0 0; padding: 0; line-height: 1;">
                                                        @if(!empty($linkInstagram))
                                                            <li style="margin-left: 0; box-sizing: border-box; list-style-type: none; display: inline-block; line-height: 1;"><a href="{{$linkInstagram}}" rel="nofollow" title="instagram" target="_blank" class="instagram" style="box-sizing: border-box; display: inline-block; color: #333; width: 25px; height: 25px; margin: 0 2px;"><img src="http://lisoped.ua/assets/img/letters/icon_instagram.png" alt="instagram" style="box-sizing: border-box; display: block; max-width: 100%; height: auto;"></a></li>
                                                        @endif
                                                        @if(!empty($linkFacebook))
                                                            <li style="margin-left: 0; box-sizing: border-box; list-style-type: none; display: inline-block; line-height: 1;"><a href="{{$linkFacebook}}" rel="nofollow" title="fasebook" target="_blank" class="facebook" style="box-sizing: border-box; display: inline-block; color: #333; width: 25px; height: 25px; margin: 0 2px;"><img src="http://lisoped.ua/assets/img/letters/icon_facebook.png" alt="facebook" style="box-sizing: border-box; display: block; max-width: 100%; height: auto;"></a></li>
                                                        @endif
                                                        <li style="margin-left: 0; box-sizing: border-box; list-style-type: none; display: inline-block; line-height: 1;"><a href="https://youtube.com" rel="nofollow" title="youtube" target="_blank" class="youtube" style="box-sizing: border-box; display: inline-block; color: #333; width: 25px; height: 25px; margin: 0 2px;"><img src="http://lisoped.ua/assets/img/letters/icon_youtube.png" alt="youtube" style="box-sizing: border-box; display: block; max-width: 100%; height: auto;"></a></li>

                                                        <li style="margin-left: 0; box-sizing: border-box; list-style-type: none; display: inline-block; line-height: 1;"><a href="viber://chat?number=+380676206611" rel="nofollow" title="viber" target="_blank" class="viber" style="box-sizing: border-box; display: inline-block; color: #333; width: 25px; height: 25px; margin: 0 2px;"><img src="http://lisoped.ua/assets/img/letters/icon_viber.png" alt="viber" style="box-sizing: border-box; display: block; max-width: 100%; height: auto;"></a></li>

                                                        <li style="margin-left: 0; box-sizing: border-box; list-style-type: none; display: inline-block; line-height: 1;"><a href="https://wa.me/380676206611" rel="nofollow" title="whatsapp" target="_blank" class="whatsapp" style="box-sizing: border-box; display: inline-block; color: #333; width: 25px; height: 25px; margin: 0 2px;"><img src="http://lisoped.ua/assets/img/letters/icon_whatsapp.png" alt="whatsapp" style="box-sizing: border-box; display: block; max-width: 100%; height: auto;"></a></li>

                                                        <li style="margin-left: 0; box-sizing: border-box; list-style-type: none; display: inline-block; line-height: 1;"><a href="https://t.me/Lisoped" rel="nofollow" title="telegram" target="_blank" class="telegram" style="box-sizing: border-box; display: inline-block; color: #333; width: 25px; height: 25px; margin: 0 2px;"><img src="http://lisoped.ua/assets/img/letters/icon_telegram.png" alt="telegram" style="box-sizing: border-box; display: block; max-width: 100%; height: auto;"></a></li>
                                                    </ul>
                                                </td>
                                                @if(!empty($siteTelephones))
                                                    <td style="box-sizing: border-box; padding: 0 15px 10px;">
                                                        <div class="phones" style="box-sizing: border-box; text-align: right;">
                                                            @foreach($siteTelephones as $telephone)
                                                                <p style="box-sizing: border-box; margin: 0;">{{$telephone}}</p>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>