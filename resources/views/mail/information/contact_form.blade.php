@extends('mail.layout')
@section('content')
    <table class="table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;"><b style="box-sizing: border-box;">{{__('mails.contacts.title')}}</b></td>
        </tr>
        </tbody>
    </table>
    <table class="main table order-description" style="box-sizing: border-box; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        @if(!empty($name))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.contacts.name')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$name}}</td>
            </tr>
        @endif
        @if($email)
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.contacts.email')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$email}}</td>
            </tr>
        @endif
        @if($text)
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.contacts.message')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$text}}</td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
