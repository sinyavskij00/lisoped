@extends('mail.layout')
@section('content')
    <table class="table main order-description" style="box-sizing: border-box; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        @if(!empty($name))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.specials.name')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$name}}</td>
            </tr>
        @endif
        @if(!empty($email))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.specials.email')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$email}}</td>
            </tr>
        @endif
        @if(!empty($text))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.specials.question')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$text}}</td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
