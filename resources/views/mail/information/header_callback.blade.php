@extends('mail.layout')
@section('content')
    <table class="table" style="box-sizing: border-box; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        <tr style="box-sizing: border-box;">
            <td style="box-sizing: border-box;"><b style="box-sizing: border-box;">{{__('mails.header_callback.title')}}</b></td>
        </tr>
        </tbody>
    </table>

    <table class="main table order-description" style="box-sizing: border-box; font-size: .9em; border-collapse: collapse; margin: 6px 35px; width: calc(100% - 70px);" width="calc(100% - 70)">
        <tbody style="box-sizing: border-box;">
        @if(!empty($name))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.header_callback.name')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$name}}</td>
            </tr>
        @endif
        @if(!empty($phone))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.header_callback.telephone')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$phone}}</td>
            </tr>
        @endif
        @if(!empty($day))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.header_callback.date')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$day}}</td>
            </tr>
        @endif
        @if(!empty($hour))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.header_callback.hours')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$hour}}</td>
            </tr>
        @endif
        @if(!empty($minute))
            <tr style="box-sizing: border-box;">
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{__('mails.header_callback.minutes')}}</td>
                <td style="box-sizing: border-box; border: 1px solid #ccc; padding: 5px;">{{$minute}}</td>
            </tr>
        @endif
        </tbody>
    </table>
@endsection
