@if(isset($breadcrumbs) && count($breadcrumbs))
    <ul class="breadcrumbs ul d-flex align-items-center flex-wrap">
        @foreach($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$loop->last)
                <li itemprop="itemListElement"><a href="{{$breadcrumb->url}}">{{$breadcrumb->title}}</a></li>
            @else
                <li><span class="current" itemprop="item">{{$breadcrumb->title}}</span></li>
            @endif
        @endforeach
    </ul>
    @php
        $microData = [
            '@context' => 'http://schema.org',
            '@type' => 'BreadcrumbList',
            'itemListElement' => []
        ];
        foreach ($breadcrumbs as $key => $breadcrumb) {
            $microData['itemListElement'][] = [
            '@type' => 'ListItem',
            'position' => $key,
            'item' => [
                '@id' => $breadcrumb->url,
                'name' => $breadcrumb->title
            ]
            ];
        }
    @endphp
    @if(!empty($microData))
        <script type="application/ld+json">
            @json($microData)
        </script>
    @endif
@endif