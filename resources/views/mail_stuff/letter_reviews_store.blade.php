@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table">
        <tr><td><h6><b>Привет!</b></h6></td></tr>
    </table>

    <table class="main character table">
        <tr>
            <td>
                <div class="row row--2">
                    <div class="col text">
                        <p>Недавно ты делал у нас заказ. Тебе всё понравилось?</p>
                        <p>Мы рады обратной связи - опиши свой опыт, оставь свой отзыв о магазине.</p>
                        <br>
                        <p class="t-a-c"><a href="https://lisoped.ua/testimonials" rel="nofollow" target="_blank" class="btn">Рассказать о впечатлениях</a></p>
                        <br>
                        <br>
                        <p class="f-z-small">Можешь оставить замечания, предложения и пожелания в письме нашему директору. Его почта:
                            <a href="mailto:director@lisoped.ua">director@lisoped.ua</a></p>
                    </div>
                    <div class="col img">
                        <img src="http://lisoped.ua/assets/img/letters/character_gotcha.png" alt="Илья">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection