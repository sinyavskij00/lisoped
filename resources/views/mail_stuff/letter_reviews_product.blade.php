@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table">
        <tr><td><h6><b>Привет!</b></h6></td></tr>
    </table>

    <table class="main table">
        <tbody>
        <tr>
            <td>
                <p>Расскажи другим покупателям о товарах, которые ты купил.</p>
                <p class="m-0">Оставь отзыв:</p>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="main table table__revies-products">
        <tbody>
        <tr>
            <td>
                <a href="https://lisoped.ua/" rel="nofollow" target="_blank">
                    <img src="https://lisoped.ua/storage/cache/shop_images/import/26-Formula-HUMMER-DD-cherno-zelenyiy-2018-1361-1980x1360-90-60.jpg" alt="Romet Orkan 3.0M">
                </a>
            </td>
            <td>
                <p><a href="https://lisoped.ua/" rel="nofollow" target="_blank">Romet Orkan 3.0M</a></p>
                <table class="attributes">
                    <tr class="attribute">
                        <td class="attribute-name">Размер:</td>
                        <td class="attribute-value">S</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Цвет:</td>
                        <td class="attribute-value">черный</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Год:</td>
                        <td class="attribute-value">2018</td>
                    </tr>
                </table>
            </td>
            <td class="t-a-c"><a href="https://lisoped.ua/" rel="nofollow" target="_blank" class="btn"><span>Оставить отзыв</span></a></td>
        </tr>
        <tr>
            <td>
                <a href="https://lisoped.ua/" rel="nofollow" target="_blank">
                    <img src="https://lisoped.ua/storage/cache/shop_images/import/26-Formula-HUMMER-DD-cherno-zelenyiy-2018-1361-1980x1360-90-60.jpg" alt="Romet Orkan 3.0M">
                </a>
            </td>
            <td>
                <p><a href="https://lisoped.ua/" rel="nofollow" target="_blank">Romet Orkan 3.0M</a></p>
                <table class="attributes">
                    <tr class="attribute">
                        <td class="attribute-name">Размер:</td>
                        <td class="attribute-value">S</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Цвет:</td>
                        <td class="attribute-value">черный</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Год:</td>
                        <td class="attribute-value">2018</td>
                    </tr>
                </table>
            </td>
            <td class="t-a-c"><a href="https://lisoped.ua/" rel="nofollow" target="_blank" class="btn"><span>Оставить отзыв</span></a></td>
        </tr>
        <tr>
            <td>
                <a href="https://lisoped.ua/" rel="nofollow" target="_blank">
                    <img src="https://lisoped.ua/storage/cache/shop_images/import/26-Formula-HUMMER-DD-cherno-zelenyiy-2018-1361-1980x1360-90-60.jpg" alt="Romet Orkan 3.0M">
                </a>
            </td>
            <td>
                <p><a href="https://lisoped.ua/" rel="nofollow" target="_blank">Romet Orkan 3.0M</a></p>
                <table class="attributes">
                    <tr class="attribute">
                        <td class="attribute-name">Размер:</td>
                        <td class="attribute-value">S</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Цвет:</td>
                        <td class="attribute-value">черный</td>
                    </tr>
                    <tr class="attribute">
                        <td class="attribute-name">Год:</td>
                        <td class="attribute-value">2018</td>
                    </tr>
                </table>
            </td>
            <td class="t-a-c"><a href="https://lisoped.ua/" rel="nofollow" target="_blank" class="btn"><span>Оставить отзыв</span></a></td>
        </tr>
        </tbody>
    </table>

    <table class="table">
        <tbody>
        <tr>
            <td>
                <p class="m-t">Спасибо!</p>
            </td>
        </tr>
        </tbody>
    </table>
    <!-- Main == end ==============================================-->
@endsection