@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table">
        <tr><td><h6>Здравствуй, <span class="accent">Артур Титов!</span></h6></td></tr>
    </table>

    <table class="main table">
        <tbody>
        <tr>
            <td>
                <p>В корзине есть забытый товар. Если тебе нужна консультация, позвони нам или закажи обратный звонок на <a href="https://lisoped.ua/" rel="nofollow" target="_blank">сайте</a>.</p>
            </td>
        </tr>
        </tbody>
    </table>

    <table class="main table table__bordered">
        <tbody>
        <tr class="t-a-c">
            <td>
                <a href="https://lisoped.ua/" rel="nofollow" target="_blank">
                    <img class="m-auto" src="https://lisoped.ua/storage/cache/shop_images/import/26-Formula-HUMMER-DD-cherno-zelenyiy-2018-1361-1980x1360-90-60.jpg" alt="Romet Orkan 3.0M">
                </a>
            </td>
            <td>
                <a href="https://lisoped.ua/" rel="nofollow" target="_blank">Romet Orkan 3.0M</a>
            </td>
            <td class="price t-a-c"><span>11 000 грн</span></td>
        </tr>
        <tr class="t-a-c">
            <td>
                <a href="https://lisoped.ua/" rel="nofollow" target="_blank">
                    <img class="m-auto" src="https://lisoped.ua/storage/cache/shop_images/import/26-Formula-HUMMER-DD-cherno-zelenyiy-2018-1361-1980x1360-90-60.jpg" alt="Romet Orkan 3.0M">
                </a>
            </td>
            <td>
                <a href="https://lisoped.ua/" rel="nofollow" target="_blank">Romet Orkan 3.0M</a>
            </td>
            <td class="price"><span>11 000 грн</span></td>
        </tr>
        </tbody>
    </table>
    <br>
    <!-- Main == end ==============================================-->
@endsection