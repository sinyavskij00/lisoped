@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table">
        <tr><td><h6>Здравствуй, <span class="accent">Артур Титов!</span></h6></td></tr>
    </table>

    <table class="main character table">
        <tr>
            <td>
                <div class="row row--2">
                    <div class="col text">
                        <p class="m-0">Ты выбрал оплату заказа переводом на карту.</p>
                        <p>Сумма коплате: <b>5678 грн</b></p>
                        <br>
                        <p class="m-b-xs">Номер карты ПриватБанк:</p>
                        <p class="m-0 pdd-l"><b>2222 3333 4444 5555</b></p>
                        <p class="pdd-l"><b>Костылев П.В.</b></p>
                        <br>
                        <p class="f-z-small">Оплатить можно в приложении Приват24 или в терминале самообслуживания ПриватБанк. Это самый простой способ, особенно если ты владелец карты ПриватБанк.</p>
                        <p class="f-z-small">Также перевод можно осуществить в кассе любого банка или других платежных онлайн - сервисах (например: Portmone, LigPay или онлайн - банкинг другого банка).</p>
                        <p class="f-z-small">Обрати внимание, платежный сервис может взимать комиссию, её оплачивает покупатель.</p>
                        <p>А мы в это время будем упаковывать твою посылку.</p>
                        <br>
                    </div>
                    <div class="col img">
                        <img src="http://lisoped.ua/assets/img/letters/character_with_tablet.png" alt="Илья">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection