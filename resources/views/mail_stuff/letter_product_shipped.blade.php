@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table">
        <tr><td><h6>Здравствуй, <span class="accent">Артур Титов!</span></h6></td></tr>
    </table>

    <table class="main character table">
        <tr>
            <td>
                <div class="row row--2">
                    <div class="col text">
                        <p>Готовся получить свою посылку.</p>
                        <p class="m-0" style="padding-bottom: 4px;">Номер декларации Новой Почты для отслеживания:</p>
                        <p class="m-0"><b>12345678901234</b></p>
                        <p><b>12345678901234</b></p>
                        <p>Всего посылок в заказе: <b>3</b></p>
                        <p>Не все посылки отправляются одновременно, мы посылаем номера деклараций по мере отправки.</p>
                    </div>
                    <div class="col img">
                        <img src="http://lisoped.ua/assets/img/letters/character_Nova_Poshta.png" alt="Илья">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection