@extends('mail_stuff.layout')
@section('content')
    <!-- Main == start ==============================================-->
    <table class="welcome table">
        <tr><td><h6>Здравствуй, <span class="accent">Артур Титов!</span></h6></td></tr>
    </table>

    <table class="main character table">
        <tr>
            <td>
                <div class="row row--2">
                    <div class="col text" style="padding-top: 7px;">
                        <h3>Оплата получена. Заказ <b class="accent">№12345678901234</b></h3>
                        <p class="m-0">Мы получили оплату по твоему заказу.</p>
                        <p>Отправим посылку в течении 1-3 рабочих дней.</p>
                        <br>
                        <p>Присоединяйся к нам на <a href="https://facebook.com" target="_blank" rel="nofollow">Facebook</a> :)</p>
                    </div>
                    <div class="col img">
                        <img src="http://lisoped.ua/assets/img/letters/character_ok.png" alt="Илья">
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <!-- Main == end ==============================================-->
@endsection