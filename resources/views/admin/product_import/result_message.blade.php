<div style="background-color: #fff; padding: 5px;">
    <span style="color: darkgreen; font-weight: 800;">
        Обработанно успешно (<span style="font-size: 15px">{{$processed['success']['count']}}</span>)
    </span>
    <br>
    <span style="color: darkred; font-weight: 800;">
        C ошибкой (<span style="font-size: 15px">{{$processed['error']['count']}}</span>)
    </span>
</div>
@if($processed['error']['count'] > 0)
    <div style="height: 40px; margin-top: 10px;">
        <a href="" class="btn btn-warning pull-right">Скачать отчет</a>
    </div>
@endif
@if(isset($processed['unique']['count']) && $processed['unique']['count'] > 0)
    <div style="height: 40px; margin-top: 10px;">
        @if (isset($uniqueArray['full_unique']))
            <a class="btn btn-success pull-right" target="_blank"
               href="{{ route("product.download") }}?name=full_unique">Скачать отчет уникальности ({{ count($uniqueArray['full_unique']) }})</a>
        @endif
        @if (isset($uniqueArray['almost_unique']))
            <a class="btn btn-warning pull-right" target="_blank"
               href="{{ route("product.download") }}?name=almost_unique">Скачать отчет полу-уникальности ({{ count($uniqueArray['almost_unique']) }})</a>
        @endif
        @if (isset($uniqueArray['none_unique']))
            <a class="btn btn-danger pull-right" target="_blank" href="{{ route("product.download") }}?name=none_unique">Скачать
                отчет не уникальности ({{ count($uniqueArray['none_unique']) }})</a>
        @endif
    </div>
@endif