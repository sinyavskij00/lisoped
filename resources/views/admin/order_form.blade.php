<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">{{ $form->title() }}</h3>

        <div class="box-tools">
            {!! $form->renderTools() !!}
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    {!! $form->open(['class' => "form-horizontal"]) !!}

    <div class="box-body">

        @if(!$tabObj->isEmpty())
            @include('admin::form.tab', compact('tabObj'))
        @else
            <div class="fields-group">

                @if($form->hasRows())
                    @foreach($form->getRows() as $row)
                        {!! $row->render() !!}
                    @endforeach
                @else
                    @foreach($form->fields() as $field)
                        {!! $field->render() !!}
                    @endforeach
                @endif


            </div>
        @endif

    </div>
    <!-- /.box-body -->

    {!! $form->renderFooter() !!}

    @foreach($form->getHiddenFields() as $field)
        {!! $field->render() !!}
    @endforeach

<!-- /.box-footer -->
    {!! $form->close() !!}
    <script>
        $('[name=customer_id]').on('change', function (e) {
            var userId = $(this).val();
            $.ajax({
                url: '{{route('admin.api.getUser')}}',
                data: {
                    user_id: userId
                },
                success: function (result) {
                    if(result){
                        var firstName = result['first_name'] ? result['first_name'] : '';
                        var lastName = result['last_name'] ? result['last_name'] : '';
                        var customerGroupId = result['customer_group_id'] ? result['customer_group_id'] : null;

                        $('[name=first_name]').val(firstName);
                        $('[name=last_name]').val(lastName);
                        console.log(customerGroupId);
                        if(customerGroupId){
                            $('[name=customer_group_id]').val(customerGroupId);
                        }
                    }
                }
            });
        });
    </script>
</div>

