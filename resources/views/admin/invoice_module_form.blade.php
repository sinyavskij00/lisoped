<form id="invoice-form" {!! $attributes !!}>
    <div class="box-body fields-group">

        @foreach($fields as $field)
            {!! $field->render() !!}
        @endforeach

    </div>

    @if ($method != 'GET')
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    @endif

    <input id="action-input" type="hidden" name="action" value="check">

<!-- /.box-body -->
    @if(count($buttons) > 0)
        <div class="box-footer">
            <div class="col-md-2"></div>

            <div class="col-md-8">
                @if(old('check') > 0)
                    <div>
                        <div class="btn-group pull-left">
                            <span><b>Количество заказов: {{old('check')}}</b></span>
                        </div>
                        <div class="btn-group pull-right">
                            <button id="download" type="button" class="btn btn-info pull-right">{{ trans('admin.download') }}</button>
                        </div>
                        <div class="btn-group pull-right" style="margin-right: 5px">
                            <button id="send-to-email" type="submit" class="btn btn-info pull-right">{{ trans('admin.send_to_vendor_email') }}</button>
                        </div>
                    </div>
                @else
                    @if(old('check') === 0)
                        <div class="btn-group pull-left">
                            <span>Количество заказов: {{old('check')}}</span>
                            <p>Пожалуйста выберите другие даты.</p>
                        </div>
                    @endif

                    @if(old('sendToEmail') == 'email_send')
                        <div class="btn-group pull-left">
                            <span><b>Письмо успешно отправленно!</b></span>
                        </div>
                    @endif

                    @if(in_array('submit', $buttons))
                        <div class="btn-group pull-right" style="margin-right: 5px">
                            <button id="check-count" type="submit" class="btn btn-info pull-right">{{ trans('admin.check') }}</button>
                        </div>
                    @endif
                @endif
            </div>
        </div>
    @endif
</form>

<script type="text/javascript">
    $('#check-count').on('click', function (e) {
        $('#action-input').val('check');
        $('#invoice-form').submit();
    });

    $('#download').on('click', function (e) {
       $('#action-input').val('download');
       $('#invoice-form').submit();
    });

    $('#send-to-email').on('click', function (e) {
        $('#action-input').val('sendToEmail');
        $('#invoice-form').submit();
    });
</script>
