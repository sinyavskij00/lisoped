<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Check</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }

        .t-a-r {
            text-align: right;
        }

        .check thead {
            text-align: center;
            font-weight: 600;
        }

        .check td {
            border: 1px solid black;
            padding: 5px;
        }

        .wrapper {
            max-width: 780px;
            margin: 0 auto;
        }

        .check-header {
            text-align: center;
            margin-bottom: 30px;
        }

        .stamp-wrapper {
            text-align: right;
            margin-top: 20px;
        }

        .stamp-image {
            margin-left: auto;
            max-width: 200px;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="check-header">
        <h1>{{__('mails.check.content.title')}}</h1>
        @if(!empty($orderNumber))
            <h5>{{__('mails.check.content.order_number', ['number' => $orderNumber])}}</h5>
        @endif
        <div class="head-text">
            <p>{{__('mails.check.content.head_text')}}</p>
        </div>
    </div>
    <table class="check">
        <thead>
        <tr>
            <td>{{__('mails.check.content.product')}}</td>
            <td>{{__('mails.check.content.price')}}</td>
            <td>{{__('mails.check.content.quantity')}}</td>
            <td>{{__('mails.check.content.total')}}</td>
        </tr>
        </thead>
        <tbody>
        @if(isset($orderSkus) && $orderSkus->count() > 0)
            @foreach($orderSkus as $orderSku)
                <tr>
                    <td>{{$orderSku->name}}</td>
                    <td>{{$orderSku->price}}</td>
                    <td>{{$orderSku->quantity}}</td>
                    <td>{{$orderSku->total}}</td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    <table>
        <tbody>
        <tr>
            <td>
                @if(!empty($orderDate))
                    {{__('mails.check.content.date')}} {{$orderDate}}
                @endif
            </td>
            <td class="t-a-r">
                @if($stamp)
                    <div class="stamp-wrapper"><img class="stamp-image" src="{{Storage::disk('public')->url($stamp)}}">
                    </div>
                @endif
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>