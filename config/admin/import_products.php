<?php

return [
    'processed' => [
        'success' => [
            'count' => 0
        ],
        'error' => [
            'count' => 0
        ],
        'unique' => [
            'count' => 0
        ]
    ],

    'insert_batch' => [
        'product_descriptions' => [
            'data' => [],
            'fields' => [
                'product_id',
                'language_id',
                'name',
                'description',
                'meta_description',
                'meta_keywords',
                'meta_title'
            ],
        ],
        'attribute_descriptions' => [
            'data' => [],
            'fields' => ['name', 'language_id', 'attribute_id']
        ],
        'product_attributes' => [
            'data' => [],
            'fields' => ['attribute_id', 'product_id', 'language_id', 'text'],
        ],
        'product_to_category' => [
            'data' => [],
            'fields' => ['product_id', 'category_id']
        ],
        'size_table_to_product' => [
            'data' => [],
            'fields' => ['product_id', 'table_id']
        ],
        'sku_specials' => [
            'data' => [],
            'fields' => ['unit_id', 'customer_group_id', 'priority', 'price', 'date_start']
        ],
        'size_descriptions' => [
            'data' => [],
            'fields' => ['comment', 'size_id', 'language_id']
        ],
        'manufacturer_descriptions' => [
            'data' => [],
            'fields' => [
                'manufacturer_id',
                'language_id',
                'name',
                'description',
                'meta_h1',
                'meta_description',
                'meta_keywords',
                'meta_title'
            ]
        ],
        'unit_images' => [
            'data' => [],
            'fields' => ['unit_id', 'image']
        ],
    ]
];
