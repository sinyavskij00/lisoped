<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*************** SWITCH LANGUAGES *********************************************************/

Route::get('setlocale/{lang}', 'Front\SwitchLanguageController@switchLanguage')->name('setlocale');

/*************** FRONT ROUTS **************************************************************/

Route::post('module/pickupBicycle', 'Front\Catalog\HomeController@chooseBicycle')->name('module.pickupBicycle');

/*************** RESET PASSWORD ***********************************************************/

Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/reset/{token} ', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

/*************** FRONT ROUTS **************************************************************/

//socialize
Route::get('/redirect/{service}', 'SocialAuthController@redirect')->name('socialize.redirect');
Route::get('/callback/{service}', 'SocialAuthController@callback')->name('socialize.callback');

Route::group(['namespace' => 'Front'], function () {
    //new post
    Route::get('newpost/city', 'Checkout\Shipping\NewPostController@getCity')->name('newpost.city');
    Route::get('newpost/stores', 'Checkout\Shipping\NewPostController@getStores')->name('newpost.stores');

    //product actions
    Route::get('getProductPrice', 'Catalog\ProductController@calculateProductPrices')->name('product.calculatePrices');
    Route::post('product/addReview', 'Catalog\ProductController@addReview')->name('product.addReview');

    //popups
    Route::post('headerCallback', 'Catalog\PopupProcessorController@headerCallback')
        ->name('popupProcessor.headerCallback');
    Route::post('oneClickBuy', 'Catalog\PopupProcessorController@oneClickBuy')->name('popupProcessor.oneClickBuy');
    Route::post('preorder', 'Catalog\PopupProcessorController@preorder')->name('popupProcessor.preorder');

    //wishlist
    Route::put('wishlist', 'Catalog\WishlistController@add')->name('wishlist.add');
    Route::delete('wishlist', 'Catalog\WishlistController@delete')->name('wishlist.delete');

    //compare
    Route::put('compare', 'Catalog\CompareController@add')->name('compare.add');
    Route::delete('compare', 'Catalog\CompareController@delete')->name('compare.delete');

    //cart
    Route::group(['prefix' => 'cart', 'namespace' => 'Cart'], function () {
        Route::post('add', 'CartController@add')->name('cart.add');
        Route::post('delete', 'CartController@delete')->name('cart.delete');
        Route::post('change', 'CartController@changeItemQuantity')->name('cart.changeQuantity');
    });
});

Route::group(['prefix' => \App\Http\Middleware\BeforeLanguagePrefix::getPrefix()], function () {
    Route::group(['prefix' => 'cabinet', 'namespace' => 'Front\Cabinet', 'middleware' => ['auth:web']], function () {
        Route::get('/', 'CabinetController@index')->name('cabinet.index');
        Route::post('/', 'CabinetController@update');
        Route::get('/orderHistory', 'CabinetController@orderHistory')->name('cabinet.orderHistory');
        Route::get('/changePassword', 'CabinetController@password')->name('cabinet.changePassword');
        Route::post('/changePassword', 'CabinetController@updatePassword');
        Route::get('/wishlist', 'CabinetController@wishlist')->name('cabinet.wishlist');
        Route::get('/purchasedGoods', 'CabinetController@purchasedGoods')->name('cabinet.purchasedGoods');
    });

    //auth and register
    Route::group(['namespace' => 'Auth'], function () {
        Route::post('/login', 'LoginController@login')->name('login');
        Route::get('/logout', 'LoginController@logout')->name('logout');
        Route::get('/checkAuth', 'LoginController@checkAuth')->name('checkAuth');
        Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('/register', 'RegisterController@register');
    });


    //permanent pages
    Route::group(['namespace' => 'Front'], function () {
        //checkout
        Route::group(['namespace' => 'Checkout'], function () {
            Route::match(['POST', 'GET'], 'checkout', 'CheckoutController@index')->name('checkout');
            Route::match(['POST', 'GET'], 'checkout/shippingInfo', 'CheckoutController@getShippingInfo')
                ->name('checkout.shippingInfo');
            Route::match(['POST', 'GET'], 'checkout/paymentInfo', 'CheckoutController@getPaymentInfo')
                ->name('checkout.paymentInfo');
            Route::post('checkout/confirm', 'CheckoutController@confirm')->name('checkout.confirm');
            Route::get('success', 'SuccessController@index')->name('success');
            Route::get('failure', 'FailureController@index')->name('failure');
            Route::match(['GET', 'POST'], 'liqpay/renderstatus', 'CheckoutController@liqPayRenderStatus')
                ->name('liqpay.renderStatus');
            Route::post('liqpay/callback', 'CheckoutController@liqPayCallback')->name('liqpay.callback');
        });

        Route::group(['namespace' => 'Information'], function () {
            Route::get('about_us', 'AboutUsController@index')->name('about_us');
            Route::get('guarantee', 'Guarantee@index')->name('guarantee');
            Route::get('credit', 'Credit@index')->name('credit');
            Route::get('delivery', 'DeliveryController@index')->name('delivery');
            Route::get('contacts', 'ContactsController@index')->name('contacts');
            Route::post('contacts', 'ContactsController@question')->name('contacts.add');
            Route::get('faq', 'FaqController@index')->name('faq');

            Route::get('testimonials', 'TestimonialController@index')->name('testimonials');
            Route::post('testimonials', 'TestimonialController@add')->name('testimonials.add');
        });

        Route::group(['namespace' => 'Catalog'], function () {
            Route::get('/', 'HomeController@index')->name('home');
            Route::post('subscribe', 'PopupProcessorController@subscribe')->name('subscribe');

            Route::get('new', 'CategoryController@newProducts')->name('new');
            Route::get('top', 'CategoryController@top')->name('top');

            Route::get('specials', 'SpecialController@list')->name('special.list');
            Route::get('special/{slug}', 'SpecialController@show')->name('special.show');
            Route::post('special', 'SpecialController@question')->name('special.question');

            Route::get('news', 'NewsController@list')->name('news.list');
            Route::get('news/{slug}', 'NewsController@show')->name('news.show');
            Route::post('news/addReview', 'NewsController@addReview')->name('news.addReview');

            Route::get('manufacturers', 'ManufacturerController@list')->name('manufacturer.list');
            Route::get('manufacturer/{slug}/{filter?}', 'ManufacturerController@show')->name('manufacturer.show');

            Route::get('search/{filter?}', 'SearchController@index')->name('search');
            Route::get('ajaxSearch', 'SearchController@ajaxSearch')->name('search.ajax');

            Route::get('compare/categories', 'CompareController@categoryList')->name('compare.categoryList');
            Route::get('compare/category/{id}', 'CompareController@productList')->name('compare.productList');

            //dynamic page (category or product)
            Route::get('product/{slug}', 'ProductController@index')->name('product_path');
            Route::get('{category_path}', 'CategoryController@index')
                ->name('category_path')
                ->where('category_path', '.+');
        });
    });
});
