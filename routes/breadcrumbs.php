<?php
// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push(__('breadcrumbs.home'), route('home'));
});

// Home > About
Breadcrumbs::for('about_us', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.about_us'), route('about_us'));
});

// Home > Contacts
Breadcrumbs::for('contacts', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.contacts'), route('contacts'));
});

// Home > Credit
Breadcrumbs::for('credit', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.credit'), route('credit'));
});

// Home > Delivery
Breadcrumbs::for('delivery', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.delivery'), route('delivery'));
});

// Home > news.list
Breadcrumbs::for('news.list', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.news_list'), route('news.list'));
});

// Home > Compare
Breadcrumbs::for('compare.categoryList', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.compare_category'), route('compare.categoryList'));
});

// Home > Manufacturers
Breadcrumbs::for('manufacturer.list', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.manufacturers'), route('manufacturer.list'));
});

// Home > manufacturers > show
Breadcrumbs::for('manufacturer.show', function ($trail, $manufacturerSlug) {
    $trail->parent('manufacturer.list');
    $manufacturer = \App\Entities\Manufacturer::where('slug', '=', $manufacturerSlug)
        ->with(['localDescription'])->get()->first();

    if (!isset($manufacturer)) {
        abort(404);
    }
    $manufacturerName = isset($manufacturer->localDescription) ? $manufacturer->localDescription->name : '';
    $trail->push($manufacturerName, route('manufacturer.show', ['slug' => $manufacturer->slug]));
});

// Home > special.list
Breadcrumbs::for('special.list', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.specials_list'), route('special.list'));
});

// Home > special.show
Breadcrumbs::for('special.show', function ($trail, $specialSlug) {
    $special = \App\Entities\Special::where('slug', '=', $specialSlug)
        ->where('status', '=', 1)
        ->with(['localDescription'])->first();

    if (!isset($special)) {
        abort(404);
    }
    $trail->parent('special.list');
    $specialName = isset($special->localDescription) ? $special->localDescription->title : '';
    $trail->push($specialName, route('special.show', $special->slug));
});

// Home > Guarantee
Breadcrumbs::for('guarantee', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.guarantee'), route('guarantee'));
});

// Home > faq
Breadcrumbs::for('faq', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.faq'), route('faq'));
});

// Home > news.show
Breadcrumbs::for('news.show', function ($trail, $newsSlug) {
    $news = \App\Entities\News::where('status', '=', 1)->where('slug', '=', $newsSlug)->with(['localDescription'])->first();

    if (!isset($news)) {
        abort(404);
    }

    $trail->parent('news.list');
    $newsName = isset($news->localDescription) ? $news->localDescription->title : '';
    $trail->push($newsName, route('news.show', $news->slug));
});

// Home > [Category]
Breadcrumbs::for('category', function ($trail, $categoryId) {
    $trail->parent('home');
    $categories = \App\Entities\Category::ancestorsAndSelf($categoryId);
    $categories->load('localDescription');
    foreach ($categories as $item) {
        $categoryName = isset($item->localDescription) ? $item->localDescription->name : '';
        $trail->push($categoryName, route('category_path', category_path($item->id)));
    }
});

// Home > product_path
Breadcrumbs::for('product_path', function ($trail, $productSlug) {
    $product = \App\Entities\Product::with('localDescription')->where('slug', '=', $productSlug)->first();
    $categoryId = $product->getAttributeValue('category_id');
    $trail->parent('category', $categoryId);
    $productName = isset($product->localDescription) ? $product->localDescription->name : '';
    $trail->push($productName, route('product_path', $product->slug));
});

// Home > category_path
Breadcrumbs::for('category_path', function ($trail, $categoryPath) {
    $categoryId = $categoryPath->id;
    $trail->parent('category', $categoryId);
});

// Testimonials
Breadcrumbs::for('testimonials', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.testimonials'), route('testimonials'));
});

// Search
Breadcrumbs::for('search', function ($trail) {
    $trail->parent('home');
    $trail->push(__('breadcrumbs.search'), route('search'));
});