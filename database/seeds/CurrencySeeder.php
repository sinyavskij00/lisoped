<?php

use Illuminate\Database\Seeder;
use App\Entities\Currency;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Гривна',
                'code' => 'UAH',
                'symbol_left' => '',
                'symbol_right' => 'грн',
                'decimal_place' => 0,
                'value' => 1,
                'status' => 1,
                'is_default' => 1,
            ],
//            [
//                'name' => 'Доллар',
//                'code' => 'USD',
//                'symbol_left' => '',
//                'symbol_right' => '$',
//                'decimal_place' => 2,
//                'value' => 26,
//                'status' => 1,
//                'is_default' => 0,
//            ]
        ];
        Currency::insert($data);
    }
}
