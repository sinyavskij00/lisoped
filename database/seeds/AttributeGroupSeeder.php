<?php

use Illuminate\Database\Seeder;

class AttributeGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = collect(\Illuminate\Support\Facades\Storage::files('shop_images/attribute_groups'));
        $data = [];
        $now = now();
        for($i = 0; $i < 6; $i++){
            $data[] = [
                'sort' => random_int(-10, 10),
                'image' => $images->random(1)->first(),
                'created_at' => $now,
                'updated_at' => $now
            ];
        }
        \App\Entities\AttributeGroup::insert($data);

        $languages = \App\Entities\Language::all();
        \App\Entities\AttributeGroup::chunk(1000, function ($attributeGroups) use ($languages) {
            $data = [];
            foreach ($attributeGroups as $attributeGroup) {
                foreach ($languages as $language){
                    $data[] = [
                        'language_id' => $language->id,
                        'attribute_group_id' => $attributeGroup->id,
                        'name' => 'группа атрибутов ' . $attributeGroup->id
                    ];
                }
            }
            \App\Entities\AttributeGroupDescription::insert($data);
        });
    }
}
