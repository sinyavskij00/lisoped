<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = collect(Storage::files('shop_images/colors'));
        $now = now();
        $faker = app()->make(Faker::class);
        $data = [];
        foreach ($images as $image) {
            $data[] = [
                'image' => $image,
                'anchor' => strtolower($faker->unique()->colorName()),
                'created_at' => $now,
                'updated_at' => $now
            ];
        }
        \App\Entities\Color::insert($data);

        $languages = \App\Entities\Language::all();
        \App\Entities\Color::chunk(1000, function ($colors) use ($languages, $faker) {
            $descriptions = [];
            foreach ($colors as $color) {
                foreach ($languages as $language){
                    $descriptions[] = [
                        'color_id' => $color->id,
                        'language_id' => $language->id,
                        'name' => $color->anchor
                    ];
                }
            }
            \App\Entities\ColorDescription::insert($descriptions);
        });
    }
}