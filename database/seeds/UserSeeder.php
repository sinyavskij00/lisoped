<?php

use Illuminate\Database\Seeder;
use App\Entities\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::make([
            'first_name' => 'Andrey',
            'last_name' => 'Test',
            'patronymic' => 'test',
            'telephone' => '0506985444',
            'address' => 'Харьков',
            'email' => '30andrey14@gmail.com',
            'remember_token' => str_random(10),
            'password' => Hash::make('123456')
        ]);
        $customerGroup = \App\Entities\CustomerGroup::all()->first();
        $user->customer_group_id = isset($customerGroup) ? $customerGroup->id : null;
        $user->save();
    }
}
