<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class TestimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = app()->make(Faker::class);
        $testimonialCount = 10;
        $testimonials = [];
        for ($i = 0; $i < $testimonialCount; $i++) {
            $testimonials[] = [
                'name' => $faker->firstName . ' ' . $faker->lastName,
                'text' => $faker->paragraph,
                'status' => 1,
                'rating' => random_int(1, 5),
                'email' => $faker->email
            ];
        }
        \App\Entities\Testimonial::insert($testimonials);

        \App\Entities\Testimonial::chunk(1000, function ($testimonials) use ($faker) {
            $data = [];
            for($i = 0; $i < 2; $i++){
                foreach ($testimonials as $testimonial) {
                    $data[] = [
                        'parent_id' => $testimonial->id,
                        'name' => $faker->firstName . ' ' . $faker->lastName,
                        'text' => $faker->paragraph,
                        'status' => 1,
                        'rating' => random_int(1, 5),
                        'email' => $faker->email
                    ];
                }
            }
            \App\Entities\Testimonial::insert($data);
        });
        \App\Entities\Testimonial::fixTree();


        $images = collect(\Illuminate\Support\Facades\Storage::files('shop_images/reviews'));
        \App\Entities\Testimonial::chunk(1000, function ($testimonials) use ($images) {
            $data = [];
            foreach ($testimonials as $testimonial){
                $data[] = [
                    'image' => $images->random(1)->first(),
                    'testimonial_id' => $testimonial->id
                ];
            }
            \App\Entities\TestimonialImage::insert($data);
        });
    }
}
