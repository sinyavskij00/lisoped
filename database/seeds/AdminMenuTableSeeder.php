<?php

use Illuminate\Database\Seeder;

class AdminMenuTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_menu')->delete();
        
        \DB::table('admin_menu')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => 0,
                'order' => 1,
                'title' => 'Главная',
                'icon' => 'fa-bar-chart',
                'uri' => '/',
                'created_at' => NULL,
                'updated_at' => '2018-11-29 17:02:08',
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => 0,
                'order' => 56,
                'title' => 'Админ',
                'icon' => 'fa-empire',
                'uri' => NULL,
                'created_at' => NULL,
                'updated_at' => '2018-11-29 17:01:42',
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => 2,
                'order' => 57,
                'title' => 'Пользователи',
                'icon' => 'fa-users',
                'uri' => 'auth/users',
                'created_at' => NULL,
                'updated_at' => '2018-11-29 17:01:42',
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => 2,
                'order' => 58,
                'title' => 'Роли',
                'icon' => 'fa-user-secret',
                'uri' => 'auth/roles',
                'created_at' => NULL,
                'updated_at' => '2018-11-29 17:01:42',
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => 2,
                'order' => 59,
                'title' => 'Разрешения',
                'icon' => 'fa-ban',
                'uri' => 'auth/permissions',
                'created_at' => NULL,
                'updated_at' => '2018-11-29 17:01:42',
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => 2,
                'order' => 60,
                'title' => 'Меню',
                'icon' => 'fa-dropbox',
                'uri' => 'auth/menu',
                'created_at' => NULL,
                'updated_at' => '2018-11-29 17:01:42',
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => 2,
                'order' => 61,
                'title' => 'Лог действий',
                'icon' => 'fa-history',
                'uri' => 'auth/logs',
                'created_at' => NULL,
                'updated_at' => '2018-11-29 17:01:42',
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => 0,
                'order' => 62,
                'title' => 'Медиа Менеджер',
                'icon' => 'fa-file',
                'uri' => 'media',
                'created_at' => '2018-10-04 11:26:12',
                'updated_at' => '2018-11-29 17:03:05',
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => 0,
                'order' => 14,
                'title' => 'Каталог',
                'icon' => 'fa-paw',
                'uri' => NULL,
                'created_at' => '2018-10-05 10:48:06',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            9 => 
            array (
                'id' => 10,
                'parent_id' => 9,
                'order' => 16,
                'title' => 'Товары',
                'icon' => 'fa-futbol-o',
                'uri' => 'products',
                'created_at' => '2018-10-05 10:48:43',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            10 => 
            array (
                'id' => 11,
                'parent_id' => 9,
                'order' => 17,
                'title' => 'Артикулы',
                'icon' => 'fa-space-shuttle',
                'uri' => 'skus',
                'created_at' => '2018-10-05 10:49:27',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            11 => 
            array (
                'id' => 12,
                'parent_id' => 9,
                'order' => 24,
                'title' => 'Производители',
                'icon' => 'fa-steam',
                'uri' => 'manufacturers',
                'created_at' => '2018-10-05 10:49:45',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            12 => 
            array (
                'id' => 13,
                'parent_id' => 9,
                'order' => 15,
                'title' => 'Категории',
                'icon' => 'fa-book',
                'uri' => 'categories',
                'created_at' => '2018-10-05 10:50:03',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            13 => 
            array (
                'id' => 14,
                'parent_id' => 9,
                'order' => 26,
                'title' => 'Новости',
                'icon' => 'fa-at',
                'uri' => 'newses',
                'created_at' => '2018-10-05 10:50:17',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            14 => 
            array (
                'id' => 15,
                'parent_id' => 9,
                'order' => 21,
                'title' => 'Годы товаров',
                'icon' => 'fa-superscript',
                'uri' => 'years',
                'created_at' => '2018-10-05 10:50:49',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            15 => 
            array (
                'id' => 16,
                'parent_id' => 9,
                'order' => 22,
                'title' => 'Цвета товаров',
                'icon' => 'fa-adjust',
                'uri' => 'colors',
                'created_at' => '2018-10-05 10:51:04',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            16 => 
            array (
                'id' => 17,
                'parent_id' => 9,
                'order' => 23,
                'title' => 'Размеры товаров',
                'icon' => 'fa-expand',
                'uri' => 'sizes',
                'created_at' => '2018-10-05 10:51:23',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            17 => 
            array (
                'id' => 18,
                'parent_id' => 9,
                'order' => 29,
                'title' => 'Языки',
                'icon' => 'fa-weixin',
                'uri' => 'languages',
                'created_at' => '2018-10-05 10:51:42',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            18 => 
            array (
                'id' => 19,
                'parent_id' => 0,
                'order' => 31,
                'title' => 'Модули',
                'icon' => 'fa-rebel',
                'uri' => NULL,
                'created_at' => '2018-10-08 05:35:05',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            19 => 
            array (
                'id' => 20,
                'parent_id' => 19,
                'order' => 35,
                'title' => 'Новая Почта',
                'icon' => 'fa-globe',
                'uri' => 'new-post',
                'created_at' => '2018-10-08 05:35:35',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            20 => 
            array (
                'id' => 21,
                'parent_id' => 0,
                'order' => 55,
                'title' => 'Настройки',
                'icon' => 'fa-shield',
                'uri' => 'site-settings',
                'created_at' => '2018-10-08 05:36:02',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            21 => 
            array (
                'id' => 22,
                'parent_id' => 64,
                'order' => 44,
                'title' => 'Массовое управление акциями',
                'icon' => 'fa-adn',
                'uri' => 'mass-special',
                'created_at' => '2018-10-08 09:55:58',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            22 => 
            array (
                'id' => 23,
                'parent_id' => 64,
                'order' => 45,
                'title' => 'Массовое управление скидками',
                'icon' => 'fa-strikethrough',
                'uri' => 'mass-discount',
                'created_at' => '2018-10-08 09:56:26',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            23 => 
            array (
                'id' => 25,
                'parent_id' => 64,
                'order' => 46,
                'title' => 'Массовое управление ценами',
                'icon' => 'fa-signal',
                'uri' => 'mass-price',
                'created_at' => '2018-10-08 09:57:32',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            24 => 
            array (
                'id' => 26,
                'parent_id' => 0,
                'order' => 2,
                'title' => 'Продажи',
                'icon' => 'fa-empire',
                'uri' => NULL,
                'created_at' => '2018-10-08 11:30:46',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            25 => 
            array (
                'id' => 27,
                'parent_id' => 26,
                'order' => 3,
                'title' => 'Заказы',
                'icon' => 'fa-diamond',
                'uri' => 'orders',
                'created_at' => '2018-10-08 11:31:06',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            26 => 
            array (
                'id' => 28,
                'parent_id' => 26,
                'order' => 11,
                'title' => 'Статусы заказов',
                'icon' => 'fa-bullhorn',
                'uri' => 'order-statuses',
                'created_at' => '2018-10-09 03:34:27',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            27 => 
            array (
                'id' => 29,
                'parent_id' => 9,
                'order' => 25,
                'title' => 'Поставщики',
                'icon' => 'fa-binoculars',
                'uri' => 'vendors',
                'created_at' => '2018-10-09 04:34:58',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            28 => 
            array (
                'id' => 30,
                'parent_id' => 19,
                'order' => 32,
                'title' => 'Собрать заказ',
                'icon' => 'fa-clipboard',
                'uri' => 'invoice-module',
                'created_at' => '2018-10-09 04:42:31',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            29 => 
            array (
                'id' => 31,
                'parent_id' => 26,
                'order' => 6,
                'title' => 'Покупатели',
                'icon' => 'fa-firefox',
                'uri' => 'customers',
                'created_at' => '2018-10-09 08:40:23',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            30 => 
            array (
                'id' => 32,
                'parent_id' => 26,
                'order' => 5,
                'title' => 'Группы покупателей',
                'icon' => 'fa-heartbeat',
                'uri' => 'customer-groups',
                'created_at' => '2018-10-09 08:40:54',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            31 => 
            array (
                'id' => 33,
                'parent_id' => 0,
                'order' => 52,
                'title' => 'FAQ',
                'icon' => 'fa-graduation-cap',
                'uri' => NULL,
                'created_at' => '2018-10-09 10:20:39',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            32 => 
            array (
                'id' => 34,
                'parent_id' => 33,
                'order' => 53,
                'title' => 'Группы вопросов',
                'icon' => 'fa-weixin',
                'uri' => 'question-groups',
                'created_at' => '2018-10-09 10:23:32',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            33 => 
            array (
                'id' => 35,
                'parent_id' => 33,
                'order' => 54,
                'title' => 'Вопросы',
                'icon' => 'fa-volume-control-phone',
                'uri' => 'questions',
                'created_at' => '2018-10-09 10:23:45',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            34 => 
            array (
                'id' => 36,
                'parent_id' => 0,
                'order' => 48,
                'title' => 'Отзывы',
                'icon' => 'fa-edit',
                'uri' => NULL,
                'created_at' => '2018-10-09 11:37:53',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            35 => 
            array (
                'id' => 37,
                'parent_id' => 36,
                'order' => 51,
                'title' => 'Отзывы к сайту',
                'icon' => 'fa-star-half-empty',
                'uri' => 'testimonials',
                'created_at' => '2018-10-09 11:38:24',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            36 => 
            array (
                'id' => 38,
                'parent_id' => 36,
                'order' => 50,
                'title' => 'Отзывы о статьях',
                'icon' => 'fa-map-signs',
                'uri' => 'news-reviews',
                'created_at' => '2018-10-09 11:41:58',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            37 => 
            array (
                'id' => 39,
                'parent_id' => 36,
                'order' => 49,
                'title' => 'Отзывы о товарах',
                'icon' => 'fa-comments-o',
                'uri' => 'product-reviews',
                'created_at' => '2018-10-09 11:43:22',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            38 => 
            array (
                'id' => 41,
                'parent_id' => 26,
                'order' => 10,
                'title' => 'Статьи рентабельности',
                'icon' => 'fa-flask',
                'uri' => 'order-profitabilities',
                'created_at' => '2018-10-10 12:08:43',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            39 => 
            array (
                'id' => 42,
                'parent_id' => 26,
                'order' => 9,
                'title' => 'Типы статей рентабельности',
                'icon' => 'fa-coffee',
                'uri' => 'order-profitability-types',
                'created_at' => '2018-10-10 12:09:35',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            40 => 
            array (
                'id' => 43,
                'parent_id' => 26,
                'order' => 8,
                'title' => 'Платежные карты',
                'icon' => 'fa-cc-visa',
                'uri' => 'billing-carts',
                'created_at' => '2018-10-12 11:42:41',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            41 => 
            array (
                'id' => 44,
                'parent_id' => 26,
                'order' => 7,
                'title' => 'Платежные счета',
                'icon' => 'fa-envira',
                'uri' => 'billing-accounts',
                'created_at' => '2018-10-12 11:43:01',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            42 => 
            array (
                'id' => 45,
                'parent_id' => 9,
                'order' => 19,
                'title' => 'Атрибуты',
                'icon' => 'fa-fort-awesome',
                'uri' => 'attributes',
                'created_at' => '2018-10-19 08:48:16',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            43 => 
            array (
                'id' => 46,
                'parent_id' => 9,
                'order' => 18,
                'title' => 'Группы атрибутов',
                'icon' => 'fa-shield',
                'uri' => 'attribute-groups',
                'created_at' => '2018-10-19 08:48:47',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            44 => 
            array (
                'id' => 47,
                'parent_id' => 9,
                'order' => 20,
                'title' => 'Значения атрибутов',
                'icon' => 'fa-book',
                'uri' => 'attribute-values',
                'created_at' => '2018-10-19 09:52:39',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            45 => 
            array (
                'id' => 49,
                'parent_id' => 62,
                'order' => 39,
            'title' => 'Отчеты по заказам ( по покупателю)',
                'icon' => 'fa-area-chart',
                'uri' => 'order-report-customer',
                'created_at' => '2018-10-23 03:29:28',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            46 => 
            array (
                'id' => 50,
                'parent_id' => 62,
                'order' => 40,
            'title' => 'Отчеты по заказам ( по группе покупателей)',
                'icon' => 'fa-area-chart',
                'uri' => 'order-report-customer-group',
                'created_at' => '2018-10-23 03:30:03',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            47 => 
            array (
                'id' => 51,
                'parent_id' => 62,
                'order' => 41,
            'title' => 'Отчеты по заказам ( по поставщику)',
                'icon' => 'fa-area-chart',
                'uri' => 'order-report-vendor',
                'created_at' => '2018-10-23 03:36:25',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            48 => 
            array (
                'id' => 52,
                'parent_id' => 9,
                'order' => 27,
                'title' => 'Акции',
                'icon' => 'fa-adn',
                'uri' => 'specials',
                'created_at' => '2018-10-25 04:19:24',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            49 => 
            array (
                'id' => 53,
                'parent_id' => 26,
                'order' => 12,
                'title' => 'Методы доставки',
                'icon' => 'fa-rocket',
                'uri' => 'shippings',
                'created_at' => '2018-10-25 05:04:57',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            50 => 
            array (
                'id' => 54,
                'parent_id' => 26,
                'order' => 13,
                'title' => 'Методы оплаты',
                'icon' => 'fa-euro',
                'uri' => 'payments',
                'created_at' => '2018-10-25 05:05:20',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            51 => 
            array (
                'id' => 55,
                'parent_id' => 19,
                'order' => 33,
                'title' => 'Баннеры на главной',
                'icon' => 'fa-cc-discover',
                'uri' => 'banners',
                'created_at' => '2018-10-25 05:22:32',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            52 => 
            array (
                'id' => 56,
                'parent_id' => 9,
                'order' => 28,
                'title' => 'Посадочные страницы',
                'icon' => 'fa-clone',
                'uri' => 'landing-pages',
                'created_at' => '2018-10-25 05:26:58',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            53 => 
            array (
                'id' => 57,
                'parent_id' => 62,
                'order' => 42,
            'title' => 'Отчет по заказам (по категории)',
                'icon' => 'fa-area-chart',
                'uri' => 'order-report-category',
                'created_at' => '2018-10-25 09:20:37',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            54 => 
            array (
                'id' => 58,
                'parent_id' => 19,
                'order' => 34,
                'title' => 'Текст на главной',
                'icon' => 'fa-file-powerpoint-o',
                'uri' => 'home-text-module',
                'created_at' => '2018-10-26 08:12:14',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            55 => 
            array (
                'id' => 59,
                'parent_id' => 26,
                'order' => 4,
                'title' => 'Истории заказов',
                'icon' => 'fa-android',
                'uri' => 'order-histories',
                'created_at' => '2018-10-29 05:44:55',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            56 => 
            array (
                'id' => 60,
                'parent_id' => 19,
                'order' => 36,
                'title' => 'LiqPay проверить заказ',
                'icon' => 'fa-institution',
                'uri' => 'liqpay-check',
                'created_at' => '2018-11-05 08:52:14',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            57 => 
            array (
                'id' => 61,
                'parent_id' => 19,
                'order' => 37,
                'title' => 'Модуль карусели на главной',
                'icon' => 'fa-dashboard',
                'uri' => 'carousel-module',
                'created_at' => '2018-11-06 12:53:32',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            58 => 
            array (
                'id' => 62,
                'parent_id' => 19,
                'order' => 38,
                'title' => 'Отчеты',
                'icon' => 'fa-key',
                'uri' => NULL,
                'created_at' => '2018-11-12 14:31:48',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            59 => 
            array (
                'id' => 64,
                'parent_id' => 19,
                'order' => 43,
                'title' => 'Акции',
                'icon' => 'fa-umbrella',
                'uri' => NULL,
                'created_at' => '2018-11-12 14:34:36',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            60 => 
            array (
                'id' => 65,
                'parent_id' => 0,
                'order' => 63,
                'title' => 'Резервные копии',
                'icon' => 'fa-support',
                'uri' => '/backup',
                'created_at' => '2018-11-20 08:07:21',
                'updated_at' => '2018-11-29 17:04:43',
            ),
            61 => 
            array (
                'id' => 66,
                'parent_id' => 9,
                'order' => 30,
                'title' => 'Таблицы размеров',
                'icon' => 'fa-bars',
                'uri' => 'size-tables',
                'created_at' => '2018-11-21 14:04:37',
                'updated_at' => '2018-11-29 17:01:42',
            ),
            62 => 
            array (
                'id' => 67,
                'parent_id' => 19,
                'order' => 47,
                'title' => 'Массовое присвоение статусов',
                'icon' => 'fa-bars',
                'uri' => 'mass-status',
                'created_at' => '2018-11-23 13:43:54',
                'updated_at' => '2018-11-29 17:01:42',
            ),
        ));
        
        
    }
}