<?php

use Illuminate\Database\Seeder;

class SiteSettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('site_settings')->delete();

        \DB::table('site_settings')->insert(array (
            0 =>
            array (
                'id' => 1,
                'code' => 'payment.liqpay',
                'key' => 'status',
                'value' => '1',
                'serialized' => 0,
            ),
            1 =>
            array (
                'id' => 2,
                'code' => 'payment.liqpay',
                'key' => 'confirm_status',
                'value' => '11',
                'serialized' => 0,
            ),
            2 =>
            array (
                'id' => 3,
                'code' => 'payment.bankCart',
                'key' => 'status',
                'value' => '1',
                'serialized' => 0,
            ),
            3 =>
            array (
                'id' => 4,
                'code' => 'payment.bankCart',
                'key' => 'confirm_status',
                'value' => '2',
                'serialized' => 0,
            ),
            4 =>
            array (
                'id' => 5,
                'code' => 'payment.receipt',
                'key' => 'status',
                'value' => '1',
                'serialized' => 0,
            ),
            5 =>
            array (
                'id' => 6,
                'code' => 'payment.receipt',
                'key' => 'confirm_status',
                'value' => '5',
                'serialized' => 0,
            ),
            6 =>
            array (
                'id' => 7,
                'code' => 'shipping.newPostCourier',
                'key' => 'status',
                'value' => '1',
                'serialized' => 0,
            ),
            7 =>
            array (
                'id' => 8,
                'code' => 'shipping.newPostCourier',
                'key' => 'cost',
                'value' => '100',
                'serialized' => 0,
            ),
            8 =>
            array (
                'id' => 9,
                'code' => 'shipping.newPostOffice',
                'key' => 'status',
                'value' => '1',
                'serialized' => 0,
            ),
            9 =>
            array (
                'id' => 10,
                'code' => 'shipping.newPostOffice',
                'key' => 'cost',
                'value' => '0',
                'serialized' => 0,
            ),
            10 =>
            array (
                'id' => 25,
                'code' => 'home_carousel',
                'key' => 'special_products',
                'value' => 'a:4:{i:0;s:1:"3";i:1;s:1:"5";i:2;s:2:"18";i:3;N;}',
                'serialized' => 1,
            ),
            11 =>
            array (
                'id' => 26,
                'code' => 'home_carousel',
                'key' => 'hit_products',
                'value' => 'a:4:{i:0;s:1:"4";i:1;s:1:"5";i:2;s:1:"6";i:3;N;}',
                'serialized' => 1,
            ),
            12 =>
            array (
                'id' => 27,
                'code' => 'home_carousel',
                'key' => 'new_products',
                'value' => 'a:4:{i:0;s:1:"5";i:1;s:1:"7";i:2;s:2:"17";i:3;N;}',
                'serialized' => 1,
            ),
            13 =>
            array (
                'id' => 58,
                'code' => 'home_text',
                'key' => 'status',
                'value' => '1',
                'serialized' => 0,
            ),
            14 =>
            array (
                'id' => 66,
                'code' => 'site_settings',
                'key' => 'admin_email',
                'value' => 'admin@gmail.com',
                'serialized' => 0,
            ),
            15 =>
            array (
                'id' => 67,
                'code' => 'site_settings',
                'key' => 'link_youtube',
                'value' => '#',
                'serialized' => 0,
            ),
            16 =>
            array (
                'id' => 68,
                'code' => 'site_settings',
                'key' => 'link_facebook',
                'value' => '#',
                'serialized' => 0,
            ),
            17 =>
            array (
                'id' => 69,
                'code' => 'site_settings',
                'key' => 'link_twitter',
                'value' => '#',
                'serialized' => 0,
            ),
            18 =>
            array (
                'id' => 70,
                'code' => 'site_settings',
                'key' => 'link_instagram',
                'value' => '#',
                'serialized' => 0,
            ),
            19 =>
            array (
                'id' => 71,
                'code' => 'site_settings',
                'key' => 'logo',
                'value' => 'shop_images/logo.svg',
                'serialized' => 0,
            ),
            20 =>
            array (
                'id' => 72,
                'code' => 'site_settings',
                'key' => 'watermark',
                'value' => 'shop_images/watermark.png',
                'serialized' => 0,
            ),
            21 =>
            array (
                'id' => 73,
                'code' => 'site_settings',
                'key' => 'status_buy_one_click',
                'value' => 1,
                'serialized' => 0,
            ),
        ));


    }
}
