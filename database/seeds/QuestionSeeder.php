<?php

use Illuminate\Database\Seeder;
use App\Entities\Language;
use App\Entities\QuestionGroup;
use App\Entities\QuestionGroupDescription;
use App\Entities\Question;
use App\Entities\QuestionDescription;


class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = Language::all();
        $now = now();

        $questionGroupsCount = 6;

        for ($i = 0; $i < $questionGroupsCount; $i++) {
            QuestionGroup::create();
        }

        QuestionGroup::chunk(1000, function ($questionGroups) use ($languages) {
            $descriptions = [];
            foreach ($questionGroups as $questionGroup) {
                foreach ($languages as $language) {
                    $descriptions[] = [
                        'question_group_id' => $questionGroup->id,
                        'language_id' => $language->id,
                        'name' => 'Группа вопросов ' . $questionGroup->id
                    ];
                }
            }
            QuestionGroupDescription::insert($descriptions);
        });

        QuestionGroup::chunk(1000, function ($questionGroups) use ($now) {
            $questionCountPerGroup = 5;
            $questions = [];
            foreach ($questionGroups as $questionGroup){
                for ($i = 0; $i < $questionCountPerGroup; $i++) {
                    $questions[] = [
                        'question_group_id' => $questionGroup->id,
                        'created_at' => $now,
                        'updated_at' => $now
                    ];
                }
            }
            Question::insert($questions);
        });

        Question::chunk(1000, function ($questions) use ($languages) {
            $descriptions = [];
            foreach ($questions as $question) {
                foreach ($languages as $language) {
                    $descriptions[] = [
                        'question_id' => $question->id,
                        'language_id' => $language->id,
                        'answer' => 'answer for question ' . $question->id . ' ' . $language->slug,
                        'question_text' => 'question text ' . $question->id
                    ];
                }
            }
            QuestionDescription::insert($descriptions);
        });
    }
}
