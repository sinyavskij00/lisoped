<?php

use Illuminate\Database\Seeder;
use \App\Entities\Product;
use \App\Entities\AttributesValues;


class AttributeValueToProduct extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributesValues = AttributesValues::all();
        Product::chunk(5000, function ($products) use ($attributesValues) {
            $data = [];
            foreach ($products as $product) {
                $values = $attributesValues->random(random_int(3, 8));
                foreach ($values as $value) {
                    $data[] = [
                        'product_id' => $product->id,
                        'attribute_value_id' => $value->id
                    ];
                }
            }
            DB::table('attributes_values_to_product')->insert($data);
        });
    }
}
