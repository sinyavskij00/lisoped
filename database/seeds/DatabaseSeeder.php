<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguageSeeder::class);

        $this->call(CustomerGroupSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CurrencySeeder::class);

        $this->call(YearSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(SizeSeeder::class);
        $this->call(VendorSeeder::class);

//        $this->call(SkuSeeder::class);
//        $this->call(ManufacturerSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ImportXmlSeeder::class);
//        $this->call(AttributeGroupSeeder::class);
//        $this->call(AttributeSeeder::class);
//        $this->call(ProductSeeder::class);
//        $this->call(SkuDiscountSeeder::class);
//        $this->call(SkuSpecialSeeder::class);
//        $this->call(AttributeValueToProduct::class);
//        $this->call(ProductReviewsSeeder::class);
//        $this->call(SpecialSeeder::class);
//        $this->call(TestimonialSeeder::class);
//        $this->call(NewsSeeder::class);
//        $this->call(QuestionSeeder::class);
//        $this->call(BannerSeeder::class);


        $this->call(AdminMenuTableSeeder::class);
        $this->call(AdminOperationLogTableSeeder::class);
        $this->call(AdminPermissionsTableSeeder::class);
        $this->call(AdminRolesTableSeeder::class);
        $this->call(AdminRoleMenuTableSeeder::class);
        $this->call(AdminRolePermissionsTableSeeder::class);
        $this->call(AdminRoleUsersTableSeeder::class);
        $this->call(AdminUsersTableSeeder::class);
        $this->call(AdminUserPermissionsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(ShippingsTableSeeder::class);
        $this->call(OrderProfitabilityTypesTableSeeder::class);
        $this->call(SiteSettingsTableSeeder::class);
        $this->call(BillingAccountsTableSeeder::class);
        $this->call(BillingCartsTableSeeder::class);
        $this->call(OrderStatusesTableSeeder::class);
    }
}
