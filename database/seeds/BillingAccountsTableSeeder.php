<?php

use Illuminate\Database\Seeder;

class BillingAccountsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('billing_accounts')->delete();
        
        \DB::table('billing_accounts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Иванов Иван Иванович',
                'employer_number' => '6516515615156',
                'bank_number' => '65156156151',
                'checking_account' => '6515661561515',
                'created_at' => '2018-10-18 12:06:57',
                'updated_at' => '2018-10-18 12:06:57',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'ООО "Солнце"',
                'employer_number' => '65514646484',
                'bank_number' => '6/878724535',
                'checking_account' => '987812135',
                'created_at' => '2018-10-18 12:07:16',
                'updated_at' => '2018-10-18 12:07:16',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'ООО "Ледник"',
                'employer_number' => '78914231',
                'bank_number' => '123548',
                'checking_account' => '9648949814416',
                'created_at' => '2018-10-18 12:07:43',
                'updated_at' => '2018-10-18 12:07:43',
            ),
        ));
        
        
    }
}