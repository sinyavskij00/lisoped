<?php

use Illuminate\Database\Seeder;

class SkuSpecialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date_yesterday = \Illuminate\Support\Carbon::yesterday();
        $date_tomorrow = \Illuminate\Support\Carbon::tomorrow();
        $customerGroups = \App\Entities\CustomerGroup::all();

        \App\Entities\StockKeepingUnit::chunk(1000, function ($skus) use ($date_yesterday, $date_tomorrow, $customerGroups) {
            $specials = [];
            foreach ($skus as $sku) {
                if (random_int(0, 1)) {
                    $specials[] = [
                        'customer_group_id' => $customerGroups->random(1)->first()->id,
                        'priority' => random_int(-10, 10),
                        'unit_id' => $sku->id,
                        'price' => intval($sku->price * (random_int(5, 9) / 10)),
                        'date_start' => $date_yesterday,
                        'date_end' => $date_tomorrow,
                    ];
                }
            }
            DB::table('sku_specials')->insert($specials);
        });
    }
}
