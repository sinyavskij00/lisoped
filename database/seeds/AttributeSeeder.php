<?php

use Illuminate\Database\Seeder;
use App\Entities\Attribute;
use \App\Entities\AttributeDescription;
use \App\Entities\AttributesValues;
use Faker\Generator as Faker;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributeGroups = \App\Entities\AttributeGroup::all();
        $now = now();
        foreach ($attributeGroups as $attributeGroup) {
            $attributesPerGroup = 2;
            $data = [];
            for ($i = 0; $i < $attributesPerGroup; $i++) {
                $data[] = [
                    'is_display' => 1,
                    'attribute_group_id' => $attributeGroup->id,
                    'sort' => random_int(-10, 10),
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
            Attribute::insert($data);
        }

        $faker = app()->make(Faker::class);
        $languages = \App\Entities\Language::all();
        Attribute::chunk(1000, function ($attributes) use ($languages, $faker) {
            $data = [];
            foreach ($attributes as $attribute) {
                foreach ($languages as $language) {
                    $data[] = [
                        'attribute_id' => $attribute->id,
                        'language_id' => $language->id,
                        'name' => $faker->sentence(1)
                    ];
                }
            }
            AttributeDescription::insert($data);
        });

        Attribute::chunk(1000, function ($attributes) {
            $data = [];
            $attrValuesPerAttr = 6;
            for($i = 0; $i < $attrValuesPerAttr; $i++){
                foreach ($attributes as $attribute) {
                    $data[] = [
                        'attribute_id' => $attribute->id
                    ];
                }
            }
            AttributesValues::insert($data);
        });

        AttributesValues::chunk(1000, function ($attributeValues) use ($languages, $faker) {
            $data = [];
            foreach ($attributeValues as $attributeValue) {
                foreach ($languages as $language) {
                    $data[] = [
                        'language_id' => $language->id,
                        'attribute_value_id' => $attributeValue->id,
                        'value' => $faker->word()
                    ];
                }
            }
            \App\Entities\AttributesValuesDescription::insert($data);
        });
    }
}
