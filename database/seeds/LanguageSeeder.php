<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currentDate = now();
        DB::table('languages')->insert([
            'title' => 'Русский',
            'image' => 'shop_images/languages/russian_language.png',
            'slug' => 'ru',
            'is_default' => '1',
            'created_at' => $currentDate,
            'updated_at' => $currentDate
        ]);
//        DB::table('languages')->insert([
//            'title' => 'Английский',
//            'image' => 'shop_images/languages/english_language.jpeg',
//            'slug' => 'en',
//            'is_default' => '0',
//            'created_at' => $currentDate,
//            'updated_at' => $currentDate
//        ]);
        DB::table('languages')->insert([
            'title' => 'Украинский',
            'image' => 'shop_images/languages/ukraine_language.png',
            'slug' => 'ua',
            'is_default' => '0',
            'created_at' => $currentDate,
            'updated_at' => $currentDate
        ]);
    }
}
