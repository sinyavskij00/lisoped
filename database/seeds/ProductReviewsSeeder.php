<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ProductReviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now();
        $faker = app()->make(Faker::class);
        \App\Entities\Product::chunk(1000, function ($products) use ($now, $faker) {
            $data = [];
            foreach ($products as $product) {
                for ($i = random_int(0, 5); $i >= 0; $i--) {
                    $data[] = [
                        'product_id' => $product->id,
                        'status' => 1,
                        'name' => $faker->firstName . ' ' . $faker->lastName,
                        'email' => $faker->email,
                        'text' => $faker->paragraph,
                        'rating' => random_int(1, 5),
                        'created_at' => $now,
                        'updated_at' => $now
                    ];
                }
            }
            \App\Entities\ProductReview::insert($data);
        });

        $images = collect(\Illuminate\Support\Facades\Storage::files('shop_images/reviews'));
        \App\Entities\ProductReview::chunk(1000, function ($reviews) use ($images) {
            $data = [];
            foreach ($reviews as $review) {
                $data[] = [
                    'product_review_id' => $review->id,
                    'image' => $images->random(1)->first()
                ];
            }
            \App\Entities\ProductReviewImage::insert($data);
        });
    }
}
