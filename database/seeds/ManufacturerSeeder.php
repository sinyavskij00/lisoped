<?php

use Illuminate\Database\Seeder;

class ManufacturerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = collect(Storage::files('shop_images/manufacturer'));
        $languages = \App\Entities\Language::all();
        $manufacturers = [];
        $now = now();
        foreach ($images as $key => $image) {
            $pathParts = pathinfo($image);
            $manufacturers[] = [
                'image' => 'shop_images/manufacturers/' . $pathParts['basename'],
                'slug' => 'manufacturer-' . ($key + 1),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        \App\Entities\Manufacturer::insert($manufacturers);

        $descriptions = [];
        foreach (\App\Entities\Manufacturer::all() as $manufacturer){
            foreach ($languages as $language) {
                $descriptions[] = [
                    'manufacturer_id' => $manufacturer->id,
                    'language_id' => $language->id,
                    'name' => 'Производитель ' . $manufacturer->id . ' ' . $language->slug,
                    'description' => 'Производитель ' . $manufacturer->id,
                    'meta_h1' => 'Производитель ' . $manufacturer->id . ' ' . $language->slug,
                    'meta_title' => 'Производитель ' . $manufacturer->id,
                    'meta_description' => 'Производитель ' . $manufacturer->id,
                    'meta_keywords' => 'Производитель ' . $manufacturer->id,
                ];
            }
        }
        \App\Entities\ManufacturerDescription::insert($descriptions);
    }
}
