<?php

use Illuminate\Database\Seeder;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values = ['S', 'M', 'L', 'XL'];
        $now = now();

        $data = [];
        foreach ($values as $value){
            $data[] = [
                'value' => $value,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        \App\Entities\Size::insert($data);

        $languages = \App\Entities\Language::all();
        \App\Entities\Size::chunk(1000, function($sizes) use ($languages) {
            $descriptions = [];
            foreach ($sizes as $size){
                foreach ($languages as $language){
                    $descriptions[] = [
                        'comment' => 'рост 1' . (50 * $size->id) . ' - 1' . (50 * $size->id) . ' см',
                        'language_id' => $language->id,
                        'size_id' => $size->id,
                    ];
                }
            }
            \App\Entities\SizeDescription::insert($descriptions);
        });
    }
}
