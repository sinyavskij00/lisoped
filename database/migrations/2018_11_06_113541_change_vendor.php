<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeVendor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->string('email')->default('')->nullable()->change();
            $table->string('email2')->default('')->nullable()->change();
            $table->string('email3')->default('')->nullable()->change();
            $table->string('name')->default('')->nullable()->change();
            $table->string('telephone_1')->default('')->nullable()->change();
            $table->string('telephone_2')->default('')->nullable()->change();
            $table->string('site')->default('')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->string('email')->default('')->change();
            $table->string('email2')->default('')->change();
            $table->string('email3')->default('')->change();
            $table->string('name')->default('')->change();
            $table->string('telephone_1')->default('')->change();
            $table->string('telephone_2')->default('')->change();
            $table->string('site')->default('')->change();
        });
    }
}
