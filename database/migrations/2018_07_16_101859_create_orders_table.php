<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_invoice')->default('');
            $table->unsignedInteger('customer_id')->default(0);
            $table->unsignedInteger('customer_group_id')->default(0);
            $table->string('first_name')->default('');
            $table->string('last_name')->default('');
            $table->string('email')->default('');
            $table->string('telephone')->default('');

            $table->text('address')->nullable();

            $table->string('payment_method')->default('');
            $table->string('payment_code')->default('');

            $table->string('shipping_method')->default('');
            $table->string('shipping_code')->default('');

            $table->text('comment')->nullable();

            $table->decimal('sub_total')->default(0);
            $table->decimal('shipping_payment')->default(0);
            $table->decimal('total')->default(0);

            $table->integer('order_status_id')->default(0);
            $table->unsignedInteger('language_id');
            $table->unsignedInteger('currency_id');
            $table->string('currency_code')->default('');
            $table->decimal('currency_value')->default(0);

            $table->timestamps();
        });

        Schema::create('order_profitabilities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('type_id');
            $table->decimal('price')->default(0);
            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade');
        });

        Schema::create('order_profitability_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_info');
    }
}
