<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockKeepingUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_keeping_units', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index()->nullable();
            $table->decimal('price')->default(0);
            $table->decimal('vendor_price')->default(0);
            $table->unsignedInteger('vendor_id')->nullable();
            $table->string('sku')->unique()->index();
            $table->string('vendor_sku')->nullable()->unique()->index();
            $table->tinyInteger('is_main')->default(0);
            $table->unsignedInteger('size_id');
            $table->unsignedInteger('color_id');
            $table->unsignedInteger('year_id');
            $table->integer('quantity')->default(0);
            $table->string('model')->default('');
            $table->timestamps();
        });

        Schema::create('unit_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('unit_id')->index();
            $table->string('image');

            $table->foreign('unit_id')
                ->references('id')->on('stock_keeping_units')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_keeping_units');
        Schema::dropIfExists('unit_images');
    }
}
