<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestimonialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testimonials', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->text('text');
            $table->tinyInteger('rating')->default(5);
            $table->tinyInteger('status')->default(0);
            $table->nestedSet();
            $table->timestamps();
        });

        Schema::create('testimonial_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image');
            $table->unsignedInteger('testimonial_id');

            $table->foreign('testimonial_id')
                ->references('id')->on('testimonials')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testimonials');
        Schema::dropIfExists('testimonial_images');
    }
}
