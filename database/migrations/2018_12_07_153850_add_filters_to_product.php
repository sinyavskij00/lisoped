<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiltersToProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sort')->default(0);
            $table->string('slug')->unique();
            $table->timestamps();
        });

        Schema::create('filter_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('filter_id')->index();
            $table->unsignedInteger('language_id');
            $table->string('name');

            $table->unique(['filter_id', 'language_id']);

            $table->foreign('filter_id')
                ->references('id')->on('filters')
                ->onDelete('cascade');

            $table->foreign('language_id')
                ->references('id')->on('languages')
                ->onDelete('cascade');
        });

        Schema::create('filter_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('filter_id')->index();
            $table->integer('sort')->default(0);
            $table->string('slug')->unique();

            $table->foreign('filter_id')
                ->references('id')->on('filters')
                ->onDelete('cascade');
        });

        Schema::create('filter_value_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('filter_value_id')->index();
            $table->unsignedInteger('language_id');
            $table->string('name');
            $table->string('filter_name')->default('')->nullable();

            $table->unique(['filter_value_id', 'language_id'], 'attr_val_lang_id');

            $table->foreign('filter_value_id')
                ->references('id')->on('filter_values')
                ->onDelete('cascade');
        });

        Schema::create('filter_value_to_product', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('filter_value_id')->index();
            $table->unique(['product_id', 'filter_value_id'], 'prod_filter_value');

            $table->foreign('filter_value_id')
                ->references('id')->on('filter_values')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters');
        Schema::dropIfExists('filter_descriptions');
        Schema::dropIfExists('filter_values');
        Schema::dropIfExists('filter_value_descriptions');
        Schema::dropIfExists('filter_value_to_product');
    }
}
