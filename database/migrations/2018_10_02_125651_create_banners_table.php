<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('background_image')->nullable();
            $table->string('main_image')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });

        Schema::create('banner_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('banner_id');
            $table->unsignedInteger('language_id');
            $table->string('text')->default('');
            $table->string('link', 255)->default('');
            $table->string('link_title')->default('');
            $table->string('link_subtitle')->default('');
            $table->unique(['banner_id', 'language_id'], 'banner_lang_id');

            $table->foreign('banner_id')
                ->references('id')->on('banners')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
