<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('image')->nullable();
            $table->integer('is_preorder')->default(0);
            $table->tinyInteger('is_new')->default(0);
            $table->tinyInteger('is_hit')->default(0);
            $table->tinyInteger('is_our_choice')->default(0);
            $table->integer('points')->default(0);
            $table->integer('points_percent')->default(0);
            $table->integer('category_id')->nullable()->index();
            $table->integer('manufacturer_id')->nullable()->index();
            $table->tinyInteger('status')->default(1);

            $table->unsignedInteger('min_weight')->nullable();
            $table->unsignedInteger('max_weight')->nullable();
            $table->unsignedInteger('min_height')->nullable();
            $table->unsignedInteger('max_height')->nullable();
            $table->unsignedInteger('gender')->nullable();
            $table->unsignedInteger('trace_type')->nullable();

            $table->timestamps();
        });

        Schema::create('product_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('language_id');
            $table->string('name')->index();
            $table->text('description')->nullable();
            $table->text('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->unique(['product_id', 'language_id'], 'product_id_to_lang_id');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });

        Schema::create('product_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('rating')->default(5);
            $table->string('name');
            $table->string('email');
            $table->text('text');
            $table->timestamps();
        });

        Schema::create('product_review_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_review_id');
            $table->string('image');
        });

        Schema::create('product_related', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('related_id')->index();
            $table->unique(['product_id', 'related_id']);

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });

        Schema::create('product_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->string('video');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_descriptions');
        Schema::dropIfExists('product_images');
        Schema::dropIfExists('product_reviews');
        Schema::dropIfExists('product_review_images');
        Schema::dropIfExists('product_related');
        Schema::dropIfExists('product_videos');
    }
}
