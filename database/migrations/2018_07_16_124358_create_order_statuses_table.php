<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('color')->default('');
            $table->tinyInteger('is_new')->default(0);
            $table->tinyInteger('is_finished')->default(0);
            $table->timestamps();
        });

        Schema::create('order_status_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_status_id')->index();
            $table->unsignedInteger('language_id');
            $table->string('name');
            $table->unique(['order_status_id', 'language_id'], 'order_lang_id');

            $table->foreign('order_status_id')
                ->references('id')->on('order_statuses')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_statuses');
        Schema::dropIfExists('order_status_descriptions');
    }
}
