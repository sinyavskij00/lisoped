<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sort')->default(0);
            $table->string('image')->nullable();
            $table->timestamps();
        });

        Schema::create('attribute_group_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('language_id');
            $table->unsignedInteger('attribute_group_id');
            $table->string('name');
            $table->unique(['language_id', 'attribute_group_id'], 'lang_attr_ids');

            $table->foreign('attribute_group_id')
                ->references('id')->on('attribute_groups')
                ->onDelete('cascade');
        });

        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attribute_group_id')->nullable();
            $table->tinyInteger('is_display')->default(0);
            $table->integer('sort')->default(0);
            $table->string('slug')->unique();
            $table->timestamps();
        });

        Schema::create('attribute_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attribute_id')->index();
            $table->unsignedInteger('language_id');
            $table->string('name');

            $table->unique(['attribute_id', 'language_id']);

            $table->foreign('attribute_id')
                ->references('id')->on('attributes')
                ->onDelete('cascade');
        });

        Schema::create('attributes_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attribute_id')->index();
            $table->string('slug')->unique();

            $table->foreign('attribute_id')
                ->references('id')->on('attributes')
                ->onDelete('cascade');
        });

        Schema::create('attributes_values_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('attribute_value_id')->index();
            $table->unsignedInteger('language_id');
            $table->string('value');
            $table->string('filter_name')->default('')->nullable();

            $table->unique(['attribute_value_id', 'language_id'], 'attr_val_lang_id');

            $table->foreign('attribute_value_id')
                ->references('id')->on('attributes_values')
                ->onDelete('cascade');
        });

        Schema::create('attributes_values_to_product', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->index();
            $table->unsignedInteger('attribute_value_id')->index();
            $table->unique(['product_id', 'attribute_value_id'], 'prod_attr_value');

            $table->foreign('attribute_value_id')
                ->references('id')->on('attributes_values')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
        Schema::dropIfExists('attribute_descriptions');
        Schema::dropIfExists('attributes_values');
        Schema::dropIfExists('attributes_values_descriptions');
        Schema::dropIfExists('attributes_values_to_product');
    }
}
