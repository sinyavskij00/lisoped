<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufacturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacturers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image')->nullable();
            $table->string('slug')->unique();
            $table->timestamps();
        });

        Schema::create('manufacturer_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('manufacturer_id')->index();
            $table->unsignedInteger('language_id');
            $table->string('name')->default('');
            $table->text('description')->nullable();
            $table->text('meta_h1')->nullable();
            $table->text('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->unique(['manufacturer_id', 'language_id']);

            $table->foreign('manufacturer_id')
                ->references('id')->on('manufacturers')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manufacturers');
        Schema::dropIfExists('manufacturer_descriptions');
    }
}
