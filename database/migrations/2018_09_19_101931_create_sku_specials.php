<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkuSpecials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sku_specials', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_group_id')->index();
            $table->unsignedInteger('unit_id')->index();
            $table->integer('priority')->default(0);
            $table->decimal('price')->default(0);
            $table->dateTime('date_start');
            $table->dateTime('date_end');

            $table->foreign('customer_group_id')
                ->references('id')->on('customer_groups')
                ->onDelete('cascade');

            $table->foreign('unit_id')
                ->references('id')->on('stock_keeping_units')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sku_specials');
    }
}
