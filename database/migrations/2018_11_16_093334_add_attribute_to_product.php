<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttributeToProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('attribute_value_1')->nullable()->after('status');
            $table->unsignedInteger('attribute_value_2')->nullable()->after('attribute_value_1');
            $table->unsignedInteger('attribute_value_3')->nullable()->after('attribute_value_2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('attribute_value_1');
            $table->dropColumn('attribute_value_2');
            $table->dropColumn('attribute_value_3');
        });
    }
}
