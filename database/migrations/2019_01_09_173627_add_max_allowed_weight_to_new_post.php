<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxAllowedWeightToNewPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_post_warehouses', function (Blueprint $table) {
            $table->integer('TotalMaxWeightAllowed')->nullable(0)->after('Ref');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_post_warehouses', function (Blueprint $table) {
            $table->dropColumn('TotalMaxWeightAllowed');
        });
    }
}
