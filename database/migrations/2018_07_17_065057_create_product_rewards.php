<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductRewards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_rewards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('customer_group_id');
            $table->unsignedInteger('product_id')->index();
            $table->integer('rewards')->default(0);
            $table->unique(['customer_group_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_rewards');
    }
}
