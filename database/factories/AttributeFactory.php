<?php

use Faker\Generator as Faker;


$factory->define(App\Entities\Attribute::class, function (Faker $faker) {
    return [
        'sort' => random_int(-20, 20)
    ];
});
